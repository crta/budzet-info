# coding=utf-8
from wtforms import SelectField, SelectMultipleField, RadioField
from lokalna_javna_produzeca_form import LokalnaJavnaProduzacaForm

class FloatingBubblesLJPForm(LokalnaJavnaProduzacaForm):

    group_by = SelectField('Grupisano po',
        choices=[
            ('Preduzeća','Preduzeća'),
            ('Opština', 'Opština'),
            ('Delatnosti', 'Delatnosti'),
            ('Osnivači', 'Osnivači'),
            ('Godina', 'Godina')
        ],
        default='Godina')

    disaggregate_by = SelectField('Razvrstaj po',
        choices=[
            ('Preduzeća', 'Preduzeća'),
            ('Opština', 'Opština'),
            ('Delatnosti', 'Delatnosti'),
            ('Osnivači', 'Osnivači'),
            ('Godina', 'Godina')
        ],
        default='Godina')

    data_series = RadioField('Skup podataka',
            choices=[
                ('Ukupni prihodi', 'Ukupni prihodi'),
                ('Ukupni rashodi', 'Ukupni rashodi'),
                ('Poslovni rashodi', 'Poslovni rashodi'),
                ('Broj zaposlenih', 'Broj zaposlenih')
            ],
            default='Ukupni prihodi')

    ukupni_rashodi_categories = SelectMultipleField('Ukupni rashodi',
        choices=[
            ('AOP 207', 'Poslovni rashodi'),
            ('AOP 216', 'Finansijski rashodi'),
            ('AOP 218', 'Ostali rashodi '),
        ],
        default='AOP 207')

    def __init__(self, *args, **kwargs):
        super(FloatingBubblesLJPForm, self).__init__("Floating Bubbles", *args, **kwargs)
