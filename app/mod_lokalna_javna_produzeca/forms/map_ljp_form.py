# coding=utf-8
from wtforms import RadioField, SelectMultipleField
from lokalna_javna_produzeca_form import LokalnaJavnaProduzacaForm

class MapLJPForm(LokalnaJavnaProduzacaForm):

    map_data_series = RadioField('Skup podataka',
            choices=[
                ('Ukupni prihodi', 'Ukupni prihodi'),
                ('Ukupni rashodi', 'Ukupni rashodi'),
                ('Poslovni rashodi', 'Poslovni rashodi'),
                ('Ostali', 'Ostali')
            ],
            default='Ukupni prihodi')

    ukupni_rashodi_categories = SelectMultipleField('Ukupni rashodi',
        choices=[
            ('AOP 207', 'Poslovni rashodi'),
            ('AOP 216', 'Finansijski rashodi'),
            ('AOP 218', 'Ostali rashodi '),
        ],
        default='AOP 207')

    poslovni_rashodi_categories = SelectMultipleField('Poslovni rashodi',
        choices=[
            ('AOP 208', 'Nabavna vrednost prodate robe'),
            ('AOP 209', 'Troškovi materijala'),
            ('AOP 210', 'Troškovi zarada, naknada zarada i ostali lični rashodi'),
            ('AOP 211', 'Troškovi amortizacije i rezervisanja'),
            ('AOP 212', 'Ostali poslovni rashodi'),
        ],
        default='AOP 208')

    other_categories = SelectMultipleField('Ostali',
        choices=[
            ('AOP 216', 'Finansijski rashodi'),
            ('AOP 218', 'Ostali rashodi'),
            ('AOP 229', 'Neto dobitak'),
            ('AOP 230', 'Neto gubitak')
        ],
        default='AOP 216')

    def __init__(self, *args, **kwargs):
        super(MapLJPForm, self).__init__("Map", *args, **kwargs)