# coding=utf-8
from wtforms import SelectField, BooleanField
from lokalna_javna_produzeca_form import LokalnaJavnaProduzacaForm

class BarchartLJPForm(LokalnaJavnaProduzacaForm):

    group_by = SelectField('Grupisano po',
        choices=[
            ('Preduzeća','Preduzeća'),
            #('Grad','Grad'),
            ('Opština','Opština'),
            ('Veličina preduzeća','Veličina preduzeća'),
            ('Osnivači','Osnivači'),
            ('Godina','Godina')
        ],
        default='Godina')

    data_series_ukupni_prihodi = BooleanField('Ukupni prihodi')
    data_series_ukupni_rashodi = BooleanField('Ukupni rashodi')
    data_series_poslovni_rashodi = BooleanField('Poslovni rashodi')
    data_series_others = BooleanField('Ostali')


    def __init__(self, *args, **kwargs):
        super(BarchartLJPForm, self).__init__("Barchart", *args, **kwargs)
