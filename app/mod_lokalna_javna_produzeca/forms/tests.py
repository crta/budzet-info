# coding=utf-8
from flask.ext.testing import TestCase
from app import create_app, mongo
from utils import FormUtils

class LokalnaJavanProducezaFormsTestCase(TestCase):
    ''' A test case for the api
    '''

    def create_app(self):
        app = create_app()
        return app

    def setUp(self):
        pass

    def tearDown(self):
        pass


class LokalnaJavanProducezaFormsUtilsTestCase(LokalnaJavanProducezaFormsTestCase):

    def test_get_municipalities(self):
        '''

        :return:
        '''
        db_count = len(mongo.db.lokalnajavnapreduzeca\
            .find({})\
            .distinct('opstina.naziv.vrednost'))

        form_options_count = len(FormUtils.get_municipalities())

        self.assertTrue(db_count == form_options_count,
                        "Number of options differs between database (%i) and form options (%i)." % (form_options_count, form_options_count))

    def test_get_company_sizes(self):
        '''

        :return:
        '''
        db_count = len(mongo.db.lokalnajavnapreduzeca\
            .find({})\
            .distinct('velicinaPreduzeca.vrednost'))

        form_options_count = len(FormUtils.get_company_sizes())

        self.assertTrue(db_count == form_options_count,
                        "Number of options differs between database (%i) and form options (%i)." % (db_count, form_options_count))

    def test_get_years(self):
        '''

        :return:
        '''
        db_count = len(mongo.db.lokalnajavnapreduzeca\
            .find({})\
            .distinct('godine'))

        form_options_count = len(FormUtils.get_years())

        self.assertTrue(db_count == form_options_count,
                        "Number of options differs between database (%i) and form options (%i)." % (db_count, form_options_count))


    def test_get_owners(self):
        '''

        :return:
        '''


        db_count = len(mongo.db.lokalnajavnapreduzeca\
            .find({})\
            .distinct('osnivaci.imena.vrednost'))

        form_options_count = len(FormUtils.get_owners())

        self.assertTrue(db_count == form_options_count, "Number of options differs between database (%i) and form options (%i)." % (db_count, form_options_count))


    def test_get_activities(self):
        '''

        :return:
        '''

        db_count = len(mongo.db.lokalnajavnapreduzeca\
            .find({})\
            .distinct('delatnost.id'))

        form_options_count = len(FormUtils.get_activities())

        self.assertTrue(db_count==form_options_count, "Number of options differs between database (%i) and form options (%i)." % (db_count, form_options_count))

    def test_get_companies(self):
        '''

        :return:
        '''

        db_count = len(mongo.db.lokalnajavnapreduzeca\
            .find({})\
            .distinct('preduzece.maticniBroj'))

        form_options_count = len(FormUtils.get_companies())

        self.assertTrue(db_count==form_options_count, "Number of options differs between database (%i) and form options (%i)." % (db_count, form_options_count))

