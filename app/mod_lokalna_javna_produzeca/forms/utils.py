# coding=utf-8
from collections import OrderedDict

class FormUtils():

    @staticmethod
    def get_tree_municipalities():
        municipalities = [{
                'id':'municipalities',
                'text': "Opštine",
                'parent': "#",
                'type': "folder"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Ada",
                "text": "Ada"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Aleksandrovac",
                "text": "Aleksandrovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Aleksinac",
                "text": "Aleksinac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Alibunar",
                "text": "Alibunar"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Apatin",
                "text": "Apatin"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Aranđelovac",
                "text": "Aranđelovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Arilje",
                "text": "Arilje"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Babušnica",
                "text": "Babušnica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bajina Bašta",
                "text": "Bajina Bašta"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Barajevo",
                "text": "Barajevo"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Batočina",
                "text": "Batočina"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bač",
                "text": "Bač"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bačka Palanka",
                "text": "Bačka Palanka"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bačka Topola",
                "text": "Bačka Topola"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bački Petrovac",
                "text": "Bački Petrovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bela Crkva",
                "text": "Bela Crkva"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bela Palanka",
                "text": "Bela Palanka"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Beočin",
                "text": "Beočin"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bečej",
                "text": "Bečej"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Blace",
                "text": "Blace"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bogatić",
                "text": "Bogatić"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bojnik",
                "text": "Bojnik"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Boljevac",
                "text": "Boljevac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bor",
                "text": "Bor"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bosilegrad",
                "text": "Bosilegrad"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Brus",
                "text": "Brus"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Bujanovac",
                "text": "Bujanovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Crna Trava",
                "text": "Crna Trava"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Crveni Krst",
                "text": "Crveni Krst"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Despotovac",
                "text": "Despotovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Dimitrovgrad",
                "text": "Dimitrovgrad"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Doljevac",
                "text": "Doljevac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Gadžin Han",
                "text": "Gadžin Han"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Golubac",
                "text": "Golubac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Gornji Milanovac",
                "text": "Gornji Milanovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Grocka",
                "text": "Grocka"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Inđija",
                "text": "Inđija"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Ivanjica",
                "text": "Ivanjica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Jagodina",
                "text": "Jagodina"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kanjiža",
                "text": "Kanjiža"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kikinda",
                "text": "Kikinda"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kladovo",
                "text": "Kladovo"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Knić",
                "text": "Knić"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Knjaževac",
                "text": "Knjaževac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Koceljeva",
                "text": "Koceljeva"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kosjerić",
                "text": "Kosjerić"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kovačica",
                "text": "Kovačica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kovin",
                "text": "Kovin"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kragujevac",
                "text": "Kragujevac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kraljevo",
                "text": "Kraljevo"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kruševac",
                "text": "Kruševac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kula",
                "text": "Kula"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kuršumlija",
                "text": "Kuršumlija"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Kučevo",
                "text": "Kučevo"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Lajkovac",
                "text": "Lajkovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Lapovo",
                "text": "Lapovo"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Lazarevac",
                "text": "Lazarevac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Lebane",
                "text": "Lebane"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Leskovac",
                "text": "Leskovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Ljig",
                "text": "Ljig"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Loznica",
                "text": "Loznica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Majdanpek",
                "text": "Majdanpek"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Mali Zvornik",
                "text": "Mali Zvornik"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Medijana",
                "text": "Medijana"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Medveđa",
                "text": "Medveđa"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Merošina",
                "text": "Merošina"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Mionica",
                "text": "Mionica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Mladenovac",
                "text": "Mladenovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Negotin",
                "text": "Negotin"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Nova Crnja",
                "text": "Nova Crnja"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Nova Varoš",
                "text": "Nova Varoš"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Novi Beograd",
                "text": "Novi Beograd"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Novi Bečej",
                "text": "Novi Bečej"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Novi Kneževac",
                "text": "Novi Kneževac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Novi Pazar",
                "text": "Novi Pazar"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Novi Sad",
                "text": "Novi Sad"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Obrenovac",
                "text": "Obrenovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Odžaci",
                "text": "Odžaci"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Opovo",
                "text": "Opovo"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Osečina",
                "text": "Osečina"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Palilula",
                "text": "Palilula"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Pančevo",
                "text": "Pančevo"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Paraćin",
                "text": "Paraćin"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Petrovac na Mlavi",
                "text": "Petrovac na Mlavi"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Pećinci",
                "text": "Pećinci"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Pirot",
                "text": "Pirot"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Požarevac",
                "text": "Požarevac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Požega",
                "text": "Požega"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Preševo",
                "text": "Preševo"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Priboj",
                "text": "Priboj"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Prijepolje",
                "text": "Prijepolje"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Prokuplje",
                "text": "Prokuplje"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Rakovica",
                "text": "Rakovica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Rača",
                "text": "Rača"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Raška",
                "text": "Raška"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Ražanj",
                "text": "Ražanj"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Rekovac",
                "text": "Rekovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Ruma",
                "text": "Ruma"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Senta",
                "text": "Senta"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Sečanj",
                "text": "Sečanj"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Sjenica",
                "text": "Sjenica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Smederevo",
                "text": "Smederevo"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Smederevska Palanka",
                "text": "Smederevska Palanka"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Sokobanja",
                "text": "Sokobanja"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Sombor",
                "text": "Sombor"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Sopot",
                "text": "Sopot"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Srbobran",
                "text": "Srbobran"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Sremska Mitrovica",
                "text": "Sremska Mitrovica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Stara Pazova",
                "text": "Stara Pazova"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Stari Grad",
                "text": "Stari Grad"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Subotica",
                "text": "Subotica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Surdulica",
                "text": "Surdulica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Svilajnac",
                "text": "Svilajnac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Svrljig",
                "text": "Svrljig"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Temerin",
                "text": "Temerin"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Titel",
                "text": "Titel"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Topola",
                "text": "Topola"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Trstenik",
                "text": "Trstenik"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Tutin",
                "text": "Tutin"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Ub",
                "text": "Ub"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Užice",
                "text": "Užice"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Valjevo",
                "text": "Valjevo"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Varvarin",
                "text": "Varvarin"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Velika Plana",
                "text": "Velika Plana"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Vladimirci",
                "text": "Vladimirci"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Vladičin Han",
                "text": "Vladičin Han"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Vlasotince",
                "text": "Vlasotince"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Voždovac",
                "text": "Voždovac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Vranje",
                "text": "Vranje"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Vračar",
                "text": "Vračar"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Vrbas",
                "text": "Vrbas"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Vrnjačka Banja",
                "text": "Vrnjačka Banja"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Zaječar",
                "text": "Zaječar"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Zrenjanin",
                "text": "Zrenjanin"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Zvezdara",
                "text": "Zvezdara"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Ćićevac",
                "text": "Ćićevac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Ćuprija",
                "text": "Ćuprija"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Čajetina",
                "text": "Čajetina"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Čačak",
                "text": "Čačak"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Čoka",
                "text": "Čoka"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Čukarica",
                "text": "Čukarica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Šabac",
                "text": "Šabac"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Šid",
                "text": "Šid"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Žabalj",
                "text": "Žabalj"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Žagubica",
                "text": "Žagubica"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Žitište",
                "text": "Žitište"
            },
            {
                "type": "file",
                "parent": "municipalities",
                "id": "Žitorađa",
                "text": "Žitorađa"
            }]
        return municipalities

    @staticmethod
    def get_municipalities():
        # db.lokalnajavnapreduzeca.distinct('nazivOpstine.opstina.vrednost').sort({'nazivOpstine.opstina.vrednost' : 1})
        municipalities = [
            "Ada",
            "Aleksandrovac",
            "Aleksinac",
            "Alibunar",
            "Apatin",
            "Aranđelovac",
            "Arilje",
            "Babušnica",
            "Bajina Bašta",
            "Barajevo",
            "Batočina",
            "Bač",
            "Bačka Palanka",
            "Bačka Topola",
            "Bački Petrovac",
            "Bela Crkva",
            "Bela Palanka",
            "Beočin",
            "Bečej",
            "Blace",
            "Bogatić",
            "Bojnik",
            "Boljevac",
            "Bor",
            "Bosilegrad",
            "Brus",
            "Bujanovac",
            "Crna Trava",
            "Crveni Krst",
            "Despotovac",
            "Dimitrovgrad",
            "Doljevac",
            "Gadžin Han",
            "Golubac",
            "Gornji Milanovac",
            "Grocka",
            "Inđija",
            "Ivanjica",
            "Jagodina",
            "Kanjiža",
            "Kikinda",
            "Kladovo",
            "Knić",
            "Knjaževac",
            "Koceljeva",
            "Kosjerić",
            "Kovačica",
            "Kovin",
            "Kragujevac",
            "Kraljevo",
            "Kruševac",
            "Kula",
            "Kuršumlija",
            "Kučevo",
            "Lajkovac",
            "Lapovo",
            "Lazarevac",
            "Lebane",
            "Leskovac",
            "Ljig",
            "Loznica",
            "Majdanpek",
            "Mali Zvornik",
            "Medijana",
            "Medveđa",
            "Merošina",
            "Mionica",
            "Mladenovac",
            "Negotin",
            "Nova Crnja",
            "Nova Varoš",
            "Novi Beograd",
            "Novi Bečej",
            "Novi Kneževac",
            "Novi Pazar",
            "Novi Sad",
            "Obrenovac",
            "Odžaci",
            "Opovo",
            "Osečina",
            "Palilula",
            "Pančevo",
            "Paraćin",
            "Petrovac na Mlavi",
            "Pećinci",
            "Pirot",
            "Požarevac",
            "Požega",
            "Preševo",
            "Priboj",
            "Prijepolje",
            "Prokuplje",
            "Rakovica",
            "Rača",
            "Raška",
            "Ražanj",
            "Rekovac",
            "Ruma",
            "Senta",
            "Sečanj",
            "Sjenica",
            "Smederevo",
            "Smederevska Palanka",
            "Sokobanja",
            "Sombor",
            "Sopot",
            "Srbobran",
            "Sremska Mitrovica",
            "Stara Pazova",
            "Stari Grad",
            "Subotica",
            "Surdulica",
            "Svilajnac",
            "Svrljig",
            "Temerin",
            "Titel",
            "Topola",
            "Trstenik",
            "Tutin",
            "Ub",
            "Užice",
            "Valjevo",
            "Varvarin",
            "Velika Plana",
            "Vladimirci",
            "Vladičin Han",
            "Vlasotince",
            "Voždovac",
            "Vranje",
            "Vračar",
            "Vrbas",
            "Vrnjačka Banja",
            "Zaječar",
            "Zrenjanin",
            "Zvezdara",
            "Ćićevac",
            "Ćuprija",
            "Čajetina",
            "Čačak",
            "Čoka",
            "Čukarica",
            "Šabac",
            "Šid",
            "Žabalj",
            "Žagubica",
            "Žitište",
            "Žitorađa"
        ]

        return municipalities

    @staticmethod
    def get_tree_companies():
        companies = [{
                'id':'companies',
                'text': "Preduzeća",
                'parent': "#",
                'type':"folder"
            },
            {
                "id": "7006578",
                "text": "10. OKTOBAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7148372",
                "text": "3.OKTOBAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8128472",
                "text": "4. OKTOBAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8037043",
                "text": "6.OKTOBAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7451440",
                "text": "ADA CIGANLIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8681350",
                "text": "ADICA JP ADA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7343914",
                "text": "AERODROM NIŠ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17028138",
                "text": "AERODROM ROSULJE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8006148",
                "text": "ATP PO PANČEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7263775",
                "text": "BADNJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8773378",
                "text": "BANAT",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17479237",
                "text": "BELA PALANKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17620983",
                "text": "BELI IZVOR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8596085",
                "text": "BELOCRKVANSKA JEZERA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17416774",
                "text": "BEOGRADSKA TVRĐAVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7156421",
                "text": "BIOKTOŠ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7384432",
                "text": "BOGOVINA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7342578",
                "text": "BOR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7203144",
                "text": "BORPREVOZ - U STEČAJU",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7113323",
                "text": "BUKULJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8432350",
                "text": "BUNAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8591148",
                "text": "ČISTOĆA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7337167",
                "text": "ČISTOĆA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8046816",
                "text": "ČISTOĆA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8049653",
                "text": "ČISTOĆA I ZELENILO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8065136",
                "text": "ČISTOĆA I ZELENILO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8148058",
                "text": "ČOKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7098499",
                "text": "DJUNIS",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7327609",
                "text": "DUBOČICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8031185",
                "text": "EKOS",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8025886",
                "text": "ELGAS JP SENTA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8042292",
                "text": "FRUŠKA GORA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8603162",
                "text": "GLOGONJ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7200579",
                "text": "GOLUBAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20580542",
                "text": "GRADSKA AUTOBUSKA STANICA LESKOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7045000",
                "text": "GRADSKA ČISTOĆA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17263838",
                "text": "GRADSKA STAMBENA AGENCIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7216009",
                "text": "GRADSKA TOPLANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20489758",
                "text": "GRADSKA TOPLANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17496255",
                "text": "GRADSKA TOPLANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7295871",
                "text": "GRADSKA TOPLANA PIROT",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7034628",
                "text": "GRADSKE PIJACE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17006312",
                "text": "GRDELICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7022662",
                "text": "GSP PO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8487529",
                "text": "HIGIJENA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8220441",
                "text": "INFORMATIVNI CENTAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7048971",
                "text": "INFOSTAN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7160658",
                "text": "IZVOR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7347189",
                "text": "JAKOP IZVOR VLADIMIRCI",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20567252",
                "text": "JATP ALIBUNAR ALIBUNAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8807370",
                "text": "JATP BELA CRKVA BELA CRKVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17119290",
                "text": "JAVNO KOMUNALNO PREDIZEĆE TOPLANA-LOZNICA LOZNICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8128260",
                "text": "JAVNO KOMUNALNO PREDUZEĆE 7. OKTOBAR NOVI KNEŽEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7221061",
                "text": "JAVNO KOMUNALNO PREDUZEĆE BELOSAVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7279094",
                "text": "JAVNO KOMUNALNO PREDUZEĆE GRADSKA TOPLANA KRUŠEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7323824",
                "text": "JAVNO KOMUNALNO PREDUZEĆE KUČEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7190174",
                "text": "JAVNO KOMUNALNO PREDUZEĆE LIM PRIJEPOLJE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7160763",
                "text": "JAVNO KOMUNALNO PREDUZEĆE MILOŠ MITROVIĆ VELIKA PLANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8676089",
                "text": "JAVNO KOMUNALNO PREDUZEĆE PARKING SUBOTICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7416253",
                "text": "JAVNO KOMUNALNO PREDUZEĆE ŠUMADIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8036446",
                "text": "JAVNO PREDUZEĆE AUTOPREVOZ SA PO KIKINDA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7165862",
                "text": "JAVNO PREDUZEĆE DIREKCIJA ZA URBANIZAM - KRAGUJEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8029458",
                "text": "JAVNO PREDUZEĆE NAŠ STAN KOVAČICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7208294",
                "text": "JAVNO PREDUZEĆE ZA STAMBENE USLUGE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17218913",
                "text": "JAVNO PREDUZEĆE ZA USLUGE I TOPLIFIKACIJU SMEDEREVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8646708",
                "text": "JAVNO URBANISTIČKO PREDUZEĆE - U STEČAJU",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7352964",
                "text": "JEDINSTVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8041822",
                "text": "JGSP NOVI SAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8744360",
                "text": "JIP BAČKA TOPOLA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20296895",
                "text": "JIP IVANJIČKI RADIO JP IVANJICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8779384",
                "text": "JIP RADIO BAP BAČKA PALANKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20297999",
                "text": "JIP RADIO POŽEGA POŽEGA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7352930",
                "text": "JKP 7.JULI BATOČINA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20547987",
                "text": "JKP 8. AVGUST",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8677425",
                "text": "JKP BELOCRKVANSKI KOMUNALAC BELA CRKVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8028362",
                "text": "JKP BEOČIN BEOČIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7023332",
                "text": "JKP BEOGRAD-PUT BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7020210",
                "text": "JKP BEOGRADSKE ELEKTRANE BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7018762",
                "text": "JKP BEOGRADSKI VODOVOD I KANALIZACIJA BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7334419",
                "text": "JKP BOGATIĆ BOGATIĆ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7576382",
                "text": "JKP ČAČAK SA PO ČAČAK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7190905",
                "text": "JKP ĆISTOĆA JP KRALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20568437",
                "text": "JKP ČISTOĆA MIONICA MIONICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8066531",
                "text": "JKP ČISTOĆA NOVI SAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8681767",
                "text": "JKP DIMNIČAR SUBOTICA - KÉMÉNYSEPRO KKV SZABADKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8588147",
                "text": "JKP DOLOVI PO DOLOVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20320443",
                "text": "JKP DONJI MILANOVAC DONJI MILANOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7168764",
                "text": "JKP DRINA MALI ZVORNIK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20104279",
                "text": "JKP DUBOKO UŽICE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8077690",
                "text": "JKP ELAN KOVAČICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17507362",
                "text": "JKP ENERGETIKA TRSTENIK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20550635",
                "text": "JKP GORICA NIŠ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7192819",
                "text": "JKP GORNJI MILANOVAC GORNJI MILANOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8067546",
                "text": "JKP GRADITELJ SRBOBRAN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "6986242",
                "text": "JKP GRADSKA ČISTOĆA NOVI PAZAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7343949",
                "text": "JKP GRADSKA GROBLJA KRAGUJEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17227050",
                "text": "JKP GRADSKA TOPLANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7317743",
                "text": "JKP GRADSKA TOPLANA UŽICE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17124048",
                "text": "JKP GRADSKE TRŽNICE, KRAGUJEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7182554",
                "text": "JKP GRADSKO ZELENILO ČAČAK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8055432",
                "text": "JKP GRADSKO ZELENILO NOVI SAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8488754",
                "text": "JKP GREJANJE PO PANČEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8023182",
                "text": "JKP INFORMATIKA NOVI SAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8685029",
                "text": "JKP KAČAREVO KAČAREVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20585439",
                "text": "JKP KANALIZACIJA PEĆINCI PEĆINCI",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7181655",
                "text": "JKP KOMUNALAC ČAČAK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8004790",
                "text": "JKP KOMUNALAC KULA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17547488",
                "text": "JKP KOMUNALAC LEBANE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17127209",
                "text": "JKP KOMUNALAC MAJDANPEK SA PO ŠAŠKA 5",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7675216",
                "text": "JKP KOMUNALAC RAŽANJ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7389566",
                "text": "JKP KOMUNALAC SMEDEREVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17224239",
                "text": "JKP KOMUNALAC VLASOTINCE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20274328",
                "text": "JKP KOMUNALNE USLUGE ALEKSINAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7216840",
                "text": "JKP KOMUNALNO IVANJICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8081255",
                "text": "JKP KOMUNALPROJEKT BAČKA PALANKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8055475",
                "text": "JKP LISJE SA POTPUNOM ODGOVORNOŠĆU NOVI SAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7618956",
                "text": "JKP MERMER REKOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7164297",
                "text": "JKP MIKULJA SMEDEREVSKA PALANKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7045859",
                "text": "JKP MLADENOVAC MLADENOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8077568",
                "text": "JKP MLADOST PANČEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7211856",
                "text": "JKP NAISSUS NIŠ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20116803",
                "text": "JKP OBJEDINJENA NAPLATA NIŠ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7041985",
                "text": "JKP OBRENOVAC OBRENOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8594872",
                "text": "JKP OMOLJICA OMOLJICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7305290",
                "text": "JKP OSEČINA SA PO OSEČINA, PERE JOVANOVIĆA KOMIRIĆANCA 35",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20448784",
                "text": "JKP PALANKA SERVIS SMEDEREVSKA PALANKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "6569285",
                "text": "JKP PARAĆIN PARAĆIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20518898",
                "text": "JKP PARKING SERVIS ČAČAK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17504894",
                "text": "JKP PARKING SERVIS KRAGUJEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20573872",
                "text": "JKP PARKING SERVIS LOZNICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20192542",
                "text": "JKP PARKING SERVIS NIŠ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8831149",
                "text": "JKP PARKING SERVIS NOVI SAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20190655",
                "text": "JKP PARKING SERVIS OBRENOVAC, OBRENOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7195591",
                "text": "JKP PIJACA KRALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20064382",
                "text": "JKP PIJACE ROPOČEVO SOPOT",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7347103",
                "text": "JKP PROGRES KOCELJEVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7365292",
                "text": "JKP RAČA RAČA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8005052",
                "text": "JKP RADNIK SIVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7262957",
                "text": "JKP RASINA BRUS",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17579606",
                "text": "JKP RIBARIĆE RIBARIĆE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20426454",
                "text": "JKP SEOSKI VODOVODI SJENICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20644419",
                "text": "JKP SEVER ILANDŽA ILANDŽA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8547718",
                "text": "JKP STADION",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7208324",
                "text": "JKP STANDARD-KNJAŽEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8736634",
                "text": "JKP STANDARD ŠID",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8057982",
                "text": "JKP STANDARD VRBAS",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17611828",
                "text": "JKP STOPANJA STOPANJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8038180",
                "text": "JKP SUBOTIČKA TOPLANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8658595",
                "text": "JKP TEMERIN,TEMERIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17441531",
                "text": "JKP TOPLANA BOR Đ.A KUNA 12",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7335393",
                "text": "JKP TOPLANA-ŠABAC ŠABAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7357494",
                "text": "JKP TOPLANA-VALJEVO VALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7311281",
                "text": "JKP TOPLANA ZAJEČAR - U LIKVIDACIJI",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20233940",
                "text": "JKP TOPLOVOD OBRENOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7174306",
                "text": "JKP TRŽNICA NIŠ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8073074",
                "text": "JKP TRŽNICA NOVI SAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8012644",
                "text": "JKP TVRĐAVA BAČ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8063753",
                "text": "JKP USLUGA ODŽACI",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7155760",
                "text": "JKP USLUGA PRIBOJ PO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7218281",
                "text": "JKP VARVARIN VARVARIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7096844",
                "text": "JKP VIDRAK VALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7167610",
                "text": "JKP VODOVOD ČAČAK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8005079",
                "text": "JKP VODOVOD CRVENKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20274301",
                "text": "JKP VODOVOD I KANALIZACIJA ALEKSINAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7165439",
                "text": "JKP VODOVOD I KANALIZACIJA KRAGUJEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20234806",
                "text": "JKP VODOVOD I KANALIZACIJA OBRENOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8487502",
                "text": "JKP VODOVOD I KANALIZACIJA PO PANČEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7190891",
                "text": "JKP VODOVOD KRALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7145837",
                "text": "JKP VODOVOD-KRUŠEVAC KRUŠEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17547496",
                "text": "JKP VODOVOD LEBANE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7204752",
                "text": "JKP VODOVOD LESKOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7134657",
                "text": "JKP VODOVOD MIONICA MIONICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8154929",
                "text": "JKP VODOVOD, SA PO ŠID, SVETOG SAVE 40",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "6812180",
                "text": "JKP VODOVOD SMEDEREVSKA PALANKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17224247",
                "text": "JKP VODOVOD VLASOTINCE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7183372",
                "text": "JKP VODOVOD ZAJEČAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20311380",
                "text": "JKP ZELENILO ARANĐELOVAC ARANĐELOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7066597",
                "text": "JKP ZELENILO-BEOGRAD BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7100124",
                "text": "JKSP ALEKSANDROVAC ALEKSANDROVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7030614",
                "text": "JKSP GROCKA GROCKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7154780",
                "text": "JKSP KOMSTAN TRSTENIK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7175019",
                "text": "JKSP RAZVITAK-ĆIĆEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8139679",
                "text": "JKSP SENTA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7210426",
                "text": "JP 3.SEPTEMBAR NOVA VAROŠ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8691193",
                "text": "JP APOLO NOVI SAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8724598",
                "text": "JP B C INFO BELA CRKVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20685107",
                "text": "JP BB TERM BAJINA BAŠTA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8677409",
                "text": "JP BELOCRKVANSKI VODOVOD I KANALIZACIJA BELA CRKVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17620975",
                "text": "JP BORJAK VRNJAČKA BANJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7207336",
                "text": "JP DOM LESKOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8143382",
                "text": "JP ENERGANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17216856",
                "text": "JP GAS ARANĐELOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8593205",
                "text": "JP GAS RUMA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8060274",
                "text": "JP GAS TEMERIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17369326",
                "text": "JP GRADSKA ČISTOĆA LAJKOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20491388",
                "text": "JP GRADSKA TOPLANA VELIKA PLANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7486251",
                "text": "JP GRADSKO STAMBENO BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17272519",
                "text": "JP GSA KRAGUJEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7451792",
                "text": "JP HIPODROM BEOGRAD BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8020981",
                "text": "JP ICK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17287419",
                "text": "JP INFORMATIVNI CENTAR OPŠTINE ALEKSANDROVAC - U LIKVIDACIJI",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20632348",
                "text": "JP INGRIN INĐIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7213573",
                "text": "JP JEDINSTVO KLADOVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7367384",
                "text": "JP KOLUBARA VALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7196709",
                "text": "JP KOMRAD VRANJE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8069565",
                "text": "JP KOMUNALAC BEČEJ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7131500",
                "text": "JP KOMUNALAC PIROT",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8646538",
                "text": "JP KOVIN-GAS KOVIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8143099",
                "text": "JP MLADOST OPOVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7298269",
                "text": "JP MORAVAC MRČAJEVCI",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17427458",
                "text": "JP NTV NIŠ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20660287",
                "text": "JP OBJEDINJENA NAPLATA ZAJEČAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7194374",
                "text": "JP OSA KRALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20026235",
                "text": "JP OSA VALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20603909",
                "text": "JP PALANKA ODRŽAVANJE SMEDEREVSKA PALANKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20466987",
                "text": "JP PALANKA STAN SMEDEREVSKA PALANKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8594953",
                "text": "JP PALIĆ-LUDAŠ PALIĆ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7096852",
                "text": "JP POLET VALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20488808",
                "text": "JP POSLOVNI CENTAR ALEKSANDROVAC-ŽUPA, ALEKSANDROVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17463756",
                "text": "JP POSLOVNI CENTAR-RAKOVICA BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20432799",
                "text": "JP POSLOVNO REKREATIVNI CENTAR RELAX KOVAČICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17495569",
                "text": "JP PUKOVAC-PUKOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8662959",
                "text": "JP RADIO NB NOVI BEČEJ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20156813",
                "text": "JP RADIO PIROT PIROT",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20153113",
                "text": "JP RADIO POŽAREVAC, POŽAREVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8169101",
                "text": "JP RADIO TELEVIZIJA INĐIJA INĐIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20054042",
                "text": "JP RADIO ZRENJANIN ZRENJANIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20198397",
                "text": "JP RTV ŠABAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7049285",
                "text": "JP SAVA CENTAR BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8755418",
                "text": "JP SC-ŠID",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7065809",
                "text": "JP SKC OBRENOVAC, OBRENOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17581139",
                "text": "JP SPORTSKI CENTAR ŠABAC, ŠABAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8157359",
                "text": "JP SPORTSKI I POSLOVNI CENTAR VOJVODINA  NOVI SAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8675295",
                "text": "JP SREM-GAS SREMSKA MITROVICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8688613",
                "text": "JP STANDARD ŽABALJ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8049548",
                "text": "JP SUBOTICA-TRANS",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8674809",
                "text": "JP TELEVIZIJA BAČKA PALANKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20297972",
                "text": "JP TELEVIZIJA POŽEGA POŽEGA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8161534",
                "text": "JP TOPLANA BEČEJ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8247323",
                "text": "JP TOPLANA BEOČIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20079592",
                "text": "JP TOPLANA JAGODINA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8792739",
                "text": "JP TOPLANA KIKINDA, TRG SRPSKIH DOBROVOLJACA 12",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7190859",
                "text": "JP TOPLANA KRALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20199601",
                "text": "JP TOPLIFIKACIJA LAZAREVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20484942",
                "text": "JP TPC ŠUMA MIKULJA SMEDEREVSKA PALANKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8745501",
                "text": "JP TRŽNICA DOO BAČKA TOPOLA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8113700",
                "text": "JP URBANIZAM  NOVI SAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17126628",
                "text": "JP URBOPLAN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7357362",
                "text": "JP USLUGA BOSILEGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7679181",
                "text": "JP VALJEVO-TURIST VALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8069921",
                "text": "JP VODOKANAL BEČEJ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "6988253",
                "text": "JP VODOVOD I KANALIZACIJA SA PO ĆUPRIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20663901",
                "text": "JP VODOVOD VLADIČIN HAN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8762198",
                "text": "JP VOJVODINAŠUME PETROVARADIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8666016",
                "text": "JP VRBAS GAS VRBAS",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7122802",
                "text": "JP ZA INFORMISANJE ŠUMADIJA ARANĐELOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20663944",
                "text": "JP ZA KOMUNALNO UREĐENJE VLADIČIN HAN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7993447",
                "text": "JP ZA PUTEVE I STAMBENO KOMUNALNU DELATNOST ALEKSINAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "6250033",
                "text": "JP ZA VODOSNABDEVANJE BOJNIK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7115911",
                "text": "JP ZA VODOVOD KANALIZACIJU I GASIFIKACIJU VODOVOD SA PO PARAĆIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8136050",
                "text": "JP ZAVOD ZA URBANIZAM KULA-ODŽACI",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20177098",
                "text": "JP ZOO-VRT JAGODINA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8597642",
                "text": "JPK VOD-KOM JABUKA PO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7165471",
                "text": "JSP KRAGUJEVAC KRAGUJEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7122403",
                "text": "JUP PLAN ŠABAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7029110",
                "text": "JVP BEOGRADVODE BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7110111",
                "text": "KJP ELAN KOSJERIĆ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17442350",
                "text": "KJP GRADSKI VODOVOD PROKUPLJE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8259127",
                "text": "KJP KOMPRED NOVO MILOŠEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7166389",
                "text": "KJP USLUGA SA PO ĆUPRIJA GROBLJANSKA BB",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8584281",
                "text": "KOMBREST",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8032874",
                "text": "KOMGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7306890",
                "text": "KOMNIS",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8285063",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7267452",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7099240",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7204787",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8099553",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7329733",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7328389",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8411131",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8584877",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7658702",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7299974",
                "text": "KOMUNALAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8050449",
                "text": "KOMUNALAC TITEL",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8234752",
                "text": "KOMUNALIJE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17223836",
                "text": "KOMUNALNE SLUŽBE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7216459",
                "text": "KOMUNALNO BLACE BLACE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8252050",
                "text": "KOVINSKI KOMUNALAC SA PO KOVIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7202687",
                "text": "KRALJEVICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7145667",
                "text": "KRUŠEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7386346",
                "text": "KSP GRADAC SA PO TUTIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7183747",
                "text": "KSP STAN JP DESPOTOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17390791",
                "text": "KURŠUMLIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7013922",
                "text": "LAZAREVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17210050",
                "text": "LJUBIČEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7319649",
                "text": "MEDIANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8356041",
                "text": "MELKOM",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17569325",
                "text": "MLADENOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7165447",
                "text": "MLADOST",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17112171",
                "text": "MORAVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7149581",
                "text": "MORAVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7253931",
                "text": "MORAVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "6389309",
                "text": "MORAVICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7254814",
                "text": "NAPREDAK",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7239408",
                "text": "NAŠ DOM",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7168845",
                "text": "NAŠ DOM",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8023263",
                "text": "NAŠ DOM",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7377878",
                "text": "NISKOGRADNJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7379625",
                "text": "NIŠSTAN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7204981",
                "text": "NOVI DOM",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8038210",
                "text": "NOVOSADSKA TOPLANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17027514",
                "text": "OBLAČINSKO JEZERO OBLAČINA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7360924",
                "text": "OBNOVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7019564",
                "text": "OLIMP-ZVEZDARA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8685657",
                "text": "P SEČANJ sa P.O. SEČANJ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8121460",
                "text": "P URBANIZAM STARA PAZOVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8786038",
                "text": "PADINA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17429299",
                "text": "PARKING-ŠABAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7046383",
                "text": "PARKING SERVIS",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20290307",
                "text": "PARKING SERVIS PIROT",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20182385",
                "text": "PARKING SERVIS POŽAREVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20121432",
                "text": "PARKING SERVIS SOMBOR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7704658",
                "text": "PEŠTERSKI VODOVOD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7711026",
                "text": "PIJACA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17314734",
                "text": "PIJACE MLADENOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8161259",
                "text": "PLAN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20074922",
                "text": "POČEKOVINA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7049455",
                "text": "POGREBNE USLUGE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8058644",
                "text": "POGREBNO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7143974",
                "text": "POSLOVNI CENTAR KRUŠEVAC JP",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8171963",
                "text": "PUT",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7190778",
                "text": "PUTEVI",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8133662",
                "text": "RAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20366753",
                "text": "RADIO BAČKI PETROVAC JP BAČKI PETROVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17061259",
                "text": "RADIO LAZAREVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7207255",
                "text": "RADIO LESKOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17034243",
                "text": "RADIO RAŠKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8126437",
                "text": "RADIO ŠID",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8009309",
                "text": "RADIO SUBOTICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "6875793",
                "text": "RADIO-TELEVIZIJA KRAGUJEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20360348",
                "text": "RADIO-TELEVIZIJA OPŠTINE KOVAČICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "6121179",
                "text": "RADIO VALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7176872",
                "text": "RAŠKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8672741",
                "text": "RAZVOJ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7187564",
                "text": "REČ RADNIKA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7832699",
                "text": "RNJP PARAĆIN - PARAĆIN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7285531",
                "text": "RT  VRANJE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7194455",
                "text": "RTK KRUŠEVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8393346",
                "text": "RTV PANČEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17257153",
                "text": "RTV PRUGA LAJKOVAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7350538",
                "text": "RZAV",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8271356",
                "text": "SAVA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8150346",
                "text": "SEČANJ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7006888",
                "text": "SOPOT",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20140143",
                "text": "SPORTSKI CENTAR MILAN GALE MUŠKATIROVIĆ JP BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8027021",
                "text": "STAMBENO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7147317",
                "text": "ŠTAMPA, RADIO I FILM",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8114854",
                "text": "STAN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7119399",
                "text": "STAN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7382685",
                "text": "STAN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7114885",
                "text": "STANDARD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8137005",
                "text": "STANDARD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7168667",
                "text": "STARI GRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7010109",
                "text": "STUDIO B",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20114223",
                "text": "SUBOTICAGAS",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8711585",
                "text": "SUBOTIČKE PIJACE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20632259",
                "text": "SVILAJNAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7349955",
                "text": "SVRLJIG",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "6847242",
                "text": "TELEVIZIJA SMEDEREVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7205929",
                "text": "TOPLANA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7172818",
                "text": "TOPLICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7351682",
                "text": "TOPLIFIKACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7123582",
                "text": "TOPOLA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20058536",
                "text": "TRŽNICE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8137838",
                "text": "UNIVERZAL",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17239139",
                "text": "URBANISTIČKI ZAVOD BEOGRADA JP BEOGRAD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8046875",
                "text": "URBANIZAM I ZAŠTITU SPOMENIKA KULTURE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8163685",
                "text": "USLUGA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7183267",
                "text": "USLUGA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "6158510",
                "text": "VETERINARSKA STANICA SURDULICA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17300806",
                "text": "VILIN LUG",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8761809",
                "text": "VODE VOJVODINE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8046751",
                "text": "VODOKANAL SOMBOR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7180101",
                "text": "VODOVOD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7226560",
                "text": "VODOVOD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7183453",
                "text": "VODOVOD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8234779",
                "text": "VODOVOD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8189544",
                "text": "VODOVOD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8099545",
                "text": "VODOVOD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7258160",
                "text": "VODOVOD",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8584885",
                "text": "VODOVOD I KANALIZACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17223810",
                "text": "VODOVOD I KANALIZACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7131518",
                "text": "VODOVOD I KANALIZACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17536320",
                "text": "VODOVOD I KANALIZACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7194129",
                "text": "VODOVOD I KANALIZACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7168861",
                "text": "VODOVOD I KANALIZACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8065195",
                "text": "VODOVOD I KANALIZACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8591130",
                "text": "VODOVOD I KANALIZACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8049637",
                "text": "VODOVOD I KANALIZACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8041083",
                "text": "VODOVOD I KANALIZACIJA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7168683",
                "text": "VODOVOD-ŠABAC",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7389574",
                "text": "VODOVOD SMEDEREVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7136277",
                "text": "VODOVOD VALJEVO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8061122",
                "text": "VRBAS",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7175213",
                "text": "VRELA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17263749",
                "text": "ZAJEČAR",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17100505",
                "text": "ZAPLANJSKE NOVINE Gadžin Han",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17223437",
                "text": "ZAVOD ZA URBANIZAM",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8144494",
                "text": "ZAVOD ZA URBANIZAM",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "20064102",
                "text": "ZAVOD ZA URBANIZAM GRADA SUBOTICE",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7261063",
                "text": "ZAVOD ZA URBANIZAM - NIŠ",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17268104",
                "text": "ZELEN",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "8487537",
                "text": "ZELENILO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7166222",
                "text": "ZELENILO",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "17610716",
                "text": "ŽITORAĐA",
                "parent": "companies",
                "type": "file"
            },
            {
                "id": "7290080",
                "text": "ZLATIBOR",
                "parent": "companies",
                "type": "file"
            }]
        return companies



    @staticmethod
    def get_companies():
        '''
        [
            { $group: { _id: { preduzece: "$preduzece" }}},
            { $sort : {  "_id.preduzece.slug"  : 1 } }
        ]
        :return:
        '''
        companies = OrderedDict([
            ("7006578", "10. OKTOBAR"),
            ("7148372", "3.OKTOBAR"),
            ("8128472", "4. OKTOBAR"),
            ("8037043", "6.OKTOBAR"),
            ("7451440", "ADA CIGANLIJA"),
            ("8681350", "ADICA JP ADA"),
            ("7343914", "AERODROM NIŠ"),
            ("17028138", "AERODROM ROSULJE"),
            ("8006148", "ATP PO PANČEVO"),
            ("7263775", "BADNJEVO"),
            ("8773378", "BANAT"),
            ("17479237", "BELA PALANKA"),
            ("17620983", "BELI IZVOR"),
            ("8596085", "BELOCRKVANSKA JEZERA"),
            ("17416774", "BEOGRADSKA TVRĐAVA"),
            ("7156421", "BIOKTOŠ"),
            ("7384432", "BOGOVINA"),
            ("7342578", "BOR"),
            ("7203144", "BORPREVOZ - U STEČAJU"),
            ("7113323", "BUKULJA"),
            ("8432350", "BUNAR"),
            ("8591148", "ČISTOĆA"),
            ("7337167", "ČISTOĆA"),
            ("8046816", "ČISTOĆA"),
            ("8049653", "ČISTOĆA I ZELENILO"),
            ("8065136", "ČISTOĆA I ZELENILO"),
            ("8148058", "ČOKA"),
            ("7098499", "DJUNIS"),
            ("7327609", "DUBOČICA"),
            ("8031185", "EKOS"),
            ("8025886", "ELGAS JP SENTA"),
            ("8042292", "FRUŠKA GORA"),
            ("8603162", "GLOGONJ"),
            ("7200579", "GOLUBAC"),
            ("20580542", "GRADSKA AUTOBUSKA STANICA LESKOVAC"),
            ("7045000", "GRADSKA ČISTOĆA"),
            ("17263838", "GRADSKA STAMBENA AGENCIJA"),
            ("7216009", "GRADSKA TOPLANA"),
            ("20489758", "GRADSKA TOPLANA"),
            ("17496255", "GRADSKA TOPLANA"),
            ("7295871", "GRADSKA TOPLANA PIROT"),
            ("7034628", "GRADSKE PIJACE"),
            ("17006312", "GRDELICA"),
            ("7022662", "GSP PO"),
            ("8487529", "HIGIJENA"),
            ("8220441", "INFORMATIVNI CENTAR"),
            ("7048971", "INFOSTAN"),
            ("7160658", "IZVOR"),
            ("7347189", "JAKOP IZVOR VLADIMIRCI"),
            ("20567252", "JATP ALIBUNAR ALIBUNAR"),
            ("8807370", "JATP BELA CRKVA BELA CRKVA"),
            ("17119290", "JAVNO KOMUNALNO PREDIZEĆE TOPLANA-LOZNICA LOZNICA"),
            ("8128260", "JAVNO KOMUNALNO PREDUZEĆE 7. OKTOBAR NOVI KNEŽEVAC"),
            ("7221061", "JAVNO KOMUNALNO PREDUZEĆE BELOSAVAC"),
            ("7279094", "JAVNO KOMUNALNO PREDUZEĆE GRADSKA TOPLANA KRUŠEVAC"),
            ("7323824", "JAVNO KOMUNALNO PREDUZEĆE KUČEVO"),
            ("7190174", "JAVNO KOMUNALNO PREDUZEĆE LIM PRIJEPOLJE"),
            ("7160763", "JAVNO KOMUNALNO PREDUZEĆE MILOŠ MITROVIĆ VELIKA PLANA"),
            ("8676089", "JAVNO KOMUNALNO PREDUZEĆE PARKING SUBOTICA"),
            ("7416253", "JAVNO KOMUNALNO PREDUZEĆE ŠUMADIJA"),
            ("8036446", "JAVNO PREDUZEĆE AUTOPREVOZ SA PO KIKINDA"),
            ("7165862", "JAVNO PREDUZEĆE DIREKCIJA ZA URBANIZAM - KRAGUJEVAC"),
            ("8029458", "JAVNO PREDUZEĆE NAŠ STAN KOVAČICA"),
            ("7208294", "JAVNO PREDUZEĆE ZA STAMBENE USLUGE"),
            ("17218913", "JAVNO PREDUZEĆE ZA USLUGE I TOPLIFIKACIJU SMEDEREVO"),
            ("8646708", "JAVNO URBANISTIČKO PREDUZEĆE - U STEČAJU"),
            ("7352964", "JEDINSTVO"),
            ("8041822", "JGSP NOVI SAD"),
            ("8744360", "JIP BAČKA TOPOLA"),
            ("20296895", "JIP IVANJIČKI RADIO JP IVANJICA"),
            ("8779384", "JIP RADIO BAP BAČKA PALANKA"),
            ("20297999", "JIP RADIO POŽEGA POŽEGA"),
            ("7352930", "JKP 7.JULI BATOČINA"),
            ("20547987", "JKP 8. AVGUST"),
            ("8677425", "JKP BELOCRKVANSKI KOMUNALAC BELA CRKVA"),
            ("8028362", "JKP BEOČIN BEOČIN"),
            ("7023332", "JKP BEOGRAD-PUT BEOGRAD"),
            ("7020210", "JKP BEOGRADSKE ELEKTRANE BEOGRAD"),
            ("7018762", "JKP BEOGRADSKI VODOVOD I KANALIZACIJA BEOGRAD"),
            ("7334419", "JKP BOGATIĆ BOGATIĆ"),
            ("7576382", "JKP ČAČAK SA PO ČAČAK"),
            ("7190905", "JKP ĆISTOĆA JP KRALJEVO"),
            ("20568437", "JKP ČISTOĆA MIONICA MIONICA"),
            ("8066531", "JKP ČISTOĆA NOVI SAD"),
            ("8681767", "JKP DIMNIČAR SUBOTICA - KÉMÉNYSEPRO KKV SZABADKA"),
            ("8588147", "JKP DOLOVI PO DOLOVO"),
            ("20320443", "JKP DONJI MILANOVAC DONJI MILANOVAC"),
            ("7168764", "JKP DRINA MALI ZVORNIK"),
            ("20104279", "JKP DUBOKO UŽICE"),
            ("8077690", "JKP ELAN KOVAČICA"),
            ("17507362", "JKP ENERGETIKA TRSTENIK"),
            ("20550635", "JKP GORICA NIŠ"),
            ("7192819", "JKP GORNJI MILANOVAC GORNJI MILANOVAC"),
            ("8067546", "JKP GRADITELJ SRBOBRAN"),
            ("6986242", "JKP GRADSKA ČISTOĆA NOVI PAZAR"),
            ("7343949", "JKP GRADSKA GROBLJA KRAGUJEVAC"),
            ("17227050", "JKP GRADSKA TOPLANA"),
            ("7317743", "JKP GRADSKA TOPLANA UŽICE"),
            ("17124048", "JKP GRADSKE TRŽNICE, KRAGUJEVAC"),
            ("7182554", "JKP GRADSKO ZELENILO ČAČAK"),
            ("8055432", "JKP GRADSKO ZELENILO NOVI SAD"),
            ("8488754", "JKP GREJANJE PO PANČEVO"),
            ("8023182", "JKP INFORMATIKA NOVI SAD"),
            ("8685029", "JKP KAČAREVO KAČAREVO"),
            ("20585439", "JKP KANALIZACIJA PEĆINCI PEĆINCI"),
            ("7181655", "JKP KOMUNALAC ČAČAK"),
            ("8004790", "JKP KOMUNALAC KULA"),
            ("17547488", "JKP KOMUNALAC LEBANE"),
            ("17127209", "JKP KOMUNALAC MAJDANPEK SA PO ŠAŠKA 5"),
            ("7675216", "JKP KOMUNALAC RAŽANJ"),
            ("7389566", "JKP KOMUNALAC SMEDEREVO"),
            ("17224239", "JKP KOMUNALAC VLASOTINCE"),
            ("20274328", "JKP KOMUNALNE USLUGE ALEKSINAC"),
            ("7216840", "JKP KOMUNALNO IVANJICA"),
            ("8081255", "JKP KOMUNALPROJEKT BAČKA PALANKA"),
            ("8055475", "JKP LISJE SA POTPUNOM ODGOVORNOŠĆU NOVI SAD"),
            ("7618956", "JKP MERMER REKOVAC"),
            ("7164297", "JKP MIKULJA SMEDEREVSKA PALANKA"),
            ("7045859", "JKP MLADENOVAC MLADENOVAC"),
            ("8077568", "JKP MLADOST PANČEVO"),
            ("7211856", "JKP NAISSUS NIŠ"),
            ("20116803", "JKP OBJEDINJENA NAPLATA NIŠ"),
            ("7041985", "JKP OBRENOVAC OBRENOVAC"),
            ("8594872", "JKP OMOLJICA OMOLJICA"),
            ("7305290", "JKP OSEČINA SA PO OSEČINA, PERE JOVANOVIĆA KOMIRIĆANCA 35"),
            ("20448784", "JKP PALANKA SERVIS SMEDEREVSKA PALANKA"),
            ("6569285", "JKP PARAĆIN PARAĆIN"),
            ("20518898", "JKP PARKING SERVIS ČAČAK"),
            ("17504894", "JKP PARKING SERVIS KRAGUJEVAC"),
            ("20573872", "JKP PARKING SERVIS LOZNICA"),
            ("20192542", "JKP PARKING SERVIS NIŠ"),
            ("8831149", "JKP PARKING SERVIS NOVI SAD"),
            ("20190655", "JKP PARKING SERVIS OBRENOVAC, OBRENOVAC"),
            ("7195591", "JKP PIJACA KRALJEVO"),
            ("20064382", "JKP PIJACE ROPOČEVO SOPOT"),
            ("7347103", "JKP PROGRES KOCELJEVA"),
            ("7365292", "JKP RAČA RAČA"),
            ("8005052", "JKP RADNIK SIVAC"),
            ("7262957", "JKP RASINA BRUS"),
            ("17579606", "JKP RIBARIĆE RIBARIĆE"),
            ("20426454", "JKP SEOSKI VODOVODI SJENICA"),
            ("20644419", "JKP SEVER ILANDŽA ILANDŽA"),
            ("8547718", "JKP STADION"),
            ("7208324", "JKP STANDARD-KNJAŽEVAC"),
            ("8736634", "JKP STANDARD ŠID"),
            ("8057982", "JKP STANDARD VRBAS"),
            ("17611828", "JKP STOPANJA STOPANJA"),
            ("8038180", "JKP SUBOTIČKA TOPLANA"),
            ("8658595", "JKP TEMERIN,TEMERIN"),
            ("17441531", "JKP TOPLANA BOR Đ.A KUNA 12"),
            ("7335393", "JKP TOPLANA-ŠABAC ŠABAC"),
            ("7357494", "JKP TOPLANA-VALJEVO VALJEVO"),
            ("7311281", "JKP TOPLANA ZAJEČAR - U LIKVIDACIJI"),
            ("20233940", "JKP TOPLOVOD OBRENOVAC"),
            ("7174306", "JKP TRŽNICA NIŠ"),
            ("8073074", "JKP TRŽNICA NOVI SAD"),
            ("8012644", "JKP TVRĐAVA BAČ"),
            ("8063753", "JKP USLUGA ODŽACI"),
            ("7155760", "JKP USLUGA PRIBOJ PO"),
            ("7218281", "JKP VARVARIN VARVARIN"),
            ("7096844", "JKP VIDRAK VALJEVO"),
            ("7167610", "JKP VODOVOD ČAČAK"),
            ("8005079", "JKP VODOVOD CRVENKA"),
            ("20274301", "JKP VODOVOD I KANALIZACIJA ALEKSINAC"),
            ("7165439", "JKP VODOVOD I KANALIZACIJA KRAGUJEVAC"),
            ("20234806", "JKP VODOVOD I KANALIZACIJA OBRENOVAC"),
            ("8487502", "JKP VODOVOD I KANALIZACIJA PO PANČEVO"),
            ("7190891", "JKP VODOVOD KRALJEVO"),
            ("7145837", "JKP VODOVOD-KRUŠEVAC KRUŠEVAC"),
            ("17547496", "JKP VODOVOD LEBANE"),
            ("7204752", "JKP VODOVOD LESKOVAC"),
            ("7134657", "JKP VODOVOD MIONICA MIONICA"),
            ("8154929", "JKP VODOVOD, SA PO ŠID, SVETOG SAVE 40"),
            ("6812180", "JKP VODOVOD SMEDEREVSKA PALANKA"),
            ("17224247", "JKP VODOVOD VLASOTINCE"),
            ("7183372", "JKP VODOVOD ZAJEČAR"),
            ("20311380", "JKP ZELENILO ARANĐELOVAC ARANĐELOVAC"),
            ("7066597", "JKP ZELENILO-BEOGRAD BEOGRAD"),
            ("7100124", "JKSP ALEKSANDROVAC ALEKSANDROVAC"),
            ("7030614", "JKSP GROCKA GROCKA"),
            ("7154780", "JKSP KOMSTAN TRSTENIK"),
            ("7175019", "JKSP RAZVITAK-ĆIĆEVAC"),
            ("8139679", "JKSP SENTA"),
            ("7210426", "JP 3.SEPTEMBAR NOVA VAROŠ"),
            ("8691193", "JP APOLO NOVI SAD"),
            ("8724598", "JP B C INFO BELA CRKVA"),
            ("20685107", "JP BB TERM BAJINA BAŠTA"),
            ("8677409", "JP BELOCRKVANSKI VODOVOD I KANALIZACIJA BELA CRKVA"),
            ("17620975", "JP BORJAK VRNJAČKA BANJA"),
            ("7207336", "JP DOM LESKOVAC"),
            ("8143382", "JP ENERGANA"),
            ("17216856", "JP GAS ARANĐELOVAC"),
            ("8593205", "JP GAS RUMA"),
            ("8060274", "JP GAS TEMERIN"),
            ("17369326", "JP GRADSKA ČISTOĆA LAJKOVAC"),
            ("20491388", "JP GRADSKA TOPLANA VELIKA PLANA"),
            ("7486251", "JP GRADSKO STAMBENO BEOGRAD"),
            ("17272519", "JP GSA KRAGUJEVAC"),
            ("7451792", "JP HIPODROM BEOGRAD BEOGRAD"),
            ("8020981", "JP ICK"),
            ("17287419", "JP INFORMATIVNI CENTAR OPŠTINE ALEKSANDROVAC - U LIKVIDACIJI"),
            ("20632348", "JP INGRIN INĐIJA"),
            ("7213573", "JP JEDINSTVO KLADOVO"),
            ("7367384", "JP KOLUBARA VALJEVO"),
            ("7196709", "JP KOMRAD VRANJE"),
            ("8069565", "JP KOMUNALAC BEČEJ"),
            ("7131500", "JP KOMUNALAC PIROT"),
            ("8646538", "JP KOVIN-GAS KOVIN"),
            ("8143099", "JP MLADOST OPOVO"),
            ("7298269", "JP MORAVAC MRČAJEVCI"),
            ("17427458", "JP NTV NIŠ"),
            ("20660287", "JP OBJEDINJENA NAPLATA ZAJEČAR"),
            ("7194374", "JP OSA KRALJEVO"),
            ("20026235", "JP OSA VALJEVO"),
            ("20603909", "JP PALANKA ODRŽAVANJE SMEDEREVSKA PALANKA"),
            ("20466987", "JP PALANKA STAN SMEDEREVSKA PALANKA"),
            ("8594953", "JP PALIĆ-LUDAŠ PALIĆ"),
            ("7096852", "JP POLET VALJEVO"),
            ("20488808", "JP POSLOVNI CENTAR ALEKSANDROVAC-ŽUPA, ALEKSANDROVAC"),
            ("17463756", "JP POSLOVNI CENTAR-RAKOVICA BEOGRAD"),
            ("20432799", "JP POSLOVNO REKREATIVNI CENTAR RELAX KOVAČICA"),
            ("17495569", "JP PUKOVAC-PUKOVAC"),
            ("8662959", "JP RADIO NB NOVI BEČEJ"),
            ("20156813", "JP RADIO PIROT PIROT"),
            ("20153113", "JP RADIO POŽAREVAC, POŽAREVAC"),
            ("8169101", "JP RADIO TELEVIZIJA INĐIJA INĐIJA"),
            ("20054042", "JP RADIO ZRENJANIN ZRENJANIN"),
            ("20198397", "JP RTV ŠABAC"),
            ("7049285", "JP SAVA CENTAR BEOGRAD"),
            ("8755418", "JP SC-ŠID"),
            ("7065809", "JP SKC OBRENOVAC, OBRENOVAC"),
            ("17581139", "JP SPORTSKI CENTAR ŠABAC, ŠABAC"),
            ("8157359", "JP SPORTSKI I POSLOVNI CENTAR VOJVODINA  NOVI SAD"),
            ("8675295", "JP SREM-GAS SREMSKA MITROVICA"),
            ("8688613", "JP STANDARD ŽABALJ"),
            ("8049548", "JP SUBOTICA-TRANS"),
            ("8674809", "JP TELEVIZIJA BAČKA PALANKA"),
            ("20297972", "JP TELEVIZIJA POŽEGA POŽEGA"),
            ("8161534", "JP TOPLANA BEČEJ"),
            ("8247323", "JP TOPLANA BEOČIN"),
            ("20079592", "JP TOPLANA JAGODINA"),
            ("8792739", "JP TOPLANA KIKINDA, TRG SRPSKIH DOBROVOLJACA 12"),
            ("7190859", "JP TOPLANA KRALJEVO"),
            ("20199601", "JP TOPLIFIKACIJA LAZAREVAC"),
            ("20484942", "JP TPC ŠUMA MIKULJA SMEDEREVSKA PALANKA"),
            ("8745501", "JP TRŽNICA DOO BAČKA TOPOLA"),
            ("8113700", "JP URBANIZAM  NOVI SAD"),
            ("17126628", "JP URBOPLAN"),
            ("7357362", "JP USLUGA BOSILEGRAD"),
            ("7679181", "JP VALJEVO-TURIST VALJEVO"),
            ("8069921", "JP VODOKANAL BEČEJ"),
            ("6988253", "JP VODOVOD I KANALIZACIJA SA PO ĆUPRIJA"),
            ("20663901", "JP VODOVOD VLADIČIN HAN"),
            ("8762198", "JP VOJVODINAŠUME PETROVARADIN"),
            ("8666016", "JP VRBAS GAS VRBAS"),
            ("7122802", "JP ZA INFORMISANJE ŠUMADIJA ARANĐELOVAC"),
            ("20663944", "JP ZA KOMUNALNO UREĐENJE VLADIČIN HAN"),
            ("7993447", "JP ZA PUTEVE I STAMBENO KOMUNALNU DELATNOST ALEKSINAC"),
            ("6250033", "JP ZA VODOSNABDEVANJE BOJNIK"),
            ("7115911", "JP ZA VODOVOD KANALIZACIJU I GASIFIKACIJU VODOVOD SA PO PARAĆIN"),
            ("8136050", "JP ZAVOD ZA URBANIZAM KULA-ODŽACI"),
            ("20177098", "JP ZOO-VRT JAGODINA"),
            ("8597642", "JPK VOD-KOM JABUKA PO"),
            ("7165471", "JSP KRAGUJEVAC KRAGUJEVAC"),
            ("7122403", "JUP PLAN ŠABAC"),
            ("7029110", "JVP BEOGRADVODE BEOGRAD"),
            ("7110111", "KJP ELAN KOSJERIĆ"),
            ("17442350", "KJP GRADSKI VODOVOD PROKUPLJE"),
            ("8259127", "KJP KOMPRED NOVO MILOŠEVO"),
            ("7166389", "KJP USLUGA SA PO ĆUPRIJA GROBLJANSKA BB"),
            ("8584281", "KOMBREST"),
            ("8032874", "KOMGRAD"),
            ("7306890", "KOMNIS"),
            ("8285063", "KOMUNALAC"),
            ("7267452", "KOMUNALAC"),
            ("7099240", "KOMUNALAC"),
            ("7204787", "KOMUNALAC"),
            ("8099553", "KOMUNALAC"),
            ("7329733", "KOMUNALAC"),
            ("7328389", "KOMUNALAC"),
            ("8411131", "KOMUNALAC"),
            ("8584877", "KOMUNALAC"),
            ("7658702", "KOMUNALAC"),
            ("7299974", "KOMUNALAC"),
            ("8050449", "KOMUNALAC TITEL"),
            ("8234752", "KOMUNALIJE"),
            ("17223836", "KOMUNALNE SLUŽBE"),
            ("7216459", "KOMUNALNO BLACE BLACE"),
            ("8252050", "KOVINSKI KOMUNALAC SA PO KOVIN"),
            ("7202687", "KRALJEVICA"),
            ("7145667", "KRUŠEVAC"),
            ("7386346", "KSP GRADAC SA PO TUTIN"),
            ("7183747", "KSP STAN JP DESPOTOVAC"),
            ("17390791", "KURŠUMLIJA"),
            ("7013922", "LAZAREVAC"),
            ("17210050", "LJUBIČEVO"),
            ("7319649", "MEDIANA"),
            ("8356041", "MELKOM"),
            ("17569325", "MLADENOVAC"),
            ("7165447", "MLADOST"),
            ("17112171", "MORAVA"),
            ("7149581", "MORAVA"),
            ("7253931", "MORAVA"),
            ("6389309", "MORAVICA"),
            ("7254814", "NAPREDAK"),
            ("7239408", "NAŠ DOM"),
            ("7168845", "NAŠ DOM"),
            ("8023263", "NAŠ DOM"),
            ("7377878", "NISKOGRADNJA"),
            ("7379625", "NIŠSTAN"),
            ("7204981", "NOVI DOM"),
            ("8038210", "NOVOSADSKA TOPLANA"),
            ("17027514", "OBLAČINSKO JEZERO OBLAČINA"),
            ("7360924", "OBNOVA"),
            ("7019564", "OLIMP-ZVEZDARA"),
            ("8685657", "P SEČANJ sa P.O. SEČANJ"),
            ("8121460", "P URBANIZAM STARA PAZOVA"),
            ("8786038", "PADINA"),
            ("17429299", "PARKING-ŠABAC"),
            ("7046383", "PARKING SERVIS"),
            ("20290307", "PARKING SERVIS PIROT"),
            ("20182385", "PARKING SERVIS POŽAREVAC"),
            ("20121432", "PARKING SERVIS SOMBOR"),
            ("7704658", "PEŠTERSKI VODOVOD"),
            ("7711026", "PIJACA"),
            ("17314734", "PIJACE MLADENOVAC"),
            ("8161259", "PLAN"),
            ("20074922", "POČEKOVINA"),
            ("7049455", "POGREBNE USLUGE"),
            ("8058644", "POGREBNO"),
            ("7143974", "POSLOVNI CENTAR KRUŠEVAC JP"),
            ("8171963", "PUT"),
            ("7190778", "PUTEVI"),
            ("8133662", "RAD"),
            ("20366753", "RADIO BAČKI PETROVAC JP BAČKI PETROVAC"),
            ("17061259", "RADIO LAZAREVAC"),
            ("7207255", "RADIO LESKOVAC"),
            ("17034243", "RADIO RAŠKA"),
            ("8126437", "RADIO ŠID"),
            ("8009309", "RADIO SUBOTICA"),
            ("6875793", "RADIO-TELEVIZIJA KRAGUJEVAC"),
            ("20360348", "RADIO-TELEVIZIJA OPŠTINE KOVAČICA"),
            ("6121179", "RADIO VALJEVO"),
            ("7176872", "RAŠKA"),
            ("8672741", "RAZVOJ"),
            ("7187564", "REČ RADNIKA"),
            ("7832699", "RNJP PARAĆIN - PARAĆIN"),
            ("7285531", "RT  VRANJE"),
            ("7194455", "RTK KRUŠEVAC"),
            ("8393346", "RTV PANČEVO"),
            ("17257153", "RTV PRUGA LAJKOVAC"),
            ("7350538", "RZAV"),
            ("8271356", "SAVA"),
            ("8150346", "SEČANJ"),
            ("7006888", "SOPOT"),
            ("20140143", "SPORTSKI CENTAR MILAN GALE MUŠKATIROVIĆ JP BEOGRAD"),
            ("8027021", "STAMBENO"),
            ("7147317", "ŠTAMPA, RADIO I FILM"),
            ("8114854", "STAN"),
            ("7119399", "STAN"),
            ("7382685", "STAN"),
            ("7114885", "STANDARD"),
            ("8137005", "STANDARD"),
            ("7168667", "STARI GRAD"),
            ("7010109", "STUDIO B"),
            ("20114223", "SUBOTICAGAS"),
            ("8711585", "SUBOTIČKE PIJACE"),
            ("20632259", "SVILAJNAC"),
            ("7349955", "SVRLJIG"),
            ("6847242", "TELEVIZIJA SMEDEREVO"),
            ("7205929", "TOPLANA"),
            ("7172818", "TOPLICA"),
            ("7351682", "TOPLIFIKACIJA"),
            ("7123582", "TOPOLA"),
            ("20058536", "TRŽNICE"),
            ("8137838", "UNIVERZAL"),
            ("17239139", "URBANISTIČKI ZAVOD BEOGRADA JP BEOGRAD"),
            ("8046875", "URBANIZAM I ZAŠTITU SPOMENIKA KULTURE"),
            ("8163685", "USLUGA"),
            ("7183267", "USLUGA"),
            ("6158510", "VETERINARSKA STANICA SURDULICA"),
            ("17300806", "VILIN LUG"),
            ("8761809", "VODE VOJVODINE"),
            ("8046751", "VODOKANAL SOMBOR"),
            ("7180101", "VODOVOD"),
            ("7226560", "VODOVOD"),
            ("7183453", "VODOVOD"),
            ("8234779", "VODOVOD"),
            ("8189544", "VODOVOD"),
            ("8099545", "VODOVOD"),
            ("7258160", "VODOVOD"),
            ("8584885", "VODOVOD I KANALIZACIJA"),
            ("17223810", "VODOVOD I KANALIZACIJA"),
            ("7131518", "VODOVOD I KANALIZACIJA"),
            ("17536320", "VODOVOD I KANALIZACIJA"),
            ("7194129", "VODOVOD I KANALIZACIJA"),
            ("7168861", "VODOVOD I KANALIZACIJA"),
            ("8065195", "VODOVOD I KANALIZACIJA"),
            ("8591130", "VODOVOD I KANALIZACIJA"),
            ("8049637", "VODOVOD I KANALIZACIJA"),
            ("8041083", "VODOVOD I KANALIZACIJA"),
            ("7168683", "VODOVOD-ŠABAC"),
            ("7389574", "VODOVOD SMEDEREVO"),
            ("7136277", "VODOVOD VALJEVO"),
            ("8061122", "VRBAS"),
            ("7175213", "VRELA"),
            ("17263749", "ZAJEČAR"),
            ("17100505", "ZAPLANJSKE NOVINE Gadžin Han"),
            ("17223437", "ZAVOD ZA URBANIZAM"),
            ("8144494", "ZAVOD ZA URBANIZAM"),
            ("20064102", "ZAVOD ZA URBANIZAM GRADA SUBOTICE"),
            ("7261063", "ZAVOD ZA URBANIZAM - NIŠ"),
            ("17268104", "ZELEN"),
            ("8487537", "ZELENILO"),
            ("7166222", "ZELENILO"),
            ("17610716", "ŽITORAĐA"),
            ("7290080", "ZLATIBOR")
        ])

        return companies

    @staticmethod
    def get_tree_activities():
        activities = [{
                'id':'activities',
                'text': "Delatnosti",
                'parent': "#",
                'type':"folder"
            },
            {
                "id": "7111",
                "text": "Arhitektonska delatnost",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "9104",
                "text": "Delatnost botaničkih i zooloških vrtova i zaštita prirodnih vrednosti",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "5914",
                "text": "Delatnost prikazivanja kinematografskih dela",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "9311",
                "text": "Delatnost sportskih objekata",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "3522",
                "text": "Distribucija gasovitih goriva gasovodom",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "6010",
                "text": "Emitovanje radio-programa",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "210",
                "text": "Gajenje šuma i ostale šumarske delatnosti",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4931",
                "text": "Gradski i prigradski kopneni prevoz putnika",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "5510",
                "text": "Hoteli i sličan smeštaj",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "7112",
                "text": "Inženjerske delatnosti i tehničko savetovanje",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4291",
                "text": "Izgradnja hidrotehničkih objekata",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4211",
                "text": "Izgradnja puteva i autoputeva",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "6820",
                "text": "Iznajmljivanje vlastitih ili iznajmljenih nekretnina i upravljanje njima",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "9001",
                "text": "Izvodjačka umetnost",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "6110",
                "text": "Kablovske telekomunikacije",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "7022",
                "text": "Konsultantske aktivnosti u vezi s poslovanjem i ostalim upravljanjem",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4391",
                "text": "Krovni radovi",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4690",
                "text": "Nespecijalizovana trgovina na veliko",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "6311",
                "text": "Obrada podataka, hosting i sl.",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4799",
                "text": "Ostala trgovina na malo izvan prodavnica, tezgi i pijaca",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "6499",
                "text": "Ostale nepomenute finansijske usluge, osim osiguranja i penzijskih fondova",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "9329",
                "text": "Ostale zabavne i rekreativne delatnosti",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4329",
                "text": "Ostali instalacioni radovi u gradjevinarstvu",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4339",
                "text": "Ostali završni radovi",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "9603",
                "text": "Pogrebne i srodne delatnosti",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "162",
                "text": "Pomoćne delatnosti u uzgoju životinja",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4321",
                "text": "Postavljanje električnih instalacija",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "6020",
                "text": "Proizvodnja i emitovanje televizijskog programa",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "9004",
                "text": "Rad umetničkih ustanova",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4110",
                "text": "Razrada gradjevinskih projekata",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "5210",
                "text": "Skladištenje",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "3811",
                "text": "Skupljanje otpada koji nije opasan",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "3600",
                "text": "Skupljanje, prečišćavanje i distribucija vode",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "3530",
                "text": "Snabdevanje parom i klimatizacija",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "4789",
                "text": "Trgovina na malo ostalom robom na tezgama i pijacama",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "6832",
                "text": "Upravljanje nekretninama za naknadu",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "8413",
                "text": "Uredjenje poslovanja i doprinos uspešnijem poslovanju u oblasti ekonomije",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "8122",
                "text": "Usluge ostalog čišćenja zgrada i opreme",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "8121",
                "text": "Usluge redovnog čišćenja zgrada",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "8130",
                "text": "Usluge uredjenja i održavanja okoline",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "5221",
                "text": "Uslužne delatnosti u kopnenom saobraćaju",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "5223",
                "text": "Uslužne delatnosti u vazdušnom saobraćaju",
                "parent": "activities",
                "type": "file"
            },
            {
                "id": "9103",
                "text": "Zaštita i održavanje nepokretnih kulturnih dobara, kulturno-istorijskih lokacija, zgrada i sličnih turističkih spomenika",
                "parent": "activities",
                "type": "file"
            }]
        return activities


    @staticmethod
    def get_activities():
        '''
        [
            { $group: { _id: { delatnost: "$delatnost" }}},
            { $sort : {  "_id.delatnost.slug"  : 1 } }
        ]
        :return:
        '''

        activities = OrderedDict([
            ("7111", "Arhitektonska delatnost"),
            ("9104", "Delatnost botaničkih i zooloških vrtova i zaštita prirodnih vrednosti"),
            ("5914", "Delatnost prikazivanja kinematografskih dela"),
            ("9311", "Delatnost sportskih objekata"),
            ("3522", "Distribucija gasovitih goriva gasovodom"),
            ("6010", "Emitovanje radio-programa"),
            ("210", "Gajenje šuma i ostale šumarske delatnosti"),
            ("4931", "Gradski i prigradski kopneni prevoz putnika"),
            ("5510", "Hoteli i sličan smeštaj"),
            ("7112", "Inženjerske delatnosti i tehničko savetovanje"),
            ("4291", "Izgradnja hidrotehničkih objekata"),
            ("4211", "Izgradnja puteva i autoputeva"),
            ("6820", "Iznajmljivanje vlastitih ili iznajmljenih nekretnina i upravljanje njima"),
            ("9001", "Izvodjačka umetnost"),
            ("6110", "Kablovske telekomunikacije"),
            ("7022", "Konsultantske aktivnosti u vezi s poslovanjem i ostalim upravljanjem"),
            ("4391", "Krovni radovi"),
            ("4690", "Nespecijalizovana trgovina na veliko"),
            ("6311", "Obrada podataka, hosting i sl."),
            ("4799", "Ostala trgovina na malo izvan prodavnica, tezgi i pijaca"),
            ("6499", "Ostale nepomenute finansijske usluge, osim osiguranja i penzijskih fondova"),
            ("9329", "Ostale zabavne i rekreativne delatnosti"),
            ("4329", "Ostali instalacioni radovi u gradjevinarstvu"),
            ("4339", "Ostali završni radovi"),
            ("9603", "Pogrebne i srodne delatnosti"),
            ("162", "Pomoćne delatnosti u uzgoju životinja"),
            ("4321", "Postavljanje električnih instalacija"),
            ("6020", "Proizvodnja i emitovanje televizijskog programa"),
            ("9004", "Rad umetničkih ustanova"),
            ("4110", "Razrada gradjevinskih projekata"),
            ("5210", "Skladištenje"),
            ("3811", "Skupljanje otpada koji nije opasan"),
            ("3600", "Skupljanje, prečišćavanje i distribucija vode"),
            ("3530", "Snabdevanje parom i klimatizacija"),
            ("4789", "Trgovina na malo ostalom robom na tezgama i pijacama"),
            ("6832", "Upravljanje nekretninama za naknadu"),
            ("8413", "Uredjenje poslovanja i doprinos uspešnijem poslovanju u oblasti ekonomije"),
            ("8122", "Usluge ostalog čišćenja zgrada i opreme"),
            ("8121", "Usluge redovnog čišćenja zgrada"),
            ("8130", "Usluge uredjenja i održavanja okoline"),
            ("5221", "Uslužne delatnosti u kopnenom saobraćaju"),
            ("5223", "Uslužne delatnosti u vazdušnom saobraćaju"),
            ("9103", "Zaštita i održavanje nepokretnih kulturnih dobara, kulturno-istorijskih lokacija, zgrada i sličnih turističkih spomenika"),
        ])

        return activities

    @staticmethod
    def get_tree_owners():
        owners = [{
                'id':'owners',
                'text': "Osnivači",
                'parent': "#",
                'type':"folder"
            },
            {
                "id": "AUTONOMNA POKRAJINA VOJVODINA",
                "text": "AUTONOMNA POKRAJINA VOJVODINA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD BEOGRAD",
                "text": "GRAD BEOGRAD",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD JAGODINA",
                "text": "GRAD JAGODINA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD KRAGUJEVAC",
                "text": "GRAD KRAGUJEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD KRUŠEVAC",
                "text": "GRAD KRUŠEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD NIŠ - GRADSKA OPŠTINA CRVENI KRST",
                "text": "GRAD NIŠ - GRADSKA OPŠTINA CRVENI KRST",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD NIŠ - SKUPŠTINA GRADA NIŠA",
                "text": "GRAD NIŠ - SKUPŠTINA GRADA NIŠA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD NIŠ",
                "text": "GRAD NIŠ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD NOVI SAD",
                "text": "GRAD NOVI SAD",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD PANČEVO",
                "text": "GRAD PANČEVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD ŠABAC",
                "text": "GRAD ŠABAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD SOMBOR",
                "text": "GRAD SOMBOR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD SREMSKA MITROVICA",
                "text": "GRAD SREMSKA MITROVICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRAD ZRENJANIN",
                "text": "GRAD ZRENJANIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRADSKA OPŠTINA LAZAREVAC",
                "text": "GRADSKA OPŠTINA LAZAREVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRADSKA OPŠTINA SOPOT",
                "text": "GRADSKA OPŠTINA SOPOT",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "GRADSKA OPŠTINA STARI GRAD",
                "text": "GRADSKA OPŠTINA STARI GRAD",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "IZVRŠNI ODBOR SKUPŠTINE OPŠTINE ZRENJANIN",
                "text": "IZVRŠNI ODBOR SKUPŠTINE OPŠTINE ZRENJANIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "JAVNO KOMUNALNO PREDUZEĆE 'KOMUNALAC' VLASOTINCE",
                "text": "JAVNO KOMUNALNO PREDUZEĆE 'KOMUNALAC' VLASOTINCE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "JAVNO KOMUNALNO PREDUZEĆE 7.OKTOBAR NOVI KNEŽEVAC",
                "text": "JAVNO KOMUNALNO PREDUZEĆE 7.OKTOBAR NOVI KNEŽEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "JAVNO KOMUNALNO PREDUZEĆE LEBANE, LEBANE",
                "text": "JAVNO KOMUNALNO PREDUZEĆE LEBANE, LEBANE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "JAVNO KOMUNALNO PREDUZEĆE NOVOSADSKA TOPLANA  SA POTPUNOM ODGOVORNOŠĆU NOVI SAD, VLADIMIRA NIKOLIĆA 1",
                "text": "JAVNO KOMUNALNO PREDUZEĆE NOVOSADSKA TOPLANA  SA POTPUNOM ODGOVORNOŠĆU NOVI SAD, VLADIMIRA NIKOLIĆA 1",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "JAVNO KOMUNALNO PREDUZEĆE VODOVOD VLASOTINCE, NEMANJINA 36",
                "text": "JAVNO KOMUNALNO PREDUZEĆE VODOVOD VLASOTINCE, NEMANJINA 36",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "JAVNO PREDUZEĆE KOMUNALAC RAŽANJ, PARTIZANSKA 68",
                "text": "JAVNO PREDUZEĆE KOMUNALAC RAŽANJ, PARTIZANSKA 68",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "KOMUNALNA RADNA ORGANIZACIJA ZLATIBOR",
                "text": "KOMUNALNA RADNA ORGANIZACIJA ZLATIBOR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "KOMUNALNO PREDUZEĆE VODOVOD",
                "text": "KOMUNALNO PREDUZEĆE VODOVOD",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "MESNA ZAJEDNICA BANATSKI BRESTOVAC",
                "text": "MESNA ZAJEDNICA BANATSKI BRESTOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "MESNA ZAJEDNICA BEZDAN",
                "text": "MESNA ZAJEDNICA BEZDAN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "MESNA ZAJEDNICA DEBELJAČA",
                "text": "MESNA ZAJEDNICA DEBELJAČA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "MESNA ZAJEDNICA JABUKA , TRG BORISA KIDRIČA 1",
                "text": "MESNA ZAJEDNICA JABUKA , TRG BORISA KIDRIČA 1",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "MESNA ZAJEDNICA KAČAREVO",
                "text": "MESNA ZAJEDNICA KAČAREVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "MESNA ZAJEDNICA MELENCI",
                "text": "MESNA ZAJEDNICA MELENCI",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "MESNA ZAJEDNICA VLADIMIROVAC",
                "text": "MESNA ZAJEDNICA VLADIMIROVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "MESNA ZAJEDNICA",
                "text": "MESNA ZAJEDNICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "NARODNI ODBOR GRADA BEOGRADA",
                "text": "NARODNI ODBOR GRADA BEOGRADA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "NARODNI ODBOR GRADA NOVOG SADA",
                "text": "NARODNI ODBOR GRADA NOVOG SADA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "NARODNI ODBOR OPŠTINE KRAGUJEVAC",
                "text": "NARODNI ODBOR OPŠTINE KRAGUJEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "NARODNI ODBOR OPŠTINE VRANJE",
                "text": "NARODNI ODBOR OPŠTINE VRANJE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "ODBOR MESNE ZAJEDNICE GLOGONJ",
                "text": "ODBOR MESNE ZAJEDNICE GLOGONJ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "ODBOR MESNE ZAJEDNICE MITA VUKOSAVLJEV",
                "text": "ODBOR MESNE ZAJEDNICE MITA VUKOSAVLJEV",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA ARANĐELOVAC",
                "text": "OPŠTINA ARANĐELOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA BAČKA PALANKA",
                "text": "OPŠTINA BAČKA PALANKA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA BAČKA TOPOLA",
                "text": "OPŠTINA BAČKA TOPOLA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA BELA CRKVA BELA CRKVA, MILETIĆEVA 2",
                "text": "OPŠTINA BELA CRKVA BELA CRKVA, MILETIĆEVA 2",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA BOLJEVAC",
                "text": "OPŠTINA BOLJEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA BOR",
                "text": "OPŠTINA BOR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA ĆIĆEVAC",
                "text": "OPŠTINA ĆIĆEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA ČOKA",
                "text": "OPŠTINA ČOKA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA GROCKA",
                "text": "OPŠTINA GROCKA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA INĐIJA",
                "text": "OPŠTINA INĐIJA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA KANJIŽA",
                "text": "OPŠTINA KANJIŽA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA KOVAČICA",
                "text": "OPŠTINA KOVAČICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA KRALJEVO",
                "text": "OPŠTINA KRALJEVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA LAJKOVAC",
                "text": "OPŠTINA LAJKOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA LAPOVO",
                "text": "OPŠTINA LAPOVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Opština Lazarevac",
                "text": "Opština Lazarevac",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA LAZAREVAC",
                "text": "OPŠTINA LAZAREVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA LESKOVAC LESKOVAC, PANA ĐUKIĆA 9-11",
                "text": "OPŠTINA LESKOVAC LESKOVAC, PANA ĐUKIĆA 9-11",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA LESKOVAC",
                "text": "OPŠTINA LESKOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA MIONICA",
                "text": "OPŠTINA MIONICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA MLADENOVAC",
                "text": "OPŠTINA MLADENOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA NEGOTIN",
                "text": "OPŠTINA NEGOTIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA NOVI BEČEJ",
                "text": "OPŠTINA NOVI BEČEJ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA OBRENOVAC",
                "text": "OPŠTINA OBRENOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA ODŽACI",
                "text": "OPŠTINA ODŽACI",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA OPOVO",
                "text": "OPŠTINA OPOVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA PANČEVO",
                "text": "OPŠTINA PANČEVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA PEĆINCI",
                "text": "OPŠTINA PEĆINCI",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA PIROT",
                "text": "OPŠTINA PIROT",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA POŽAREVAC",
                "text": "OPŠTINA POŽAREVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA PROKUPLJE",
                "text": "OPŠTINA PROKUPLJE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA ŠABAC",
                "text": "OPŠTINA ŠABAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA SEČANJ",
                "text": "OPŠTINA SEČANJ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA SENTA",
                "text": "OPŠTINA SENTA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA ŠID",
                "text": "OPŠTINA ŠID",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA SMEDEREVO SMEDEREVO, OMLADINSKA 1",
                "text": "OPŠTINA SMEDEREVO SMEDEREVO, OMLADINSKA 1",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA SMEDEREVO",
                "text": "OPŠTINA SMEDEREVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA SMEDEREVSKA PALANKA SMEDEREVSKA PALANKA, VUKA KARADZIĆA 25",
                "text": "OPŠTINA SMEDEREVSKA PALANKA SMEDEREVSKA PALANKA, VUKA KARADZIĆA 25",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA SREMSKA MITROVICA",
                "text": "OPŠTINA SREMSKA MITROVICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA STARA PAZOVA",
                "text": "OPŠTINA STARA PAZOVA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA SUBOTICA",
                "text": "OPŠTINA SUBOTICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA SVILAJNAC",
                "text": "OPŠTINA SVILAJNAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA TEMERIN OPŠTINSKA UPRAVA",
                "text": "OPŠTINA TEMERIN OPŠTINSKA UPRAVA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA UB",
                "text": "OPŠTINA UB",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA VELIKA PLANA VELIKA PLANA,  MILOŠA VELIKOG 30",
                "text": "OPŠTINA VELIKA PLANA VELIKA PLANA,  MILOŠA VELIKOG 30",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA VRANJE",
                "text": "OPŠTINA VRANJE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA VRBAS",
                "text": "OPŠTINA VRBAS",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA ZAJEČAR",
                "text": "OPŠTINA ZAJEČAR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINAŠABAC",
                "text": "OPŠTINAŠABAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINSKA UPRAVA VRBAS",
                "text": "OPŠTINSKA UPRAVA VRBAS",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "POKRAJINSKI SEKRETARIJAT ZA INFORMACIJE",
                "text": "POKRAJINSKI SEKRETARIJAT ZA INFORMACIJE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "PROJEKTNO PREDUZEĆE VRBAS VRBAS",
                "text": "PROJEKTNO PREDUZEĆE VRBAS VRBAS",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "REPUBLIKA SRBIJA AUTONOMNA POKRAJINA VOJVODINA SKUPŠTINA OPŠTINE SUBOTICA",
                "text": "REPUBLIKA SRBIJA AUTONOMNA POKRAJINA VOJVODINA SKUPŠTINA OPŠTINE SUBOTICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "RO URBIS  NOVI SAD",
                "text": "RO URBIS  NOVI SAD",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SAVET MESNE ZAJEDNICE KOVAČICA",
                "text": "SAVET MESNE ZAJEDNICE KOVAČICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SIZ STANOVANJA OPŠTINE KNIĆ",
                "text": "SIZ STANOVANJA OPŠTINE KNIĆ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKKUPŠTINA OPŠTINA BELA CRKVA",
                "text": "SKKUPŠTINA OPŠTINA BELA CRKVA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUOŠTINA OPŠTINE LESKOVAC",
                "text": "SKUOŠTINA OPŠTINE LESKOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠRINA OPŠTINE VRANJE",
                "text": "SKUPŠRINA OPŠTINE VRANJE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GADA NIŠA",
                "text": "SKUPŠTINA GADA NIŠA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA BEOGRADA BEOGRAD, DRAGOSLAVA JOVANOVIĆA 2",
                "text": "SKUPŠTINA GRADA BEOGRADA BEOGRAD, DRAGOSLAVA JOVANOVIĆA 2",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA BEOGRADA",
                "text": "SKUPŠTINA GRADA BEOGRADA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA ČAČKA",
                "text": "SKUPŠTINA GRADA ČAČKA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA KRAGUJEVCA",
                "text": "SKUPŠTINA GRADA KRAGUJEVCA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA NOVI SAD",
                "text": "SKUPŠTINA GRADA NOVI SAD",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA NOVOG SADA",
                "text": "SKUPŠTINA GRADA NOVOG SADA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA SMEDEREVO",
                "text": "SKUPŠTINA GRADA SMEDEREVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA ZAJEČARA",
                "text": "SKUPŠTINA GRADA ZAJEČARA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA",
                "text": "SKUPŠTINA GRADA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠINE KRAGUJEVAC",
                "text": "SKUPŠTINA OPŠINE KRAGUJEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA BEČEJ",
                "text": "SKUPŠTINA OPŠTINA BEČEJ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA BELA CRKVA",
                "text": "SKUPŠTINA OPŠTINA BELA CRKVA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA BEOČIN",
                "text": "SKUPŠTINA OPŠTINA BEOČIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA KRUŠEVAC",
                "text": "SKUPŠTINA OPŠTINA KRUŠEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA OPOVO",
                "text": "SKUPŠTINA OPŠTINA OPOVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA PANČEVO",
                "text": "SKUPŠTINA OPŠTINA PANČEVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA PARAĆIN",
                "text": "SKUPŠTINA OPŠTINA PARAĆIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA RUMA",
                "text": "SKUPŠTINA OPŠTINA RUMA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA SJENICA",
                "text": "SKUPŠTINA OPŠTINA SJENICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA SOMBOR",
                "text": "SKUPŠTINA OPŠTINA SOMBOR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ADA",
                "text": "SKUPŠTINA OPŠTINE ADA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ALEKSANDROVAC",
                "text": "SKUPŠTINA OPŠTINE ALEKSANDROVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ALEKSINAC",
                "text": "SKUPŠTINA OPŠTINE ALEKSINAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ALIBUNAR",
                "text": "SKUPŠTINA OPŠTINE ALIBUNAR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE APATIN",
                "text": "SKUPŠTINA OPŠTINE APATIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ARANĐELOVAC",
                "text": "SKUPŠTINA OPŠTINE ARANĐELOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ARILJE",
                "text": "SKUPŠTINA OPŠTINE ARILJE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BABUŠNICA",
                "text": "SKUPŠTINA OPŠTINE BABUŠNICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BAČ",
                "text": "SKUPŠTINA OPŠTINE BAČ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BAČKA PALANKA",
                "text": "SKUPŠTINA OPŠTINE BAČKA PALANKA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BAČKA TOPOLA",
                "text": "SKUPŠTINA OPŠTINE BAČKA TOPOLA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BAČKI PETROVAC",
                "text": "SKUPŠTINA OPŠTINE BAČKI PETROVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BAJINA BAŠTA",
                "text": "SKUPŠTINA OPŠTINE BAJINA BAŠTA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BARAJEVO",
                "text": "SKUPŠTINA OPŠTINE BARAJEVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BELA CRKVA",
                "text": "SKUPŠTINA OPŠTINE BELA CRKVA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BELA PALANKA",
                "text": "SKUPŠTINA OPŠTINE BELA PALANKA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BOJNIK",
                "text": "SKUPŠTINA OPŠTINE BOJNIK",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BOR",
                "text": "SKUPŠTINA OPŠTINE BOR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BRUS",
                "text": "SKUPŠTINA OPŠTINE BRUS",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE BUJANOVAC",
                "text": "SKUPŠTINA OPŠTINE BUJANOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ČAČAK",
                "text": "SKUPŠTINA OPŠTINE ČAČAK",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ČAJETINA",
                "text": "SKUPŠTINA OPŠTINE ČAJETINA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE CRNA TRAVA",
                "text": "SKUPŠTINA OPŠTINE CRNA TRAVA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE DESPOTOVAC",
                "text": "SKUPŠTINA OPŠTINE DESPOTOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE DIMITROVGRAD",
                "text": "SKUPŠTINA OPŠTINE DIMITROVGRAD",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE DOLJEVAC",
                "text": "SKUPŠTINA OPŠTINE DOLJEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE GOLUBAC",
                "text": "SKUPŠTINA OPŠTINE GOLUBAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE GORNJI MILANOVAC",
                "text": "SKUPŠTINA OPŠTINE GORNJI MILANOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE GROCKA",
                "text": "SKUPŠTINA OPŠTINE GROCKA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE INĐIJA",
                "text": "SKUPŠTINA OPŠTINE INĐIJA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE IVANJICA",
                "text": "SKUPŠTINA OPŠTINE IVANJICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE JAGODINA",
                "text": "SKUPŠTINA OPŠTINE JAGODINA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KIKINDA",
                "text": "SKUPŠTINA OPŠTINE KIKINDA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KLADOVO",
                "text": "SKUPŠTINA OPŠTINE KLADOVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KNJAŽEVAC",
                "text": "SKUPŠTINA OPŠTINE KNJAŽEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KOCELJEVA",
                "text": "SKUPŠTINA OPŠTINE KOCELJEVA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KOSJERIĆ",
                "text": "SKUPŠTINA OPŠTINE KOSJERIĆ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KOVAČICA",
                "text": "SKUPŠTINA OPŠTINE KOVAČICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KOVIN",
                "text": "SKUPŠTINA OPŠTINE KOVIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KRALJEVO",
                "text": "SKUPŠTINA OPŠTINE KRALJEVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KRUŠEVAC",
                "text": "SKUPŠTINA OPŠTINE KRUŠEVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KULA",
                "text": "SKUPŠTINA OPŠTINE KULA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KULA, KULA",
                "text": "SKUPŠTINA OPŠTINE KULA, KULA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KURŠUMLIJA",
                "text": "SKUPŠTINA OPŠTINE KURŠUMLIJA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE LAJKOVAC",
                "text": "SKUPŠTINA OPŠTINE LAJKOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE LEBANE",
                "text": "SKUPŠTINA OPŠTINE LEBANE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE LESKOVAC",
                "text": "SKUPŠTINA OPŠTINE LESKOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE LJIG",
                "text": "SKUPŠTINA OPŠTINE LJIG",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE LOZNICA",
                "text": "SKUPŠTINA OPŠTINE LOZNICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE LOZNICE",
                "text": "SKUPŠTINA OPŠTINE LOZNICE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE LUČANI",
                "text": "SKUPŠTINA OPŠTINE LUČANI",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE MAJDANPEK",
                "text": "SKUPŠTINA OPŠTINE MAJDANPEK",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE MALI ZVORNIK",
                "text": "SKUPŠTINA OPŠTINE MALI ZVORNIK",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE MEDVEDJA",
                "text": "SKUPŠTINA OPŠTINE MEDVEDJA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE MIONICA",
                "text": "SKUPŠTINA OPŠTINE MIONICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE NIŠ NIŠ",
                "text": "SKUPŠTINA OPŠTINE NIŠ NIŠ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE NOVA CRNJA",
                "text": "SKUPŠTINA OPŠTINE NOVA CRNJA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE NOVA VAROŠ",
                "text": "SKUPŠTINA OPŠTINE NOVA VAROŠ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE NOVI BEČEJ",
                "text": "SKUPŠTINA OPŠTINE NOVI BEČEJ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE OBRENOVAC",
                "text": "SKUPŠTINA OPŠTINE OBRENOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ODŽACI",
                "text": "SKUPŠTINA OPŠTINE ODŽACI",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE PANČEVO",
                "text": "SKUPŠTINA OPŠTINE PANČEVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE PARAĆIN",
                "text": "SKUPŠTINA OPŠTINE PARAĆIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE PEĆINCI",
                "text": "SKUPŠTINA OPŠTINE PEĆINCI",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE PIROT",
                "text": "SKUPŠTINA OPŠTINE PIROT",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE POŽAREVAC",
                "text": "SKUPŠTINA OPŠTINE POŽAREVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE POŽEGA",
                "text": "SKUPŠTINA OPŠTINE POŽEGA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE PREŠEVO",
                "text": "SKUPŠTINA OPŠTINE PREŠEVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE PRIJEPOLJE",
                "text": "SKUPŠTINA OPŠTINE PRIJEPOLJE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE RAKOVICA",
                "text": "SKUPŠTINA OPŠTINE RAKOVICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE RAŠKA",
                "text": "SKUPŠTINA OPŠTINE RAŠKA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE REKOVAC",
                "text": "SKUPŠTINA OPŠTINE REKOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE RUMA",
                "text": "SKUPŠTINA OPŠTINE RUMA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ŠABAC",
                "text": "SKUPŠTINA OPŠTINE ŠABAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SEČANJ",
                "text": "SKUPŠTINA OPŠTINE SEČANJ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SENTA",
                "text": "SKUPŠTINA OPŠTINE SENTA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ŠID",
                "text": "SKUPŠTINA OPŠTINE ŠID",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SJENICA I TUTIN",
                "text": "SKUPŠTINA OPŠTINE SJENICA I TUTIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SJENICA",
                "text": "SKUPŠTINA OPŠTINE SJENICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SMEDEDREVO",
                "text": "SKUPŠTINA OPŠTINE SMEDEDREVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SMEDEREVSKA PALANKA",
                "text": "SKUPŠTINA OPŠTINE SMEDEREVSKA PALANKA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SOMBOR",
                "text": "SKUPŠTINA OPŠTINE SOMBOR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SOPOT",
                "text": "SKUPŠTINA OPŠTINE SOPOT",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SRBOBRAN",
                "text": "SKUPŠTINA OPŠTINE SRBOBRAN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SREMSKA MITROVICA",
                "text": "SKUPŠTINA OPŠTINE SREMSKA MITROVICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SUBOTICA",
                "text": "SKUPŠTINA OPŠTINE SUBOTICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Skupština opštine Subotica",
                "text": "Skupština opštine Subotica",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SURDULICA",
                "text": "SKUPŠTINA OPŠTINE SURDULICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SVILAJNAC",
                "text": "SKUPŠTINA OPŠTINE SVILAJNAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE SVRLJIG",
                "text": "SKUPŠTINA OPŠTINE SVRLJIG",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE TEMERIN",
                "text": "SKUPŠTINA OPŠTINE TEMERIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE TOPOLA",
                "text": "SKUPŠTINA OPŠTINE TOPOLA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE TRSTENIK",
                "text": "SKUPŠTINA OPŠTINE TRSTENIK",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE TUTIN",
                "text": "SKUPŠTINA OPŠTINE TUTIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE UB",
                "text": "SKUPŠTINA OPŠTINE UB",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE UŽICE",
                "text": "SKUPŠTINA OPŠTINE UŽICE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE VALJEVO",
                "text": "SKUPŠTINA OPŠTINE VALJEVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE VARVARIN",
                "text": "SKUPŠTINA OPŠTINE VARVARIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE VELIKA PLANA",
                "text": "SKUPŠTINA OPŠTINE VELIKA PLANA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE VLADIČIN HAN",
                "text": "SKUPŠTINA OPŠTINE VLADIČIN HAN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE VLADIMIRCI",
                "text": "SKUPŠTINA OPŠTINE VLADIMIRCI",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE VRNJAČKA BANJA",
                "text": "SKUPŠTINA OPŠTINE VRNJAČKA BANJA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ŽABALJ",
                "text": "SKUPŠTINA OPŠTINE ŽABALJ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ŽAGUBICA",
                "text": "SKUPŠTINA OPŠTINE ŽAGUBICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ZAJEČAR",
                "text": "SKUPŠTINA OPŠTINE ZAJEČAR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ŽITIŠTE",
                "text": "SKUPŠTINA OPŠTINE ŽITIŠTE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ŽITORAĐA",
                "text": "SKUPŠTINA OPŠTINE ŽITORAĐA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE ZRENJANIN",
                "text": "SKUPŠTINA OPŠTINE ZRENJANIN",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OŠTINE BOGATIĆ",
                "text": "SKUPŠTINA OŠTINE BOGATIĆ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA SAMOUPRAVNE INTERESNE ZAJEDNICE ZA FIZIČKU KULTURU GRADA NOVOG SADA",
                "text": "SKUPŠTINA SAMOUPRAVNE INTERESNE ZAJEDNICE ZA FIZIČKU KULTURU GRADA NOVOG SADA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINE OPŠTINE BEČEJ",
                "text": "SKUPŠTINE OPŠTINE BEČEJ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SO BLACE",
                "text": "SO BLACE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SO VRANJE",
                "text": "SO VRANJE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SO ZVEZDARA",
                "text": "SO ZVEZDARA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SUPŠTINA OPŠTINE VRANJE",
                "text": "SUPŠTINA OPŠTINE VRANJE",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Grad Kragujevac",
                "text": "Grad Kragujevac",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Deoničarski kapital",
                "text": "Deoničarski kapital",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Državni kapital",
                "text": "Državni kapital",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Društveni kapital",
                "text": "Društveni kapital",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Opština Vrbas",
                "text": "Opština Vrbas",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Opština Kovačica",
                "text": "Opština Kovačica",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "OPŠTINA SUBOTICA",
                "text": "OPŠTINA SUBOTICA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Opština Trstenik",
                "text": "Opština Trstenik",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA AP VOJVODINA",
                "text": "SKUPŠTINA AP VOJVODINA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Skupština grada Beograda",
                "text": "Skupština grada Beograda",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA BEOGRADA",
                "text": "SKUPŠTINA GRADA BEOGRADA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Skupština grada Kragujevca",
                "text": "Skupština grada Kragujevca",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA GRADA NIŠA",
                "text": "SKUPŠTINA GRADA NIŠA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA NOVI BEČEJ",
                "text": "SKUPŠTINA OPŠTINA NOVI BEČEJ",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINA ŠID",
                "text": "SKUPŠTINA OPŠTINA ŠID",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE - PETROVAC NA MLAVI",
                "text": "SKUPŠTINA OPŠTINE - PETROVAC NA MLAVI",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE VALjEVO",
                "text": "SKUPŠTINA OPŠTINE VALjEVO",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Skupština opštine Gornji Milanovac",
                "text": "Skupština opštine Gornji Milanovac",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE INĐIJA",
                "text": "SKUPŠTINA OPŠTINE INĐIJA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE KIKINDA",
                "text": "SKUPŠTINA OPŠTINE KIKINDA",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE LESKOVAC",
                "text": "SKUPŠTINA OPŠTINE LESKOVAC",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTINA OPŠTINE NOVI PAZAR",
                "text": "SKUPŠTINA OPŠTINE NOVI PAZAR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Skupština opštine Obrenovac",
                "text": "Skupština opštine Obrenovac",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Skupština opštine Raška",
                "text": "Skupština opštine Raška",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Skupština opštine Smederevska Palanka",
                "text": "Skupština opštine Smederevska Palanka",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Skupština opštine subotica",
                "text": "Skupština opštine subotica",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "Skupština Opštine Ćuprija",
                "text": "Skupština Opštine Ćuprija",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SKUPŠTNA OPŠTINE NOVI PAZAR",
                "text": "SKUPŠTNA OPŠTINE NOVI PAZAR",
                "parent": "owners",
                "type": "file"
            },
            {
                "id": "SO BELA CRKVA",
                "text": "SO BELA CRKVA",
                "parent": "owners",
                "type": "file"
            }]

        return owners

    @staticmethod
    def get_owners():
        owners = [
            "AUTONOMNA POKRAJINA VOJVODINA",
            "GRAD BEOGRAD",
            "GRAD JAGODINA",
            "GRAD KRAGUJEVAC",
            "GRAD KRUŠEVAC",
            "GRAD NIŠ - GRADSKA OPŠTINA CRVENI KRST",
            "GRAD NIŠ - SKUPŠTINA GRADA NIŠA",
            "GRAD NIŠ - SKUPŠTINA GRADA NIŠA",
            "GRAD NIŠ",
            "GRAD NOVI SAD",
            "GRAD PANČEVO",
            "GRAD ŠABAC",
            "GRAD SOMBOR",
            "GRAD SREMSKA MITROVICA",
            "GRAD ZRENJANIN",
            "GRADSKA OPŠTINA LAZAREVAC",
            "GRADSKA OPŠTINA SOPOT",
            "GRADSKA OPŠTINA STARI GRAD",
            "IZVRŠNI ODBOR SKUPŠTINE OPŠTINE ZRENJANIN",
            "JAVNO KOMUNALNO PREDUZEĆE 'KOMUNALAC' VLASOTINCE",
            "JAVNO KOMUNALNO PREDUZEĆE 7.OKTOBAR NOVI KNEŽEVAC",
            "JAVNO KOMUNALNO PREDUZEĆE LEBANE, LEBANE",
            "JAVNO KOMUNALNO PREDUZEĆE NOVOSADSKA TOPLANA  SA POTPUNOM ODGOVORNOŠĆU NOVI SAD, VLADIMIRA NIKOLIĆA 1",
            "JAVNO KOMUNALNO PREDUZEĆE VODOVOD VLASOTINCE, NEMANJINA 36",
            "JAVNO PREDUZEĆE KOMUNALAC RAŽANJ, PARTIZANSKA 68",
            "KOMUNALNA RADNA ORGANIZACIJA ZLATIBOR",
            "KOMUNALNO PREDUZEĆE VODOVOD",
            "MESNA ZAJEDNICA BANATSKI BRESTOVAC",
            "MESNA ZAJEDNICA BEZDAN",
            "MESNA ZAJEDNICA DEBELJAČA",
            "MESNA ZAJEDNICA JABUKA , TRG BORISA KIDRIČA 1",
            "MESNA ZAJEDNICA KAČAREVO",
            "MESNA ZAJEDNICA MELENCI",
            "MESNA ZAJEDNICA VLADIMIROVAC",
            "MESNA ZAJEDNICA",
            "NARODNI ODBOR GRADA BEOGRADA",
            "NARODNI ODBOR GRADA NOVOG SADA",
            "NARODNI ODBOR OPŠTINE KRAGUJEVAC",
            "NARODNI ODBOR OPŠTINE VRANJE",
            "ODBOR MESNE ZAJEDNICE GLOGONJ",
            "ODBOR MESNE ZAJEDNICE MITA VUKOSAVLJEV",
            "OPŠTINA ARANĐELOVAC",
            "OPŠTINA BAČKA PALANKA",
            "OPŠTINA BAČKA TOPOLA",
            "OPŠTINA BELA CRKVA BELA CRKVA, MILETIĆEVA 2",
            "OPŠTINA BOLJEVAC",
            "OPŠTINA BOR",
            "OPŠTINA ĆIĆEVAC",
            "OPŠTINA ČOKA",
            "OPŠTINA GROCKA",
            "OPŠTINA INĐIJA",
            "OPŠTINA KANJIŽA",
            "OPŠTINA KOVAČICA",
            "OPŠTINA KRALJEVO",
            "OPŠTINA LAJKOVAC",
            "OPŠTINA LAPOVO",
            "Opština Lazarevac",
            "OPŠTINA LAZAREVAC",
            "OPŠTINA LESKOVAC LESKOVAC, PANA ĐUKIĆA 9-11",
            "OPŠTINA LESKOVAC",
            "OPŠTINA MIONICA",
            "OPŠTINA MLADENOVAC",
            "OPŠTINA NEGOTIN",
            "OPŠTINA NOVI BEČEJ",
            "OPŠTINA OBRENOVAC",
            "OPŠTINA ODŽACI",
            "OPŠTINA OPOVO",
            "OPŠTINA PANČEVO",
            "OPŠTINA PEĆINCI",
            "OPŠTINA PIROT",
            "OPŠTINA POŽAREVAC",
            "OPŠTINA PROKUPLJE",
            "OPŠTINA ŠABAC",
            "OPŠTINA SEČANJ",
            "OPŠTINA SENTA",
            "OPŠTINA ŠID",
            "OPŠTINA SMEDEREVO SMEDEREVO, OMLADINSKA 1",
            "OPŠTINA SMEDEREVO",
            "OPŠTINA SMEDEREVSKA PALANKA SMEDEREVSKA PALANKA, VUKA KARADZIĆA 25",
            "OPŠTINA SREMSKA MITROVICA",
            "OPŠTINA STARA PAZOVA",
            "OPŠTINA SUBOTICA",
            "OPŠTINA SVILAJNAC",
            "OPŠTINA TEMERIN OPŠTINSKA UPRAVA",
            "OPŠTINA UB",
            "OPŠTINA VELIKA PLANA VELIKA PLANA,  MILOŠA VELIKOG 30",
            "OPŠTINA VRANJE",
            "OPŠTINA VRBAS",
            "OPŠTINA ZAJEČAR",
            "OPŠTINAŠABAC",
            "OPŠTINSKA UPRAVA VRBAS",
            "POKRAJINSKI SEKRETARIJAT ZA INFORMACIJE",
            "PROJEKTNO PREDUZEĆE VRBAS VRBAS",
            "REPUBLIKA SRBIJA AUTONOMNA POKRAJINA VOJVODINA SKUPŠTINA OPŠTINE SUBOTICA",
            "RO URBIS  NOVI SAD",
            "SAVET MESNE ZAJEDNICE KOVAČICA",
            "SIZ STANOVANJA OPŠTINE KNIĆ",
            "SKKUPŠTINA OPŠTINA BELA CRKVA",
            "SKUOŠTINA OPŠTINE LESKOVAC",
            "SKUPŠRINA OPŠTINE VRANJE",
            "SKUPŠTINA GADA NIŠA",
            "SKUPŠTINA GRADA BEOGRADA BEOGRAD, DRAGOSLAVA JOVANOVIĆA 2",
            "SKUPŠTINA GRADA BEOGRADA",
            "SKUPŠTINA GRADA ČAČKA",
            "SKUPŠTINA GRADA KRAGUJEVCA",
            "SKUPŠTINA GRADA NOVI SAD",
            "SKUPŠTINA GRADA NOVOG SADA",
            "SKUPŠTINA GRADA SMEDEREVO",
            "SKUPŠTINA GRADA ZAJEČARA",
            "SKUPŠTINA GRADA",
            "SKUPŠTINA OPŠINE KRAGUJEVAC",
            "SKUPŠTINA OPŠTINA BEČEJ",
            "SKUPŠTINA OPŠTINA BELA CRKVA",
            "SKUPŠTINA OPŠTINA BEOČIN",
            "SKUPŠTINA OPŠTINA KRUŠEVAC",
            "SKUPŠTINA OPŠTINA OPOVO",
            "SKUPŠTINA OPŠTINA PANČEVO",
            "SKUPŠTINA OPŠTINA PARAĆIN",
            "SKUPŠTINA OPŠTINA RUMA",
            "SKUPŠTINA OPŠTINA SJENICA",
            "SKUPŠTINA OPŠTINA SOMBOR",
            "SKUPŠTINA OPŠTINE ADA",
            "SKUPŠTINA OPŠTINE ALEKSANDROVAC",
            "SKUPŠTINA OPŠTINE ALEKSINAC",
            "SKUPŠTINA OPŠTINE ALIBUNAR",
            "SKUPŠTINA OPŠTINE APATIN",
            "SKUPŠTINA OPŠTINE ARANĐELOVAC",
            "SKUPŠTINA OPŠTINE ARILJE",
            "SKUPŠTINA OPŠTINE BABUŠNICA",
            "SKUPŠTINA OPŠTINE BAČ",
            "SKUPŠTINA OPŠTINE BAČKA PALANKA",
            "SKUPŠTINA OPŠTINE BAČKA TOPOLA",
            "SKUPŠTINA OPŠTINE BAČKI PETROVAC",
            "SKUPŠTINA OPŠTINE BAJINA BAŠTA",
            "SKUPŠTINA OPŠTINE BARAJEVO",
            "SKUPŠTINA OPŠTINE BELA CRKVA",
            "SKUPŠTINA OPŠTINE BELA PALANKA",
            "SKUPŠTINA OPŠTINE BOJNIK",
            "SKUPŠTINA OPŠTINE BOR",
            "SKUPŠTINA OPŠTINE BRUS",
            "SKUPŠTINA OPŠTINE BUJANOVAC",
            "SKUPŠTINA OPŠTINE ČAČAK",
            "SKUPŠTINA OPŠTINE ČAJETINA",
            "SKUPŠTINA OPŠTINE CRNA TRAVA",
            "SKUPŠTINA OPŠTINE DESPOTOVAC",
            "SKUPŠTINA OPŠTINE DIMITROVGRAD",
            "SKUPŠTINA OPŠTINE DOLJEVAC",
            "SKUPŠTINA OPŠTINE GOLUBAC",
            "SKUPŠTINA OPŠTINE GORNJI MILANOVAC",
            "SKUPŠTINA OPŠTINE GROCKA",
            "SKUPŠTINA OPŠTINE INĐIJA",
            "SKUPŠTINA OPŠTINE IVANJICA",
            "SKUPŠTINA OPŠTINE JAGODINA",
            "SKUPŠTINA OPŠTINE KIKINDA",
            "SKUPŠTINA OPŠTINE KLADOVO",
            "SKUPŠTINA OPŠTINE KNJAŽEVAC",
            "SKUPŠTINA OPŠTINE KOCELJEVA",
            "SKUPŠTINA OPŠTINE KOSJERIĆ",
            "SKUPŠTINA OPŠTINE KOVAČICA",
            "SKUPŠTINA OPŠTINE KOVIN",
            "SKUPŠTINA OPŠTINE KRALJEVO",
            "SKUPŠTINA OPŠTINE KRUŠEVAC",
            "SKUPŠTINA OPŠTINE KULA",
            "SKUPŠTINA OPŠTINE KULA, KULA",
            "SKUPŠTINA OPŠTINE KURŠUMLIJA",
            "SKUPŠTINA OPŠTINE LAJKOVAC",
            "SKUPŠTINA OPŠTINE LEBANE",
            "SKUPŠTINA OPŠTINE LESKOVAC",
            "SKUPŠTINA OPŠTINE LJIG",
            "SKUPŠTINA OPŠTINE LOZNICA",
            "SKUPŠTINA OPŠTINE LOZNICE",
            "SKUPŠTINA OPŠTINE LUČANI",
            "SKUPŠTINA OPŠTINE MAJDANPEK",
            "SKUPŠTINA OPŠTINE MALI ZVORNIK",
            "SKUPŠTINA OPŠTINE MEDVEDJA",
            "SKUPŠTINA OPŠTINE MIONICA",
            "SKUPŠTINA OPŠTINE NIŠ NIŠ",
            "SKUPŠTINA OPŠTINE NOVA CRNJA",
            "SKUPŠTINA OPŠTINE NOVA VAROŠ",
            "SKUPŠTINA OPŠTINE NOVI BEČEJ",
            "SKUPŠTINA OPŠTINE OBRENOVAC",
            "SKUPŠTINA OPŠTINE ODŽACI",
            "SKUPŠTINA OPŠTINE PANČEVO",
            "SKUPŠTINA OPŠTINE PARAĆIN",
            "SKUPŠTINA OPŠTINE PEĆINCI",
            "SKUPŠTINA OPŠTINE PIROT",
            "SKUPŠTINA OPŠTINE POŽAREVAC",
            "SKUPŠTINA OPŠTINE POŽEGA",
            "SKUPŠTINA OPŠTINE PREŠEVO",
            "SKUPŠTINA OPŠTINE PRIJEPOLJE",
            "SKUPŠTINA OPŠTINE RAKOVICA",
            "SKUPŠTINA OPŠTINE RAŠKA",
            "SKUPŠTINA OPŠTINE REKOVAC",
            "SKUPŠTINA OPŠTINE RUMA",
            "SKUPŠTINA OPŠTINE ŠABAC",
            "SKUPŠTINA OPŠTINE SEČANJ",
            "SKUPŠTINA OPŠTINE SENTA",
            "SKUPŠTINA OPŠTINE ŠID",
            "SKUPŠTINA OPŠTINE SJENICA I TUTIN",
            "SKUPŠTINA OPŠTINE SJENICA",
            "SKUPŠTINA OPŠTINE SMEDEDREVO",
            "SKUPŠTINA OPŠTINE SMEDEREVSKA PALANKA",
            "SKUPŠTINA OPŠTINE SOMBOR",
            "SKUPŠTINA OPŠTINE SOPOT",
            "SKUPŠTINA OPŠTINE SRBOBRAN",
            "SKUPŠTINA OPŠTINE SREMSKA MITROVICA",
            "SKUPŠTINA OPŠTINE SUBOTICA",
            "Skupština opštine Subotica",
            "SKUPŠTINA OPŠTINE SURDULICA",
            "SKUPŠTINA OPŠTINE SVILAJNAC",
            "SKUPŠTINA OPŠTINE SVRLJIG",
            "SKUPŠTINA OPŠTINE TEMERIN",
            "SKUPŠTINA OPŠTINE TOPOLA",
            "SKUPŠTINA OPŠTINE TRSTENIK",
            "SKUPŠTINA OPŠTINE TUTIN",
            "SKUPŠTINA OPŠTINE UB",
            "SKUPŠTINA OPŠTINE UŽICE",
            "SKUPŠTINA OPŠTINE VALJEVO",
            "SKUPŠTINA OPŠTINE VARVARIN",
            "SKUPŠTINA OPŠTINE VELIKA PLANA",
            "SKUPŠTINA OPŠTINE VLADIČIN HAN",
            "SKUPŠTINA OPŠTINE VLADIMIRCI",
            "SKUPŠTINA OPŠTINE VRNJAČKA BANJA",
            "SKUPŠTINA OPŠTINE ŽABALJ",
            "SKUPŠTINA OPŠTINE ŽAGUBICA",
            "SKUPŠTINA OPŠTINE ZAJEČAR",
            "SKUPŠTINA OPŠTINE ŽITIŠTE",
            "SKUPŠTINA OPŠTINE ŽITORAĐA",
            "SKUPŠTINA OPŠTINE ZRENJANIN",
            "SKUPŠTINA OŠTINE BOGATIĆ",
            "SKUPŠTINA SAMOUPRAVNE INTERESNE ZAJEDNICE ZA FIZIČKU KULTURU GRADA NOVOG SADA",
            "SKUPŠTINE OPŠTINE BEČEJ",
            "SO BLACE",
            "SO VRANJE",
            "SO ZVEZDARA",
            "SUPŠTINA OPŠTINE VRANJE",
            "Град Крагујевац",
            "Деоничарски капитал",
            "Државни капитал",
            "Друштвени капитал",
            "Општина Врбас",
            "Општина Ковачица",
            "ОПШТИНА СУБОТИЦА",
            "Општина Трстеник",
            "СКУПШТИНА АП ВОЈВОДИНА",
            "Скупштина града Београда",
            "СКУПШТИНА ГРАДА БЕОГРАДА",
            "Скупштина града Крагујевца",
            "СКУПШТИНА ГРАДА НИША",
            "СКУПШТИНА ОПШТИНА НОВИ БЕЧЕЈ",
            "СКУПШТИНА ОПШТИНА ШИД",
            "СКУПШТИНА ОПШТИНЕ - ПЕТРОВАЦ НА МЛАВИ",
            "СКУПШТИНА ОПШТИНЕ ВАЉЕВО",
            "Скупштина општине Горњи Милановац",
            "СКУПШТИНА ОПШТИНЕ ИНЂИЈА",
            "СКУПШТИНА ОПШТИНЕ КИКИНДА",
            "СКУПШТИНА ОПШТИНЕ ЛЕСКОВАЦ",
            "СКУПШТИНА ОПШТИНЕ НОВИ ПАЗАР",
            "Скупштина општине Обреновац",
            "Скупштина општине Рашка",
            "Скупштина општине Смедеревска Паланка",
            "Скупштина општине суботица",
            "Скупштина Општине Ћуприја",
            "СКУПШТНА ОПШТИНЕ НОВИ ПАЗАР",
            "СО БЕЛА ЦРКВА",
        ]

        return owners

    @staticmethod
    def get_company_sizes():
        sizes = [ "malo", "srednje", "veliko" ]

        return sizes

    @staticmethod
    def get_years():
        years = [
            2006, 2007, 2008, 2009, 2010
        ]

        return years

    @staticmethod
    def get_tree_years():
        years = [{
                'id':'year',
                'text': "Godine",
                'type':"folder",
                'parent': "#"
            },
            {
                'id':'2006',
                'text': "2006",
                'parent': "year",
                'type':"file"
            },
            {
                'id':'2007',
                'text': "2007",
                'parent': "year",
                'type':"file"
            },
            {
                'id':'2008',
                'text': "2008",
                'parent': "year",
                'type':"file"
            },
            {
                'id':'2009',
                'text': "2009",
                'parent': "year",
                'type':"file"
            },
            {
                'id':'2010',
                'text': "2010",
                'parent': "year",
                'type':"file"
            }
        ]
        return years