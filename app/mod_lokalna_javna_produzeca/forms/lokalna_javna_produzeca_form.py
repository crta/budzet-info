# coding=utf-8
from wtforms import Form, SelectField, SelectMultipleField
from utils import FormUtils

class LokalnaJavnaProduzacaForm(Form):

    chart_type = SelectField(
        'Tip grafikona',
        choices=[
            ('Barchart', 'Barchart'),
            ('Floating Bubbles', 'Floating Bubbles'),
            ('Map', 'Mapa'),
            ('Scatterplot', 'Scatterplot')
        ]
    )

    ukupni_rashodi_categories = SelectMultipleField('Ukupni rashodi',
        choices=[
            ('AOP 207', 'Poslovni rashodi'),
            ('AOP 216', 'Finansijski rashodi'),
            ('AOP 218', 'Ostali rashodi '),
        ],
        default='AOP 207')

    poslovni_rashodi_categories = SelectMultipleField('Poslovni rashodi',
        choices=[
            ('AOP 208', 'Nabavna vrednost prodate robe'),
            ('AOP 209', 'Troškovi materijala'),
            ('AOP 210', 'Troškovi zarada, naknada zarada i ostali lični rashodi'),
            ('AOP 211', 'Troškovi amortizacije i rezervisanja'),
            ('AOP 212', 'Ostali poslovni rashodi'),
        ],
        default='AOP 208')

    other_categories = SelectMultipleField('Ostali',
        choices=[
            ('AOP 216', 'Finansijski rashodi'),
            ('AOP 218', 'Ostali rashodi'),
            ('AOP 229', 'Neto dobitak'),
            ('AOP 230', 'Neto gubitak')
        ],
        default='AOP 208')

    municipalities = SelectMultipleField('Opštine')
    activities = SelectMultipleField('Delatnosti')
    companies = SelectMultipleField('Preduzeća')
    #cities = SelectMultipleField('Grad')
    #company_sizes = SelectMultipleField('Company sizes')
    owners = SelectMultipleField('Osnivači')
    years = SelectMultipleField('Godine')

    def __init__(self, default_chart_type, *args, **kwargs):
        self.chart_type.kwargs['default'] = default_chart_type

        # Municipalities filter options.
        self.municipalities.kwargs['choices'] = [
            (
                municipality,
                municipality
            )
            for municipality in FormUtils.get_municipalities()
        ]

        # Companies filter options.
        self.activities.kwargs['choices'] = [
            (
                int(activity_id),
                activity_name
            )
            for activity_id, activity_name in FormUtils.get_activities().iteritems()
        ]

        # Companies filter options.
        self.companies.kwargs['choices'] = [
            (
                int(company_id),
                company_name
            )
            for company_id, company_name in FormUtils.get_companies().iteritems()
        ]

        # Owners filter options.
        self.owners.kwargs['choices'] = [
            (
                owner.decode('utf-8'),
                owner.decode('utf-8')
            )
            for owner in FormUtils.get_owners()
        ]

        # Years filter options.
        self.years.kwargs['choices'] = [
            (
                year,
                year
            )
            for year in FormUtils.get_years()
        ]


        Form.__init__(self, *args, **kwargs)
