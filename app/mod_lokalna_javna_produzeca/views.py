# coding=utf-8
from flask import Blueprint, render_template, request
from urlparse import urlparse

from forms.barchart_ljp_form import BarchartLJPForm
from forms.map_ljp_form import MapLJPForm
from forms.scatterplot_ljp_form import ScatterplotLJPForm
from forms.floating_bubbles_ljp_form import FloatingBubblesLJPForm
from urlparse import parse_qsl
from bson import json_util
from app import data_manager_ljp
from app.utils.url_query_utils import UrlQueryUtils
from app.mod_lokalna_javna_produzeca.forms.utils import FormUtils

mod_lokalna_javna_produzeca = Blueprint('lokalna_javna_produzeca', __name__, url_prefix='/lokalna-javna-produzeca')

@mod_lokalna_javna_produzeca.route('/', methods=['GET'])
def index():

    data = None
    chart_type = None
    selected_municipalities = None
    selected_years = None
    selected_companies = None
    selected_activities = None
    selected_owners = None
    data_series = None
    x_series = None
    y_series = None
    categories_y = None
    categories_x = None
    categories = None
    disaggregated_by = None
    group_by_bubble = None
    selected_labels = None
    ukupni_rashodi_categories = None
    poslovni_rashodi_categories = None
    ostali_categories = None
    all_activities = None
    all_companies = None
    all_municipalities = None
    all_owners = None

    barchart_form = BarchartLJPForm()
    map_form = MapLJPForm()
    scatterplot_form = ScatterplotLJPForm()
    floating_bubbles_form = FloatingBubblesLJPForm()

    tree_years = FormUtils.get_tree_years()
    tree_municipalities = FormUtils.get_tree_municipalities()
    tree_companies = FormUtils.get_tree_companies()
    tree_owners = FormUtils.get_tree_owners()
    tree_activities = FormUtils.get_tree_activities()

    url_query = urlparse(request.url).query

    if url_query != '':

        queries = parse_qsl(url_query)
        queries_dict = {}

        # Transform the list of tuples into a dictionary.
        # We do this so we can elegantly handle optional parameters (descriptionRevenues and descriptionExpenditures).
        for query in queries:
            queries_dict[query[0]] = query[1]
        query_params = UrlQueryUtils.build_ljp_query_params_json_from_url_query(queries_dict)

        if "municipalities" in queries_dict and queries_dict['municipalities'] == "Ukupno":
            all_municipalities = True
        if "activities" in queries_dict and queries_dict['activities'] == "Ukupno":
            all_activities = True
        if "companies" in queries_dict and queries_dict['companies'] == "Ukupno":
            all_companies = True
        if "owners" in queries_dict and queries_dict['owners'] == "Ukupno":
            all_owners = True

        chart_type = query_params['tipGrafikona']
        data = data_manager_ljp.get_chart_data(query_params)
        selected_municipalities = query_params['filteri']['opstine']
        selected_years = query_params['filteri']['godine']
        selected_companies = query_params['filteri']['preduzeca']
        selected_activities = query_params['filteri']['delatnosti']
        selected_owners = query_params['filteri']['osnivaci']

        if chart_type == "Scatterplot":
            data_series = query_params['podaci']['series']
            scatterplot_form.x_axis.data = query_params['podaci']['x']['selected']
            scatterplot_form.y_axis.data = query_params['podaci']['y']['selected']
            y_series = query_params['podaci']['y']['selected']
            x_series = query_params['podaci']['x']['selected']
            categories_x = query_params['podaci']['x']['params']
            categories_y = query_params['podaci']['y']['params']

        if chart_type == "Map":
            map_form.map_data_series.data = query_params['podaci']['selected']
            data_series = query_params['podaci']['selected']
            categories = json_util.dumps(query_params['podaci']['params'])

        if chart_type == "Barchart":
            data_series = [query_params['podaci']['ukupniRashodi']['selected'], query_params['podaci']['poslovniRashodi']['selected'], query_params['podaci']['ostali']['selected']]

            barchart_form.group_by.data = query_params['podaci']['grupisanoPo']

            if query_params['podaci']['ukupniPrihodi'] is True:
                barchart_form.data_series_ukupni_prihodi.data = query_params['podaci']['ukupniPrihodi']

            if query_params['podaci']['ukupniRashodi']['selected'] is True:
                barchart_form.data_series_ukupni_rashodi.data = query_params['podaci']['ukupniRashodi']['selected']
                ukupni_rashodi_categories = json_util.dumps(query_params['podaci']['ukupniRashodi']["params"])

            if query_params['podaci']['poslovniRashodi']['selected'] is True:
                barchart_form.data_series_poslovni_rashodi.data = query_params['podaci']['poslovniRashodi']['selected']
                poslovni_rashodi_categories = json_util.dumps(query_params['podaci']['poslovniRashodi']["params"])

            if query_params['podaci']['ostali']['selected'] is True:
                barchart_form.data_series_others.data = query_params['podaci']['ostali']['selected']
                ostali_categories = json_util.dumps(query_params['podaci']['ostali']['params'])


        if chart_type == 'Floating Bubbles':
            disaggregated_by = query_params['podaci']['disaggregateBy']
            group_by_bubble = query_params['podaci']['grupisanoPo']
            floating_bubbles_form.data_series.data = query_params['podaci']['selected']
            if disaggregated_by == 'Godina':
                selected_labels = selected_years

            elif disaggregated_by == 'Preduzeća':
                selected_labels = selected_companies

            elif disaggregated_by == 'Opština':
                selected_labels = selected_municipalities

            elif disaggregated_by == 'Delatnosti':
                selected_labels = selected_activities

            elif disaggregated_by == 'Osnivači':
                selected_labels = selected_owners

    return render_template(
        'mod_lokalna_javna_produzeca/index.html',
        chart_type=chart_type,
        data=json_util.dumps(data),
        barchart_form=barchart_form,
        map_form=map_form,
        scatterplot_form=scatterplot_form,
        floating_bubbles_form=floating_bubbles_form,
        tree_years=json_util.dumps(tree_years),
        tree_municipalities=json_util.dumps(tree_municipalities),
        tree_activities=json_util.dumps(tree_activities),
        tree_companies=json_util.dumps(tree_companies),
        tree_owners=json_util.dumps(tree_owners),
        selected_municipalities=json_util.dumps(selected_municipalities),
        selected_years=json_util.dumps(selected_years),
        selected_companies=json_util.dumps(selected_companies),
        selected_activities=json_util.dumps(selected_activities),
        selected_owners=json_util.dumps(selected_owners),
        data_series=json_util.dumps(data_series),
        y_series=y_series,
        x_series=x_series,
        categories_x=json_util.dumps(categories_x),
        categories_y=json_util.dumps(categories_y),
        categories=categories,
        disaggregated_by=disaggregated_by,
        group_by_bubble=group_by_bubble,
        selected_labels=json_util.dumps(selected_labels),
        ukupni_rashodi_categories=ukupni_rashodi_categories,
        poslovni_rashodi_categories=poslovni_rashodi_categories,
        ostali_categories=ostali_categories,
        all_activities=all_activities,
        all_companies=all_companies,
        all_municipalities=all_municipalities,
        all_owners=all_owners
    )
