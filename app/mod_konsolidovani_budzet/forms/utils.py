# coding=utf-8
from collections import OrderedDict

class FormUtils():

    @staticmethod
    def get_years():
        years = [2006,2007,2008,2009,2010]


        return years



    @staticmethod
    def get_tree_years():
        years = [{
                'id':'year',
                'text': "Godine",
                'type':"folder",
                'parent': "#"
            },
            {
                'id':'2006',
                'text': "2006",
                'parent': "year",
                'type':"file"
            },
            {
                'id':'2007',
                'text': "2007",
                'parent': "year",
                'type':"file"
            },
            {
                'id':'2008',
                'text': "2008",
                'parent': "year",
                'type':"file"
            },
            {
                'id':'2009',
                'text': "2009",
                'parent': "year",
                'type':"file"
            },
            {
                'id':'2010',
                'text': "2010",
                'parent': "year",
                'type':"file"
            }
        ]


        return years

    @staticmethod
    def get_tree_municipalities():
        municipalities = [
            {
                'id':'municipality',
                'text': "Opštine",
                'type':"folder",
                'parent': "#"
            },
            {
                'id':'Ada',
                'text': "Ada",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Aleksandrovac',
                'text': "Aleksandrovac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Aleksinac',
                'text': "Aleksinac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Alibunar',
                'text': "Alibunar",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Apatin',
                'text': "Apatin",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Aranđelovac',
                'text': "Aranđelovac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Arilje',
                'text': "Arilje",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Babušnica',
                'text': "Babušnica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bajina Bašta',
                'text': "Bajina Bašta",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Batočina',
                'text': "Batočina",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bač',
                'text': "Bač",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bačka Palanka',
                'text': "Bačka Palanka",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bačka Topola',
                'text': "Bačka Topola",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bački Petrovac',
                'text': "Bački Petrovac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bela Crkva',
                'text': "Bela Crkva",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bela Palanka',
                'text': "Bela Palanka",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Belgrade',
                'text': "Belgrade",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Beočin',
                'text': "Beočin",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bečej',
                'text': "Bečej",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Blace',
                'text': "Blace",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bogatić',
                'text': "Bogatić",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bojnik',
                'text': "Bojnik",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Boljevac',
                'text': "Boljevac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bor',
                'text': "Bor",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bosilegrad',
                'text': "Bosilegrad",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Brus',
                'text': "Brus",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Bujanovac',
                'text': "Bujanovac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Crna Trava',
                'text': "Crna Trava",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Despotovac',
                'text': "Despotovac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Dimitrovgrad',
                'text': "Dimitrovgrad",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Doljevac',
                'text': "Doljevac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Gadžin Han',
                'text': "Gadžin Han",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Golubac',
                'text': "Golubac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Gornji Milanovac',
                'text': "Gornji Milanovac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Inđija',
                'text': "Inđija",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Irig',
                'text': "Irig",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Ivanjica',
                'text': "Ivanjica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Jagodina',
                'text': "Jagodina",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kanjiža',
                'text': "Kanjiža",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kikinda',
                'text': "Kikinda",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kladovo',
                'text': "Kladovo",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Knić',
                'text': "Knić",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Knjaževac',
                'text': "Knjaževac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Koceljeva',
                'text': "Koceljeva",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kosjerić',
                'text': "Kosjerić",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kosovska Mitrovica',
                'text': "Kosovska Mitrovica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kovačica',
                'text': "Kovačica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kovin',
                'text': "Kovin",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kragujevac',
                'text': "Kragujevac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kraljevo',
                'text': "Kraljevo",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Krupanj',
                'text': "Krupanj",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kruševac',
                'text': "Kruševac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kula',
                'text': "Kula",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kuršumlija',
                'text': "Kuršumlija",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Kučevo',
                'text': "Kučevo",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Lajkovac',
                'text': "Lajkovac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Lapovo',
                'text': "Lapovo",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Lebane',
                'text': "Lebane",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Leposavić',
                'text': "Leposavić",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Leskovac',
                'text': "Leskovac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Ljig',
                'text': "Ljig",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Ljubovija',
                'text': "Ljubovija",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Loznica',
                'text': "Loznica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Lučani',
                'text': "Lučani",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Majdanpek',
                'text': "Majdanpek",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Mali Iđoš',
                'text': "Mali Iđoš",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Mali Zvornik',
                'text': "Mali Zvornik",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Malo Crniće',
                'text': "Malo Crniće",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Medveđa',
                'text': "Medveđa",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Merošina',
                'text': "Merošina",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Mionica',
                'text': "Mionica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Negotin',
                'text': "Negotin",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Niš',
                'text': "Niš",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Nova Crnja',
                'text': "Nova Crnja",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Novi Bečej',
                'text': "Novi Bečej",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Novi Kneževac',
                'text': "Novi Kneževac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Novi Pazar',
                'text': "Novi Pazar",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Novi Sad',
                'text': "Novi Sad",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Odžaci',
                'text': "Odžaci",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Opovo',
                'text': "Opovo",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Osečina',
                'text': "Osečina",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Pančevo',
                'text': "Pančevo",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Paraćin',
                'text': "Paraćin",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Petrovac',
                'text': "Petrovac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Petrovac na Mlavi',
                'text': "Petrovac na Mlavi",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Pećinci',
                'text': "Pećinci",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Pirot',
                'text': "Pirot",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Plandište',
                'text': "Plandište",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Požarevac',
                'text': "Požarevac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Požega',
                'text': "Požega",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Preševo',
                'text': "Preševo",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Priboj',
                'text': "Priboj",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Prijepolje',
                'text': "Prijepolje",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Prokuplje',
                'text': "Prokuplje",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Rača',
                'text': "Rača",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Raška',
                'text': "Raška",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Ražanj',
                'text': "Ražanj",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Rekovac',
                'text': "Rekovac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Ruma',
                'text': "Ruma",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Senta',
                'text': "Senta",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Sečanj',
                'text': "Sečanj",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Sjenica',
                'text': "Sjenica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Smederevo',
                'text': "Smederevo",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Smederevska Palanka',
                'text': "Smederevska Palanka",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Sokobanja',
                'text': "Sokobanja",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Sombor',
                'text': "Sombor",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Srbobran',
                'text': "Srbobran",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Sremska Mitrovica',
                'text': "Sremska Mitrovica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Sremski Karlovci',
                'text': "Sremski Karlovci",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Stara Pazova',
                'text': "Stara Pazova",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Subotica',
                'text': "Subotica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Surdulica',
                'text': "Surdulica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Svilajnac',
                'text': "Svilajnac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Svrljig',
                'text': "Svrljig",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Temerin',
                'text': "Temerin",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Titel',
                'text': "Titel",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Topola',
                'text': "Topola",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Trgovište',
                'text': "Trgovište",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Trstenik',
                'text': "Trstenik",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Tutin',
                'text': "Tutin",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Ub',
                'text': "Ub",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Užice',
                'text': "Užice",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Valjevo',
                'text': "Valjevo",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Varvarin',
                'text': "Varvarin",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Velika Plana',
                'text': "Velika Plana",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Veliko Gradište',
                'text': "Veliko Gradište",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Vladimirci',
                'text': "Vladimirci",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Vladimirovtsi',
                'text': "Vladimirovtsi",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Vladičin Han',
                'text': "Vladičin Han",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Vlasotince',
                'text': "Vlasotince",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Vojvodina',
                'text': "Vojvodina",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Vranje',
                'text': "Vranje",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Vrbas',
                'text': "Vrbas",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Vrnjačka Banja',
                'text': "Vrnjačka Banja",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Vršac',
                'text': "Vršac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Zaječar',
                'text': "Zaječar",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Zrenjanin',
                'text': "Zrenjanin",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Zvečan',
                'text': "Zvečan",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Ćićevac',
                'text': "Ćićevac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Ćuprija',
                'text': "Ćuprija",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Čajetina',
                'text': "Čajetina",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Čačak',
                'text': "Čačak",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Čoka',
                'text': "Čoka",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Šabac',
                'text': "Šabac",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Šid',
                'text': "Šid",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Žabalj',
                'text': "Žabalj",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Žabari',
                'text': "Žabari",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Žagubica',
                'text': "Žagubica",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Žitište',
                'text': "Žitište",
                'parent': "municipality",
                'type':"file"
            },
            {
                'id':'Žitorađa',
                'text': "Žitorađa",
                'parent': "municipality",
                'type':"file"
            }]

        return municipalities

    @staticmethod
    def get_municipalities():

        municipalities = [
            "Ada",
            "Aleksandrovac",
            "Aleksinac",
            "Alibunar",
            "Apatin",
            "Aranđelovac",
            "Arilje",
            "Babušnica",
            "Bajina Bašta",
            "Batočina",
            "Bač",
            "Bačka Palanka",
            "Bačka Topola",
            "Bački Petrovac",
            "Bela Crkva",
            "Bela Palanka",
            "Belgrade",
            "Beočin",
            "Bečej",
            "Blace",
            "Bogatić",
            "Bojnik",
            "Boljevac",
            "Bor",
            "Bosilegrad",
            "Brus",
            "Bujanovac",
            "Crna Trava",
            "Despotovac",
            "Dimitrovgrad",
            "Doljevac",
            "Gadžin Han",
            "Golubac",
            "Gornji Milanovac",
            "Inđija",
            "Irig",
            "Ivanjica",
            "Jagodina",
            "Kanjiža",
            "Kikinda",
            "Kladovo",
            "Knić",
            "Knjaževac",
            "Koceljeva",
            "Kosjerić",
            "Kosovska Mitrovica",
            "Kovačica",
            "Kovin",
            "Kragujevac",
            "Kraljevo",
            "Krupanj",
            "Kruševac",
            "Kula",
            "Kuršumlija",
            "Kučevo",
            "Lajkovac",
            "Lapovo",
            "Lebane",
            "Leposavić",
            "Leskovac",
            "Ljig",
            "Ljubovija",
            "Loznica",
            "Lučani",
            "Majdanpek",
            "Mali Iđoš",
            "Mali Zvornik",
            "Malo Crniće",
            "Medveđa",
            "Merošina",
            "Mionica",
            "Negotin",
            "Niš",
            "Nova Crnja",
            "Nova Varoš",
            "Novi Bečej",
            "Novi Kneževac",
            "Novi Pazar",
            "Novi Sad",
            "Odžaci",
            "Opovo",
            "Osečina",
            "Pančevo",
            "Paraćin",
            "Petrovac",
            "Petrovac na Mlavi",
            "Pećinci",
            "Pirot",
            "Plandište",
            "Požarevac",
            "Požega",
            "Preševo",
            "Priboj",
            "Prijepolje",
            "Prokuplje",
            "Rača",
            "Raška",
            "Ražanj",
            "Rekovac",
            "Ruma",
            "Senta",
            "Sečanj",
            "Sjenica",
            "Smederevo",
            "Smederevska Palanka",
            "Sokobanja",
            "Sombor",
            "Srbobran",
            "Sremska Mitrovica",
            "Sremski Karlovci",
            "Stara Pazova",
            "Subotica",
            "Surdulica",
            "Svilajnac",
            "Svrljig",
            "Temerin",
            "Titel",
            "Topola",
            "Trgovište",
            "Trstenik",
            "Tutin",
            "Ub",
            "Užice",
            "Valjevo",
            "Varvarin",
            "Velika Plana",
            "Veliko Gradište",
            "Vladimirci",
            "Vladimirovtsi",
            "Vladičin Han",
            "Vlasotince",
            "Vojvodina",
            "Vranje",
            "Vrbas",
            "Vrnjačka Banja",
            "Vršac",
            "Zaječar",
            "Zrenjanin",
            "Zvečan",
            "Ćićevac",
            "Ćuprija",
            "Čajetina",
            "Čačak",
            "Čoka",
            "Šabac",
            "Šid",
            "Žabalj",
            "Žabari",
            "Žagubica",
            "Žitište",
            "Žitorađa"

        ]

        return municipalities


    @staticmethod
    def get_income_types():
        income_types = {
            "Република": "Republika",
            "Аутономна покрајина": "Autonomna pokrajina",
            "Општина града": "Opstina grada",
            "ООСО": "OOCO",
            "Донације": "Donacije",
            "Остали извори": "Ostali izvori"
        }

        return income_types

    @staticmethod
    def get_tree_income_type():
        tree_income_type = [{
                'id':'izvori-prihoda',
                'text': "Izvori prihoda",
                'type':"folder",
                'parent': "#"
            },
            {
                'id':'Republika',
                'text': "Republika",
                'parent': "izvori-prihoda",
                'type':"file"
            },
            {
                'id':'Autonomna pokrajina',
                'text': "Autonomna pokrajina",
                'parent': "izvori-prihoda",
                'type':"file"
            },
            {
                'id':'Opstina grada',
                'text': "Opstina grada",
                'parent': "izvori-prihoda",
                'type':"file"
            },
            {
                'id':'OOCO',
                'text': "OOCO",
                'parent': "izvori-prihoda",
                'type':"file"
            },
            {
                'id':'Donacije',
                'text': "Donacije",
                'parent': "izvori-prihoda",
                'type':"file"
            },
            {
                'id':'Ostali izvori',
                'text': "Ostali izvori",
                'parent': "izvori-prihoda",
                'type':"file"
            }
        ]


        return tree_income_type
    @staticmethod
    def get_revenue_descriptions():

        revenue_descriptions = OrderedDict([
            ('711', 'ПОРЕЗ НА ДОХОДАК, ДОБИТ И КАПИТАЛНЕ ДОБИТКЕ'),
            ('712', 'ПОРЕЗ НА ФОНД ЗАРАДА (5009)'),
            ('713', 'ПОРЕЗ НА ИМОВИНУ (од 5011 до 5016)'),
            ('714', 'ПОРЕЗ НА ДОБРА И УСЛУГЕ (од 5018 до 5022)'),
            ('715', 'ПОРЕЗ НА МЕЂУНАРОДНУ ТРГОВИНУ И ТРАНСАКЦИЈЕ (од 5024 до 5029)'),
            ('716', 'ДРУГИ ПОРЕЗИ (5031 + 5032)'),
            ('717', 'АКЦИЗЕ (од 5034 до 5039)'),
            ('719', 'ЈЕДНОКРАТНИ ПОРЕЗ НА ЕКСТРА ПРОФИТ И ЕКСТРА ИМОВИНУ СТЕЧЕНУ КОРИШЋЕЊЕМ ПОСЕБНИХ ПОГОДНОСТИ (од 5041до 5046)'),
            ('721', 'ДОПРИНОСИ ЗА СОЦИЈАЛНО ОСИГУРАЊЕ (од 5049 до 5052)'),
            ('722', 'ОСТАЛИ СОЦИЈАЛНИ ДОПРИНОСИ (од 5054 до 5056)'),
            ('731', 'ДОНАЦИЈЕ ОД ИНОСТРАНИХ ДРЖАВА (5059 + 5060)'),
            ('732', 'ДОНАЦИЈЕ ОД МЕЂУНАРОДНИХ ОРГАНИЗАЦИЈА (5062 + 5063)'),
            ('733', 'ТРАНСФЕРИ ОД ДРУГИХ НИВОА ВЛАСТИ (5065 + 5066)'),
            ('741', 'ПРИХОДИ ОД ИМОВИНЕ (од 5069 до 5074)'),
            ('742', 'ПРИХОДИ ОД ПРОДАЈЕ ДОБАРА И УСЛУГА (од 5076 до 5079)'),
            ('743', 'НОВЧАНЕ КАЗНЕ И ОДУЗЕТА ИМОВИНСКА КОРИСТ (од 5081 до 5086)'),
            ('744', 'ДОБРОВОЉНИ ТРАНСФЕРИ ОД ФИЗИЧКИХ И ПРАВНИХ ЛИЦА (5088 + 5089)'),
            ('745', 'МЕШОВИТИ И НЕОДРЕЂЕНИ ПРИХОДИ (5091)'),
            ('771', 'МЕМОРАНДУМСКЕ СТАВКЕ ЗА РЕФУНДАЦИЈУ РАСХОДА (5094)'),
            ('772', 'МЕМОРАНДУМСКЕ СТАВКЕ ЗА РЕФУНДАЦИЈУ РАСХОДА ИЗ ПРЕТХОДНЕ ГОДИНЕ (5096)'),
            ('781', 'ТРАНСФЕРИ ИЗМЕЂУ БУЏЕТСКИХ КОРИСНИКА НА ИСТОМ НИВОУ (5099 + 5100)'),
            ('791', 'ПРИХОДИ ИЗ БУЏЕТА (5103)'),
            ('811', 'ПРИМАЊА ОД ПРОДАЈЕ НЕПОКРЕТНОСТИ (5107)'),
            ('812', 'ПРИМАЊА ОД ПРОДАЈЕ ПОКРЕТНЕ ИМОВИНЕ (5109)'),
            ('813', 'ПРИМАЊА ОД ПРОДАЈЕ ОСТАЛИХ ОСНОВНИХ СРЕДСТАВА (5111)'),
            ('821', 'ПРИМАЊА ОД ПРОДАЈЕ РОБНИХ РЕЗЕРВИ (5114)'),
            ('822', 'ПРИМАЊА ОД ПРОДАЈЕ ЗАЛИХА  ПРОИЗВОДЊЕ (5116)'),
            ('823', 'ПРИМАЊА ОД ПРОДАЈЕ РОБЕ ЗА ДАЉУ ПРОДАЈУ (5118)'),
            ('831', 'ПРИМАЊА ОД ПРОДАЈЕ ДРАГОЦЕНОСТИ (5121)'),
            ('841', 'ПРИМАЊА ОД ПРОДАЈЕ ЗЕМЉИШТА (5124)'),
            ('842', 'ПРИМАЊА ОД ПРОДАЈЕ ПОДЗЕМНИХ БЛАГА (5126)'),
            ('843', 'ПРИМАЊА ОД ПРОДАЈЕ ШУМА И ВОДА (5128)'),
            ('911', 'ПРИМАЊА ОД ДОМАЋИХ ЗАДУЖИВАЊА (од 5132 до 5140)'),
            ('912', 'ПРИМАЊА ОД ИНОСТРАНОГ ЗАДУЖИВАЊА (од 5142 до 5148)'),
            ('913', 'ПРИМАЊА ПО ОСНОВУ ГАРАНЦИЈА'),
            ('921', 'ПРИМАЊА ОД ПРОДАЈЕ ДОМАЋЕ ФИНАНСИЈСКЕ ИМОВИНЕ (од 5151 до 5159)'),
            ('922', 'ПРИМАЊА ОД ПРОДАЈЕ СТРАНЕ ФИНАНСИЈСКЕ ИМОВИНЕ (од 5161 до 5168)'),
        ])

        return revenue_descriptions


    @staticmethod
    def get_tree_revenue_descriptions():
        revenue_descriptions = [{
                'id':'revenue_descriptions',
                'text': "Opis prihodi",
                'type':"folder",
                'parent': "#"
            },
            {
                'id':'711',
                'text': "POREZ NA DOHODAK, DOBIT I KAPITALNE DOBITKE",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'712',
                'text': "POREZ NA FOND ZARADA (5009)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'713',
                'text': "POREZ NA IMOVINU (od 5011 do 5016)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'714',
                'text': "POREZ NA DOBRA I USLUGE (od 5018 do 5022)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'715',
                'text': "POREZ NA MEĐUNARODNU TRGOVINU I TRANSAKCIJE (od 5024 do 5029)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'716',
                'text': "DRUGI POREZI (5031 + 5032)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'717',
                'text': "AKCIZE (od 5034 do 5039)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'719',
                'text': "JEDNOKRATNI POREZ NA EKSTRA PROFIT I EKSTRA IMOVINU STEČENU KORIŠĆENjEM POSEBNIH POGODNOSTI (od 5041do 5046)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'721',
                'text': "DOPRINOSI ZA SOCIJALNO OSIGURANjE (od 5049 do 5052)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'722',
                'text': "OSTALI SOCIJALNI DOPRINOSI (od 5054 do 5056)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'731',
                'text': "DONACIJE OD INOSTRANIH DRŽAVA (5059 + 5060)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'732',
                'text': "DONACIJE OD MEĐUNARODNIH ORGANIZACIJA (5062 + 5063)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'733',
                'text': "TRANSFERI OD DRUGIH NIVOA VLASTI (5065 + 5066)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'741',
                'text': "PRIHODI OD IMOVINE (od 5069 do 5074)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'742',
                'text': "PRIHODI OD PRODAJE DOBARA I USLUGA (od 5076 do 5079)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'743',
                'text': "NOVČANE KAZNE I ODUZETA IMOVINSKA KORIST (od 5081 do 5086)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'744',
                'text': "DOBROVOLjNI TRANSFERI OD FIZIČKIH I PRAVNIH LICA (5088 + 5089)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'745',
                'text': "MEŠOVITI I NEODREĐENI PRIHODI (5091)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'771',
                'text': "MEMORANDUMSKE STAVKE ZA REFUNDACIJU RASHODA (5094)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'772',
                'text': "MEMORANDUMSKE STAVKE ZA REFUNDACIJU RASHODA IZ PRETHODNE GODINE (5096)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'781',
                'text': "TRANSFERI IZMEĐU BUDžETSKIH KORISNIKA NA ISTOM NIVOU (5099 + 5100)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'791',
                'text': "PRIHODI IZ BUDžETA (5103)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'811',
                'text': "PRIMANjA OD PRODAJE NEPOKRETNOSTI (5107)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'812',
                'text': "PRIMANjA OD PRODAJE POKRETNE IMOVINE (5109)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'813',
                'text': "PRIMANjA OD PRODAJE OSTALIH OSNOVNIH SREDSTAVA (5111)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'821',
                'text': "PRIMANjA OD PRODAJE ROBNIH REZERVI (5114)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'822',
                'text': "PRIMANjA OD PRODAJE ZALIHA  PROIZVODNjE (5116)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'823',
                'text': "PRIMANjA OD PRODAJE ROBE ZA DALjU PRODAJU (5118",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'831',
                'text': "PRIMANjA OD PRODAJE DRAGOCENOSTI (5121)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'841',
                'text': "PRIMANjA OD PRODAJE ZEMLjIŠTA (5124)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'842',
                'text': "PRIMANjA OD PRODAJE PODZEMNIH BLAGA (5126)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'843',
                'text': "PRIMANjA OD PRODAJE ŠUMA I VODA (5128)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'911',
                'text': "PRIMANjA OD DOMAĆIH ZADUŽIVANjA (od 5132 do 5140)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'912',
                'text': "PRIMANjA OD INOSTRANOG ZADUŽIVANjA (od 5142 do 5148)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'913',
                'text': "PRIMANjA PO OSNOVU GARANCIJA",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'921',
                'text': "PRIMANjA OD PRODAJE DOMAĆE FINANSIJSKE IMOVINE (od 5151 do 5159)",
                'parent': "revenue_descriptions",
                'type':"file"
            },
            {
                'id':'922',
                'text': "PRIMANjA OD PRODAJE STRANE FINANSIJSKE IMOVINE (od 5161 do 5168)",
                'parent': "revenue_descriptions",
                'type':"file"
            }
        ]

        return revenue_descriptions

    @staticmethod
    def get_expenditure_descriptions():

        expenditure_descriptions = OrderedDict([
            ('411', 'ПЛАТЕ, ДОДАЦИ И НАКНАДЕ ЗАПОСЛЕНИХ (ЗАРАДЕ) (5174)'),
            ('412', 'СОЦИЈАЛНИ ДОПРИНОСИ НА ТЕРЕТ ПОСЛОДАВЦА (од 5176 до 5178)'),
            ('413', 'НАКНАДЕ У НАТУРИ (5180)'),
            ('414', 'СОЦИЈАЛНА ДАВАЊА ЗАПОСЛЕНИМА (од 5182 до 5185)'),
            ('415', 'НАКНАДА ТРОШКОВА ЗА ЗАПОСЛЕНЕ (5187)'),
            ('416', 'НАГРАДЕ ЗАПОСЛЕНИМА И ОСТАЛИ ПОСЕБНИ РАСХОДИ (5189)'),
            ('417', 'ПОСЛАНИЧКИ ДОДАТАК (5191)'),
            ('418', 'СУДИЈСКИ ДОДАТАК (5193)'),
            ('421', 'СТАЛНИ ТРОШКОВИ (од 5196 до 5202)'),
            ('422', 'ТРОШКОВИ ПУТОВАЊА (од 5204 до 5208)'),
            ('423', 'УСЛУГЕ ПО УГОВОРУ (од 5210 до 5217)'),
            ('424', 'СПЕЦИЈАЛИЗОВАНЕ УСЛУГЕ (од 5219 до 5225)'),
            ('425', 'ТЕКУЋЕ ПОПРАВКЕ И ОДРЖАВАЊЕ (5227 + 5228)'),
            ('426', 'МАТЕРИЈАЛ (од 5230 до 5238)'),
            ('431', 'АМОРТИЗАЦИЈА НЕКРЕТНИНА И ОПРЕМЕ (од 5241 до 5243)'),
            ('432', 'АМОРТИЗАЦИЈА КУЛТИВИСАНЕ ИМОВИНЕ (5245)'),
            ('433', 'УПОТРЕБА ДРАГОЦЕНОСТИ (5247)'),
            ('434', 'УПОТРЕБА ПРИРОДНЕ ИМОВИНЕ (од 5249 до 5251)'),
            ('435', 'АМОРТИЗАЦИЈА НЕМАТЕРИЈАЛНЕ ИМОВИНЕ (5253)'),
            ('441', 'ОТПЛАТЕ ДОМАЋИХ КАМАТА (од 5256 до 5264)'),
            ('442', 'ОТПЛАТА СТРАНИХ КАМАТА (од 5266 до 5271)'),
            ('443', 'ОТПЛАТА КАМАТА ПО ГАРАНЦИЈАМА (5273)'),
            ('444', 'ПРАТЕЋИ ТРОШКОВИ ЗАДУЖИВАЊА (од 5275 до 5277)'),
            ('451', 'СУБВЕНЦИЈЕ ЈАВНИМ НЕФИНАНСИЈСКИМ ПРЕДУЗЕЋИМА И ОРГАНИЗАЦИЈАМА (5280 + 5281)'),
            ('452', 'СУБВЕНЦИЈЕ ПРИВАТНИМ ФИНАНСИЈСКИМ ИНСТИТУЦИЈАМА (5283 + 5284)'),
            ('453', 'СУБВЕНЦИЈЕ ЈАВНИМ ФИНАНСИЈСКИМ ИНСТИТУЦИЈАМА (5286 + 5287)'),
            ('454', 'СУБВЕНЦИЈЕ ПРИВАТНИМ ПРЕДУЗЕЋИМА (5289 + 5290)'),
            ('461', 'ДОНАЦИЈЕ СТРАНИМ ВЛАДАМА (5293 + 5294)'),
            ('462', 'ДОТАЦИЈЕ МЕЂУНАРОДНИМ ОРГАНИЗАЦИЈАМА (5296 + 5297)'),
            ('463', 'ТРАНСФЕРИ ОСТАЛИМ НИВОИМА ВЛАСТИ (5299 + 5300)'),
            ('464', 'ДОТАЦИЈЕ ОРГАНИЗАЦИЈАМА ОБАВЕЗНОГ СОЦИЈАЛНОГ ОСИГУРАЊА (5302 + 5303)'),
            ('465', 'ОСТАЛЕ ДОТАЦИЈЕ И ТРАНСФЕРИ (5305 + 5306)'),
            ('471', 'ПРАВА ИЗ СОЦИЈАЛНОГ ОСИГУРАЊА (ОРГАНИЗАЦИЈЕ ОБАВЕЗНОГ СОЦИЈАЛНОГ ОСИГУРАЊА) (од 5309 до 5311)'),
            ('472', 'НАКНАДЕ ЗА СОЦИЈАЛНУ ЗАШТИТУ ИЗ БУЏЕТА (од 5313 до 5321)'),
            ('481', 'ДОТАЦИЈЕ НЕВЛАДИНИМ ОРГАНИЗАЦИЈАМА (5324 + 5325)'),
            ('482', 'ПОРЕЗИ, ОБАВЕЗНЕ ТАКСЕ И КАЗНЕ (од 5327 до 5329)'),
            ('483', 'НОВЧАНЕ КАЗНЕ И ПЕНАЛИ ПО РЕШЕЊУ СУДОВА (5331)'),
            ('484', 'НАКНАДА ШТЕТЕ ЗА ПОВРЕДЕ ИЛИ ШТЕТУ НАСТАЛУ УСЛЕД ЕЛЕМЕНТАРНИХ НЕПОГОДА ИЛИ ДРУГИХ ПРИРОДНИХ УЗРОКА (5333 + 5334)'),
            ('485', 'НАКНАДА ШТЕТЕ ЗА ПОВРЕДЕ ИЛИ ШТЕТУ НАНЕТУ ОД СТРАНЕ ДРЖАВНИХ ОРГАНА (5336)'),
            ('489', 'РАСХОДИ КОЈИ СЕ ФИНАНСИРАЈУ ИЗ СРЕДСТАВА ЗА РЕАЛИЗАЦИЈУ НАЦИОНАЛНОГ ИНВЕСТИЦИОНОГ ПЛАНА (5338)'),
            ('499', 'Средства резерве'),
            ('511', 'ЗГРАДЕ И ГРАЂЕВИНСКИ ОБЈЕКТИ (од 5342 до 5345)'),
            ('512', 'МАШИНЕ И ОПРЕМА (од 5347 до 5355)'),
            ('513', 'ОСТАЛЕ НЕКРЕТНИНЕ И ОПРЕМА (5357)'),
            ('514', 'КУЛТИВИСАНА ИМОВИНА (5359)'),
            ('515', 'НЕМАТЕРИЈАЛНА ИМОВИНА (5361)'),
            ('521', 'РОБНЕ РЕЗЕРВЕ (5364)'),
            ('522', 'ЗАЛИХЕ ПРОИЗВОДЊЕ (од 5366 до 5368)'),
            ('523', 'ЗАЛИХЕ РОБЕ ЗА ДАЉУ ПРОДАЈУ (5370)'),
            ('531', 'ДРАГОЦЕНОСТИ (5373)'),
            ('541', 'ЗЕМЉИШТЕ (5376)'),
            ('542', 'РУДНА БОГАТСТВА (5378)'),
            ('543', 'ШУМЕ И ВОДЕ (5380 + 5381)'),
            ('551', 'НЕФИНАНСИЈСКА ИМОВИНА КОЈА СЕ ФИНАНСИРА ИЗ СРЕДСТАВА ЗА РЕАЛИЗАЦИЈУ НАЦИОНАЛНОГ ИНВЕСТИЦИОНОГ ПЛАНА (5384)'),
            ('611', 'ОТПЛАТА ГЛАВНИЦЕ ДОМАЋИМ КРЕДИТОРИМА (од 5388 до 5396)'),
            ('612', 'ОТПЛАТА ГЛАВНИЦЕ СТРАНИМ КРЕДИТОРИМА (од 5398 до 5404)'),
            ('613', 'ОТПЛАТА ГЛАВНИЦЕ ПО ГАРАНЦИЈАМА (5406)'),
            ('614', 'ОТПЛАТА ГЛАВНИЦЕ ЗА ФИНАНСИЈСКИ ЛИЗИНГ (5408)'),
            ('621', 'НАБАВКА ДОМАЋЕ ФИНАНСИЈСКЕ ИМОВИНЕ (од 5411 до 5419)'),
            ('622', 'НАБАВКА СТРАНЕ ФИНАНСИЈСКЕ ИМОВИНЕ (од 5421 до 5428)'),
            ('623', 'НАБАВКА ФИНАНСИЈСКЕ ИМОВИНЕ КОЈА СЕ ФИНАНСИРА ИЗ СРЕДСТАВА ЗА РЕАЛИЗАЦИЈУ НАЦИОНАЛНОГ ИНВЕСТИЦИОНОГ ПЛАНА (5430)'),
        ])

        return expenditure_descriptions

    @staticmethod
    def get_tree_expenditure_descriptions():

        expenditure_descriptions = [{
                'id':'expenditure_descriptions',
                'text': "Opis rashodi",
                'type':"folder",
                'parent': "#"
            },
            {
                'id':'411',
                'text': "PLATE, DODACI I NAKNADE ZAPOSLENIH (ZARADE) (5174)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'412',
                'text': "SOCIJALNI DOPRINOSI NA TERET POSLODAVCA (od 5176 do 5178)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'413',
                'text': "NAKNADE U NATURI (5180)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'414',
                'text': "SOCIJALNA DAVANjA ZAPOSLENIMA (od 5182 do 5185)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'415',
                'text': "NAKNADA TROŠKOVA ZA ZAPOSLENE (5187)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'416',
                'text': "NAGRADE ZAPOSLENIMA I OSTALI POSEBNI RASHODI (5189)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'417',
                'text': "POSLANIČKI DODATAK (5191)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'418',
                'text': "SUDIJSKI DODATAK (5193)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'421',
                'text': "STALNI TROŠKOVI (od 5196 do 5202)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'422',
                'text': "TROŠKOVI PUTOVANjA (od 5204 do 5208)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'423',
                'text': "USLUGE PO UGOVORU (od 5210 do 5217)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'424',
                'text': "SPECIJALIZOVANE USLUGE (od 5219 do 5225)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'425',
                'text': "TEKUĆE POPRAVKE I ODRŽAVANjE (5227 + 5228)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'426',
                'text': "MATERIJAL (od 5230 do 5238)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'431',
                'text': "AMORTIZACIJA NEKRETNINA I OPREME (od 5241 do 5243)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'432',
                'text': "AMORTIZACIJA KULTIVISANE IMOVINE (5245)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'433',
                'text': "UPOTREBA DRAGOCENOSTI (5247)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'434',
                'text': "UPOTREBA PRIRODNE IMOVINE (od 5249 do 5251)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'435',
                'text': "AMORTIZACIJA NEMATERIJALNE IMOVINE (5253)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'441',
                'text': "OTPLATE DOMAĆIH KAMATA (od 5256 do 5264)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'442',
                'text': "OTPLATA STRANIH KAMATA (od 5266 do 5271)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'443',
                'text': "OTPLATA KAMATA PO GARANCIJAMA (5273)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'444',
                'text': "PRATEĆI TROŠKOVI ZADUŽIVANjA (od 5275 do 5277)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'451',
                'text': "SUBVENCIJE JAVNIM NEFINANSIJSKIM PREDUZEĆIMA I ORGANIZACIJAMA (5280 + 5281)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'452',
                'text': "SUBVENCIJE PRIVATNIM FINANSIJSKIM INSTITUCIJAMA (5283 + 5284)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'453',
                'text': "SUBVENCIJE JAVNIM FINANSIJSKIM INSTITUCIJAMA (5286 + 5287)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'454',
                'text': "SUBVENCIJE PRIVATNIM PREDUZEĆIMA (5289 + 5290)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'461',
                'text': "DONACIJE STRANIM VLADAMA (5293 + 5294)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'462',
                'text': "DOTACIJE MEĐUNARODNIM ORGANIZACIJAMA (5296 + 5297)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'463',
                'text': "TRANSFERI OSTALIM NIVOIMA VLASTI (5299 + 5300)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'464',
                'text': "DOTACIJE ORGANIZACIJAMA OBAVEZNOG SOCIJALNOG OSIGURANjA (5302 + 5303)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'465',
                'text': "OSTALE DOTACIJE I TRANSFERI (5305 + 5306)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'471',
                'text': "PRAVA IZ SOCIJALNOG OSIGURANjA (ORGANIZACIJE OBAVEZNOG SOCIJALNOG OSIGURANjA) (od 5309 do 5311)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'472',
                'text': "NAKNADE ZA SOCIJALNU ZAŠTITU IZ BUDžETA (od 5313 do 5321)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'481',
                'text': "DOTACIJE NEVLADINIM ORGANIZACIJAMA (5324 + 5325)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'482',
                'text': "POREZI, OBAVEZNE TAKSE I KAZNE (od 5327 do 5329)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'483',
                'text': "NOVČANE KAZNE I PENALI PO REŠENjU SUDOVA (5331)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'484',
                'text': "NAKNADA ŠTETE ZA POVREDE ILI ŠTETU NASTALU USLED ELEMENTARNIH NEPOGODA ILI DRUGIH PRIRODNIH UZROKA (5333 + 5334)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'485',
                'text': "NAKNADA ŠTETE ZA POVREDE ILI ŠTETU NANETU OD STRANE DRŽAVNIH ORGANA (5336)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'489',
                'text': "RASHODI KOJI SE FINANSIRAJU IZ SREDSTAVA ZA REALIZACIJU NACIONALNOG INVESTICIONOG PLANA (5338)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'499',
                'text': "Sredstva rezerve",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'511',
                'text': "ZGRADE I GRAĐEVINSKI OBJEKTI (od 5342 do 5345)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'512',
                'text': "MAŠINE I OPREMA (od 5347 do 5355)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'513',
                'text': "OSTALE NEKRETNINE I OPREMA (5357)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'514',
                'text': "KULTIVISANA IMOVINA (5359)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'515',
                'text': "NEMATERIJALNA IMOVINA (5361)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'521',
                'text': "ROBNE REZERVE (5364)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'522',
                'text': "ZALIHE PROIZVODNjE (od 5366 do 5368)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'523',
                'text': "ZALIHE ROBE ZA DALjU PRODAJU (5370)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'531',
                'text': "DRAGOCENOSTI (5373)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'541',
                'text': "ZEMLjIŠTE (5376)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'542',
                'text': "RUDNA BOGATSTVA (5378)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'543',
                'text': "ŠUME I VODE (5380 + 5381)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'551',
                'text': "NEFINANSIJSKA IMOVINA KOJA SE FINANSIRA IZ SREDSTAVA ZA REALIZACIJU NACIONALNOG INVESTICIONOG PLANA (5384",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'611',
                'text': "OTPLATA GLAVNICE DOMAĆIM KREDITORIMA (od 5388 do 5396)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'612',
                'text': "OTPLATA GLAVNICE STRANIM KREDITORIMA (od 5398 do 5404)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'613',
                'text': "OTPLATA GLAVNICE PO GARANCIJAMA (5406)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'614',
                'text': "OTPLATA GLAVNICE ZA FINANSIJSKI LIZING (5408)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'621',
                'text': "NABAVKA DOMAĆE FINANSIJSKE IMOVINE (od 5411 do 5419)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'622',
                'text': "NABAVKA STRANE FINANSIJSKE IMOVINE (od 5421 do 5428)",
                'parent': "expenditure_descriptions",
                'type':"file"
            },
            {
                'id':'623',
                'text': "NABAVKA FINANSIJSKE IMOVINE KOJA SE FINANSIRA IZ SREDSTAVA ZA REALIZACIJU NACIONALNOG INVESTICIONOG PLANA (5430)",
                'parent': "expenditure_descriptions",
                'type':"file"
            }]

        return expenditure_descriptions
