# coding=utf-8
from wtforms import Form, SelectField, SelectMultipleField
from utils import FormUtils

class RevenuesAndExpenditureForm(Form):

    visualizations = SelectField('Tip grafikona',
        choices=[
            ('Barchart','Barchart'),
            ('Floating Bubbles','Floating Bubbles'),
            ('Map','Mapa'),
            ('Scatterplot','Scatterplot')
        ],
        default='Barchart')

    fiscal_type_barchart = SelectField('Prihod/Rashod',
        choices=[
            ('Revenue','Prihodi'),
            ('Expenditure','Rashodi')
        ],
        default='Expenditure')

    fiscal_type_map = SelectField('Prihod/Rashod',
        choices=[
            ('Expenditure','Rashodi'),
            ('Revenue','Prihodi')
        ],
        default='Expenditure')

    fiscal_type_floating_bubbles = SelectField('Prihod/Rashod',
        choices=[
            ('Expenditure','Rashodi'),
            ('Revenue','Prihodi')
        ],
        default='Expenditure')

    disaggregate_by = SelectField('Razvrstaj po',
        choices=[
            ('Description','Opis konta'),
            ('Municipality','Opština'),
            ('Year','Godina')
        ],
        default='Year')

    group_by = SelectField('Grupisano po',
        choices=[
            ('Description','Opis konta'),
            ('Income Type','Izvori prihoda/rashoda'),
            ('Municipality','Opština'),
            ('Year','Godina')
        ],
        default='Year')

    floating_bubbles_group_by = SelectField('Grupisano po',
        choices=[
            ('Description','Opis konta'),
            ('Municipality','Opština'),
            ('Year','Godina')
        ],
        default='Year')

    data_series_barchart = SelectField('Skup podataka',
        choices=[
            ('Descriptions', 'Opis konta'),
            ('Income Types', 'Izvori prihoda/rashoda')
        ],
        default='Income Types')

    data_series_map = SelectField('Skup podataka',
        choices=[
            ('Descriptions', 'Opis konta'),
            ('Income Types', 'Izvori prihoda/rashoda')
        ],
        default='Income Types')

    data_series_scatterplot = SelectMultipleField('Skup podataka',
        choices=[
            ('Years', 'Godine'),
            ('Municipalities', 'Opštine'),
            ('Income Types', 'Izvori prihoda/rashoda'),
            #('Descriptions', 'Opis konta')
        ],
        default='Income Types')

    data_series_floating_bubbles = SelectField('Skup podataka',
        choices=[
            ('Descriptions', 'Opis konta'),
            ('Income Types', 'Izvori prihoda/rashoda')
        ],
        default='Income Types')

    municipalities = SelectMultipleField('Opštine')
    revenue_descriptions = SelectMultipleField('Opis prihoda')
    expenditure_descriptions = SelectMultipleField('Opis rashoda')

    income_types = SelectMultipleField('Izvori prihoda')
    # years = SelectMultipleField('Godine')

    def __init__(self, *args, **kwargs):

        # Municipalities filter options.
        self.municipalities.kwargs['choices'] = [
            (
                municipality_latin,
                municipality_latin
            )
            for  municipality_latin in FormUtils.get_municipalities()
        ]

        # Revenues filter options.
        self.revenue_descriptions.kwargs['choices'] = [
            (
                description_id,
                description_val.decode('utf-8')
            )
            for description_id, description_val in FormUtils.get_revenue_descriptions().iteritems()
        ]

        # Expenditure description filter options.
        self.expenditure_descriptions.kwargs['choices'] = [
            (
                description_id,
                description_val.decode('utf-8'),
            )
            for description_id, description_val in FormUtils.get_expenditure_descriptions().iteritems()
        ]


        # Income types filter options.
        self.income_types.kwargs['choices'] = [
            (
                income_type_latin,
                income_type_latin
            )
            for income_type_cyrillic, income_type_latin in FormUtils.get_income_types().iteritems()
        ]

        # Years filter options.
        # self.years.kwargs['choices'] = [
        #     (
        #         year,
        #         year
        #     )
        #     for year in FormUtils.get_years()
        # ]


        Form.__init__(self, *args, **kwargs)
