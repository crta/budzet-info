# coding=utf-8
from flask.ext.testing import TestCase
from app import create_app, mongo
from utils import FormUtils

class KonsolidovaniBudzetFormsTestCase(TestCase):

    def create_app(self):
        app = create_app()
        return app

    def setUp(self):
        pass

    def tearDown(self):
        pass

class KonsolidovaniBudzetFormsUtilsTestCase(KonsolidovaniBudzetFormsTestCase):

    def test_get_municipalities(self):

        db_count = len(mongo.db.konsolidovanibudzet\
            .find({})\
            .distinct('opstina.latinica'))

        form_options_count = len(FormUtils.get_municipalities())

        self.assertTrue(db_count == form_options_count,
                        "Number of options differs between database (%i) and form options (%i)." % (form_options_count, form_options_count))



    def test_get_years(self):

        db_count = len(mongo.db.konsolidovanibudzet\
            .find({})\
            .distinct('godine'))

        form_options_count = len(FormUtils.get_years())

        self.assertTrue(db_count == form_options_count,
                        "Number of options differs between database (%i) and form options (%i)." % (form_options_count, form_options_count))





