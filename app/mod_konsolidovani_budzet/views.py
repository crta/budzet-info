# -*- coding: utf-8 -*-
from flask import Blueprint, render_template, request
from urlparse import urlparse
from bson import json_util
from app.mod_konsolidovani_budzet.forms.form import RevenuesAndExpenditureForm
from app.utils.url_query_utils import UrlQueryUtils
from app import data_manager_kb
from app.mod_konsolidovani_budzet.forms.utils import FormUtils

mod_konsolidovani_budzet = Blueprint('konsolidovani_budzet', __name__, url_prefix='/finansijski-izvestaji')


@mod_konsolidovani_budzet.route('/', methods=['GET'])
def index():

    data = None
    chart_type = None
    data_series = None
    fiscal_type = None
    selected_municipalities = None
    selected_years = None
    selected_fiscal_type = None
    selected_descriptions_exp = None
    selected_descriptions_rev = None
    group_by = None
    group_by_bubble = None
    disaggregated_by = None

    tree_years = FormUtils.get_tree_years()

    tree_municipalities = FormUtils.get_tree_municipalities()

    tree_income_types = FormUtils.get_tree_income_type()

    tree_revenue_descriptions = FormUtils.get_tree_revenue_descriptions()

    tree_expenditure_descriptions = FormUtils.get_tree_expenditure_descriptions()

    url_query = urlparse(request.url).query

    if url_query != '':
        # convert url query string into into JSON doc
        query_params = UrlQueryUtils.build_bd_query_params_from_url_query(url_query)

        chart_type = query_params['chartType']
        if chart_type == 'Map':
            fiscal_type = query_params['data']['fiscalType']['map']
            data_series = query_params['data']['series']['map']

        elif chart_type == 'Barchart':
            fiscal_type = query_params['data']['fiscalType']['barchart']
            data_series = query_params['data']['series']['barchart']
            group_by = query_params['data']['groupBy']

        elif chart_type == 'Scatterplot':
            data_series = query_params['data']['series']['scatterplot']

        elif chart_type == 'Floating Bubbles':
            disaggregated_by = query_params['data']['disaggregateBy']
            group_by_bubble = query_params['data']['floatingBubblesGroupBy']
            fiscal_type = query_params['data']['fiscalType']['floatingBubbles']
            data_series = query_params['data']['series']['floatingBubbles']

        data = data_manager_kb.get_chart_data(query_params)
        selected_municipalities = query_params['filters']['municipalities']
        selected_years = query_params['filters']['years']
        selected_fiscal_type = query_params['filters']['incomeTypes']
        selected_descriptions_exp = query_params['filters']['descriptions']['expenditures']
        selected_descriptions_rev = query_params['filters']['descriptions']['revenues']


    form = RevenuesAndExpenditureForm()
    return render_template(
        'mod_konsolidovani_budzet/index.html',
        form=form,
        tree_years=json_util.dumps(tree_years),
        tree_municipalities=json_util.dumps(tree_municipalities),
        tree_income_types=json_util.dumps(tree_income_types),
        tree_revenue_descriptions=json_util.dumps(tree_revenue_descriptions),
        tree_expenditure_descriptions=json_util.dumps(tree_expenditure_descriptions),
        data=json_util.dumps(data),
        chart_type=chart_type,
        data_series=data_series,
        fiscal_type=fiscal_type,
        group_by=group_by,
        disaggregated_by=disaggregated_by,
        group_by_bubble=group_by_bubble,
        selected_municipalities=json_util.dumps(selected_municipalities),
        selected_years=json_util.dumps(selected_years),
        selected_income_type=json_util.dumps(selected_fiscal_type),
        selected_descriptions_exp=json_util.dumps(selected_descriptions_exp),
        selected_descriptions_rev=json_util.dumps(selected_descriptions_rev)
    )
