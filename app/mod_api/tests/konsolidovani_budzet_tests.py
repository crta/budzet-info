# flask_testing/test_base.py
# coding=utf-8

from flask import url_for
from flask.ext.testing import TestCase
import json
from app import create_app
from app.utils.url_query_utils import UrlQueryUtils


class ApiUrlQueryTestCase(TestCase):
    ''' A test case for the api
    '''

    def test_konsolidovani_budzet_fiters(self):
        '''

        :return:
        '''
        self.assertEqual(1, 1, "")

    def create_app(self):
        app = create_app()
        return app

    def setUp(self):
        pass

    def tearDown(self):
        pass


class KonsolidovaniBudzetBarchartTestCase(ApiUrlQueryTestCase):

    def test_konsolidovani_budzet_empty_query_params(self):
        ''' Test a request to the API with an invalid (empty) message body.
        :return:
        '''
        response = self.client.post(
            url_for('api.konsolidovani_budzet'),
            data={}
        )
        self.assert500(response)

    def test_konsolidovani_budzet_sum_of_income_types_expenditures(self):
        '''
        TEST CASE: We want to test if the sum of income types are imported correctly for expenditures
        Years: 2006, 2007
        Income Types: all
        Municipality: VRŠAC

        :return:
        '''

        query_param = UrlQueryUtils.build_bd_query_params_json("Barchart", "Expenditure", "Income Types", [2006, 2007], ["Vršac"], ["Opstina grada", "Donacije", "Autonomna pokrajina", "OOCO", "Republika", "Ostali izvori"], [], [], "Municipality")

        response = self.client.post(
            url_for('api.konsolidovani_budzet'),
            data=json.dumps(query_param)
        )

        ooco = response.json["series"][0]["data"][0]
        opstina_grada = response.json["series"][1]["data"][0]
        ostali_izvori = response.json["series"][2]["data"][0]
        donacije = response.json["series"][3]["data"][0]
        republika = response.json["series"][4]["data"][0]
        autonomna_pokrajina = response.json["series"][5]["data"][0]

        self.assertEqual(ooco, 0, "Unexpected sum returned for 'ooco' of municipality Vršac")
        self.assertEqual(opstina_grada, 777956582, "Unexpected sum returned for 'opstina grada' of municipality Vršac")
        self.assertEqual(ostali_izvori, 0, "Unexpected sum returned for 'ostali izvori' of municipality Vršac")
        self.assertEqual(donacije, 29962000, "Unexpected sum returned for 'donacije' of municipality Vršac")
        self.assertEqual(republika, 200177000, "Unexpected sum returned for 'republika' of municipality Vršac")
        self.assertEqual(autonomna_pokrajina, 14336000, "Unexpected sum returned for 'autonomna pokrajina' of municipality Vršac")





    def test_konsolidovani_budzet_sum_of_description_account_expenditures_411(self):


        """ TEST CASE: We want to test if first line description for year 2006,2007,2008,2009 is imported correctly for expenditures
            Description ids: 411
            Income Types: all
            Years: all
            Chart: Barchart
        """

        query_param = UrlQueryUtils.build_bd_query_params_json("Barchart", "Expenditure", "Income Types", [2006, 2007, 2008, 2009], ["Vojvodina"], ["Opstina grada", "Donacije", "Autonomna pokrajina", "OOCO", "Republika", "Ostali izvori"], [], [411], "Municipality")

        response = self.client.post(
            url_for('api.konsolidovani_budzet'),
            data=json.dumps(query_param)
        )

        ooco = response.json["series"][0]["data"][0]
        opstina_grada = response.json["series"][1]["data"][0]
        ostali_izvori = response.json["series"][2]["data"][0]
        donacije = response.json["series"][3]["data"][0]
        republika = response.json["series"][4]["data"][0]
        autonomna_pokrajina = response.json["series"][5]["data"][0]

        self.assertEqual(ooco, 0, "Unexpected sum returned for all 'ooco' descriptions with id 411")
        self.assertEqual(opstina_grada, 23443+24371000+28550+30275, "Unexpected sum returned for all 'opstina grada' descriptions with id 411")
        self.assertEqual(ostali_izvori, 41009+47524000+54753+52432, "Unexpected sum returned for all 'ostali izvori' descriptions with id 411")
        self.assertEqual(donacije, 818+595000+104, "Unexpected sum returned for all 'donacije' descriptions with id 411")
        self.assertEqual(republika, 481535000+596325+630858, "Unexpected sum returned for all 'republika' descriptions with id 411")
        self.assertEqual(autonomna_pokrajina, 1255720+997909000+1248477+1414944, "Unexpected sum returned for all 'autonomna pokrajina' descriptions with id 411")


    def test_konsolidovani_budzet_sum_of_description_account_revenues_711(self):


        """ TEST CASE: We want to test if first line description for year 2006,2007,2008,2009 is imported correctly for revenues
            Description ids: 411
            Income Types: all
            Years: all
            Chart: Barchart
        """

        query_param = UrlQueryUtils.build_bd_query_params_json("Barchart", "Revenue", "Income Types", [2006, 2007, 2008, 2009], ["Vojvodina"], ["Opstina grada", "Donacije", "Autonomna pokrajina", "OOCO", "Republika", "Ostali izvori"], [711], [], "Municipality")

        response = self.client.post(
            url_for('api.konsolidovani_budzet'),
            data=json.dumps(query_param)
        )

        ooco = response.json["series"][0]["data"][0]
        opstina_grada = response.json["series"][1]["data"][0]
        ostali_izvori = response.json["series"][2]["data"][0]
        donacije = response.json["series"][3]["data"][0]
        republika = response.json["series"][4]["data"][0]
        autonomna_pokrajina = response.json["series"][5]["data"][0]

        self.assertEqual(ooco, 0, "Unexpected sum returned for all 'ooco' descriptions with id 411")
        self.assertEqual(opstina_grada, 0, "Unexpected sum returned for all 'opstina grada' descriptions with id 411")
        self.assertEqual(ostali_izvori, 0, "Unexpected sum returned for all 'ostali izvori' descriptions with id 411")
        self.assertEqual(donacije, 0, "Unexpected sum returned for all 'donacije' descriptions with id 411")
        self.assertEqual(republika,0 , "Unexpected sum returned for all 'republika' descriptions with id 411")
        self.assertEqual(autonomna_pokrajina, 6562655829, "Unexpected sum returned for all 'autonomna pokrajina' descriptions with id 411")


class KonsolidovaniBudzetMapTestCase(ApiUrlQueryTestCase):

    def test_konsolidovani_budzet_sum_of_description_account_expenditures_411_for_map(self):


        """ TEST CASE: We want to test if first line description for year 2010 is imported correctly for expenditures
            Description ids: 411
            Income Types: all
            Years: 2010
            Chart: Map
        """

        #this is a test case to show that for year 2010 the first row of description is not being imported


        query_param = UrlQueryUtils.build_bd_query_params_json("Map", "Expenditure", "Income Types", [2010], ["Kovin"], ["Opstina grada", "Donacije", "Autonomna pokrajina", "OOCO", "Republika", "Ostali izvori"], [], [411])

        response = self.client.post(
            url_for('api.konsolidovani_budzet'),
            data=json.dumps(query_param)
        )
        ooco = response.json[0]["ooco"]
        opstina_grada = response.json[0]["opstinaGrada"]
        ostali_izvori = response.json[0]["ostaliIzvori"]
        donacije = response.json[0]["donacije"]
        republika = response.json[0]["republika"]
        autonomna_pokrajina = response.json[0]["autonomnaPokrajina"]


        self.assertEqual(ooco, 0, "unexpected sum")
        self.assertEqual(opstina_grada, 99420, "unexpected sum")
        self.assertEqual(ostali_izvori, 4494, "unexpected sum")
        self.assertEqual(donacije, 0, "unexpected sum")
        self.assertEqual(republika, 15984, "unexpected sum")
        self.assertEqual(autonomna_pokrajina, 244, "unexpected sum")


    def test_konsolidovani_budzet_sum_of_description_account_revenues_711_for_map(self):


        """ TEST CASE: We want to test if first line description for year 2010 is imported correctly for revenues
            Description ids: 711
            Income Types: all
            Years: 2010
            Chart: Map
        """

        query_param = UrlQueryUtils.build_bd_query_params_json("Map", "Revenue", "Income Types", [2010], ["Kovin"], ["Opstina grada", "Donacije", "Autonomna pokrajina", "OOCO", "Republika", "Ostali izvori"], [711], [])

        response = self.client.post(
            url_for('api.konsolidovani_budzet'),
            data=json.dumps(query_param)
        )
        ooco = response.json[0]["ooco"]
        opstina_grada = response.json[0]["opstinaGrada"]
        ostali_izvori = response.json[0]["ostaliIzvori"]
        donacije = response.json[0]["donacije"]
        republika = response.json[0]["republika"]
        autonomna_pokrajina = response.json[0]["autonomnaPokrajina"]


        self.assertEqual(ooco, 0, "unexpected sum")
        self.assertEqual(opstina_grada, 206143, "unexpected sum")
        self.assertEqual(ostali_izvori, 0, "unexpected sum")
        self.assertEqual(donacije, 0, "unexpected sum")
        self.assertEqual(republika, 0, "unexpected sum")
        self.assertEqual(autonomna_pokrajina, 0, "unexpected sum")



    def test_konsolidovani_budzet_sum_of_description_account_expenditures_621_for_map(self):


        """ TEST CASE: We want to test if last line description is imported correctly for expenditures
            Description ids: 621
            Income Types: all
            Years: all
            Chart: Map
        """

        query_param = UrlQueryUtils.build_bd_query_params_json("Map", "Expenditure", "Income Types", [2006, 2007, 2008, 2009, 2010], ["Kovin"], ["Opstina grada", "Donacije", "Autonomna pokrajina", "OOCO", "Republika", "Ostali izvori"], [], [621])

        response = self.client.post(
            url_for('api.konsolidovani_budzet'),
            data=json.dumps(query_param)
        )
        ooco = response.json[0]["ooco"]
        opstina_grada = response.json[0]["opstinaGrada"]
        ostali_izvori = response.json[0]["ostaliIzvori"]
        donacije = response.json[0]["donacije"]
        republika = response.json[0]["republika"]
        autonomna_pokrajina = response.json[0]["autonomnaPokrajina"]


        self.assertEqual(ooco, 0, "unexpected sum")
        self.assertEqual(opstina_grada, 500+4454000+2411+2280, "unexpected sum")
        self.assertEqual(ostali_izvori, 12+3514000+16975+1910+22390, "unexpected sum")
        self.assertEqual(donacije, 0, "unexpected sum")
        self.assertEqual(republika, 0, "unexpected sum")
        self.assertEqual(autonomna_pokrajina, 0, "unexpected sum")




    def test_konsolidovani_budzet_sum_of_description_account_revenues_811_for_map(self):


        """ TEST CASE: We want to test if some random line description for each year is imported correctly for revenues
            Description ids: 811
            Income Types: all
            Years: all
            Chart: Map
        """

        query_param = UrlQueryUtils.build_bd_query_params_json("Map", "Revenue", "Income Types", [2006, 2007, 2008, 2009, 2010], ["Novi Sad"], ["Opstina grada", "Donacije", "Autonomna pokrajina", "OOCO", "Republika", "Ostali izvori"], [811], [])

        response = self.client.post(
            url_for('api.konsolidovani_budzet'),
            data=json.dumps(query_param)
        )

        ooco = response.json[0]["ooco"]
        opstina_grada = response.json[0]["opstinaGrada"]
        ostali_izvori = response.json[0]["ostaliIzvori"]
        donacije = response.json[0]["donacije"]
        republika = response.json[0]["republika"]
        autonomna_pokrajina = response.json[0]["autonomnaPokrajina"]


        self.assertEqual(ooco, 0, "unexpected sum")
        self.assertEqual(opstina_grada, 0, "unexpected sum")
        self.assertEqual(ostali_izvori, 23223+88868000+38297+31996+29, "unexpected sum")
        self.assertEqual(donacije, 0, "unexpected sum")
        self.assertEqual(republika, 0, "unexpected sum")
        self.assertEqual(autonomna_pokrajina, 56007, "unexpected sum")


class KonsolidovaniBudzetScatterplotTestCase(ApiUrlQueryTestCase):

    def test_konsolidovani_budzet_scatterplot_sum_of_description_account_revenues_and_expenditures(self):

        """ TEST CASE: We want to test if first line description for year 2006 is imported correctly for revenues and expenditures
            Description ids: 711, 411
            Income Types: all
            Years: all
            Chart: Scatterplot
        """

        query_param = UrlQueryUtils.build_bd_query_params_json("Scatterplot", "Expenditure", ["Municipalities"], [2006], ["Bač"], ["Opstina grada", "Donacije", "Autonomna pokrajina", "OOCO", "Republika", "Ostali izvori"], [711], [411], "Municipality")

        response = self.client.post(
            url_for('api.konsolidovani_budzet'),
            data=json.dumps(query_param)
        )

        x = response.json[0]["data"][0]["x"]
        y = response.json[0]["data"][0]["y"]

        self.assertEqual(x, 77682, "unexpected sum")
        self.assertEqual(y, 31572, "unexpected sum")












