# flask_testing/test_base.py
# coding=utf-8

from flask import url_for
from flask.ext.testing import TestCase
import json
from app import create_app
from app.utils.url_query_utils import UrlQueryUtils

class ApiUrlQueryTestCase(TestCase):
    ''' A test case for the api
    '''

    def test_lokalna_javna_produzeca_fiters(self):
        '''

        :return:
        '''
        self.assertEqual(1, 1, "")

    def create_app(self):
        app = create_app()
        return app

    def setUp(self):
        pass

    def tearDown(self):
        pass

class LokalnaJavnaProduzecaBarchartTestCase(ApiUrlQueryTestCase):

    def test_lokalna_javna_produzeca_empty_query_params(self):
        ''' Test a request to the API with an invalid (empty) message body.
        :return:
        '''
        response = self.client.post(
            url_for('api.lokalna_javna_produzeca'),
            data={}
        )
        self.assert500(response)

    def test_lokalna_javna_produzeca_first_line_company_total_revenue_and_expenditure(self):

        """
        TEST CASE: We want to test if first line company is imported correctly for revenues and expenditures
        year :2006
        :return:
        """

        query_param = UrlQueryUtils.build_ljp_barchart_query_params_json(True, True, ["AOP 207", "AOP 216", "AOP 218"], False, [], False, [], "Godina", [2006], ["Aleksandrovac"], [3600], [7100124], ["SKUPŠTINA OPŠTINE ALEKSANDROVAC"])

        response = self.client.post(
            url_for('api.lokalna_javna_produzeca'),
            data=json.dumps(query_param)
        )

        ukupni_prihodi = response.json["series"][3]["data"][0]
        poslovni_rashodi =response.json["series"][2]["data"][0]
        finansijski_rashodi = response.json["series"][0]["data"][0]
        ostali_rashodi = response.json["series"][1]["data"][0]

        self.assertEqual(ukupni_prihodi, 70800, "unexpected sum")
        self.assertEqual(poslovni_rashodi, 65299, "unexpected sum")
        self.assertEqual(finansijski_rashodi, 2105, "unexpected sum")
        self.assertEqual(ostali_rashodi, 3028, "unexpected sum")


    def test_lokalna_javna_produzeca_last_line_company_total_revenue_and_expenditure(self):
        """
        TEST CASE: We want to test if last line company is imported correctly for revenues and expenditures
        year :2006
        :return:
        """
        query_param = UrlQueryUtils.build_ljp_barchart_query_params_json(True, True, ["AOP 207", "AOP 216", "AOP 218"], False, [], False, [], "Godina", [2006], ["Šid"], [9311], [8755418], ["OPŠTINA ŠID"])

        response = self.client.post(
            url_for('api.lokalna_javna_produzeca'),
            data=json.dumps(query_param)
        )

        ukupni_prihodi = response.json["series"][3]["data"][0]
        poslovni_rashodi =response.json["series"][2]["data"][0]
        finansijski_rashodi = response.json["series"][0]["data"][0]
        ostali_rashodi = response.json["series"][1]["data"][0]

        self.assertEqual(ukupni_prihodi, 6656, "unexpected sum")
        self.assertEqual(poslovni_rashodi, 6151, "unexpected sum")
        self.assertEqual(finansijski_rashodi, 348, "unexpected sum")
        self.assertEqual(ostali_rashodi, 110, "unexpected sum")


class LokalnaJavnaProduzecaScatterPlotTestCase(ApiUrlQueryTestCase):

    def test_lokalna_javna_produzeca_empty_query_params(self):
        ''' Test a request to the API with an invalid (empty) message body.
        :return:
        '''
        response = self.client.post(
            url_for('api.lokalna_javna_produzeca'),
            data={}
        )
        self.assert500(response)


    def test_lokalna_javna_produzeca_first_line_company_total_revenue_and_expenditure(self):

        """
        TEST CASE: We want to test if first line company is imported correctly for revenues and expenditures
        year :2006
        :return:
        """

        query_param = UrlQueryUtils.build_ljp_scatterplot_query_params_json(["Godine"],
            "Ukupni prihodi",
            [],
            "Ukupni rashodi",
            ["AOP 207", "AOP 216", "AOP 218"],
            [2006],
            ["Aleksandrovac"],
            [3600],
            [7100124],
            ["SKUPŠTINA OPŠTINE ALEKSANDROVAC"])

        response = self.client.post(
            url_for('api.lokalna_javna_produzeca'),
            data=json.dumps(query_param)
        )

        x = response.json[0]["data"][0]["x"]
        y = response.json[0]["data"][0]["y"]


        self.assertEqual(x, 70800, "unexpected sum")
        self.assertEqual(y, 70432, "unexpected sum")


    def test_lokalna_javna_produzeca_last_line_company_total_revenue_and_expenditure(self):
        """
        TEST CASE: We want to test if last line company is imported correctly for revenues and expenditures
        year :2006
        :return:
        """
        query_param = UrlQueryUtils.build_ljp_scatterplot_query_params_json(["Opštine"],
            "Ukupni prihodi",
            [],
            "Ukupni rashodi",
            ["AOP 207", "AOP 216", "AOP 218"],
            [2006],
            ["Šid"],
            [9311],
            [8755418],
            ["OPŠTINA ŠID"])

        response = self.client.post(
            url_for('api.lokalna_javna_produzeca'),
            data=json.dumps(query_param)
        )

        x = response.json[0]["data"][0]["x"]
        y = response.json[0]["data"][0]["y"]


        self.assertEqual(x, 6656, "unexpected sum")
        self.assertEqual(y, 6609, "unexpected sum")



class LokalnaJavnaProduzecaMapTestCase(ApiUrlQueryTestCase):

    def test_lokalna_javna_produzeca_first_line_company_total_expenditure(self):

        """
        TEST CASE: We want to test if first line company is imported correctly expenditures
        year :2006
        :return:
        """

        query_param = UrlQueryUtils.build_ljp_map_query_params_json("Ukupni rashodi", ["AOP 207", "AOP 216", "AOP 218"], [2006], ["Aleksandrovac"], [3600], [7100124], ["SKUPŠTINA OPŠTINE ALEKSANDROVAC"])

        response = self.client.post(
            url_for('api.lokalna_javna_produzeca'),
            data=json.dumps(query_param)
        )

        value = response.json[0]["value"]

        self.assertEqual(value, 70432, "Unexcpected sum ")

    def test_lokalna_javna_produzeca_last_line_company_total_expenditure(self):

        """
        TEST CASE: We want to test if first line company is imported correctly expenditures
        year :2006
        :return:
        """

        query_param = UrlQueryUtils.build_ljp_map_query_params_json("Ukupni rashodi", ["AOP 207", "AOP 216", "AOP 218"], [2006], ["Šid"], [9311], [8755418], ["OPŠTINA ŠID"])

        response = self.client.post(
            url_for('api.lokalna_javna_produzeca'),
            data=json.dumps(query_param)
        )

        value = response.json[0]["value"]

        self.assertEqual(value, 6609, "Unexcpected sum ")


