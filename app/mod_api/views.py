# -*- coding: utf-8 -*-
from flask import Blueprint, Response, request, render_template
import json
from app import data_manager_kb, data_manager_ljp

mod_api = Blueprint('api', __name__, url_prefix='/api')

@mod_api.route("/grafikona/konsolidovani-budzet/", methods=['POST'])
def konsolidovani_budzet():
    '''
        Retrieves revenues and expenditures data.
        Filter based on values given in POST body.
    '''

    json_data = request.data
    json_obj = json.loads(json_data)
    result = data_manager_kb.get_chart_data(json_obj)

    return Response(response=json.dumps(result), status=200, mimetype='application/json')


@mod_api.route("/grafikona/lokalna-javna-produzeca", methods=['POST'])
def lokalna_javna_produzeca():
    '''
        Retrieves public companies data.
        Filter based on values given in POST body.
    '''

    json_data = request.data
    json_obj = json.loads(json_data)
    result = data_manager_ljp.get_chart_data(json_obj)

    # Return JSON response based on filtering parameters we gave in post request body
    return Response(response=json.dumps(result), status=200, mimetype='application/json')


@mod_api.route("/", methods=['GET'])
def index():
    '''
    Renders the API documentation page.
    :return:
    '''
    return render_template('mod_api/index.html')

@mod_api.route("/konsolidovani-budzet", methods=['POST'])
def get_raw_konsolidovani_budzet():
    ''' Fetches documents for konsolidovani budzet.
    :return:
    '''

    query_obj = json.loads(request.data)
    result = data_manager_kb.get_chart_data(query_obj)

    return Response(response=result, status=200, mimetype='application/json')

@mod_api.route("/lokalna-javna-produzeca", methods=['POST'])
def get_raw_lokalna_javna_produzeca():
    ''' Fetches documents for lokalna javna produzeca.
    :return:
    '''
    query_obj = json.loads(request.data)
    result = data_manager_ljp.get_chart_data(query_obj)

    return Response(response=result, status=200, mimetype='application/json')


