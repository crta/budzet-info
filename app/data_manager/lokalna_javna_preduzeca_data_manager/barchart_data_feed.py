# coding=utf-8
from bson.son import SON

from abstract_datafeed import AbstractDataFeed
import abc

class BarchartDataFeed(object):

    mongo = None

    def __init__(self, mongo):
        self.mongo = mongo

    def get_data(self, query_params):
        '''

        :param query_params:
        :return:
        '''

        # aggregation pipelines's container list
        pipeline_stages = []

        # build match operation stage
        match = self.build_match_operation_pipeline(query_params)
        pipeline_stages.extend(match)

        # build match operation stage
        group = self.build_group_operation_pipeline(query_params)
        pipeline_stages.extend(group)


        field_param = ''
        # Project by Godina
        if query_params['podaci']['grupisanoPo'] == 'Godina':
            field_param = '$_id.godine'

        # Project by Preduzeća
        elif query_params['podaci']['grupisanoPo'] == 'Preduzeća':
            field_param = '$_id.preduzece'

        # Project by Preduzeća
        elif query_params['podaci']['grupisanoPo'] == 'Opština':
            field_param = '$_id.nazivOpstine'

        # Project by Preduzeća
        elif query_params['podaci']['grupisanoPo'] == 'Veličina preduzeća':
            field_param = '$_id.velicinaPreduzeca'

        # Project by Osnivači
        elif query_params['podaci']['grupisanoPo'] == 'Osnivači':
            field_param = '$_id.osnivaci'

        # projected json structure (in project stage) makes it easier for us to build barchart json response
        project = self.build_project_operation_pipeline(query_params, field_param)
        pipeline_stages.append(project)

        json_result = self.mongo.db.lokalnajavnapreduzeca.aggregate(pipeline_stages)

        # Let's build a compatible structure fot bar chart data series
        response = self.build_json_structure_compatible_for_barchart_data_series(json_result['result'])

        return response

    def build_match_operation_pipeline(self, query_params):
        '''
        Let's build MongoDB match pipeline
        :param query_params: will enable us to filter based on the filtering params we picked in the filtering bar
        :return: complete filtering options
        '''

        # define mongo match stage
        match = {'$match': {}}

        # match docs based on the document year, for all years we picked in the filtering box
        if query_params['filteri']['godine'] != []:
            match['$match']['godine'] = {"$in": query_params['filteri']['godine']}

        # match docs based on municipality name, for all municipalities we picked in the filtering box
        if query_params['filteri']['opstine'] != []:
            match['$match']['opstina.naziv.vrednost'] = {}
            match['$match']['opstina.naziv.vrednost'] = {
                "$in": query_params['filteri']['opstine']
            }

        # match docs based on activities name, for all activities we picked in the filtering box
        if query_params['filteri']['delatnosti'] != []:
            match['$match']['delatnost.id'] = {}
            match['$match']['delatnost.id'] = {
                "$in": query_params['filteri']['delatnosti']
            }

        # match docs based on companies name, for all companies we picked in the filtering box
        if query_params['filteri']['preduzeca'] != []:
            match['$match']['preduzece.maticniBroj'] = {}
            match['$match']['preduzece.maticniBroj'] = {
                "$in": query_params['filteri']['preduzeca']
            }

        # match docs based on companies name, for all companies we picked in the filtering box
        if query_params['filteri']['osnivaci'] != [] or 'grupisanoPo' in query_params['podaci'] and query_params['podaci']['grupisanoPo'] == 'Osnivači':

            # knowing that we have owners inside a list, we first unwind (destruct) the list then match for
            # elements we need, let's unwind
            unwind = {
                "$unwind": "$osnivaci.imena"
            }
            match['$match']['osnivaci.imena.vrednost'] = {}
            match['$match']['osnivaci.imena.vrednost'] = {
                "$in": query_params['filteri']['osnivaci']
            }

            return [unwind, match]

        else:

            return [match]

    def build_group_operation_pipeline(self, query_params):
        '''
        Let's build MongoDB group pipeline
        :param query_params: will enable us to group based on the what params we picked in the filtering bar
        :return: grouping params
        '''

        # this variable will be assigned when a grouping parameter is defined, and will be used in mongo sorting stage
        sort_param = ''
        pipeline_stages = []
        # define mongo group stage
        group = {'$group': {
            '_id': {}
        }}

        # Group by Godina
        if query_params['podaci']['grupisanoPo'] == 'Godina':
            group['$group']['_id']['godine'] = '$godine'
            sort_param = '_id.godine'

        # else Group by Preduzeća
        elif query_params['podaci']['grupisanoPo'] == 'Preduzeća':
            group['$group']['_id']['preduzece'] = '$preduzece.naziv'
            sort_param = '_id.preduzece'

        # else Group by Opština
        elif query_params['podaci']['grupisanoPo'] == 'Opština':
            group['$group']['_id']['nazivOpstine'] = '$opstina.naziv.vrednost'
            sort_param = '_id.nazivOpstine'

        # else Group by Veličina preduzeća
        elif query_params['podaci']['grupisanoPo'] == 'Veličina preduzeća':
            group['$group']['_id']['velicinaPreduzeca'] = '$velicinaPreduzeca.vrednost'
            sort_param = '_id.velicinaPreduzeca'

        # else Group by Osnivači
        elif query_params['podaci']['grupisanoPo'] == 'Osnivači':
            group['$group']['_id']['osnivaci'] = '$osnivaci.imena.vrednost'
            sort_param = '_id.osnivaci'

        # Calculate the sum of ukupni Prihodi
        if query_params['podaci']['ukupniPrihodi'] is True:
            group['$group']['ukupniPrihodi'] = {"$sum": "$ukupniPrihodi.vrednost"}


        # Calculate the sum of ukupni Rashodi, based on selected values in the filtering box
        if query_params['podaci']['ukupniRashodi']['selected'] is True and query_params['podaci']['ukupniRashodi']['params'] != []:

            for param in query_params['podaci']['ukupniRashodi']['params']:

                if param == "AOP 207":
                    group['$group']['Poslovni rashodi'] = {"$sum": "$poslovniRashodi.vrednost"}

                elif  param == "AOP 216":
                    group['$group']['Finansijski rashodi'] = {"$sum": "$finansijskiRashodi.vrednost"}

                else:
                    group['$group']['Ostali rashodi'] = {"$sum": "$ostaliRashodi.vrednost"}


        # Calculate the sum of Poslovni Rashodi, based on selected values in the filtering box
        if query_params['podaci']['poslovniRashodi']['selected'] is True and query_params['podaci']['poslovniRashodi']['params'] != []:

            for param in query_params['podaci']['poslovniRashodi']['params']:

                if param == "AOP 208":
                    group['$group']['Nabavna vrednost prodate robe'] = {"$sum": "$nabavnaVrednostProdateRobe.vrednost"}

                elif  param == "AOP 209":
                    group['$group']['Troskovi materijala'] = {"$sum": "$troskoviMaterijala.vrednost"}

                elif  param == "AOP 210":
                    group['$group']['Troškovi zarada, naknada zarada i ostali lični rashodi'] = {"$sum": "$troskoviZaradaNaknadaZaradaOstaliLicniRashodi.vrednost"}

                elif  param == "AOP 211":
                    group['$group']['Troskovi amortizacije i rezervisanja'] = {"$sum": "$troskoviAmortizacijeRezervisanja.vrednost"}

                else:
                    group['$group']['Ostali poslovni rashodi'] = {"$sum": "$ostaliPoslovniRashodi.vrednost"}


        # Calculate the sum of other categories, based on selected values in the filtering box Ostali
        if query_params['podaci']['ostali']['selected'] is True and query_params['podaci']['ostali']['params'] != []:

            for param in query_params['podaci']['ostali']['params']:

                if param == "AOP 216":
                    group['$group']['Finansijski rashodi'] = {"$sum": "$finansijskiRashodi.vrednost"}

                elif  param == "AOP 218":
                    group['$group']['Ostali rashodi'] = {"$sum": "$ostaliRashodi.vrednost"}

                elif  param == "AOP 229":
                    group['$group']['Neto dobitak'] = {"$sum": "$netoDobitak.vrednost"}

                else:
                    group['$group']['Neto gubitak'] = {"$sum": "$netoGubitak.vrednost"}
        # build sorting stage for grouping pipeline of descriptions
        sort = {
            "$sort": SON([
                (sort_param, 1)
            ])
        }
        return [group, sort]


    def build_project_operation_pipeline(self, query_params, project_field_param):
        '''

        :param query_params:
        :return:
        '''
        project = {
            "$project": {
                "_id": 0,
                'groupParam': project_field_param
            }

        }

        if query_params['podaci']['ukupniPrihodi'] is True:
            project['$project']['Ukupni Prihodi'] = '$ukupniPrihodi'


        # Project Ukupni Rashodi sub categories inside project filed name ukupniRashodi
        if query_params['podaci']['ukupniRashodi']['selected'] is True and query_params['podaci']['ukupniRashodi']['params'] != []:

            project['$project']['Ukupni Rashodi'] = {}

            for param in query_params['podaci']['ukupniRashodi']['params']:

                if param == "AOP 207":
                    project['$project']['Ukupni Rashodi']['Poslovni rashodi'] = '$Poslovni rashodi'

                elif  param == "AOP 216":
                    project['$project']['Ukupni Rashodi']['Finansijski rashodi'] = "$Finansijski rashodi"

                else:
                    project['$project']['Ukupni Rashodi']['Ostali rashodi'] = "$Ostali rashodi"

        # Project Poslovni Rashodi sub categories inside project filed name poslovniRashodi
        if query_params['podaci']['poslovniRashodi']['selected'] is True and query_params['podaci']['poslovniRashodi']['params'] != []:

            project['$project']['Poslovni Rashodi'] = {}

            for param in query_params['podaci']['poslovniRashodi']['params']:

                if param == "AOP 208":
                    project['$project']['Poslovni Rashodi']['Nabavna vrednost prodate robe'] = "$Nabavna vrednost prodate robe"

                elif  param == "AOP 209":
                    project['$project']['Poslovni Rashodi']['Troskovi materijala'] = "$Troskovi materijala"

                elif  param == "AOP 210":
                    project['$project']['Poslovni Rashodi']['Troškovi zarada, naknada zarada i ostali lični rashodi'] = "$Troškovi zarada, naknada zarada i ostali lični rashodi"

                elif  param == "AOP 211":
                    project['$project']['Poslovni Rashodi']['Troskovi amortizacije i rezervisanja'] = "$Troskovi amortizacije i rezervisanja"

                else:
                    project['$project']['Poslovni Rashodi']['Ostali poslovni rashodi'] = "$Ostali poslovni rashodi"


        # Project other categories inside project filed name ostali
        if query_params['podaci']['ostali']['selected'] is True and query_params['podaci']['ostali']['params'] != []:

            project['$project']['Ostali'] = {}

            for param in query_params['podaci']['ostali']['params']:

                if param == "AOP 216":
                    project['$project']['Ostali']['Finansijski rashodi'] = "$Finansijski rashodi"

                elif  param == "AOP 218":
                    project['$project']['Ostali']['Ostali rashodi'] = "$Ostali rashodi"

                elif  param == "AOP 229":
                    project['$project']['Ostali']['Neto dobitak'] = "$Neto dobitak"

                else:
                    project['$project']['Ostali']['Neto gubitak'] = "$Neto gubitak"

        return project


    def build_json_structure_compatible_for_barchart_data_series(self, json_list):
        '''

        :param json_list:
        :return:
        '''
        categories = []
        data_series = []
        for item in json_list:
            categories.append(item['groupParam'])
            series_counter = 0
            for fs_type in item:
                if fs_type != 'groupParam':
                    if len(categories) == 1:
                        if fs_type == 'Ukupni Prihodi':
                            json_obj = {}
                            json_obj['name'] = fs_type
                            json_obj['data'] = [item[fs_type]]
                            json_obj['stack'] = fs_type
                            data_series.append(json_obj)
                        else:
                            for key in item[fs_type]:
                                json_obj = {}
                                json_obj['name'] = key
                                json_obj['data'] = [item[fs_type][key]]
                                json_obj['stack'] = fs_type
                                data_series.append(json_obj)

                    else:
                        if fs_type == 'Ukupni Prihodi':
                            data_series[series_counter]['data'].append(item[fs_type])
                            series_counter += 1
                        else:
                            for key in item[fs_type]:
                                data_series[series_counter]['data'].append(item[fs_type][key])
                                series_counter += 1

        main_json = {
            'categories': categories,
            'series': data_series
        }

        return main_json

AbstractDataFeed.register(BarchartDataFeed)