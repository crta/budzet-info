# coding=utf-8
from bson.son import SON
from app.data_manager.lokalna_javna_preduzeca_data_manager.barchart_data_feed import BarchartDataFeed

# instantiate Barchart class object so we can use necessary class attributes in ScatterPlot Class
barchart_obj = BarchartDataFeed(None)

from abstract_datafeed import AbstractDataFeed
import abc

class ScatterplotDataFeed(object):

    mongo = None


    def __init__(self, mongo):
        self.mongo = mongo

    def get_data(self, query_params):

        main_series_array = []
        for series in query_params['podaci']['series']:

            if series.encode('utf-8') == "Godine":
                group_param = 'godine'
                field_param = 'godine'
                json_result = self.build_scatterplot_aggregation_pipeline_response(query_params, group_param, field_param)
                data_series = self.build_scatterplot_data_series_structure(json_result, query_params)
                main_json = {
                    'name': 'Godine',
                    'color': 'rgba(223, 83, 83, .5)',
                    'data': data_series
                }
                main_series_array.append(main_json)

            if series.encode('utf-8') == "Opštine":
                group_param = 'opstina.naziv.vrednost'
                field_param = 'opstina'
                json_result = self.build_scatterplot_aggregation_pipeline_response(query_params, group_param, field_param)
                data_series = self.build_scatterplot_data_series_structure(json_result, query_params)
                main_json = {
                    'name': 'Opštine',
                    'color': 'rgba(134, 45, 55, .5)',
                    'data': data_series
                }
                main_series_array.append(main_json)

            if series.encode('utf-8') == "Delatnosti":
                group_param = 'delatnost.naziv'
                field_param = 'delatnost'
                json_result = self.build_scatterplot_aggregation_pipeline_response(query_params, group_param, field_param)
                data_series = self.build_scatterplot_data_series_structure(json_result, query_params)
                main_json = {
                    'name': 'Delatnosti',
                    'color': 'rgba(211, 113, 53, .5)',
                    'data': data_series
                }
                main_series_array.append(main_json)

            if series.encode('utf-8') == "Preduzeća":
                group_param = 'preduzece.naziv'
                field_param = 'preduzece'
                json_result = self.build_scatterplot_aggregation_pipeline_response(query_params, group_param, field_param)
                data_series = self.build_scatterplot_data_series_structure(json_result, query_params)
                main_json = {
                    'name': 'Preduzeća',
                    'color': 'rgba(53, 283, 183, .5)',
                    'data': data_series
                }
                main_series_array.append(main_json)

            if series.encode('utf-8') == "Osnivači":
                group_param = 'osnivaci.imena.vrednost'
                field_param = 'osnivaci'
                json_result = self.build_scatterplot_aggregation_pipeline_response(query_params, group_param, field_param)
                data_series = self.build_scatterplot_data_series_structure(json_result, query_params)
                main_json = {
                    'name': 'Osnivači',
                    'color': 'rgba(23, 33, 283, .5)',
                    'data': data_series
                }
                main_series_array.append(main_json)

        return main_series_array


    def build_scatterplot_aggregation_pipeline_response(self, query_params, group_param, field_param):

        pipeline_stages = []

        group = {
            "$group": {
                '_id': {}
            }
        }

        group['$group']['_id'][field_param] = '$' + group_param
        project_param = "$_id." + field_param
        sort_param = '_id.' + field_param

        # build match pipeline
        match = barchart_obj.build_match_operation_pipeline(query_params)
        pipeline_stages.extend(match)

        # build group pipeline
        group = self.build_group_operation_pipeline(query_params, group)
        pipeline_stages.append(group)

        # build sort pipeline
        sort = {
            "$sort":
                SON([
                    (sort_param, 1)
                ])
        }
        pipeline_stages.append(sort)

        # build project pipeline
        project = self.build_project_operation_pipeline(query_params, project_param)
        pipeline_stages.append(project)

        json_result = self.mongo.db.lokalnajavnapreduzeca.aggregate(pipeline_stages)

        return json_result['result']


    def build_scatterplot_data_series_structure(self, json_result, query_params):
        '''

        :param match:
        :param group:
        :return:
        '''

        data_series = []
        for item in json_result:
            json_obj = {}
            for key in item:
                if key == 'groupParam':
                    json_obj['label'] = item[key]
                else:
                    if key == 'Ukupni prihodi':
                        filed_axis = self.set_axis_field_query_param(query_params, key)
                        json_obj[filed_axis] = item[key]
                    else:
                        filed_axis = self.set_axis_field_query_param(query_params, key)
                        sum = 0
                        # calculate the total sum of selected options for a certain category
                        for element in item[key]:
                            sum += item[key][element]
                        json_obj[filed_axis] = sum
            data_series.append(json_obj)
            # this is the json structure we are trying to build for scatterplot chart
            # {'label': 2007, 'x': 12334, 'y': 654209}

        return data_series


    def build_group_operation_pipeline(self, query_params, group):
        '''

        :param query_params:
        :return:
        '''

        # check the query parameters you have chosen in filtering bar for X or Y axis
        if query_params['podaci']['x']['selected'] == "Ukupni prihodi" or query_params['podaci']['y']['selected'] == "Ukupni prihodi":
            group['$group']['Ukupni Prihodi'] = {"$sum": "$ukupniPrihodi.vrednost"}


        if query_params['podaci']['x']['selected'] == "Ukupni rashodi" or query_params['podaci']['y']['selected'] == "Ukupni rashodi":

            # let's check which axis options have been selected for category Ukupni rashodi
            query_param = self.set_axis_field_query_param(query_params, "Ukupni rashodi")

            # Based on the query field that is defined above,  let's iterate in the query json
            for param in query_params['podaci'][query_param]['params']:

                if param == "AOP 207":
                    group['$group']['Poslovni rashodi'] = {"$sum": "$poslovniRashodi.vrednost"}

                elif  param == "AOP 216":
                    group['$group']['Finansijski rashodi'] = {"$sum": "$finansijskiRashodi.vrednost"}

                else:
                    group['$group']['Ostali rashodi'] = {"$sum": "$ostaliRashodi.vrednost"}


        if query_params['podaci']['x']['selected'] == "Poslovni rashodi" or query_params['podaci']['y']['selected'] == "Poslovni rashodi":

            # let's check which axis options have been selected for category Poslovni rashodi
            query_param = self.set_axis_field_query_param(query_params, "Poslovni rashodi")

            for param in query_params['podaci'][query_param]['params']:

                if param == "AOP 208":
                    group['$group']['Nabavna vrednost prodate robe'] = {"$sum": "$nabavnaVrednostProdateRobe.vrednost"}

                elif  param == "AOP 209":
                    group['$group']['Troskovi materijala'] = {"$sum": "$troskoviMaterijala.vrednost"}

                elif  param == "AOP 210":
                    group['$group']['Troškovi zarada, naknada zarada i ostali lični rashodi'] = {"$sum": "$troskoviZaradaNaknadaZaradaOstaliLicniRashodi.vrednost"}

                elif  param == "AOP 211":
                    group['$group']['Troskovi amortizacije i rezervisanja'] = {"$sum": "$troskoviAmortizacijeRezervisanja.vrednost"}

                else:
                    group['$group']['Ostali poslovni rashodi'] = {"$sum": "$ostaliPoslovniRashodi.vrednost"}


        if query_params['podaci']['x']['selected'] == "Ostali" or query_params['podaci']['y']['selected'] == "Ostali":

            # let's check which axis options have been selected for category Ostali
            query_param = self.set_axis_field_query_param(query_params, "Ostali")

            for param in query_params['podaci'][query_param]['params']:

                if param == "AOP 216":
                    group['$group']['Finansijski rashodi'] = {"$sum": "$finansijskiRashodi.vrednost"}

                elif  param == "AOP 218":
                    group['$group']['Ostali rashodi'] = {"$sum": "$ostaliRashodi.vrednost"}

                elif  param == "AOP 229":
                    group['$group']['Neto dobitak'] = {"$sum": "$netoDobitak.vrednost"}

                elif param == "AOP 230":
                    group['$group']['Neto gubitak'] = {"$sum": "$netoGubitak.vrednost"}
                else:
                    pass

        return  group


    def build_project_operation_pipeline(self, query_params, project_field_param):
        '''

        :param query_params:
        :return:
        '''
        project = {
            "$project": {
                "_id": 0,
                'groupParam': project_field_param
            }

        }

        if query_params['podaci']['x']['selected'] == "Ukupni prihodi" or query_params['podaci']['y']['selected'] == "Ukupni prihodi":
            project['$project']['Ukupni prihodi'] = '$Ukupni Prihodi'


        # Project Ukupni Rashodi sub categories inside project filed name ukupniRashodi
        if query_params['podaci']['x']['selected'] == "Ukupni rashodi" or query_params['podaci']['y']['selected'] == "Ukupni rashodi":

            project['$project']['Ukupni rashodi'] = {}
            # let's check which axis options have been selected for category Ukupni rashodi
            query_param = self.set_axis_field_query_param(query_params, "Ukupni rashodi")

            for param in query_params['podaci'][query_param]['params']:

                if param == "AOP 207":
                    project['$project']['Ukupni rashodi']['Poslovni rashodi'] = '$Poslovni rashodi'

                elif  param == "AOP 216":
                    project['$project']['Ukupni rashodi']['Finansijski rashodi'] = "$Finansijski rashodi"

                else:
                    project['$project']['Ukupni rashodi']['Ostali rashodi'] = "$Ostali rashodi"


        # Project Poslovni Rashodi sub categories inside project filed name poslovniRashodi
        if query_params['podaci']['x']['selected'] == "Poslovni rashodi" or query_params['podaci']['y']['selected'] == "Poslovni rashodi":

            # let's check which axis options have been selected for category Poslovni rashodi
            query_param = self.set_axis_field_query_param(query_params, "Poslovni rashodi")

            project['$project']['Poslovni rashodi'] = {}

            for param in query_params['podaci'][query_param]['params']:

                if param == "AOP 208":
                    project['$project']['Poslovni rashodi']['Nabavna vrednost prodate robe'] = "$Nabavna vrednost prodate robe"

                elif  param == "AOP 209":
                    project['$project']['Poslovni rashodi']['Troskovi materijala'] = "$Troskovi materijala"

                elif  param == "AOP 210":
                    project['$project']['Poslovni rashodi']['Troškovi zarada, naknada zarada i ostali lični rashodi'] = "$Troškovi zarada, naknada zarada i ostali lični rashodi"

                elif  param == "AOP 211":
                    project['$project']['Poslovni rashodi']['Troskovi amortizacije i rezervisanja'] = "$Troskovi amortizacije i rezervisanja"

                else:
                    project['$project']['Poslovni rashodi']['Ostali poslovni rashodi'] = "$Ostali poslovni rashodi"


        if query_params['podaci']['x']['selected'] == "Ostali" or query_params['podaci']['y']['selected'] == "Ostali":

            # let's check which axis options have been selected for category Ostali
            query_param = self.set_axis_field_query_param(query_params, "Ostali")

            project['$project']['Ostali'] = {}

            for param in query_params['podaci'][query_param]['params']:

                if param == "AOP 216":
                    project['$project']['Ostali']['Finansijski rashodi'] = "$Finansijski rashodi"

                elif  param == "AOP 218":
                    project['$project']['Ostali']['Ostali rashodi'] = "$Ostali rashodi"

                elif  param == "AOP 229":
                    project['$project']['Ostali']['Neto dobitak'] = "$Neto dobitak"

                else:
                    project['$project']['Ostali']['Neto gubitak'] = "$Neto gubitak"

        return project


    def set_axis_field_query_param(self, query_params, filed_value):

        # in order to make only one iterating loop for this category,
        # let's check which axis options have been selected
        query_param = ''

        # X-axis options...
        if filed_value == str(query_params['podaci']['x']['selected']):
            query_param = 'x'

        # or Y-axis options...
        elif filed_value == str(query_params['podaci']['y']['selected']):
            query_param = 'y'

        return query_param

AbstractDataFeed.register(ScatterplotDataFeed)