
class AbstractChartDataFeedFactoryLJP(object):

    def __init__(self, data_factory=None):
        self._data_factory = data_factory

    def get_chart_data(self, query_params):
        ''' Invokes the appropriate data fetching function based on given chart type.
        :param query_params: JSON document which contains all mongo query parameters required to build mongo request
        :return: based on the chart type, return the respective JSON response
        '''

        # If no chart type is defined, then just return a generic JSON
        # response.
        if 'tipGrafikona' not in query_params:

            datafeed_obj = self._data_factory.get_simple_datafeed_provider()


        # if chart type in query params is selected to Barchart,
        # invoke data fetching of get_barchart_json_response()
        elif query_params['tipGrafikona'] == 'Barchart':

            datafeed_obj = self._data_factory.get_barchart_datafeed_provider()

        # else if chart type in query params is selected to Map,
        # invoke data fetching of get_map_json_response()
        elif query_params['tipGrafikona'] == 'Map':

            datafeed_obj = self._data_factory.get_map_datafeed_provider()

        # else if Scatterplot chart type in query params is selected,
        # invoke data fetching of get_scatterplot_json_response()
        elif query_params['tipGrafikona'] == 'Scatterplot':

            datafeed_obj = self._data_factory.get_scatterplot_datafeed_provider()

        # else if chart type in query params is selected to Bubble,
        # invoke data fetching of get_bubble_json_response()
        elif query_params['tipGrafikona'] == 'Floating Bubbles':

            datafeed_obj = self._data_factory.get_bubble_datafeed_provider()

        return datafeed_obj.get_data(query_params)