from slugify import slugify
from bson import json_util

from abstract_datafeed import AbstractDataFeed
import abc

class SimpleDataFeed(object):

    mongo = None

    def __init__(self, mongo):
        self.mongo = mongo

    def get_data(self, query_params):

        print query_params
        match = {}
        sort_param = ""
        if query_params['sort'] == "godine":
            sort_param = 'godine'
        elif query_params['sort'] == "opstina":
            sort_param = "opstina.naziv.slug"
        elif query_params['sort'] =="delatnost":
            sort_param = "delatnost.id"
        elif query_params['sort'] == "preduzeca":
            sort_param = "preduzece.maticniBroj"
        elif query_params['sort'] == "osnivaci":
            sort_param = "osnivaci.imena.slug"

        # Years
        if 'godine' in query_params:
            match['godine'] = {
                "$in": query_params['godine']
            }

        # Municipalities
        if 'opstina' in query_params:
            match['opstina.naziv.slug'] = {
                "$in": [slugify(n, to_lower=True) for n in query_params['opstina']]
            }

        # TODO: Implement founders filter.
        # Founders
        #if 'osnivaci' in query_params:
        #    pass

        # Companies

        if 'osnivaci' in query_params:
            match['osnivaci.imena.slug'] = {
                "$in": [slugify(n, to_lower=True) for n in query_params['osnivaci']]
            }


        if 'preduzece' in query_params:
            match['preduzece.maticniBroj'] = {
                "$in": query_params['preduzece']
            }

        # Activities
        if 'delatnost' in query_params:
            match['delatnost.id'] = {
                "$in": query_params['delatnost']
            }

        # TODO: Implement order by.



        response = self.mongo.db.lokalnajavnapreduzeca.find(match).sort([(sort_param, 1)])

        return json_util.dumps(response)

AbstractDataFeed.register(SimpleDataFeed)