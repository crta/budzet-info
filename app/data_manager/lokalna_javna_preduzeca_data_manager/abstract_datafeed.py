import abc
class AbstractDataFeed(object):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_data(self, query_params):
        '''
        Returns a JSON response listing all the data needed to render
        a certain visualization.
        :param query_params: Data filter parameters selected by the user.
        :return: JSON result.
        '''

        return



