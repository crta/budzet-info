from barchart_data_feed import BarchartDataFeed
from map_data_feed import MapDataFeed
from scatterplot_data_feed import ScatterplotDataFeed
from bubble_data_feed import BubbleDataFeed
from simple_data_feed import SimpleDataFeed

class ChartDataFeedFactoryLJP(object):

    def __init__(self, mongo = None):
        self.mongo = mongo

    def get_barchart_datafeed_provider(self):

        return BarchartDataFeed(self.mongo)

    def get_map_datafeed_provider(self):

        return MapDataFeed(self.mongo)

    def get_scatterplot_datafeed_provider(self):

        return ScatterplotDataFeed(self.mongo)

    def get_bubble_datafeed_provider(self):

        return BubbleDataFeed(self.mongo)

    def get_simple_datafeed_provider(self):

        return SimpleDataFeed(self.mongo)