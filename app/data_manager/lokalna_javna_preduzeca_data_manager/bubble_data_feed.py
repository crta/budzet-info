# coding=utf-8
from bson.son import SON
from abstract_datafeed import AbstractDataFeed
from barchart_data_feed import BarchartDataFeed
from random import randint

bar_chart_obj = BarchartDataFeed(None)


class BubbleDataFeed(object):

    mongo = None


    def __init__(self, mongo):
        self.mongo = mongo

    def get_data(self, query_params):

        # main Json response structure
        response = {
            'aggregated': [],
            'disaggregated': []
        }

        pipeline_stages = []
        # build group stage
        match = bar_chart_obj.build_match_operation_pipeline(query_params)
        pipeline_stages.extend(match)   # match handler contains matching and unwind stages(if groupisanoPo param is osnivaca)

        # build group stage
        group = self.build_group_operation_pipeline(query_params)
        pipeline_stages.extend(group)   # group handler contains grouping and sorting stages

        # build project stage
        field_param = ''
        if query_params['podaci']['grupisanoPo'] == 'Godina':
            field_param = '_id.godine'

        elif query_params['podaci']['grupisanoPo'] == 'Preduzeća':
            field_param = '_id.preduzece'

        elif query_params['podaci']['grupisanoPo'] == 'Opština':
            field_param = '_id.nazivOpstine'

        elif query_params['podaci']['grupisanoPo'] == 'Delatnosti':
            field_param = '_id.delatnost'

        elif query_params['podaci']['grupisanoPo'] == 'Osnivači':
            field_param = '_id.osnivaci'


        project = self.build_project_pipeline(field_param, None)
        pipeline_stages.append(project)

        # execute mongo query
        aggregated_json_result = self.mongo.db.lokalnajavnapreduzeca.aggregate(pipeline_stages)
        for doc in aggregated_json_result['result']:
            doc['id'] = abs(randint(1, 2000))
            response['aggregated'].append(doc)


        disaggregation_stages = []
        #####   Let's build disaggregation form ######
        if query_params['podaci']['disaggregateBy'] == 'Godina':
            group[0]['$group']['_id']['godine'] = '$godine'
            sort_param = '_id.godine'

        elif query_params['podaci']['disaggregateBy'] == 'Preduzeća':
            group[0]['$group']['_id']['preduzece'] = '$preduzece.naziv'
            sort_param = '_id.preduzece'

        elif query_params['podaci']['disaggregateBy'] == 'Opština':
            group[0]['$group']['_id']['nazivOpstine'] = '$opstina.naziv.vrednost'
            sort_param = '_id.nazivOpstine'

        elif query_params['podaci']['disaggregateBy'] == 'Delatnosti':
            group[0]['$group']['_id']['delatnost'] = '$delatnost.naziv'
            sort_param = '_id.delatnost'

        elif query_params['podaci']['disaggregateBy'] == 'Osnivači':
            group[0]['$group']['_id']['osnivaci'] = '$osnivaci.imena.vrednost'
            sort_param = '_id.osnivaci'
            # build sorting stage for grouping pipeline of descriptions
        sort = {
            "$sort": SON([
                (field_param, 1),
                (sort_param, 1)
            ])
        }
        group[1] = sort
        disaggregation_stages.extend(match)
        disaggregation_stages.extend(group)

        project = self.build_project_pipeline(field_param, sort_param)
        disaggregation_stages.append(project)

        # execute mongo query
        disaggregated_json_result = self.mongo.db.lokalnajavnapreduzeca.aggregate(disaggregation_stages)

        for doc in disaggregated_json_result['result']:
            doc['id'] = abs(randint(1, 2000))
            response['disaggregated'].append(doc)

        return response


    def build_group_operation_pipeline(self, query_params):
        '''
        Let's build MongoDB group pipeline
        :param query_params: will enable us to group based on the what params we picked in the filtering bar
        :return: grouping params
        '''

        # this variable will be assigned when a grouping parameter is defined, and will be used in mongo sorting stage
        sort_param = ''
        pipeline_stages = []
        # define mongo group stage
        group = {'$group': {
            '_id': {}
        }}

        # Group by Godina
        if query_params['podaci']['grupisanoPo'] == 'Godina':
            group['$group']['_id']['godine'] = '$godine'
            group['$group']['value'] = self.check_which_field_to_calculate(query_params)
            sort_param = '_id.godine'

        # else Group by Preduzeća
        elif query_params['podaci']['grupisanoPo'] == 'Preduzeća':
            group['$group']['_id']['preduzece'] = '$preduzece.naziv'
            group['$group']['value'] = self.check_which_field_to_calculate(query_params)
            sort_param = '_id.preduzece'

        # else Group by Opština
        elif query_params['podaci']['grupisanoPo'] == 'Opština':
            group['$group']['_id']['nazivOpstine'] = '$opstina.naziv.vrednost'
            group['$group']['value'] = self.check_which_field_to_calculate(query_params)
            sort_param = '_id.nazivOpstine'

        # else Group by Delatnosti
        elif query_params['podaci']['grupisanoPo'] == 'Delatnosti':
            group['$group']['_id']['delatnost'] = '$delatnost.naziv'
            group['$group']['value'] = self.check_which_field_to_calculate(query_params)
            sort_param = '_id.delatnost'

        # else Group by Osnivači
        elif query_params['podaci']['grupisanoPo'] == 'Osnivači':
            group['$group']['_id']['osnivaci'] = '$osnivaci.imena.vrednost'
            group['$group']['value'] = self.check_which_field_to_calculate(query_params)
            sort_param = '_id.osnivaci'

        # build sorting stage for grouping pipeline of descriptions
        sort = {
            "$sort": SON([
                (sort_param, 1)
            ])
        }
        return [group, sort]


    def check_which_field_to_calculate(self, query_params):

        field  = ''
        # Calculate the sum of ukupni Prihodi
        if query_params['podaci']['selected'] == 'Ukupni prihodi':
            field = {"$sum": "$ukupniPrihodi.vrednost"}


        # Calculate the sum of ukupni Rashodi, based on selected values in the filtering box
        elif query_params['podaci']['selected'] == 'Ukupni rashodi':
            field = {"$sum": "$ukupniRashodi.vrednost"}


        # Calculate the sum of Poslovni Rashodi, based on selected values in the filtering box
        elif query_params['podaci']['selected'] == 'Poslovni rashodi':
            field = {"$sum": "$poslovniRashodi.vrednost"}

        # Calculate the sum of other categories, based on selected values in the filtering box Broj zaposlenih
        elif query_params['podaci']['selected'] == 'Broj zaposlenih':
            field = {"$sum": "$brojZaposlenih.vrednost"}

        return field

    def build_project_pipeline(self, group_param, disaggregation_param):
        '''

        :param group:
        :return:
        '''

        project = {
            "$project": {
                "_id": 0,
                'groupParam': "$" + group_param,
                'value': "$value"
            }

        }

        if disaggregation_param is not None:
            project['$project']['dataSeries'] = "$" + disaggregation_param

        return project


AbstractDataFeed.register(BubbleDataFeed)