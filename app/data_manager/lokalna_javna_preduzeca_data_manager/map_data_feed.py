# coding=utf-8
from bson.son import SON
from app.data_manager.lokalna_javna_preduzeca_data_manager.barchart_data_feed import BarchartDataFeed

# instantiate Barchart class object so we can use necessary class attributes in ScatterPlot Class
barchart_obj = BarchartDataFeed(None)

from abstract_datafeed import AbstractDataFeed
import abc

class MapDataFeed(object):

    mongo = None

    def __init__(self, mongo):
        self.mongo = mongo


    def get_data(self, query_params):

        pipeline_stages = []
        # build match pipeline
        match = barchart_obj.build_match_operation_pipeline(query_params)
        pipeline_stages.extend(match)

        # build group pipeline
        group = self.build_group_operation_pipeline(query_params)
        pipeline_stages.append(group)

        # build project pipeline
        project = self.build_project_operation_pipeline(query_params)
        pipeline_stages.append(project)

        json_result = self.mongo.db.lokalnajavnapreduzeca.aggregate(pipeline_stages)
        main_series =self.build_map_data_series_structure(json_result['result'])

        return main_series


    def build_map_data_series_structure(self, json_result):
        '''

        :param match:
        :param group:
        :return:
        '''

        data_series = []
        for item in json_result:
            json_obj = {}
            for key in item:
                if key == 'name':
                    json_obj['name'] = item[key]
                else:
                    if key == 'Ukupni prihodi':
                        json_obj['value'] = item[key]
                    else:
                        sum = 0
                        # calculate the total sum of selected options for a certain category
                        for element in item[key]:
                            sum += item[key][element]
                        json_obj['value'] = sum
            data_series.append(json_obj)
            # this is the json structure we are trying to build for map chart
            # {'name': 'Nis', 'value': 123455}

        return data_series


    def build_group_operation_pipeline(self, query_params):
        '''

        :param query_params:
        :return:
        '''

        # Build the group pipeline operator
        # We group by Municipality
        group = {'$group': {
            '_id': {'opstina': '$opstina.naziv.vrednost'}
        }}

        # check the query parameters you have chosen in filtering bars
        if query_params['podaci']['selected'] == "Ukupni prihodi":
            group['$group']['Ukupni Prihodi'] = {"$sum": "$ukupniPrihodi.vrednost"}


        if query_params['podaci']['selected'] == "Ukupni rashodi":

            # Based on the query field that is defined above,  let's iterate in the query json
            for param in query_params['podaci']['params']:

                if param == "AOP 207":
                    group['$group']['Poslovni rashodi'] = {"$sum": "$poslovniRashodi.vrednost"}

                elif  param == "AOP 216":
                    group['$group']['Finansijski rashodi'] = {"$sum": "$finansijskiRashodi.vrednost"}

                else:
                    group['$group']['Ostali rashodi'] = {"$sum": "$ostaliRashodi.vrednost"}


        if query_params['podaci']['selected'] == "Poslovni rashodi":

            for param in query_params['podaci']['params']:

                if param == "AOP 208":
                    group['$group']['Nabavna vrednost prodate robe'] = {"$sum": "$nabavnaVrednostProdateRobe.vrednost"}

                elif  param == "AOP 209":
                    group['$group']['Troskovi materijala'] = {"$sum": "$troskoviMaterijala.vrednost"}

                elif  param == "AOP 210":
                    group['$group']['Troškovi zarada, naknada zarada i ostali lični rashodi'] = {"$sum": "$troskoviZaradaNaknadaZaradaOstaliLicniRashodi.vrednost"}

                elif  param == "AOP 211":
                    group['$group']['Troskovi amortizacije i rezervisanja'] = {"$sum": "$troskoviAmortizacijeRezervisanja.vrednost"}

                else:
                    group['$group']['Ostali poslovni rashodi'] = {"$sum": "$ostaliPoslovniRashodi.vrednost"}


        if query_params['podaci']['selected'] == "Ostali":

            for param in query_params['podaci']['params']:

                if param == "AOP 216":
                    group['$group']['Finansijski rashodi'] = {"$sum": "$finansijskiRashodi.vrednost"}

                elif  param == "AOP 218":
                    group['$group']['Ostali rashodi'] = {"$sum": "$ostaliRashodi.vrednost"}

                elif  param == "AOP 229":
                    group['$group']['Neto dobitak'] = {"$sum": "$netoDobitak.vrednost"}

                elif param == "AOP 230":
                    group['$group']['Neto gubitak'] = {"$sum": "$netoGubitak.vrednost"}
                else:
                    pass

        return  group


    def build_project_operation_pipeline(self, query_params):
        '''

        :param query_params:
        :return:
        '''

        project = {
            "$project": {
                "_id": 0,
                'name': '$_id.opstina'
            }
        }

        if query_params['podaci']['selected'] == "Ukupni prihodi":
            project['$project']['Ukupni prihodi'] = '$Ukupni Prihodi'

        # Project Ukupni Rashodi sub categories inside project filed name ukupniRashodi
        if query_params['podaci']['selected'] == "Ukupni rashodi":

            project['$project']['Ukupni rashodi'] = {}


            for param in query_params['podaci']['params']:

                if param == "AOP 207":
                    project['$project']['Ukupni rashodi']['Poslovni rashodi'] = '$Poslovni rashodi'

                elif  param == "AOP 216":
                    project['$project']['Ukupni rashodi']['Finansijski rashodi'] = "$Finansijski rashodi"

                else:
                    project['$project']['Ukupni rashodi']['Ostali rashodi'] = "$Ostali rashodi"


        # Project Poslovni Rashodi sub categories inside project filed name poslovniRashodi
        if query_params['podaci']['selected'] == "Poslovni rashodi":

            project['$project']['Poslovni rashodi'] = {}

            for param in query_params['podaci']['params']:

                if param == "AOP 208":
                    project['$project']['Poslovni rashodi']['Nabavna vrednost prodate robe'] = "$Nabavna vrednost prodate robe"

                elif  param == "AOP 209":
                    project['$project']['Poslovni rashodi']['Troskovi materijala'] = "$Troskovi materijala"

                elif  param == "AOP 210":
                    project['$project']['Poslovni rashodi']['Troškovi zarada, naknada zarada i ostali lični rashodi'] = "$Troškovi zarada, naknada zarada i ostali lični rashodi"

                elif  param == "AOP 211":
                    project['$project']['Poslovni rashodi']['Troskovi amortizacije i rezervisanja'] = "$Troskovi amortizacije i rezervisanja"

                else:
                    project['$project']['Poslovni rashodi']['Ostali poslovni rashodi'] = "$Ostali poslovni rashodi"


        if query_params['podaci']['selected'] == "Ostali":

            project['$project']['Ostali'] = {}

            for param in query_params['podaci']['params']:

                if param == "AOP 216":
                    project['$project']['Ostali']['Finansijski rashodi'] = "$Finansijski rashodi"

                elif  param == "AOP 218":
                    project['$project']['Ostali']['Ostali rashodi'] = "$Ostali rashodi"

                elif  param == "AOP 229":
                    project['$project']['Ostali']['Neto dobitak'] = "$Neto dobitak"

                else:
                    project['$project']['Ostali']['Neto gubitak'] = "$Neto gubitak"

        return project

AbstractDataFeed.register(MapDataFeed)