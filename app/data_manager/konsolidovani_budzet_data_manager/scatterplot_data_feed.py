# coding=utf-8
import json, itertools
from collections import defaultdict, Counter

from bson.son import SON
from abstract_datafeed import AbstractDataFeed
import abc

class ScatterplotDataFeed(object):

    mongo = None

    def __init__(self, mongo):
        self.mongo = mongo


    def pipeline_match_operation_scatterplot(self, match_by):
        '''
        Let's build MongoDB match pipeline
        :param match_by: will enable us to filter based on the filtering params we picked in the filtering bar
        :return: complete filtering options
        '''

        # define match mongo operator
        match = {'$match': {}}
        # for scatterplot chart we are matching for the description types here
        if match_by['chartType'] == 'Scatterplot':
            if match_by['filters']['descriptions']['expenditures'] != [] or match_by['filters']['descriptions']['revenues'] != []:
                filter_array = match_by['filters']['descriptions']['expenditures'] + match_by['filters']['descriptions']['revenues']
                match['$match']['konta.id'] = {}
                match['$match']['konta.id'] = {"$in": filter_array}


        # match docs based on the document year, for all years we picked in the filtering box
        if match_by['filters']['years'] != []:
            match['$match']['godine'] = {"$in": match_by['filters']['years']}

        # match docs based on municipality name, for all municipalities we picked in the filtering box
        if match_by['filters']['municipalities']:
            match['$match']['opstina.latinica'] = {}
            match['$match']['opstina.latinica'] = {
                "$in": match_by['filters']['municipalities']
            }

        # return built match pipeline
        return match




    def get_data(self, query_params):
        '''

        :param query_params:
        :return:
        '''

        match = self.pipeline_match_operation_scatterplot(query_params)


        group = {
            "$group": {}
        }
        group['$group']['_id'] = {}
        group['$group']['_id']['prihodiRashodi'] = '$prihodRashod'

        main_series_array = []
        for series in query_params['data']['series']['scatterplot']:

            if series == "Years":
                sort_param = "_id.godine"
                database_key = 'godine'
                field_key = 'godine'

                data_series = self.build_scatterplot_chart_series_structure(query_params, field_key, match, group, sort_param, database_key)

                main_json = {
                    'name': 'Godine',
                    'color': 'rgba(223, 83, 83, .5)',
                    'data': data_series
                }
                main_series_array.append(main_json)

            elif series == "Municipalities":
                sort_param = "_id.opstina"
                database_key = 'opstina.latinica'
                field_key = 'opstina'

                data_series = self.build_scatterplot_chart_series_structure(query_params, field_key, match, group, sort_param, database_key)

                main_json = {
                    'name': 'Opstina',
                    'color': 'rgba(119, 152, 191, .5)',
                    'data': data_series
                }
                main_series_array.append(main_json)

            elif series == "Income Types":

                # calculate the sum of остали извори
                if 'Ostali izvori' in query_params['filters']['incomeTypes']:
                    group['$group']['Ostali izvori'] = {'$sum':"$izvoriPrihodaRashoda.ostaliIzvori.vrednost"}

                # ООСО
                if 'OOCO' in query_params['filters']['incomeTypes']:
                    group['$group']['OOCO'] = {'$sum':"$izvoriPrihodaRashoda.ooco.vrednost"}

                # аутономна покрајина
                if 'Autonomna pokrajina' in query_params['filters']['incomeTypes']:
                    group['$group']['Autonomna pokrajina'] = {'$sum':"$izvoriPrihodaRashoda.autonomnaPokrajina.vrednost"}

                # донације
                if 'Donacije' in query_params['filters']['incomeTypes']:
                    group['$group']['Donacije'] = {'$sum':"$izvoriPrihodaRashoda.donacije.vrednost"}

                # република
                if 'Republika' in query_params['filters']['incomeTypes']:
                    group['$group']['Republika'] = {'$sum':"$izvoriPrihodaRashoda.republika.vrednost"}

                # општина града
                if 'Opstina grada' in query_params['filters']['incomeTypes']:
                    group['$group']['Opstina grada'] = {'$sum':"$izvoriPrihodaRashoda.opstinaGrada.vrednost"}

                income_tps_docs = self.build_scatterplot_json_for_income_types(match, group)

                main_json = {
                    'name': 'Izvori Prihoda/Rashoda',
                    'color': 'rgba(32, 32, 32, .5)',
                    'data': income_tps_docs
                }

                main_series_array.append(main_json)

            # Not actually implemented, after long and brainy analysis
            elif series == 'Descriptions':
                sort_param = "_id.opisKonta"
                database_key = 'konta.opis.cirilica'
                field_key = 'opisKonta'

                data_series = self.build_scatterplot_chart_series_structure(query_params, field_key, match, group, sort_param, database_key)

                main_json = {
                    'name': 'Opis konta',
                    'color': 'rgba(102, 0, 51, .5)',
                    'data': data_series
                }

                main_series_array.append(main_json)


        return main_series_array

    def build_scatterplot_chart_series_structure(self, query_params, field_key, match, group, sort_param, database_key):
        '''
        :param json_result:
        :return:
        '''
        group['$group']['_id'][field_key] = '$' + database_key

        # if there is not selected any income type, sum up the total value
        if query_params['filters']['incomeTypes'] == []:
            group['$group']['value'] = {'$sum': "$izvoriPrihodaRashoda.ukupno.vrednost"}

        # else calculate the ones that are selected in the income types filtering box
        else:
            # calculate the sum of остали извори
            if 'Ostali izvori' in query_params['filters']['incomeTypes']:
                group['$group']['ostaliIzvori'] = {'$sum':"$izvoriPrihodaRashoda.ostaliIzvori.vrednost"}

            # ООСО
            if 'OOCO' in query_params['filters']['incomeTypes']:
                group['$group']['ooco'] = {'$sum':"$izvoriPrihodaRashoda.ooco.vrednost"}

            # аутономна покрајина
            if 'Autonomna pokrajina' in query_params['filters']['incomeTypes']:
                group['$group']['autonomnaPokrajina'] = {'$sum':"$izvoriPrihodaRashoda.autonomnaPokrajina.vrednost"}

            # донације
            if 'Donacije' in query_params['filters']['incomeTypes']:
                group['$group']['donacije'] = {'$sum':"$izvoriPrihodaRashoda.donacije.vrednost"}

            # република
            if 'Republika' in query_params['filters']['incomeTypes']:
                group['$group']['republika'] = {'$sum':"$izvoriPrihodaRashoda.republika.vrednost"}

            # општина града
            if 'Opstina grada' in query_params['filters']['incomeTypes']:
                group['$group']['opstinaGrada'] = {'$sum':"$izvoriPrihodaRashoda.opstinaGrada.vrednost"}


        # build sorting stage
        sort = {
            "$sort": SON([
                (sort_param, 1),
                ("_id.prihodiRashodi", 1)
            ])
        }
        pipeline_stages = [match, group, sort]
        scatter_plot_docs = self.mongo.db.konsolidovanibudzet.aggregate(pipeline_stages)
        if query_params['filters']['incomeTypes'] != []:
            sp_docs = self.calculate_total_sum_of_selected_description(scatter_plot_docs['result'])
        else:
            sp_docs = scatter_plot_docs['result']

        ds_array = []
        previous_field_value = ''
        for item in sp_docs:
            if item['_id'][field_key] != previous_field_value:
                json_obj = {
                    'label': item['_id'][field_key],
                    'x': item['value']
                }
                previous_field_value = item['_id'][field_key]
            else:
                json_obj['y'] = item['value']

            if len(json_obj) == 3 and field_key != 'opisKonta':
                ds_array.append(json_obj)

            elif len(json_obj) == 2 and field_key == 'opisKonta':
                ds_array.append(json_obj)

        return ds_array


    def build_scatterplot_json_for_income_types(self, match, group):
        '''

        :param match:
        :param group:
        :return:
        '''
        pipeline_stages = [match, group]
        scatter_plot_docs = self.mongo.db.konsolidovanibudzet.aggregate(pipeline_stages)

        sp_list = scatter_plot_docs['result']
        data_series = []

        first_list_dict = sp_list[0]

        dict_keys = []
        for key in first_list_dict.keys():
            dict_keys.append(key)

        for key in dict_keys:
            if key != '_id':
                data_series.append({'label': key, 'x': sp_list[0][key], 'y': sp_list[1][key]})

        return data_series

    def calculate_total_sum_of_selected_description(self, json_doc):
        main_json_array = []
        for doc in json_doc:
            json_obj = {}
            sum = 0
            for item in doc:
                if item != '_id':
                    sum += doc[item]
            json_obj = {
                '_id': doc['_id'],
                'value': sum
            }
            main_json_array.append(json_obj)

        return main_json_array

AbstractDataFeed.register(ScatterplotDataFeed)

