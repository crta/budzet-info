# coding=utf-8
from bson.son import SON

from abstract_datafeed import AbstractDataFeed
import abc

class BarchartDataFeed(object):

    mongo = None

    def __init__(self, mongo):
        self.mongo = mongo


    def pipeline_match_operation_barchart(self, match_by):
        '''
        Let's build MongoDB match pipeline
        :param match_by: will enable us to filter based on the filtering params we picked in the filtering bar
        :return: complete filtering options
        '''

        # define match mongo operator
        match = {'$match': {}}
        # we are going to filter based on fiscal type only when chart type is Bar chart
        if match_by['chartType'] == 'Barchart':
            # First we filter for the docs which contain fiscal type Revenue
            if match_by['data']['fiscalType']['barchart'] == 'Revenue':
                match['$match']['prihodRashod'] = 'prihodi'

                # if there are docs with revenue fiscal type, let's match the ones we picked in the description filtering box
                if match_by['filters']['descriptions']['revenues'] != []:
                    match['$match']['konta.id'] = {}
                    match['$match']['konta.id'] = {"$in": match_by['filters']['descriptions']['revenues']}

            # or Expenditure
            elif match_by['data']['fiscalType']['barchart'] == 'Expenditure':
                match['$match']['prihodRashod'] = 'rashodi'

                # if there are docs with expenditures fiscal type, let's match the ones we picked in the description filtering box
                if match_by['filters']['descriptions']['expenditures'] != []:
                    match['$match']['konta.id'] = {}
                    match['$match']['konta.id'] = {"$in": match_by['filters']['descriptions']['expenditures']}


        # match docs based on the document year, for all years we picked in the filtering box
        if match_by['filters']['years'] != []:
            match['$match']['godine'] = {"$in": match_by['filters']['years']}

        # match docs based on municipality name, for all municipalities we picked in the filtering box
        if match_by['filters']['municipalities']:
            match['$match']['opstina.latinica'] = {}
            match['$match']['opstina.latinica'] = {
                "$in": match_by['filters']['municipalities']
            }

        # return built match pipeline
        return match


    def pipeline_group_operation(self, query_params):
        '''
        Let's build MongoDB group pipeline
        :param query_params: will enable us to group based on the what params we picked in the filtering bar
        :return: grouping params
        '''

        # this variable will be assigned when a grouping parameter is defined, and will be used in mongo sorting stage
        sort_param = ''
        pipeline_stages = []
        group = {'$group': {
            '_id': {}
        }}

        # depending on what grouping by parameter we picked in filtering bar
        # group by Municipality
        if query_params['data']['groupBy'] == 'Municipality':
            group['$group']['_id']['opstina'] = '$opstina.latinica'
            sort_param = "_id.opstina"

        # or group by description
        elif query_params['data']['groupBy'] == 'Description':
            group['$group']['_id']['opis_konta'] = '$konta.opis.cirilica'
            sort_param = '_id.opis_konta'

        # or group by year
        elif query_params['data']['groupBy'] == 'Year':
            group['$group']['_id']['year'] = '$godine'
            sort_param = '_id.year'

        # or group by Income Types
        elif query_params['data']['groupBy'] == 'Income Type':

            # group by description first
            group['$group']['_id']['description'] = '$konta.opis.cirilica'

            # Knowing that we have several income types, we are able to group for every type that has been selected, so
            # let's group by any income type and calculate the sum of the description related to that income type...(1)
            # Income type: остали извори
            if 'Ostali izvori' in query_params['filters']['incomeTypes']:
                group['$group']['_id']["Ostali izvori"] = "$izvoriPrihodaRashoda.ostaliIzvori.polje.latinica"
                group['$group']['Ostali izvori'] = {'$sum':"$izvoriPrihodaRashoda.ostaliIzvori.vrednost"}

            # ООСО
            if 'OOCO' in query_params['filters']['incomeTypes']:
                group['$group']['_id']["OOCO"] = "$izvoriPrihodaRashoda.ooco.polje.latinica"
                group['$group']['OOCO'] = {'$sum':"$izvoriPrihodaRashoda.ooco.vrednost"}

            # аутономна покрајина
            if 'Autonomna pokrajina' in query_params['filters']['incomeTypes']:
                group['$group']['_id']["Autonomna pokrajina"] = "$izvoriPrihodaRashoda.autonomnaPokrajina.polje.latinica"
                group['$group']['Autonomna pokrajina'] = {'$sum':"$izvoriPrihodaRashoda.autonomnaPokrajina.vrednost"}

            # донације
            if 'Donacije' in query_params['filters']['incomeTypes']:
                group['$group']['_id']["Donacije"] = "$izvoriPrihodaRashoda.donacije.polje.latinica"
                group['$group']['Donacije'] = {'$sum':"$izvoriPrihodaRashoda.donacije.vrednost"}

            # република
            if 'Republika' in query_params['filters']['incomeTypes']:
                group['$group']['_id']["Republika"] = "$izvoriPrihodaRashoda.republika.polje.latinica"
                group['$group']['Republika'] = {'$sum':"$izvoriPrihodaRashoda.republika.vrednost"}

            # општина града
            if 'Opstina grada' in query_params['filters']['incomeTypes']:
                group['$group']['_id']["Opstina grada"] = "$izvoriPrihodaRashoda.opstinaGrada.polje.latinica"
                group['$group']['Opstina grada'] = {'$sum':"$izvoriPrihodaRashoda.opstinaGrada.vrednost"}

            pipeline_stages.append(group)

        # depending on the data series option we picked in filtering option, we have to be able to visualize data
        # related to the group by option, i.e. group by: Municipality then calculate the sum of income type
        if query_params['data']['series']['barchart'] == 'Income Types':

            # calculate the sum of остали извори
            if 'Ostali izvori' in query_params['filters']['incomeTypes']:
                group['$group']['Ostali izvori'] = {'$sum':"$izvoriPrihodaRashoda.ostaliIzvori.vrednost"}

            # ООСО
            if 'OOCO' in query_params['filters']['incomeTypes']:
                group['$group']['OOCO'] = {'$sum':"$izvoriPrihodaRashoda.ooco.vrednost"}

            # аутономна покрајина
            if 'Autonomna pokrajina' in query_params['filters']['incomeTypes']:
                group['$group']['Autonomna pokrajina'] = {'$sum':"$izvoriPrihodaRashoda.autonomnaPokrajina.vrednost"}

            # донације
            if 'Donacije' in query_params['filters']['incomeTypes']:
                group['$group']['Donacije'] = {'$sum':"$izvoriPrihodaRashoda.donacije.vrednost"}

            # република
            if 'Republika' in query_params['filters']['incomeTypes']:
                group['$group']['Republika'] = {'$sum':"$izvoriPrihodaRashoda.republika.vrednost"}

            # општина града
            if 'Opstina grada' in query_params['filters']['incomeTypes']:
                group['$group']['Opstina grada'] = {'$sum':"$izvoriPrihodaRashoda.opstinaGrada.vrednost"}

            # build sorting stage for grouping pipeline of descriptions
            sort = {
                "$sort": SON([
                    (sort_param, 1)
                ])
            }
            pipeline_stages = [group, sort]

        # or calculate the sum of description
        elif query_params['data']['series']['barchart'] == 'Descriptions' and query_params['data']['groupBy'] != 'Income Type':

            # first check if fiscal type is Expenditures
            if query_params['data']['fiscalType']['barchart'] == 'Expenditure' and query_params['filters']['descriptions']['expenditures'] != []:

                group['$group']['_id']['description'] = '$konta.opis.cirilica'

            # or Revenue
            elif query_params['data']['fiscalType']['barchart'] == 'Revenue' and query_params['filters']['descriptions']['revenues'] != []:
                group['$group']['_id']['description'] = '$konta.opis.cirilica'

            # do the calculation here, for every grouping by parameters beside Income Types, because of it's specification
            # grouping for income types is implemented above [case: (1)]
            if query_params['data']['groupBy'] != 'Income Type':
                group['$group']['value'] = {"$sum": "$izvoriPrihodaRashoda.ukupno.vrednost"}

            # build sorting stage for grouping pipeline of descriptions
            sort = {
                "$sort": SON([
                    (sort_param, 1),
                    ("_id.description", 1)
                ])
            }

            pipeline_stages = [group, sort]

        # return completed grouping stage
        return pipeline_stages

    def get_data(self, query_params):

        # Initialize match pipeline
        match = self.pipeline_match_operation_barchart(query_params)

        # Initialize group pipeline
        group = self.pipeline_group_operation(query_params)

        # Field parameter will be used to build json document based on which parameter we pick as grouping parameter
        # in group by option in filtering bar
        check_filed_parameter = ''
        if query_params['data']['groupBy'] == 'Municipality':
            check_filed_parameter = "opstina"

        elif query_params['data']['groupBy'] == 'Description':
            check_filed_parameter = 'opis_konta'

        elif query_params['data']['groupBy'] == 'Year':
            check_filed_parameter = 'year'


        # build aggregation pipeline for Income Types data series option
        if query_params['data']['series']['barchart'] == 'Income Types':

            pipeline = [match]
            pipeline.extend(group)

            # Execute mongo request
            docs = self.mongo.db.konsolidovanibudzet.aggregate(pipeline)

            # Restructure aggregation result so it can be rendered in the bar chart
            income_docs = self.build_json_for_income_types(docs['result'], check_filed_parameter)
            return income_docs

         # build aggregation pipeline for Descriptions data series option
        elif query_params['data']['series']['barchart'] == 'Descriptions':

            pipeline = [match]
            pipeline.extend(group)

            # Execute mongo request
            docs = self.mongo.db.konsolidovanibudzet.aggregate(pipeline)

            # JSON response object
            description_docs = {}
            if query_params['data']['groupBy'] != 'Income Type':

                #  Restructure aggregation result so it can be rendered in the bar chart
                description_docs = self.build_json_for_description(docs['result'], check_filed_parameter)
            elif query_params['data']['groupBy'] == 'Income Type':
                #  Restructure aggregation result so it can be rendered in the bar chart
                description_docs = self.build_json_for_income_types_grouping(docs['result'])

            # return json result from aggregation query
            return description_docs


    #  Restructures aggregation result for description data series, so those data can be rendered in the bar chart
    def build_json_for_description(self, json_document, check_filed_parameter):
        '''
        Returns a compatible JSON structure for bar chart data entry
        :param json_document: mongo aggregation query result
        :param check_filed_parameter: enable us to do manipulation for every grouping parameter we pick
        :return:  JSON document which contains categories and data series that barchart requires
        '''

        # categories array will contain values grouping parameter we picked
        categories = []

        # This array will contain json object of description and it's values related to the grouping parameter
        data_series =  []

        # this indexing variable enables us sync values of description with respective category
        ds_index = 0
        last_description_val = ""

        for item in json_document:
            json_obj = {
                'name': '',
                'data': []
            }
            if item['_id'][check_filed_parameter] not in categories:
                categories.append(item['_id'][check_filed_parameter])
                ds_index = 0

            if item['_id']['description'] != last_description_val:

                if len(categories) == 1:
                    json_obj['name'] = item['_id']['description']
                    json_obj['data'].append(item['value'])
                    data_series.append(json_obj)
                else:
                    data_series[ds_index]['data'].append(item['value'])

                last_description_val = item['_id']['description']

            elif item['_id']['description'] == last_description_val:
                data_series[ds_index]['data'].append(item['value'])

            ds_index += 1

        main_json = {'categories': categories, 'series': data_series}

        # Return JSON with categories and data series
        return main_json


    #  Restructures aggregation result for income types data series, so those data can be rendered in the bar chart
    def build_json_for_income_types(self, json_document, check_filed_parameter):
        '''
        Returns a compatible JSON structure for bar chart data entry
        :param json_document: mongo aggregation query result
        :param check_filed_parameter: enable us to do manipulation for every grouping parameter
        :return:  JSON document which contains categories and data series that barchart requires
        '''
        # categories array will contain values of grouping parameter we picked
        categories = []

        # This array will contain json object of description and it's values related to the grouping parameter
        data_series = []

        # this indexing variable enables us sync values of income types with respective category
        series_index = 0

        for element in json_document:
            if element['_id'][check_filed_parameter] not in categories:
                categories.append(element['_id'][check_filed_parameter])
                series_index = 0

            for item in element:

                if len(categories) == 1 and item != '_id':
                    json_obj = {
                        'name': '',
                        'data': []
                    }
                    json_obj['name'] = item
                    json_obj['data'].append(element[item])
                    data_series.append(json_obj)

                elif len(categories) > 1 and item != '_id':

                    data_series[series_index]['data'].append(element[item])

                    series_index += 1
        main_json = {'categories': categories, 'series': data_series}

        # Return JSON with categories and data series
        return main_json

    #  Restructures aggregation result for description data series when grouping parameter is Income types,
    # so those data can be rendered in the bar chart
    def build_json_for_income_types_grouping(self, json_document):
        '''
        Returns a compatible JSON structure for bar chart data entry
        :param json_document: mongo aggregation query result
        :return: JSON document which contains categories and data series that barchart requires
        '''

        # categories array will contain values grouping parameter we picked
        categories = []

        # This array will contain json object of description and it's values related to the grouping parameter(Income Types)
        data_series = []


        for element in json_document:
            json_obj = {
                'name': '',
                'data': []
            }
            for key, item in element.iteritems():
                if key == "_id":
                    for income_type in item:
                        if income_type == "description":
                            json_obj['name'] = item['description']

                else:
                    if key not in categories:
                        categories.append(key)
                    json_obj['data'].append(item)
            data_series.append(json_obj)

        main_json = {'categories': categories, 'series': data_series}

        # Return JSON with categories and data series
        return main_json

AbstractDataFeed.register(BarchartDataFeed)