# coding=utf-8

from abstract_datafeed import AbstractDataFeed


class MapDataFeed(object):

    mongo = None

    def __init__(self, mongo):
        self.mongo = mongo

    def get_data(self, query_params):
        '''
        Returns a JSON response listing all the data needed to render
        a map visualization.
        :param query_params: Data filter parameters selected by the user.
        :return: JSON result.
        '''

        # Initialize the last group pipeline operation.
        # We do it now because we may not need it hence it need to be initialized to None
        group_descriptions_sum = None

        # Build the $match pipeline operation
        match = {'$match': {}}

        # Match based on Revenue fiscal type.
        if query_params['data']['fiscalType']['map'] == 'Revenue':
            match['$match']['prihodRashod'] = 'prihodi'

            # Match based on selected Revenue descriptions.
            if query_params['filters']['descriptions']['revenues'] != []:
                match['$match']['konta.id'] = {}
                match['$match']['konta.id'] = {
                    "$in": query_params['filters']['descriptions']['revenues']
                }

        # Match based on Expenditure fiscal type.
        elif query_params['data']['fiscalType']['map'] == 'Expenditure':
            match['$match']['prihodRashod'] = 'rashodi'

            # Match based on selected Expenditure descriptions.
            if query_params['filters']['descriptions']['expenditures'] != []:
                match['$match']['konta.id'] = {}
                match['$match']['konta.id'] = {
                    "$in": query_params['filters']['descriptions']['expenditures']
                }

        # Match based on selected years.
        if query_params['filters']['years']:
            match['$match']['godine'] = {
                "$in": query_params['filters']['years']
            }

        # Match based on selected municipalities.
        if query_params['filters']['municipalities']:
            match['$match']['opstina.latinica'] = {
                "$in": query_params['filters']['municipalities']
            }

        # Build the group pipeline operator
        # We group by Municipality
        group = {'$group': {
            '_id': {'opstina': '$opstina.latinica'}
        }}

        # For each municipality, we either calculate the sum or its different income types...
        if query_params['data']['series']['map'] == 'Income Types':

            # calculate the sum of остали извори
            if 'Ostali izvori' in query_params['filters']['incomeTypes']:
                group['$group']['ostaliIzvori'] = {'$sum':"$izvoriPrihodaRashoda.ostaliIzvori.vrednost"}

            # ООСО
            if 'OOCO' in query_params['filters']['incomeTypes']:
                group['$group']['ooco'] = {'$sum':"$izvoriPrihodaRashoda.ooco.vrednost"}

            # аутономна покрајина
            if 'Autonomna pokrajina' in query_params['filters']['incomeTypes']:
                group['$group']['autonomnaPokrajina'] = {'$sum':"$izvoriPrihodaRashoda.autonomnaPokrajina.vrednost"}

            # донације
            if 'Donacije' in query_params['filters']['incomeTypes']:
                group['$group']['donacije'] = {'$sum':"$izvoriPrihodaRashoda.donacije.vrednost"}

            # република
            if 'Republika' in query_params['filters']['incomeTypes']:
                group['$group']['republika'] = {'$sum':"$izvoriPrihodaRashoda.republika.vrednost"}

            # општина града
            if 'Opstina grada' in query_params['filters']['incomeTypes']:
                group['$group']['opstinaGrada'] = {'$sum':"$izvoriPrihodaRashoda.opstinaGrada.vrednost"}

        # Or the sum of the descriptions...
        elif query_params['data']['series']['map'] == 'Descriptions':
            if query_params['data']['fiscalType']['map'] == 'Expenditure' and query_params['filters']['descriptions']['expenditures'] != []:
                group['$group']['_id']['description'] = '$konta.id'

            elif query_params['data']['fiscalType']['map'] == 'Revenue' and query_params['filters']['descriptions']['revenues'] != []:
                group['$group']['_id']['description'] = '$konta.id'

            group['$group']['value'] = {"$sum": "$izvoriPrihodaRashoda.ukupno.vrednost"}

            # Calculate the sum of all values of all the selected descriptions
            group_descriptions_sum = {
                '$group': {
                    "_id":  {"opstina": "$_id.opstina"},
                    "value": {"$sum": "$value"}
                }
            }

        # Build the pipeline
        pipeline = [match, group]
        if group_descriptions_sum:
            pipeline.append(group_descriptions_sum)

        # Execute the query.
        docs = self.mongo.db.konsolidovanibudzet.aggregate(pipeline)

        # Return the results
        return docs['result']

AbstractDataFeed.register(MapDataFeed)
