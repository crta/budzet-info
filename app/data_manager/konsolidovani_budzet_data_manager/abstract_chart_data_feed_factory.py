

class AbstractChartDataFeedFactoryKB(object):

    def __init__(self, data_factory=None):
        self._data_factory = data_factory

    def get_chart_data(self, query_params):

        ''' Invokes the appropriate data fetching function based on given chart type.

        :param query_params: JSON document which contains all mongo query parameters required to build mongo request
        :return: based on the chart type, return the respective JSON response
        '''

        # If no chart type is defined, then just return a generic JSON
        # response.

        if 'chartType' not in query_params:

            datafeed_obj = self._data_factory.get_simple_datafeed_provider()

        elif query_params['chartType'] == 'Barchart':

            datafeed_obj = self._data_factory.get_barchart_datafeed_provider()

        elif query_params['chartType'] == 'Map':

            datafeed_obj = self._data_factory.get_map_datafeed_provider()

        elif query_params['chartType'] == 'Scatterplot':

            datafeed_obj = self._data_factory.get_scatterplot_datafeed_provider()

        elif query_params['chartType'] == 'Floating Bubbles':

            datafeed_obj = self._data_factory.get_bubble_datafeed_provider()


        return datafeed_obj.get_data(query_params)