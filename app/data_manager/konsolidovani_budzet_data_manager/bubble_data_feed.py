# coding=utf-8
from bson.son import SON
from abstract_datafeed import AbstractDataFeed
from random import randint


class BubbleDataFeed(object):

    mongo = None

    def __init__(self, mongo):
        self.mongo = mongo


    def get_data(self, query_params):
        '''
        :param query_params:
        :return:
        '''
        response = {
            'aggregated': [],
            'disaggregated': []
        }

        # build mongo pipelines
        match = self.pipeline_match_operation_floating_bubbles(query_params)
        group_sort_pipes = self.pipeline_group_operation(query_params)

        pipeline_stages = [match] + group_sort_pipes
        # build aggregation pipeline for Income Types data series option
        if query_params['data']['series']['floatingBubbles'] == 'Income Types':

            # Execute mongo request
            docs = self.mongo.db.konsolidovanibudzet.aggregate(pipeline_stages)

            grouping_param = ''
            # Restructure aggregation result so it can be rendered in the bar chart
            if query_params['data']['floatingBubblesGroupBy'] == 'Municipality':
                grouping_param = "opstina"

            # or group by description
            elif query_params['data']['floatingBubblesGroupBy'] == 'Description':
                grouping_param = 'opis_konta'

            # or group by year
            elif query_params['data']['floatingBubblesGroupBy'] == 'Year':
                grouping_param = 'year'

            json_result = self.build_json_for_income_types(docs['result'], grouping_param, None)
            response['aggregated'].extend(json_result)

            sort_param = ''
            disaggregation_param = ''
            disaggregation_pipeline_stages = pipeline_stages
            # here we will check for disaggregation params so that we can group based on that
            if query_params['data']['disaggregateBy'] == 'Municipality':
                disaggregation_param = 'opstina'
                sort_param = '_id.' + disaggregation_param
                disaggregation_pipeline_stages[1]['$group']['_id']['opstina'] = '$opstina.latinica'

            elif query_params['data']['disaggregateBy'] == 'Description':
                disaggregation_param = 'description'
                sort_param = '_id.' + disaggregation_param
                disaggregation_pipeline_stages[1]['$group']['_id']['description'] ='$konta.opis.cirilica'

            elif query_params['data']['disaggregateBy'] == 'Year':
                disaggregation_param = 'year'
                sort_param = '_id.' + disaggregation_param
                disaggregation_pipeline_stages[1]['$group']['_id']['year'] = '$godine'

            disaggregation_pipeline_stages[2]= {
                '$sort':  SON([
                    (grouping_param, 1),
                    (sort_param, 1)
                ])
            }

            # Execute mongo request
            disaggregated_docs = self.mongo.db.konsolidovanibudzet.aggregate(disaggregation_pipeline_stages)
            disaggregated_docs_result = self.build_json_for_income_types(disaggregated_docs['result'], grouping_param, disaggregation_param)
            response['disaggregated'].extend(disaggregated_docs_result)


        # build aggregation pipeline for Descriptions data series option
        elif query_params['data']['series']['floatingBubbles'] == 'Descriptions':

            # execute mongo query
            json_result = self.mongo.db.konsolidovanibudzet.aggregate(pipeline_stages)

            for doc in json_result['result']:
                doc['id'] = abs(randint(1, 2000))
                response['aggregated'].append(doc)


            # Let's build disaggregation json response
            disaggregation_group_pipes = group_sort_pipes
            if query_params['data']['disaggregateBy'] == 'Municipality':
                disaggregation_group_pipes[0]['$group']['_id']['opstina'] = '$opstina.latinica'
                disaggregation_group_pipes[1] = {
                    '$sort':  SON([
                        ("_id.opstina", 1)
                    ])
                }
                disaggregation_group_pipes[2]['$project']['dataSeries'] = '$_id.opstina'


            # or group by description
            elif query_params['data']['disaggregateBy'] == 'Description':

                disaggregation_group_pipes[0]['$group']['_id']['description'] ='$konta.opis.cirilica'
                disaggregation_group_pipes[1] = {
                    '$sort':  SON([
                        ("_id.description", 1)
                    ])
                }
                disaggregation_group_pipes[2]['$project']['dataSeries'] = '$_id.description'

            # or group by year
            elif query_params['data']['disaggregateBy'] == 'Year':
                disaggregation_group_pipes[0]['$group']['_id']['year'] = '$godine'
                disaggregation_group_pipes[1] = {
                    '$sort':  SON([
                        ("_id.year", 1)
                    ])
                }
                disaggregation_group_pipes[2]['$project']['dataSeries'] = '$_id.year'


            pipelines = [match] + disaggregation_group_pipes
            # execute mongo query
            disaggregated_json_result = self.mongo.db.konsolidovanibudzet.aggregate(pipelines)

            for doc in disaggregated_json_result['result']:
                doc['id'] = abs(randint(1, 2000))
                if doc['value'] != 0:
                    response['disaggregated'].append(doc)

        return response


    def pipeline_match_operation_floating_bubbles(self, match_by):
        '''
        Let's build MongoDB match pipeline
        :param match_by: will enable us to filter based on the filtering params we picked in the filtering bar
        :return: complete filtering options
        '''

        # define match mongo operator
        match = {'$match': {}}
        # First we filter for the docs which contain fiscal type Revenue
        if match_by['data']['fiscalType']['floatingBubbles'] == 'Revenue':
            match['$match']['prihodRashod'] = 'prihodi'

            # if there are docs with revenue fiscal type, let's match the ones we picked in the description filtering box
            if match_by['filters']['descriptions']['revenues'] != []:
                match['$match']['konta.id'] = {}
                match['$match']['konta.id'] = {"$in": match_by['filters']['descriptions']['revenues']}

        # or Expenditure
        elif match_by['data']['fiscalType']['floatingBubbles'] == 'Expenditure':
            match['$match']['prihodRashod'] = 'rashodi'

            # if there are docs with expenditures fiscal type, let's match the ones we picked in the description filtering box
            if match_by['filters']['descriptions']['expenditures'] != []:
                match['$match']['konta.id'] = {}
                match['$match']['konta.id'] = {"$in": match_by['filters']['descriptions']['expenditures']}


        # match docs based on the document year, for all years we picked in the filtering box
        if match_by['filters']['years'] != []:
            match['$match']['godine'] = {"$in": match_by['filters']['years']}

        # match docs based on municipality name, for all municipalities we picked in the filtering box
        if match_by['filters']['municipalities']:
            match['$match']['opstina.latinica'] = {}
            match['$match']['opstina.latinica'] = {
                "$in": match_by['filters']['municipalities']
            }

        # return built match pipeline
        return match


    def pipeline_group_operation(self, query_params):
        '''
        Let's build MongoDB group pipeline
        :param query_params: will enable us to group based on the what params we picked in the filtering bar
        :return: grouping params
        '''

        grouping_param = ''
        series_param = ''
        pipeline_stages = []
        group = {'$group': {
            '_id': {}
        }}

        # depending on what grouping by parameter we picked in filtering bar
        # group by Municipality
        if query_params['data']['floatingBubblesGroupBy'] == 'Municipality':
            group['$group']['_id']['opstina'] = '$opstina.latinica'
            grouping_param = "_id.opstina"

        # or group by description
        elif query_params['data']['floatingBubblesGroupBy'] == 'Description':
            group['$group']['_id']['opis_konta'] = '$konta.opis.cirilica'
            grouping_param = '_id.opis_konta'

        # or group by year
        elif query_params['data']['floatingBubblesGroupBy'] == 'Year':
            group['$group']['_id']['year'] = '$godine'
            grouping_param = '_id.year'


        # depending on the data series option we picked in filtering option, we have to be able to visualize data
        # related to the group by option, i.e. group by: Municipality then calculate the sum of income type
        if query_params['data']['series']['floatingBubbles'] == 'Income Types':

            # calculate the sum of остали извори
            if 'Ostali izvori' in query_params['filters']['incomeTypes']:
                group['$group']['Ostali izvori'] = {'$sum':"$izvoriPrihodaRashoda.ostaliIzvori.vrednost"}

            # ООСО
            if 'OOCO' in query_params['filters']['incomeTypes']:
                group['$group']['OOCO'] = {'$sum':"$izvoriPrihodaRashoda.ooco.vrednost"}

            # аутономна покрајина
            if 'Autonomna pokrajina' in query_params['filters']['incomeTypes']:
                group['$group']['Autonomna pokrajina'] = {'$sum':"$izvoriPrihodaRashoda.autonomnaPokrajina.vrednost"}

            # донације
            if 'Donacije' in query_params['filters']['incomeTypes']:
                group['$group']['Donacije'] = {'$sum':"$izvoriPrihodaRashoda.donacije.vrednost"}

            # република
            if 'Republika' in query_params['filters']['incomeTypes']:
                group['$group']['Republika'] = {'$sum':"$izvoriPrihodaRashoda.republika.vrednost"}

            # општина града
            if 'Opstina grada' in query_params['filters']['incomeTypes']:
                group['$group']['Opstina grada'] = {'$sum':"$izvoriPrihodaRashoda.opstinaGrada.vrednost"}

            # build sorting stage for grouping pipeline of descriptions
            sort = {
                "$sort": SON([
                    (grouping_param, 1)
                ])
            }
            pipeline_stages = [group, sort]

            # return completed grouping stage
            return pipeline_stages

        # or calculate the sum of description
        elif query_params['data']['series']['floatingBubbles'] == 'Descriptions' and query_params['data']['floatingBubblesGroupBy'] != 'Income Type':

            # do the calculation here, for every grouping by parameters beside Income Types, because of it's specification
            # grouping for income types is implemented above [case: (1)]
            if query_params['data']['floatingBubblesGroupBy'] != 'Income Type':
                group['$group']['value'] = {"$sum": "$izvoriPrihodaRashoda.ukupno.vrednost"}
                series_param = "value"

            # build sorting stage for grouping pipeline of descriptions
            sort = {
                "$sort": SON([
                    (grouping_param, 1)
                ])
            }

            pipeline_stages = [group, sort]

            project = self.build_project_pipeline(grouping_param, series_param, None)
            pipeline_stages.append(project)


            # return completed grouping stage
            return pipeline_stages

    def build_project_pipeline(self, group_param, series_param, disaggregation_param):
        '''

        :param group:
        :return:
        '''

        project = {
            "$project": {
                "_id": 0,
                'groupParam': "$" + group_param,
                'value': "$" + series_param
            }

        }

        return project


    def build_json_for_income_types(self, json_docs, grouping_param, disaggregated_by):
        '''

        :param json_docs:
        :param grouping_param:
        :return:
        '''

        main_json_array = []

        for doc in json_docs:
            json_obj = {}
            sum = 0
            for key, value in doc.items():
                if key == "_id":
                    json_obj['groupParam'] = value[grouping_param]
                    if disaggregated_by is not None:
                        json_obj['dataSeries'] = value[disaggregated_by]
                else:
                    sum += value
            if sum != 0:
                json_obj['value'] = sum
                json_obj['id'] = abs(randint(1, 2000))

                main_json_array.append(json_obj)


        return main_json_array


AbstractDataFeed.register(BubbleDataFeed)