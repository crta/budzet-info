from slugify import slugify
from bson import json_util

from abstract_datafeed import AbstractDataFeed

class SimpleDataFeed(object):

    mongo = None

    def __init__(self, mongo):
        self.mongo = mongo

    def get_data(self, query_params):


        match = {}

        sort_param = ""
        if query_params['sort'] == "godine":
            sort_param = 'godine'
        elif query_params['sort'] == "opstina":
            sort_param = "opstina.slug"
        elif query_params['sort'] =="prihodRashod":
            sort_param = "prihodRashod"
        elif query_params['sort'] == "konta":
            sort_param = "konta.id"

        # Years
        if 'godine' in query_params:
            match['godine'] = {
                "$in": query_params['godine']
            }

        # Municipalities
        if 'opstina' in query_params:
            match['opstina.slug'] = {
                "$in": [slugify(n, to_lower=True) for n in query_params['opstina']]
            }

        # Activities
        if 'prihodRashod' in query_params:
            match['prihodRashod'] = query_params['prihodRashod']

        # Accounts
        if 'konta' in query_params:
            match['konta.id'] = {
                "$in": query_params['konta']
            }
        # TODO: Implement order by.

        response = self.mongo.db.konsolidovanibudzet.find(match).sort([(sort_param, 1)])

        return json_util.dumps(response)

AbstractDataFeed.register(SimpleDataFeed)