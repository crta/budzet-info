from flask import Flask
import os
import ConfigParser
from logging.handlers import RotatingFileHandler
from flask.ext.pymongo import PyMongo

from app.data_manager.konsolidovani_budzet_data_manager.chart_data_feed_factory import ChartDataFeedFactoryKB
from app.data_manager.konsolidovani_budzet_data_manager.abstract_chart_data_feed_factory import AbstractChartDataFeedFactoryKB
from app.data_manager.lokalna_javna_preduzeca_data_manager.chart_data_feed_factory import ChartDataFeedFactoryLJP
from app.data_manager.lokalna_javna_preduzeca_data_manager.abstract_chart_data_feed_factory import AbstractChartDataFeedFactoryLJP



# Create MongoDB database object.
mongo = PyMongo()

data_manager_kb = AbstractChartDataFeedFactoryKB(ChartDataFeedFactoryKB(mongo))
data_manager_ljp = AbstractChartDataFeedFactoryLJP(ChartDataFeedFactoryLJP(mongo))



def create_app():
    # Here we  create flask instance
    app = Flask(__name__)

    # Load application configurations
    load_config(app)

    # Configure logging.
    configure_logging(app)

    # Init modules
    init_modules(app)

    # Initialize the app to work with MongoDB
    mongo.init_app(app, config_prefix='MONGO')

    return app


def load_config(app):
    ''' Reads the config file and loads configuration properties into the Flask app.
    :param app: The Flask app object.
    '''
    # Get the path to the application directory, that's where the config file resides.
    par_dir = os.path.join(__file__, os.pardir)
    par_dir_abs_path = os.path.abspath(par_dir)
    app_dir = os.path.dirname(par_dir_abs_path)

    # Read config file
    config = ConfigParser.RawConfigParser()
    config_filepath = app_dir + '/config.cfg'
    config.read(config_filepath)

    app.config['HOST'] = config.get('Application', 'HOST')
    app.config['SERVER_PORT'] = config.get('Application', 'SERVER_PORT')
    app.config['BITLY_API_KEY'] = config.get('Application', 'BITLY_API_KEY')
    app.config['BITLY_LOG_IN'] = config.get('Application', 'BITLY_LOG_IN')
    app.config['MONGO_DBNAME'] = config.get('Mongo', 'DB_NAME')

    # Set the secret key, keep this really secret, we use it to secure wtform filed data
    app.secret_key = config.get('Application', 'SECRET_KEY')

    # Logging path might be relative or starts from the root.
    # If it's relative then be sure to prepend the path with the application's root directory path.
    log_path = config.get('Logging', 'PATH')
    if log_path.startswith('/'):
        app.config['LOG_PATH'] = log_path
    else:
        app.config['LOG_PATH'] = app_dir + '/' + log_path

    app.config['LOG_LEVEL'] = config.get('Logging', 'LEVEL').upper()


def configure_logging(app):
    ''' Configure the app's logging.
     param app: The Flask app object
    '''

    log_path = app.config['LOG_PATH']
    log_level = app.config['LOG_LEVEL']

    # If path directory doesn't exist, create it.
    log_dir = os.path.dirname(log_path)
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    # Create and register the log file handler.
    log_handler = RotatingFileHandler(log_path, maxBytes=250000, backupCount=5)
    log_handler.setLevel(log_level)
    app.logger.addHandler(log_handler)

    # First log informs where we are logging to.
    # Bit silly but serves  as a confirmation that logging works.
    app.logger.info('Logging to: %s', log_path)


def init_modules(app):

    # Import blueprint modules
    from app.mod_landing_page.views import mod_landing_page
    from app.mod_api.views import mod_api
    from app.mod_konsolidovani_budzet.views import mod_konsolidovani_budzet
    from app.mod_lokalna_javna_produzeca.views import mod_lokalna_javna_produzeca
    from app.mod_admin.views import mod_admin

    app.register_blueprint(mod_landing_page)
    app.register_blueprint(mod_api)
    app.register_blueprint(mod_konsolidovani_budzet)
    app.register_blueprint(mod_lokalna_javna_produzeca)
    app.register_blueprint(mod_admin)
