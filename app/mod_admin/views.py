# -*- coding: utf-8 -*-
from flask import Blueprint, render_template, request, redirect, current_app, session, url_for
from flask.ext.scrypt import check_password_hash
from bson import ObjectId
from app.mod_admin.forms.form import AdminForm
from app import mongo

mod_admin = Blueprint('admin', __name__)

@mod_admin.route('/admin/panel', methods=['GET'])
def panel():
    if 'logged_in' in session:
        form = AdminForm()

        charts_doc = list(mongo.db.charts.find())
        return render_template('mod_admin/admin_panel.html', form=form, charts_doc=charts_doc)
    else:
        return redirect(url_for('admin.login'))

@mod_admin.route('/odabrani-grafikoni', methods=['GET'])
def featured_charts():

    charts_doc = list(mongo.db.charts.find())

    return render_template('mod_admin/featured-charts.html', charts_doc=charts_doc)

@mod_admin.route('/admin/add-chart', methods=['POST'])
def addchart():

    form = AdminForm(request.form)

    chart_doc = {
        "title": form.title.data,
        "chart_type": form.chart_type.data,
        "description": form.description.data,
        "share_link": form.share_link.data
    }
    mongo.db.charts.insert(chart_doc)
    return redirect(url_for('admin.panel'))

@mod_admin.route('/admin/edit-chart', methods=['POST'])
def editchart():
    form = AdminForm(request.form)

    chart_id = form.chart_id.data
    edited_doc = {
        "title": form.title.data,
        "chart_type": form.chart_type.data,
        "description": form.description.data,
        "share_link": form.share_link.data
    }

    mongo.db.charts.update(
        {"_id": ObjectId(chart_id)},
        {
            "$set": edited_doc
        }
    )

    return redirect(url_for('admin.panel'))

@mod_admin.route('/admin/delete-chart/<string:chart_id>', methods=['GET'])
def delete_chart(chart_id):

    mongo.db.charts.remove({"_id": ObjectId(chart_id)})

    return redirect(url_for('admin.panel'))



@mod_admin.route('/admin/log-in', methods=['GET', 'POST'])
def login():
    error = None
    form = AdminForm(request.form)

    if request.method == "POST":

        username = form.username.data
        password = form.password.data
        user_doc = mongo.db.users.find_one({"username": username})

        if user_doc is None:
            error = "Korisnik ne postoji"
        elif password == "" or not check_password_hash(password, user_doc["password"], user_doc["salt"]):
            error = "Pogrešna lozinka"

        else:
            session['logged_in'] = True
            session['username'] = username
            return redirect(url_for('admin.panel'))

        return render_template('mod_admin/login_form.html', form=form, error=error)

    elif request.method == "GET":

        return render_template('mod_admin/login_form.html', form=form, error=error)


@mod_admin.route('/admin/log-out', methods=['GET'])
def logout():
    if 'logged_in' in session and session['logged_in'] is True:
        session.pop('logged_in', None)

        username = session['username']
        session.pop('username', None)

        current_app.logger.info('user %s is logged out' % username)

        return redirect(url_for('landing_page.index'))

    else:
        return redirect(url_for('admin.login'))


