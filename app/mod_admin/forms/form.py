# coding=utf-8
from wtforms import Form, StringField, PasswordField, TextAreaField, TextField, validators, HiddenField, SelectField
from wtforms.validators import Required

class AdminForm(Form):
    username = StringField('Username', validators=[Required()])
    password = PasswordField('Password', validators=[Required()])

    title = TextField('Title', [validators.required(), validators.length(max=10)])
    chart_type = SelectField('Tip grafikona', choices=[('Barchart', 'Barchart'),('Floating Bubbles', 'Floating Bubbles'),('Map', 'Mapa'),('Scatterplot', 'Scatterplot')])
    description = TextAreaField('Description', [validators.optional(), validators.length(max=200)])
    share_link = TextField('Share Link', [validators.required(), validators.length(max=10)])
    chart_id = HiddenField('Chart ID')
