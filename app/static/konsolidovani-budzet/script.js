// Constants that we use to resize chart div container height
var BAR_CHART_FOOTER_HEIGHT = 150;
var BAR_CHART_COLUMN_WIDTH = 60;
var BAR_CHART_GROUPING_WIDTH = 120;

function showHideDescriptions(triggerElemId){
    if($("#chart-type-dd").val() === "Scatterplot"){
        $("#revenue-descriptions-container").show();
        $("#expenditure-descriptions-container").show();

    }else if($("#" + triggerElemId).val() === "Revenue"){
        $("#revenue-descriptions-container").show();
        $("#expenditure-descriptions-container").hide();

    }else if($("#" + triggerElemId).val() === "Expenditure"){
        $("#revenue-descriptions-container").hide();
        $("#expenditure-descriptions-container").show();
    }
}

function initFiscalTypeDropdowns(){
    $("#fiscal-type-map").change(function() {
        showHideDescriptions('fiscal-type-map');
    });

    $("#fiscal-type-barchart").change(function() {
        showHideDescriptions('fiscal-type-barchart');
    });

    $("#fiscal-type-scatterplot").change(function() {
        showHideDescriptions('fiscal-type-scatterplot');
    });

    $("#fiscal-type-floating-bubbles").change(function() {
        showHideDescriptions('fiscal-type-floating-bubbles');
    });
}

function initChartTypeDropdown(){
    $( "#chart-type-dd" ).change(function() {
        if($( "#chart-type-dd" ).val() === "Scatterplot"){
            $('#all').removeClass('active');
            $(".cbp-spmenu-vertical-item-parent[data-section='data-scatterplot']").show();
            $(".cbp-spmenu-vertical-item-child[data-section='data-scatterplot']").show()

            $(".cbp-spmenu-vertical-item-parent[data-section='data-barchart']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-barchart']").hide();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-map']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-map']").hide();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-floating-bubbles']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-floating-bubbles']").hide();

            showHideDescriptions('fiscal-type-scatterplot');

        }else if($( "#chart-type-dd" ).val() === "Barchart"){
            $('#all').removeClass('active');
            $(".cbp-spmenu-vertical-item-parent[data-section='data-barchart']").show();
            $(".cbp-spmenu-vertical-item-child[data-section='data-barchart']").show();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-scatterplot']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-scatterplot']").hide();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-map']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-map']").hide();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-floating-bubbles']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-floating-bubbles']").hide();

            showHideDescriptions('fiscal-type-barchart');

        }else if($( "#chart-type-dd" ).val() === "Map"){
            $('#all').removeClass('active');
            $(".cbp-spmenu-vertical-item-parent[data-section='data-map']").show();
            $(".cbp-spmenu-vertical-item-child[data-section='data-map']").show();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-barchart']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-barchart']").hide();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-scatterplot']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-scatterplot']").hide();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-floating-bubbles']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-floating-bubbles']").hide();

            showHideDescriptions('fiscal-type-map');

        }else if($( "#chart-type-dd" ).val() === "Floating Bubbles"){

            $(".cbp-spmenu-vertical-item-parent[data-section='data-map']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-map']").hide();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-barchart']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-barchart']").hide();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-scatterplot']").hide();
            $(".cbp-spmenu-vertical-item-child[data-section='data-scatterplot']").hide();

            $(".cbp-spmenu-vertical-item-parent[data-section='data-floating-bubbles']").show();
            $(".cbp-spmenu-vertical-item-child[data-section='data-floating-bubbles']").show();

            showHideDescriptions('fiscal-type-floating-bubbles');
        }
    });
}

function run_waitMe(effect){
        $('#main, #chart-container').waitMe({

        //none, rotateplane, stretch, orbit, roundBounce, win8,
        //win8_linear, ios, facebook, rotation, timer, pulse,
        //progressBar, bouncePulse or img
        effect: 'roundBounce',

        //place text under the effect (string).
        text: '',

        //background for container (string).
        bg: 'rgba(255,255,255,0.7)',

        //color for background animation and text (string).
        color: '#000',

        //change width for elem animation (string).
        sizeW: '',

        //change height for elem animation (string).
        sizeH: '',

        // url to image
        source: ''

        });

}

function initVisualizeButton(){
    $( "#visualize-button" ).click(function() {

        $( ".toollabel" ).remove();
        $('#floatingLabels').hide();
        // Before we do so, let's build our POST request's message body.
        var selected_elem_arr = [];
        if($( "#chart-type-dd" ).val() == "Floating Bubbles"){
            if($('#disaggregate-by').val() == 'Year'){
                selected_elem_arr = getSelectedTreeElement('tree_years');
            }
            else if ($('#disaggregate-by').val() == 'Description') {
                if($('#fiscal-type-floating-bubbles').val() == 'Expenditure'){
                    selected_elem_arr = getSelectedTextElement('tree_expenditure_descriptions');
                }
                else{
                    selected_elem_arr = getSelectedTextElement('tree_revenue_descriptions');
                }

            }
            else if ($('#disaggregate-by').val() == 'Municipality') {
                selected_elem_arr = getSelectedTreeElement('tree_municipalities');
            }

            if(selected_elem_arr.length > 4){
                $( "#modalMessage" ).empty();
                $( "#modalMessage" ).append("<p style='font-size: large;'>Molimo vas da ponovo odaberete filtere. Balončići prikazuju podatke za najviše 4 odabrana filtera.</p>");
                $("#myModal").modal('show');
                $('#main, #chart-container').waitMe('hide');
            }
            else {
                if(selected_elem_arr.length == 0){
                    $( "#modalMessage" ).empty();
                    $( "#modalMessage" ).append("<p style='font-size: large;'>Za izabrani parametar razvrstavanja (iz polja 'razvrstano po') odaberite odgovarajuće filtere (u polju 'filteri'). Balončići prikazuju podatke za najviše 4 odabrana filtera.</p>");
                    $("#myModal").modal('show');
                    $('#main, #chart-container').waitMe('hide');
                }
                else{
                    for(var i=0; i<selected_elem_arr.length; i++){
                        if (selected_elem_arr.length == 4){
                           $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'" style="width: 200px; margin-left: 50px; margin-right: 50px;word-wrap: break-word;" align="center">' + selected_elem_arr[i] + '</label>');
                        }
                        else if (selected_elem_arr.length == 3){
                           $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'"  style="width: 170px; margin-left: 90px; margin-right: 20px;word-wrap: break-word;">' + selected_elem_arr[i] + '</label>');
                        }
                        else if (selected_elem_arr.length == 2){
                           $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'"  style="width: 170px; margin-left: 150px; margin-right: 20px;word-wrap: break-word;">' + selected_elem_arr[i] + '</label>');
                        }
                    }

                    // Before we do so, let's build our POST request's message body.
                    var body = buildRequestBody();

                    // We've build the POST request's message body, let's make the request!
                    $.ajax({
                       type: "POST",
                       url: POST_URL,
                       contentType: "application/json; charset=UTF-8", // charset is to properly retrieve special serbian characters
                       beforeSend: function( xhr ){
                           run_waitMe("roundBounce");
                       },
                       data: JSON.stringify(body),
                       success: function(respData){
                           console.log('RESPONSE:');
                           console.log(respData);
                           // Render the chart
                           if (respData["aggregated"].length == 0){
                                    $('#main, #chart-container').waitMe('hide');
                                    $("#modalMessage").empty();
                                    $("#modalMessage").append("<p style='font-size: large;'>The selected filtering options couldn't find any data to visualize.</p>");
                                    $("#myModal").modal('show');
                                    $("#main").hide();
                                    $("#socialShare").hide();

                           }else{
                               $('#main, #chart-container').waitMe('hide');
                               $('body').waitMe('hide');
                               $('#main').waitMe('hide');
                               $("#socialShare").show();
                               renderChart(respData, $('#chart-type-dd').val(), $('#data-series-map').val(), $('#fiscal-type-map').val());
                           }
                       }
                   });
                }

                }
        }else if ($( "#chart-type-dd" ).val() == "Barchart"){

            if ($('#data-series-barchart').val()=="Descriptions"){

                if ($('#fiscal-type-barchart').val() == "Revenue"){
                    selected_elem_arr = getSelectedTreeElement('tree_revenue_descriptions');
                    if (selected_elem_arr.length == 0) {
                        modalRestrictionMessage();

                    }else if($('#group-by').val()=='Municipality'){
                        if(getSelectedTextElement('tree_municipalities').length==0){
                            modalRestrictionMessage();
                        }else{
                            postRequest();
                        }
                    }else if($('#group-by').val()=='Income Type'){
                        if(getSelectedTextElement('tree_income_types').length==0){
                            modalRestrictionMessage();
                        }else{
                            postRequest();
                        }
                    }else if($('#group-by').val()=='Description'){
                        $("#modalMessage").empty();
                        $('#chart-container').empty();
                        $("#modalMessage").append("<p style='font-size: large;'>Izaberi drugi element.</p>");
                        $("#myModal").modal('show');
                        $(".hideshare-wrap").hide();
                        $('#main, #chart-container').waitMe('hide');
                    }else{
                        postRequest();

                    }
                }else if($('#fiscal-type-barchart').val() == "Expenditure"){
                    selected_elem_arr = getSelectedTreeElement('tree_expenditure_descriptions');
                    if (selected_elem_arr.length == 0){
                        modalRestrictionMessage();

                    }else if ($('#group-by').val()=='Municipality'){
                        if(getSelectedTextElement('tree_municipalities').length==0){
                            modalRestrictionMessage();
                        }else{
                            postRequest();
                        }
                    }else if($('#group-by').val()=='Income Type'){
                        if(getSelectedTextElement('tree_income_types').length==0){
                            modalRestrictionMessage();
                        }else{
                            postRequest();
                        }
                    }else if($('#group-by').val()=='Description'){
                        $("#modalMessage").empty();
                        $('#chart-container').empty();
                        $("#modalMessage").append("<p style='font-size: large;'>Izaberi drugi element.</p>");
                        $("#myModal").modal('show');
                        $(".hideshare-wrap").hide();
                        $('#main, #chart-container').waitMe('hide');
                    }else{
                        postRequest();
                    }
                }
            }else if($('#data-series-barchart').val()=="Income Types"){
                selected_elem_arr = getSelectedTreeElement('tree_income_types');
                if(selected_elem_arr.length == 0){
                    modalRestrictionMessage();

                }else if($('#group-by').val()=='Municipality'){
                    if(getSelectedTextElement('tree_municipalities').length==0){
                        modalRestrictionMessage();
                    }else{
                        postRequest();
                    }

                }else if($('#group-by').val()=='Income Type'){
                        $("#modalMessage").empty();
                        $('#chart-container').empty();
                        $("#modalMessage").append("<p style='font-size: large;'>Izaberi drugi element.</p>");
                        $("#myModal").modal('show');
                        $(".hideshare-wrap").hide();
                        $('#main, #chart-container').waitMe('hide');

                }else if($('#group-by').val()=='Description'){
                    if($('#fiscal-type-barchart').val() == "Expenditure"){
                        selected_elem_arr = getSelectedTreeElement('tree_expenditure_descriptions');
                        if (selected_elem_arr.length == 0){
                            modalRestrictionMessage();
                            $('#main, #chart-container').waitMe('hide');
                        }else{
                            postRequest();
                        }
                    }else if($('#fiscal-type-barchart').val() == "Revenue"){
                        selected_elem_arr = getSelectedTreeElement('tree_revenue_descriptions');
                        if (selected_elem_arr.length == 0){
                            modalRestrictionMessage();
                            $('#main, #chart-container').waitMe('hide');
                        }else{
                            postRequest();
                        }
                    }
                }else{
                    postRequest();
                }

            }else {
                postRequest();
            }
        }
        else{
            postRequest();
        }
    });
}

function modalRestrictionMessage(){
    $("#modalMessage").empty();
    $('#chart-container').empty();
    $("#modalMessage").append("<p style='font-size: large;'>Izaberti najmanje jednu stavku iz svake fliter sekcije.</p>");
    $("#myModal").modal('show');
    $(".hideshare-wrap").hide();
    $('#main, #chart-container').waitMe('hide');
}

function postRequest(){
        // Before we do so, let's build our POST request's message body.
        var body = buildRequestBody();
        // We've build the POST request's message body, let's make the request!
        $.ajax({
            type: "POST",
            url: POST_URL,
            beforeSend: function( xhr ){
                run_waitMe("roundBounce");
            },
            contentType: "application/json; charset=UTF-8", // charset is to properly retrieve special serbian characters
            data: JSON.stringify(body),
            success: function(respData){
                $("#main", "#chart-container").waitMe('hide');
                console.log('RESPONSE:');
                console.log(respData);

                renderChart(respData, $('#chart-type-dd').val(), $('#data-series-map').val(), $('#fiscal-type-map').val());
            }
        });
}

function buildShareUrl(){

    var filterString = "chartType=" + current_query_params.chartType + "&";

    if(current_query_params.chartType != "Scatterplot"){
        filterString = filterString + "fiscalType=" +current_query_params.data.fiscalType[current_query_params.chartType.toLowerCase()] + "&"
    }

    filterString = filterString  + "groupBy=" + current_query_params.data.groupBy + "&" +
        "series=" + current_query_params.data.series[current_query_params.chartType.toLowerCase()] + "&" +
        "years=" + current_query_params.filters.years + "&" +
        "municipalities=" + current_query_params.filters.municipalities + "&" +
        "incomeTypes=" + current_query_params.filters.incomeTypes + "&" +
        "descriptionRevenues=" + current_query_params.filters.descriptions.revenues + "&" +
        "descriptionExpenditures="  + current_query_params.filters.descriptions.expenditures;

    if(current_query_params.chartType == "Floating Bubbles"){
        filterString = filterString + "&" + "disaggregateBy=" +  current_query_params.data.disaggregateBy + "&bubbleSeries=" + current_query_params.data.series.floatingBubbles
            + "&floatingBubblesGroupBy=" + current_query_params.data.floatingBubblesGroupBy
            + "&bubbleFiscalType=" + current_query_params.data.fiscalType.floatingBubbles;
    }

    var shareUrl = HOST_AND_DIR + "?" + filterString;

    console.log(shareUrl);

    return shareUrl;
}

function buildRequestBody(){
    var body = {};
    // Get selected chart type and data values:
    body.chartType = $('#chart-type-dd').val();

    body.data = {};
    body.data.fiscalType = {};
    body.data.fiscalType.barchart = $('#fiscal-type-barchart').val();
    body.data.fiscalType.map = $('#fiscal-type-map').val();
    body.data.fiscalType.floatingBubbles = $('#fiscal-type-floating-bubbles').val();
    body.data.groupBy = $('#group-by').val();
    body.data.floatingBubblesGroupBy = $('#floating-bubbles-group-by').val();
    body.data.disaggregateBy = $('#disaggregate-by').val();
    body.data.series = {};

    // We have different series values depending on the chart type.
    body.data.series.barchart = $('#data-series-barchart').val();
    body.data.series.floatingBubbles = $('#data-series-floating-bubbles').val();
    body.data.series.map = $('#data-series-map').val();
    body.data.series.scatterplot = [];
    $('#data-series-scatterplot :selected').each(function(i, selected){
      body.data.series.scatterplot[i] = $(selected).val();
    });

    // Get selected filter values
    body.filters = {};
    body.filters.years = [];
    body.filters.municipalities = [];
    body.filters.incomeTypes = [];
    body.filters.descriptions = {};
    body.filters.descriptions.revenues = [];
    body.filters.descriptions.expenditures = [];

    body.filters.years = getSelectedTreeElement("tree_years");

    body.filters.municipalities = getSelectedTreeElement("tree_municipalities");

    body.filters.incomeTypes = getSelectedTreeElement("tree_income_types");

    body.filters.descriptions.revenues = getSelectedTreeElement('tree_revenue_descriptions');

    body.filters.descriptions.expenditures = getSelectedTreeElement('tree_expenditure_descriptions');

     // Initiate dynamic chart div container resize based on parameter selection
      initDynamicChartResize(body);



    // Set the filters global variable in case we want to build a share link for the generated chart.
    // Read more about this global variable at the top of this script.
    current_query_params = body;

    //TODO: Remove once no longer in development
    console.log('POST REQUEST MESSAGE BODY:');
    console.log(body);

    return body;
}


function getSelectedTreeElement(selectedItem){
  var selectedNodes = [];

  checked = $($('#' + selectedItem).jstree(true).get_selected('full', true)).each(function () {
      if (this.type === 'file') {
          if(selectedItem == "tree_years"){
            selectedNodes.push(parseInt(this.text));
          }
          else if(selectedItem == "tree_revenue_descriptions"){
            selectedNodes.push(parseInt(this.id));
          }
          else if(selectedItem == "tree_expenditure_descriptions"){
            selectedNodes.push(parseInt(this.id));
          }
          else{
            selectedNodes.push(this.text);
          }

      }
  });

  return selectedNodes;
}

function getSelectedTextElement(selectedItem){
  var selectedNodes = [];

  checked = $($('#' + selectedItem).jstree(true).get_selected('full', true)).each(function () {
      if (this.type === 'file') {
          selectedNodes.push(this.text);
      }
  });

  return selectedNodes;
}


/*
 * Initiate height increase for chart container div as the number of elements increases in multi-select box
 * This makes barchart visualizer more comprehensible
*/
function initDynamicChartResize(body){

    if($("#chart-type-dd").val() === "Barchart"){
        if ($('#group-by').val() === "Municipality" || $('#group-by').val() === "Description"){

                if ($('#data-series-barchart').val() === 'Income Types'){
                    if ($('#group-by').val() === "Description") {
                        if ($('#fiscal-type-barchart').val() == 'Expenditure') {
                            len_elem = (body.filters.descriptions.expenditures.length * BAR_CHART_GROUPING_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
                        }
                        else {
                            len_elem = (body.filters.descriptions.revenues.length * BAR_CHART_GROUPING_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
                        }
                    }
                    else{
                        len_elem = (body.filters.municipalities.length * BAR_CHART_GROUPING_WIDTH) + BAR_CHART_FOOTER_HEIGHT;

                    }

                }
                else if($('#data-series-barchart').val() === 'Descriptions') {
                    if ($('#fiscal-type-barchart').val() == 'Expenditure'){
                        len_elem = (body.filters.municipalities.length * body.filters.descriptions.expenditures.length * BAR_CHART_COLUMN_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
                    }
                    else {
                        len_elem = (body.filters.municipalities.length * body.filters.descriptions.revenues.length * BAR_CHART_COLUMN_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
                    }
                }

                div_height = len_elem.toString() + "px";
                console.log(div_height);
                $("#chart-container").css('height', div_height);

        }
        else {
            if($('#data-series-barchart').val() === 'Descriptions') {
                    if ($('#fiscal-type-barchart').val() == 'Expenditure'){
                        len_elem = (body.filters.descriptions.expenditures.length * BAR_CHART_GROUPING_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
                    }
                    else {
                        len_elem = (body.filters.descriptions.revenues.length * BAR_CHART_GROUPING_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
                    }
                div_height = len_elem.toString() + "px";
                console.log(div_height);
                $("#chart-container").css('height', div_height);
            }
            else {
                $("#chart-container").css('height', '700px');
            }

        }

    }
}

function renderChart(respData, chartType, mapSeries, mapFiscalType){

    // Get rid of the welcome message jumbotron
    $('.jumbotron').hide();

    if(chartType == 'Map'){
        $('#main').css('display', 'none');
        $('#chart-container').css('display', 'block');
       renderMap(respData, mapSeries, mapFiscalType)
    }
    else if(chartType == 'Barchart'){
        $('#main').css('display', 'none');
        $('#chart-container').css('display', 'block');
       renderBarChart(respData)
    }
    else if(chartType == 'Scatterplot'){
        $('#main').css('display', 'none');
        $('#chart-container').css('display', 'block');
       renderScatterPlotChart(respData);
    }

    else if(chartType == 'Floating Bubbles'){
       $('#chart-container').css('display', 'none');
       $('#main').css('display', 'block');
        $('#year').removeClass('active');
       $('#all').toggleClass('active');
       renderFloatingBubblesChart(respData);
       toggle_all();
       renderShortenedShareUrlButton(buildShareUrl, renderShareButton);
    }
}

function renderMap(respData, dataSeries, fiscalType){
    var chartData = [];
    var minValue = 0;
    var maxValue = 0;
    if (fiscalType == 'Expenditure'){
        fs_type = 'Rashodi';
    }
    else {
        fs_type = 'Prihodi';
    }
    $(respData).each(function(i, selected){

        if(dataSeries === 'Income Types'){
            var value = 0;

            if('autonomnaPokrajina' in selected){
                value = value + selected['autonomnaPokrajina'];
            }

            if('donacije' in selected){
                value = value + selected['donacije'];
            }

            if('ooco' in selected){
                value = value + selected['ooco'];
            }

            if('opstinaGrada' in selected){
                value = value + selected['opstinaGrada'];
            }

            if('republika' in selected){
                value = value + selected['republika'];
            }

            if('ostaliIzvori' in selected){
                value = value + selected['ostaliIzvori'];
            }

            if(i == 0){
                minValue = value;
            }else if(value < minValue){
                minValue = value;
            }

            if(value > maxValue){
                maxValue = value;
            }

            chartData[i] = {
                "name": selected['_id']['opstina'],
                "value": value
            }

        }else if(dataSeries === 'Descriptions'){
            if(i == 0){
                minValue = selected['value'];
            }else if(selected['value'] < minValue){
                minValue = selected['value'];
            }

            chartData[i] = {
                "name": selected['_id']['opstina'],
                "value": selected['value']
            }
        }
    });

    console.log('CHART DATA:');
    console.log(chartData);

    $.getJSON(MAP_GEOJSON_URL, function (geojson) {

        var options = {
            chart: {
                events: {
                    load: function (event) {
                        renderShortenedShareUrlButton(buildShareUrl, renderShareButton)
                    }
                }
            },
            title: {
                text: 'Budzet.info vizualizacija'
            },

            mapNavigation: {
                enabled: false
            },

            colorAxis: {
                min: minValue
                //type: 'logarithmic'
            },

            series: [{
                data: chartData,
                mapData: geojson,
                joinBy: ['NAME_2', 'name'],
                name: fs_type,
                states: {
                    hover: {
                        color: '#BADA55'
                    }
                },
                dataLabels: {
                    enabled: false,
                    format: ''
                },
                tooltip: {
                    pointFormat: '{point.NAME_2}: {point.value:,.2f}<br/>'
                }
            }]
        };

        // Initiate the chart
        $('#chart-container').highcharts('Map',options);
    });
}

// Render Bar chart
function renderBarChart(respData) {
    Highcharts.setOptions({
		lang: {
			decimalPoint: ',',
            thousandsSep: '.'
		}
	});

    var options = {
        chart: {
            type: 'bar',
            events: {
                load: function(event) {
                    renderShortenedShareUrlButton(buildShareUrl, renderShareButton)
                }
            }
        },
        title: {
            text: 'Budzet.info vizualizacija'
        },
        xAxis: {
            categories: respData['categories'],
            title: {
                text: null
            }
        },
        yAxis: {
            allowDecimals: true,
            min: 0,
            title: {
                text: 'Values',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueDecimals: 2,
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            },
            series:{
                pointWidth: 14,
                pointPadding: 0.4,
                groupPadding: 0.04
            }
        },
        legend: {
           align: 'center',
           verticalAlign: 'bottom',
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')
        },
        credits: {
            enabled: false
        },
        series: respData['series']
    };

    $('#chart-container').highcharts(options);

}

// Render Scatter plot
function renderScatterPlotChart(respData){
    var options = {
        chart: {
            type: 'scatter',
            zoomType: 'xy',
            events: {
                load: function(event) {
                    renderShortenedShareUrlButton(buildShareUrl, renderShareButton)
                }
            }
        },
        title: {
            text: 'Budzet.info vizualizacija'
        },
        subtitle: {
            text: 'Zoom in by selecting the area you want, for a better view'
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Rashodi'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: 'Prihodi'
            }
        },
        legend: {
           align: 'center',
           verticalAlign: 'bottom',
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')
           },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    valueDecimals: 2,
                    headerFormat: '<b></b>',
                    pointFormat: '<b>{point.label}</b><br/>Prihodi: {point.y:,.2f}<br/>Rashodi: {point.x:,.2f}<br/>'
                }
            }
        },
        series: respData
    };
    $('#chart-container').highcharts(options);
}

function renderShareButton(shortShareUrl){
    $('#socialShare').empty();

    $('#socialShare').share({
        networks: ['twitter','facebook','googleplus', 'linkedin'],
        orientation: 'vertical',
        urlToShare: shortShareUrl,
        affix: 'right center'
    });
}

function initTour(){
    tour = $('#tour').tourbus( {
        onLegStart: function( leg, bus ) {
            if( leg.rawData.highlight ) {
                leg.$target.addClass('tour-highlight');
                $('.tour-overlay').show();
            }
        },
        onLegEnd: function( leg, bus ) {
            if( leg.rawData.highlight ) {
                leg.$target.removeClass('tour-highlight');
                $('.tour-overlay').hide();
            }
        }
    } );
}

function startTour(){
    tour.trigger('depart.tourbus');
}

function initMapOnLoadDropDownShowHide(){
   $('#all').removeClass('active');
    $(".cbp-spmenu-vertical-item-parent[data-section='data-map']").show();
    $(".cbp-spmenu-vertical-item-child[data-section='data-map']").show();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-barchart']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-barchart']").hide();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-scatterplot']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-scatterplot']").hide();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-floating-bubbles']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-floating-bubbles']").hide();
}

function initScatterplotOnLoadDropDownShowHide(){
    $('#all').removeClass('active');
    $(".cbp-spmenu-vertical-item-parent[data-section='data-scatterplot']").show();
    $(".cbp-spmenu-vertical-item-child[data-section='data-scatterplot']").show();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-barchart']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-barchart']").hide();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-map']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-map']").hide();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-floating-bubbles']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-floating-bubbles']").hide();

    showHideDescriptions('fiscal-type-scatterplot');

}

function initBarchartOnLoadDropDownShowHide(){
    $('#all').removeClass('active');
    $(".cbp-spmenu-vertical-item-parent[data-section='data-barchart']").show();
    $(".cbp-spmenu-vertical-item-child[data-section='data-barchart']").show();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-scatterplot']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-scatterplot']").hide();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-map']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-map']").hide();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-floating-bubbles']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-floating-bubbles']").hide();

    showHideDescriptions('fiscal-type-barchart');
}

function initFloatingBubblesOnLoadDropDownShowHide(){
    $(".cbp-spmenu-vertical-item-parent[data-section='data-map']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-map']").hide();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-barchart']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-barchart']").hide();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-scatterplot']").hide();
    $(".cbp-spmenu-vertical-item-child[data-section='data-scatterplot']").hide();

    $(".cbp-spmenu-vertical-item-parent[data-section='data-floating-bubbles']").show();
    $(".cbp-spmenu-vertical-item-child[data-section='data-floating-bubbles']").show();

    showHideDescriptions('fiscal-type-floating-bubbles');
}

function generateLabelsFlotingBubbles(data_selected){
    var selected_elem_arr = data_selected;
    for(var i=0; i<selected_elem_arr.length; i++){
        if (selected_elem_arr.length == 4){
           $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'" style="width: 200px; margin-left: 50px; margin-right: 50px;word-wrap: break-word;" align="center">' + selected_elem_arr[i] + '</label>');
        }
        else if (selected_elem_arr.length == 3){
           $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'"  style="width: 170px; margin-left: 90px; margin-right: 20px;word-wrap: break-word;">' + selected_elem_arr[i] + '</label>');
        }
        else if (selected_elem_arr.length == 2){
           $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'"  style="width: 170px; margin-left: 150px; margin-right: 20px;word-wrap: break-word;">' + selected_elem_arr[i] + '</label>');
        }
    }
}

var config_js = {
    "core": {
        "data": null
        },
        "dataType": "text",
        "check_callback": true,
        "types": {
            "#": {
                "name": "YO",
                "valid_children": ["folder", "file"]
            },
            "root": {
                "icon": false,
                "valid_children": ["folder", "file"]
            },
            "folder": {
                "icon": false,
                "valid_children": ["folder", "file"]
            },
            "file": {
                "icon": false,
                "valid_children": []
            }
        },
        "checkbox": {"real_checkboxes": "true"},
        "plugins": ["unique", "sort", "types", "checkbox"]
};

var config_js_income_types = {
    "core": {
        "data": null
        },
        "dataType": "text",
        "check_callback": true,
        "types": {
            "#": {
                "name": "YO",
                "valid_children": ["folder", "file"]
            },
            "root": {
                "icon": false,
                "valid_children": ["folder", "file"]
            },
            "folder": {
                "icon": false,
                "valid_children": ["folder", "file"]
            },
            "file": {
                "icon": false,
                "valid_children": []
            }
        },
        "checkbox": {"real_checkboxes": "true"},
        "plugins": ["unique", "types", "checkbox"]
};