// Generated by CoffeeScript 1.8.0
function renderFloatingBubblesChart(respData) {
  var BubbleChart, root,

    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  BubbleChart = (function() {
    function BubbleChart(data, colors_array) {
      this.hide_details = __bind(this.hide_details, this);
      this.show_details = __bind(this.show_details, this);
      this.hide_years = __bind(this.hide_years, this);
      this.display_years = __bind(this.display_years, this);
      this.move_towards_year = __bind(this.move_towards_year, this);
      this.display_by_year = __bind(this.display_by_year, this);
      this.move_towards_center = __bind(this.move_towards_center, this);
      this.display_group_all = __bind(this.display_group_all, this);
      this.start = __bind(this.start, this);
      this.create_vis = __bind(this.create_vis, this);
      this.create_nodes = __bind(this.create_nodes, this);
      var max_amount;

      this.colors_array = colors_array;
      this.data = data;
      this.width = 1300;
      this.height = 700;
      this.tooltip = CustomTooltip("chart_tooltip", 240);
      this.center = {
        x: this.width / (3.2),
        y: this.height / (2.4)
      };


      this.layout_gravity = -0.01;
      this.damper = 0.1;
      this.vis = null;
      this.nodes = [];
      this.force = null;
      this.circles = null;

      var data_series = [];
      var group_series_array = [];
      for(var i in this.data){

        if(!(group_series_array.indexOf(data[i]['dataSeries']) > -1)){
          group_series_array.push(this.data[i]['dataSeries']);
        }
        data_series.push(this.data[i]['groupParam']);
      }
      this.year_centers = {};
      console.log(group_series_array);
      if (group_series_array != undefined) {
        var counter = 0;
        for (var element in group_series_array) {
          if (group_series_array.length == 2) {

            this.year_centers[group_series_array[element]] = {
              x: counter + 360,
              y: this.height / 2
            };
            counter = 300;
          }
          else if(group_series_array.length == 3){
            this.year_centers[group_series_array[element]] = {
              x: counter + 380,
              y: this.height / 2
            };
            counter += 150;
          }
          else if(group_series_array.length == 4){
            this.year_centers[group_series_array[element]] = {
              x: counter + 450,
              y: this.height / 2.9 + 130
            };
            counter += 120;
          }
          else if(group_series_array.length == 5){
            this.year_centers[group_series_array[element]] = {
              x: counter + 200,
              y: this.height / 2
            };
            counter += 200;
          }
        }
      }

      this.fill_color = d3.scale.ordinal().domain(data_series).range(this.colors_array);
      max_amount = d3.max(this.data, function(d) {
        return parseInt(d.value);
      });
      this.radius_scale = d3.scale.pow().exponent(0.5).domain([0, max_amount]).range([2, 85]);
      this.create_nodes();
      this.create_vis();
    }

    BubbleChart.prototype.create_nodes = function() {

      this.data.forEach((function(_this) {
        return function(d) {
          var node;
          node = {
            id: d.id,
            radius: _this.radius_scale(parseInt(d.value)),
            value: d.value,
            name: d.groupParam,
            group: d.groupParam,
            year: d.dataSeries,
            x: Math.random() * 900,
            y: Math.random() * 800
          };
          return _this.nodes.push(node);
        };
      })(this));
      return this.nodes.sort(function(a, b) {
        return b.value - a.value;
      });
    };

    BubbleChart.prototype.create_vis = function() {
      var that;
      this.vis = d3.select("#vis").append("svg").attr("width", this.width).attr("height", this.height).attr("id", "svg_vis").attr('style','overflow-y: scroll');
      this.circles = this.vis.selectAll("circle").data(this.nodes, function(d) {
        return d.id;
      });
      that = this;
      this.circles.enter().append("circle").attr("r", 0).attr("fill", (function(_this) {
        return function(d) {
          return _this.fill_color(d.group);
        };
      })(this)).attr("stroke-width", 2).attr("stroke", (function(_this) {
        return function(d) {
          return d3.rgb(_this.fill_color(d.group)).darker();
        };
      })(this)).attr("id", function(d) {
        return "bubble_" + d.id;
      }).on("mouseover", function(d, i) {
        return that.show_details(d, i, this);
      }).on("mouseout", function(d, i) {
        return that.hide_details(d, i, this);
      });
      return this.circles.transition().duration(2000).attr("r", function(d) {
        return d.radius;
      });
    };

    BubbleChart.prototype.charge = function(d) {
      return -Math.pow(d.radius, 2.0) / 8;
    };

    BubbleChart.prototype.start = function() {
      return this.force = d3.layout.force().nodes(this.nodes).size([this.width, this.height]);
    };

    BubbleChart.prototype.display_group_all = function() {
      this.force.gravity(this.layout_gravity).charge(this.charge).friction(0.9).on("tick", (function(_this) {
        return function(e) {
          return _this.circles.each(_this.move_towards_center(e.alpha)).attr("cx", function(d) {
            return d.x;
          }).attr("cy", function(d) {
            return d.y;
          });
        };
      })(this));
      this.force.start();
      return this.hide_years();
    };

    BubbleChart.prototype.move_towards_center = function(alpha) {
      return (function(_this) {
        return function(d) {
          d.x = d.x + (_this.center.x - d.x) * (_this.damper + 0.02) * alpha;
          return d.y = d.y + (_this.center.y - d.y) * (_this.damper + 0.02) * alpha;
        };
      })(this);
    };

    BubbleChart.prototype.display_by_year = function() {
      this.force.gravity(this.layout_gravity).charge(this.charge).friction(0.9).on("tick", (function(_this) {
        return function(e) {
          return _this.circles.each(_this.move_towards_year(e.alpha)).attr("cx", function(d) {
            return d.x;
          }).attr("cy", function(d) {
            return d.y;
          });
        };
      })(this));
      this.force.start();
      return this.display_years();
    };

    BubbleChart.prototype.move_towards_year = function(alpha) {
      return (function(_this) {
        return function(d) {
          var target;
          target = _this.year_centers[d.year];
          d.x = d.x + (target.x - d.x) * (_this.damper + 0.02) * alpha * 1.1;
          return d.y = d.y + (target.y - d.y) * (_this.damper + 0.02) * alpha * 1.1;
        };
      })(this);
    };

    /**** Because of our label specifiactions we wont use this function to show labels
     *
    BubbleChart.prototype.display_years = function() {
      var years, years_data;
      var years_x = {};
      years_data = d3.keys(this.year_centers);
      for(var element in years_data){
        years_x[years_data[element]] = this.year_centers[years_data[element]]['x'] - 70;
      }

      years_data = d3.keys(years_x);
      years = this.vis.selectAll(".years").data(years_data);
      return years.enter().append("text").attr("class", "years").attr("x", (function(_this) {
        return function(d) {
          return years_x[d];
        };
      })(this)).attr("y", 40).attr("text-anchor", "middle").text(function(d) {
        return d;
      });
    };
    */
    BubbleChart.prototype.hide_years = function() {
      var years;
      return years = this.vis.selectAll(".years").remove();
    };

    BubbleChart.prototype.show_details = function(data, i, element) {
      var content;
      d3.select(element).attr("stroke", "black");
      content = "<span class=\"name\">Title:</span><span class=\"value\"> " + data.name + "</span><br/>";
      content += "<span class=\"name\">Amount:</span><span class=\"value\"> $" + (addCommas(data.value)) + "</span><br/>";
      if(data.year != undefined){
        content += "<span class=\"name\">Disaggregated by:</span><span class=\"value\"> " + data.year + "</span>";
      }
      return this.tooltip.showTooltip(content, d3.event);
    };

    BubbleChart.prototype.hide_details = function(data, i, element) {
      d3.select(element).attr("stroke", (function(_this) {
        return function(d) {
          return d3.rgb(_this.fill_color(d.group)).darker();
        };
      })(this));
      return this.tooltip.hideTooltip();
    };

    return BubbleChart;

  })();

  var colors_array;
  root = typeof exports !== "undefined" && exports !== null ? exports : this;
  root.toggle_year = (function(_this) {
      return function() {
          $('#chart_tooltip').remove();
          $('#svg_vis').remove();
          d3.json("/static/colors.json", function(data) {
            colors_array = data;
            initDisaggregatedDataVis(root, respData, BubbleChart, colors_array);
          });

      };
    })(this);

  root.toggle_all = (function(_this) {
      return function() {
        $('#chart_tooltip').remove();
          $('#svg_vis').remove();

          d3.json("/static/colors.json", function(data) {
            colors_array = data;
            initAggregatedDataVis(root, respData, BubbleChart, colors_array);
          });
      };
    })(this);



}
function initAggregatedDataVis(root, respData, BubbleChart, colors_array) {
    var chart, render_vis;

    chart = null;
    render_vis = function(json) {
      chart = new BubbleChart(json, colors_array);
      chart.start();
      return root.display_all();
    };
    root.display_all = (function() {
      return function() {
        return chart.display_group_all();
      };
    })(this);
    return render_vis(respData['aggregated'])
  }

function initDisaggregatedDataVis(root, respData, BubbleChart, colors_array) {
    var chart, render_vis;

    chart = null;
    render_vis = function(json) {
      chart = new BubbleChart(json, colors_array);
      chart.start();
      return root.display_all();
    };
    root.display_all = (function() {
      return function() {
        return chart.display_by_year();
      };
    })(this);

    return render_vis(respData['disaggregated'])
  }