$(document).ready(function(){
  var menuLeft = document.getElementById( 'cbp-spmenu-s1' );
  var showLeft = document.getElementById( 'showLeft' );
  var menuTitle = document.getElementById( 'menuTitle' );


  classie.toggle( menuLeft, 'cbp-spmenu-open' );

  $('#menuTitle').click(function() {

    classie.toggle( menuLeft, 'cbp-spmenu-open' );
    classie.toggle( menuTitle, 'showLeft-collapsed' );

    if(classie.has( menuTitle, 'showLeft-collapsed' )){
      $('#showLeft').html('&raquo');

      // Hide menu item borders when sidebar menu collapsed
      $( ".cbp-spmenu li" ).each(function() {
        $( this ).css('border-bottom-color', '#222')
      });


    }else{
      $('#showLeft').html('&laquo');

      // Show menu item borders when sidebar menu collapsed
      $( ".cbp-spmenu li" ).each(function() {
        $( this ).css('border-bottom-color', '#258ecd')
      });
    }
  });

  /* Accordion display between menu parent items and their children. */
  $( ".cbp-spmenu-vertical-item-parent" ).click(function() {
    var section = $(this).attr('data-section');

    var sectionContent = $(".cbp-spmenu-vertical-item-child[data-section='" + section + "']");

    if ( sectionContent.is( ":hidden" ) ) {
      sectionContent.slideDown( "fast", function() {
        // Animation complete.
      });
    }else{
      sectionContent.slideUp( "fast", function() {
          // Animation complete.
      });
    }
  });


});