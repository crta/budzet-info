// Constants that we use to resize chart div container height
var BAR_CHART_FOOTER_HEIGHT = 150;
var BAR_CHART_GROUPING_WIDTH = 120;
var NUMBER_OF_COMPANY_SIZE_TYPES = 3;


function initChartTypeDropdowns(){
    $('.chart-type-dd').each(function( index ) {
        $(this).change(function () {
            if(this.value == "Barchart"){

                 jstree_menu('tree_years', 'bar_');
                 jstree_menu('tree_municipalities', 'bar_');
                 jstree_menu('tree_activities', 'bar_');
                 jstree_menu('tree_companies', 'bar_');
                 jstree_menu('tree_owners', 'bar_');
                $('#barchart-config-form').show();
                $('#map-config-form').hide();
                $('#scatterplot-config-form').hide();
                $('#floatingbubbles-config-form').hide();
                // OK, this is tricky.
                // Each form has the same chart_type dropdown box.
                // When we select one of the choices, we don't actually want the value to change.
                // We just want to display the target form and hide the form we are currently in (the source form).
                // We don't want the value to change so that when we go back to the source form with its
                // chart_type dropdown still set as the value representing the source form.
                // If this is not clear, a bit of javascript debugging and breakpoints will shed some light. You can do it!
                $('#chart-type-map').val('Map');
                $('#chart-type-scatterplot').val('Scatterplot');
                $('#chart-type-floatingbubbles').val('Floating Bubbles');

            }else if(this.value == "Floating Bubbles") {
                 jstree_menu('tree_years', 'floating_bubbles_');
                 jstree_menu('tree_municipalities', 'floating_bubbles_');
                 jstree_menu('tree_activities', 'floating_bubbles_');
                 jstree_menu('tree_companies', 'floating_bubbles_');
                 jstree_menu('tree_owners', 'floating_bubbles_');
                $('#barchart-config-form').hide();
                $('#floatingbubbles-config-form').show();
                $('#map-config-form').hide();
                $('#scatterplot-config-form').hide();

                $('#chart-type-barchart').val('Barchart');
                $('#chart-type-scatterplot').val('Scatterplot');
                $('#chart-type-map').val('Map');

            }
            else if(this.value == "Map"){
                 jstree_menu('tree_years', 'map_');
                 jstree_menu('tree_municipalities', 'map_');
                 jstree_menu('tree_activities', 'map_');
                 jstree_menu('tree_companies', 'map_');
                 jstree_menu('tree_owners', 'map_');
                $('#barchart-config-form').hide();
                $('#floatingbubbles-config-form').hide();
                $('#map-config-form').show();
                $('#scatterplot-config-form').hide();

                $('#chart-type-scatterplot').val('Scatterplot');
                $('#chart-type-barchart').val('Barchart');
                $('#chart-type-floatingbubbles').val('Floating Bubbles');
            }
            else if(this.value == "Scatterplot"){
                 jstree_menu('tree_years', 'scatter_');
                 jstree_menu('tree_municipalities', 'scatter_');
                 jstree_menu('tree_activities', 'scatter_');
                 jstree_menu('tree_companies', 'scatter_');
                 jstree_menu('tree_owners', 'scatter_');
                $('#barchart-config-form').hide();
                $('#floatingbubbles-config-form').hide();
                $('#map-config-form').hide();
                $('#scatterplot-config-form').show();
                $('#chart-type-map').val('Map');
                $('#chart-type-barchart').val('Barchart');
                $('#chart-type-floatingbubbles').val('Floating Bubbles');
            }
        });
    })

}

function initBarchartCategories(){
    $(".form-control-barchart.ukupni-rashodi").change(function () {
        displayCategoriesField(this.checked, ".form-control-barchart.ukupni-rashodi-categories");
    });

    $(".form-control-barchart.poslovni-rashodi").change(function () {
        displayCategoriesField(this.checked, ".form-control-barchart.poslovni-rashodi-categories");
    });

    $(".form-control-barchart.others").change(function () {
        displayCategoriesField(this.checked, ".form-control-barchart.others-categories");
    });
}

function initFloatingBubblesCategories(){

    $(".form-control-floatingbubbles.data-series").change(function () {

        if($('input:radio[name=data_series]:checked').val() == "Ukupni rashodi"){
            displayCategoriesField(true, ".form-control-floatingbubbles.ukupni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-floatingbubbles.ukupni-rashodi-categories");
        }
    });
}

function initMapCategories(){
    $("#map_data_series").change(function () {
        var my_val = $("input[type='radio'][name='map_data_series']:checked").val();

        if(my_val == "Ukupni rashodi"){
            displayCategoriesField(true, ".form-control-map.ukupni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-map.ukupni-rashodi-categories");
        }

        if(my_val == "Poslovni rashodi"){
            displayCategoriesField(true, ".form-control-map.poslovni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-map.poslovni-rashodi-categories");
        }

        if(my_val == "Ostali"){
            displayCategoriesField(true, ".form-control-map.others-categories");
        }else{
            displayCategoriesField(false, ".form-control-map.others-categories");
        }
    });
}

function initScatterplotCategories(){

    // For X-AXIS
    $(".form-control-scatterplot.x-axis").change(function () {
        if($('input:radio[name=x_axis]:checked').val() == "Ukupni rashodi"){
            displayCategoriesField(true, ".form-control-scatterplot-x-axis.ukupni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-x-axis.ukupni-rashodi-categories");
        }

        if($('input:radio[name=x_axis]:checked').val() == "Poslovni rashodi"){
            displayCategoriesField(true, ".form-control-scatterplot-x-axis.poslovni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-x-axis.poslovni-rashodi-categories");
        }

        if($('input:radio[name=x_axis]:checked').val() == "Ostali"){
            displayCategoriesField(true, ".form-control-scatterplot-x-axis.others-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-x-axis.others-categories");
        }
    });

    // For Y-AXIS
    $(".form-control-scatterplot.y-axis").change(function () {
        if($('input:radio[name=y_axis]:checked').val() == "Ukupni rashodi"){
            displayCategoriesField(true, ".form-control-scatterplot-y-axis.ukupni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-y-axis.ukupni-rashodi-categories");
        }

        if($('input:radio[name=y_axis]:checked').val() == "Poslovni rashodi"){
            displayCategoriesField(true, ".form-control-scatterplot-y-axis.poslovni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-y-axis.poslovni-rashodi-categories");
        }

        if($('input:radio[name=y_axis]:checked').val() == "Ostali"){
            displayCategoriesField(true, ".form-control-scatterplot-y-axis.others-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-y-axis.others-categories");
        }
    });
}

function run_waitMe(effect){
        $('#main, #chart-container').waitMe({

        //none, rotateplane, stretch, orbit, roundBounce, win8,
        //win8_linear, ios, facebook, rotation, timer, pulse,
        //progressBar, bouncePulse or img
        effect: 'roundBounce',

        //place text under the effect (string).
        text: '',

        //background for container (string).
        bg: 'rgba(255,255,255,0.7)',

        //color for background animation and text (string).
        color: '#000',

        //change width for elem animation (string).
        sizeW: '',

        //change height for elem animation (string).
        sizeH: '',

        // url to image
        source: ''

        });

}

function hideCategories(parentClass){
    displayCategoriesField(false, parentClass + ".ukupni-rashodi-categories");
    displayCategoriesField(false, parentClass + ".poslovni-rashodi-categories");
    displayCategoriesField(false, parentClass + ".others-categories");
}

function displayCategoriesField(display, categoriesClassRef){
    if(display == true){
       $(categoriesClassRef).show();
    }else{
       $(categoriesClassRef).hide();
    }
}

function initBarchartVisualizationButton(){
    $( "#visualize-barchart-button" ).click(function() {
        $('#main').css('display', 'none');
        $('#chart-container').css('display', 'block');
        // We are going to make a POST request to the backend with the values
        // selected in the fields.


        // Before we do so, let's build our POST request's message body.
        var queryParams = buildBarchartRequestBody();

        var selected_year_tree_element = getSelectedTreeElement('bar_', 'tree_years');
        var selected_municipalities_tree_element =  getSelectedTreeElement('bar_', 'tree_municipalities');
        var selected_companies_tree_element =  getSelectedTreeElement('bar_', 'tree_companies');
        var selected_owners_tree_element =  getSelectedTreeElement('bar_', 'tree_owners');

        if ($('#group_by').val() === "Godina"){
            if (selected_year_tree_element==0){
                $("#modalMessage").empty();
                $('#chart-container').empty();
                $("#modalMessage").append("<p style='font-size: large;'>Izaberti najmanje jednu stavku iz svake fliter sekcije.</p>");
                $("#myModal").modal('show');
                $(".hideshare-wrap").hide();
                $('#main, #chart-container').waitMe('hide');

            }
            else{
                // Now let's make the request.
                postRequest(queryParams, renderBarchart);
            }
        }
        else if ($('#group_by').val()=== "Opština"){
            if (selected_municipalities_tree_element==0){
                $("#modalMessage").empty();
                $('#chart-container').empty();
                $("#modalMessage").append("<p style='font-size: large;'>Izaberti najmanje jednu stavku iz svake fliter sekcije.</p>");
                $("#myModal").modal('show');
                $(".").hide();
                $("#main, #chart-container").hide()
                $('#main, #chart-container').waitMe('hide');

            }
            else{
                // Now let's make the request.
                postRequest(queryParams, renderBarchart);
            }
        }else if ($('#group_by').val()==="Preduzeća"){
            if(selected_companies_tree_element==0){
                $("#modalMessage").empty();
                $('#chart-container').empty();
                $("#modalMessage").append("<p style='font-size: large;'>Izaberti najmanje jednu stavku iz svake fliter sekcije.</p>");
                $("#myModal").modal('show');
                $("#share-button-container").hide();
                $('#main, #chart-container').waitMe('hide');
            }
            else{
                // Now let's make the request.
                postRequest(queryParams, renderBarchart);
            }
        }else if ($('#group_by').val()==="Osnivači"){
            if(selected_owners_tree_element==0){
                $("#modalMessage").empty();
                $('#chart-container').empty();
                $("#modalMessage").append("<p style='font-size: large;'>Izaberti najmanje jednu stavku iz svake fliter sekcije.</p>");
                $("#myModal").modal('show');
                $("#share-button-container").hide();
                $("#main, #chart-container").hide()
                $('#main, #chart-container').waitMe('hide');
            }
            else{
                // Now let's make the request.
                postRequest(queryParams, renderBarchart);
            }

        } else{
                // Now let's make the request.
                postRequest(queryParams, renderBarchart);
            }




    });
}

function initMapVisualizationButton(){
    $( "#visualize-map-button" ).click(function() {
        $('#main').css('display', 'none');
        $('#chart-container').css('display', 'block');
        // We are going to make a POST request to the backend with the values
        // selected in the fields.

        // Before we do so, let's build our POST request's message body.
        var queryParams = buildMapRequestBody();

        // Now let's make the request.
        postRequest(queryParams, renderMap);
    });
}

function initScatterplotVisualizationButton(){
    $("#visualize-scatterplot-button").click(function() {
        $('#main').css('display', 'none');
        $('#chart-container').css('display', 'block');
        // We are going to make a POST request to the backend with the values
        // selected in the fields.

        // Before we do so, let's build our POST request's message body.
        var queryParams = buildScatterplotRequestBody();

        // Now let's make the request.
        postRequest(queryParams, renderScatterplot);
    });
}

function initFloatingBubblesVisualizationButton(){
    $( "#visualize-floatingbubbles-button" ).click(function() {
        // Get rid of the welcome message jumbotron
        $('.jumbotron').hide();
        $('#chart-container').css('display', 'none');
        $('#main').css('display', 'block');
        $('#year').removeClass('active');
        $('#all').toggleClass('active');
        $( ".toollabel" ).remove();
        $('#floatingLabels').hide();
        // We are going to make a POST request to the backend with the values
        // selected in the fields.
        $('.modal-backdrop').remove();
        var selected_elem_arr = [];
        if($('#disaggregate_by').val() == 'Godina') {
            selected_elem_arr = getSelectedTreeElement('floating_bubbles_', 'tree_years');
        }
        else if($('#disaggregate_by').val() == 'Preduzeća'){
            selected_elem_arr = getSelectedTextElement('floating_bubbles_', 'tree_companies');
        }
        else if($('#disaggregate_by').val() == 'Opština'){
            selected_elem_arr = getSelectedTextElement('floating_bubbles_', 'tree_municipalities');
        }
        else if($('#disaggregate_by').val() == 'Delatnosti'){
            selected_elem_arr = getSelectedTextElement('floating_bubbles_', 'tree_activities');
        }
        else if($('#disaggregate_by').val() == 'Osnivači'){
            selected_elem_arr = getSelectedTextElement('floating_bubbles_', 'tree_owners');
        }
        if(selected_elem_arr.length > 4){
            $( "#modalMessage" ).empty();
            $( "#modalMessage" ).append("<p style='font-size: large;'>Molimo vas da ponovo odaberete filtere. Balončići prikazuju podatke za najviše 4 odabrana filtera.</p>");
            $("#myModal").modal('show');
            $("#main, #chart-container").hide()
            $('#main, #chart-container').waitMe('hide');
        }
        else {
            if(selected_elem_arr.length == 0){
                $( "#modalMessage" ).empty();
                $( "#modalMessage" ).append("<p style='font-size: large;'>Za izabrani parametar razvrstavanja (iz polja 'razvrstano po') odaberite odgovarajuće filtere (u polju 'filteri'). Balončići prikazuju podatke za najviše 4 odabrana filtera.</p>");
                $("#myModal").modal('show');
                $("#main, #chart-container").hide()
                $('#main, #chart-container').waitMe('hide');
            }
            else {
                for(var i=0; i<selected_elem_arr.length; i++){
                    if (selected_elem_arr.length == 4){
                       $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'" style="width: 200px; margin-left: 50px; margin-right: 50px;word-wrap: break-word;" align="center">' + selected_elem_arr[i] + '</label>');
                    }
                    else if (selected_elem_arr.length == 3){
                       $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'"  style="width: 170px; margin-left: 90px; margin-right: 20px;word-wrap: break-word;">' + selected_elem_arr[i] + '</label>');
                    }
                    else if (selected_elem_arr.length == 2){
                       $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'"  style="width: 170px; margin-left: 150px; margin-right: 20px;word-wrap: break-word;">' + selected_elem_arr[i] + '</label>');
                    }
                }
                // Before we do so, let's build our POST request's message body.
                var queryParams = buildFloatingBubblesRequestBody();

                // Now let's make the request.
                // We've build the POST request's message body, let's make the request!
                $.ajax({
                    type: "POST",
                    url: POST_URL,
                    beforeSend: function( xhr ){
                        run_waitMe("roundBounce");
                    },
                    contentType: "application/json; charset=UTF-8", // charset is to properly retrieve special serbian characters
                    data: JSON.stringify(queryParams),
                    success: function (respData) {
                        console.log('RESPONSE:');
                        console.log(respData);

                        // Render the chart
                        if (respData["aggregated"].length == 0){
                                $('#main, #chart-container').waitMe('hide');
                                $("#modalMessage").empty();
                                $("#modalMessage").append("<p style='font-size: large;'>The selected filtering options couldn't find any data to visualize.</p>");
                                $("#myModal").modal('show');
                                $("#main").hide();
                                $("#socialShare").hide();
                                $('#main').waitMe('hide');
                                $("#main, #chart-container").hide()

                        }
                        else {
                            $('#main, #chart-container').waitMe('hide');
                            $('#main').waitMe('hide');
                            renderFloatingBubblesChart(respData);
                            $("#socialShare").show();
                            toggle_all();
                            renderShortenedShareUrlButton(buildFloatingBubblesShareUrl, renderShareButton);
                        }

                    }
                });
            }
        }

    });
}

function getSelectedTextElement(parentClass, selectedItem){
  var selectedNodes = [];
  var my_selector = parentClass + selectedItem;
  checked = $($('.' + my_selector).jstree(true).get_selected('full', true)).each(function () {
      if (this.type === 'file') {
          selectedNodes.push(this.text);
      }
  });

  return selectedNodes;
}

function postRequest(queryParams, callbackFunction){
    // We've build the POST request's message body, let's make the request!
    $.ajax({
        type: "POST",
        url: POST_URL,
        beforeSend: function( xhr ) {
            run_waitMe("roundBounce");
          },
        contentType: "application/json; charset=UTF-8", // charset is to properly retrieve special serbian characters
        data: JSON.stringify(queryParams),
        success: function(respData){
            $('#main, #chart-container').waitMe('hide');
            $('body').waitMe('hide');
            console.log('RESPONSE:');
            console.log(respData);

            // Render the chart
            callbackFunction(respData);
       }
    });
}

function buildBarchartRequestBody(){
    var body = {};
    // Get selected chart type and data values:
    body.tipGrafikona = $('#chart-type-barchart').val();

    body.podaci = {};
    body.podaci.ukupniPrihodi = $('.form-control-barchart.ukupni-prihodi').is(":checked");

    body.podaci.ukupniRashodi = {};
    body.podaci.ukupniRashodi.selected = $('.form-control-barchart.ukupni-rashodi').is(":checked");

    if(body.podaci.ukupniRashodi.selected == true){
        body.podaci.ukupniRashodi.params = [];
        $('.form-control-barchart.ukupni-rashodi-categories :selected').each(function(i, selected){
          body.podaci.ukupniRashodi.params[i] = $(selected).val();
        });
    }

    body.podaci.poslovniRashodi = {};
    body.podaci.poslovniRashodi.selected = $('.form-control-barchart.poslovni-rashodi').is(":checked");
    if(body.podaci.poslovniRashodi.selected == true){
        body.podaci.poslovniRashodi.params = [];
        $('.form-control-barchart.poslovni-rashodi-categories :selected').each(function(i, selected){
         body.podaci.poslovniRashodi.params[i] = $(selected).val();
        });
    }

    body.podaci.ostali = {};
    body.podaci.ostali.selected = $('.form-control-barchart.others').is(":checked");

    if(body.podaci.ostali.selected == true){
         body.podaci.ostali.params = [];
        $('.form-control-barchart.others-categories :selected').each(function(i, selected){
          body.podaci.ostali.params[i] = $(selected).val();
        });
    }

    body.podaci.grupisanoPo = $('.form-control-barchart.group-by').val();

    body.filteri = buildRequestBodyFilters('bar_');

    //Initiate chart div resize based on the filter param
    initDynamicChartResize(body);


    // Set the filters global variable in case we want to build a share link for the generated chart.
    // Read more about this global variable at the top of this script.
    current_query_params = body;

    console.log('QUERY PARAMS:');
    console.log(body);

    return body;
}

/*
 * Initiate height increase for chart container div as the number of elements increases in multi-select box
 * This makes barchart visualizer more comprehensible
*/
function initDynamicChartResize(body){

    if($("#chart-type-barchart").val() === "Barchart"){

        if ($('#group_by').val() === "Opština"){
            len_elem = (body.filteri.opstine.length * BAR_CHART_GROUPING_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
        }
        else if ($('#group_by').val() === "Preduzeća"){
            len_elem = (body.filteri.preduzeca.length * BAR_CHART_GROUPING_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
        }
        else if ($('#group_by').val() === "Veličina preduzeća"){
            len_elem = (NUMBER_OF_COMPANY_SIZE_TYPES * BAR_CHART_GROUPING_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
        }
        else if ($('#group_by').val() === "Godina"){
            len_elem = (body.filteri.godine.length * BAR_CHART_GROUPING_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
        }
        else if ($('#group_by').val() === "Osnivači"){
            len_elem = (body.filteri.osnivaci.length * BAR_CHART_GROUPING_WIDTH) + BAR_CHART_FOOTER_HEIGHT;
        }
        div_height = len_elem.toString() + "px";
        $("#chart-container").css('height', div_height);
    }
}

function buildFloatingBubblesRequestBody(){
    var body = {};
    // Get selected chart type and data values:
    body.tipGrafikona = $('#chart-type-floatingbubbles').val();

    body.podaci = {};
    body.podaci.selected = $('input:radio[name=data_series]:checked').val();

    if(body.podaci.selected == "Ukupni rashodi") {
        body.podaci.params = [];
        $('.form-control-floatingbubbles.ukupni-rashodi-categories :selected').each(function (i, selected) {
            body.podaci.params[i] = $(selected).val();
        });
    }

    body.podaci.grupisanoPo = $('.form-control-floatingbubbles.group-by').val();
    body.podaci.disaggregateBy = $('.form-control-floatingbubbles.disaggregate-by').val();

    body.filteri = buildRequestBodyFilters('floating_bubbles_');

    // Set the filters global variable in case we want to build a share link for the generated chart.
    // Read more about this global variable at the top of this script.
    current_query_params = body;

    console.log('QUERY PARAMS:');
    console.log(body);

    return body;
}

function buildMapRequestBody(){
    var body = {};
    // Get selected chart type and data values:
    body.tipGrafikona = $('#chart-type-map').val();

    body.podaci = {};
    body.podaci.selected = $("input[type='radio'][name='map_data_series']:checked").val();


    if(body.podaci.selected == "Ukupni rashodi") {
        body.podaci.params = [];
        $('.form-control-map.ukupni-rashodi-categories :selected').each(function (i, selected) {
            body.podaci.params[i] = $(selected).val();
        });
    }
    else if(body.podaci.selected == "Poslovni rashodi"){
        body.podaci.params = [];
        $('.form-control-map.poslovni-rashodi-categories :selected').each(function (i, selected) {
            body.podaci.params[i] = $(selected).val();
        });
    }
    else if(body.podaci.selected == "Ostali"){
        body.podaci.params = [];
        $('.form-control-map.others-categories :selected').each(function (i, selected) {
            body.podaci.params[i] = $(selected).val();
        });
    }

    body.filteri = buildRequestBodyFilters('map_');

    // Set the filters global variable in case we want to build a share link for the generated chart.
    // Read more about this global variable at the top of this script.
    current_query_params = body;

    console.log('QUERY PARAMS:');
    console.log(body);

    return body;
}

function buildScatterplotRequestBody(){
    var body = {};
    body.tipGrafikona = $('#chart-type-scatterplot').val();

    body.podaci = {};
    body.podaci.series = [];
    $('.form-control-scatterplot.data-series :selected').each(function (i, selected) {
        body.podaci.series[i] = $(selected).val();
    });

    // X-AXIS
    body.podaci.x = {};
    body.podaci.x.selected = $('input:radio[name=x_axis]:checked').val();

    if(body.podaci.x.selected == "Ukupni rashodi") {
        body.podaci.x.params = [];
        $('.form-control-scatterplot-x-axis.ukupni-rashodi-categories :selected').each(function (i, selected) {
            body.podaci.x.params[i] = $(selected).val();
        });
    }

    else if(body.podaci.x.selected == "Poslovni rashodi"){
        body.podaci.x.params = [];
        $('.form-control-scatterplot-x-axis.poslovni-rashodi-categories :selected').each(function (i, selected) {
            body.podaci.x.params[i] = $(selected).val();
        });
    }

    else if(body.podaci.x.selected == "Ostali"){
        body.podaci.x.params = [];
        $('.form-control-scatterplot-x-axis.others-categories :selected').each(function (i, selected) {
            body.podaci.x.params[i] = $(selected).val();
        });
    }

    // Y-AXIS
    body.podaci.y = {};
    body.podaci.y.selected = $('input:radio[name=y_axis]:checked').val();

    if(body.podaci.y.selected == "Ukupni rashodi") {
        body.podaci.y.params = [];
        $('.form-control-scatterplot-y-axis.ukupni-rashodi-categories :selected').each(function (i, selected) {
            body.podaci.y.params[i] = $(selected).val();
        });
    }

    else if(body.podaci.y.selected == "Poslovni rashodi"){
        body.podaci.y.params = [];
        $('.form-control-scatterplot-y-axis.poslovni-rashodi-categories :selected').each(function (i, selected) {
            body.podaci.y.params[i] = $(selected).val();
        });
    }

    else if(body.podaci.y.selected == "Ostali"){
        body.podaci.y.params = [];
        $('.form-control-scatterplot-y-axis.others-categories :selected').each(function (i, selected) {
            body.podaci.y.params[i] = $(selected).val();
        });
    }

    // FILTERS
    body.filteri = buildRequestBodyFilters('scatter_');

    // Set the filters global variable in case we want to build a share link for the generated chart.
    // Read more about this global variable at the top of this script.
    current_query_params = body;

    console.log('QUERY PARAMS:');
    console.log(body);

    return body;
}

function buildRequestBodyFilters(parentClass){
    // Get selected filter values
    filteri = {};
    filteri.godine = [];
    filteri.opstine = [];
    filteri.delatnosti = [];
    filteri.preduzeca = [];
    filteri.osnivaci = [];

    filteri.godine = getSelectedTreeElement(parentClass, 'tree_years');

    filteri.opstine  = getSelectedTreeElement(parentClass, 'tree_municipalities');

    filteri.delatnosti = getSelectedTreeElement(parentClass, 'tree_activities');

    filteri.preduzeca = getSelectedTreeElement(parentClass, 'tree_companies');

    filteri.osnivaci = getSelectedTreeElement(parentClass, 'tree_owners');

    return filteri;
}

function getSelectedTreeElement(parentClass, selectedItem){
  var selectedNodes = [];
  var my_selector = parentClass + selectedItem;
  checked = $($('.' + my_selector).jstree(true).get_selected('full', true)).each(function () {
      if (this.type === 'file') {
          if(my_selector == (parentClass + "tree_years")){
            selectedNodes.push(parseInt(this.text));
          }
          else if(my_selector == (parentClass + "tree_companies")){
            selectedNodes.push(parseInt(this.id));
          }
          else if(my_selector == (parentClass + "tree_activities")){
            selectedNodes.push(parseInt(this.id));
          }
          else{
            selectedNodes.push(this.text);
          }

      }
  });

  return selectedNodes;
}



function buildBarchartShareUrl(){

    var filterString = "tipGrafikona=" + safeGetValue(current_query_params.tipGrafikona) + "&" +
            "ukupniPrihodi=" + safeGetValue(current_query_params.podaci.ukupniPrihodi) + "&" +
            "ukupniRashodi=" + safeGetValue(current_query_params.podaci.ukupniRashodi.selected) + "&" +
            "ukupniRashodiParams=" + safeGetValue(current_query_params.podaci.ukupniRashodi.params) + "&" +
            "poslovniRashodi=" + safeGetValue(current_query_params.podaci.poslovniRashodi.selected) + "&" +
            "poslovniRashodiParams=" + safeGetValue(current_query_params.podaci.poslovniRashodi.params) + "&" +
            "ostali=" + safeGetValue(current_query_params.podaci.ostali.selected) + "&" +
            "ostaliParams=" + safeGetValue(current_query_params.podaci.ostali.params) + "&" +
            "grupisanoPo=" + safeGetValue(current_query_params.podaci.grupisanoPo)+ "&" +
            "years=" + safeGetValue(current_query_params.filteri.godine);

    if(current_query_params.filteri.opstine.length != 147){
        filterString = filterString + "&municipalities=" + safeGetValue(current_query_params.filteri.opstine);
    }
    else{
        filterString = filterString + "&municipalities=" + "Ukupno";
    }

    if(current_query_params.filteri.delatnosti.length != 43){
        filterString = filterString + "&activities=" + safeGetValue(current_query_params.filteri.delatnosti);
    }
    else{
        filterString = filterString + "&activities=" + "Ukupno";
    }

    if(current_query_params.filteri.preduzeca.length != 417){
        filterString = filterString + "&companies=" + safeGetValue(current_query_params.filteri.preduzeca);
    }
    else{
        filterString = filterString + "&companies=" + "Ukupno";
    }

    if(current_query_params.filteri.osnivaci.length != 254){
        filterString = filterString + "&owners=" + safeGetValue(current_query_params.filteri.osnivaci);
    }
    else{
        filterString = filterString + "&owners=" + "Ukupno";
    }

    var shareUrl = HOST_AND_DIR + "?" + filterString;

    console.log(shareUrl);

    return shareUrl;
}

function buildScatterplotShareUrl(){
    var filterString = "tipGrafikona=" + safeGetValue(current_query_params.tipGrafikona) + "&" +
            "series=" + safeGetValue(current_query_params.podaci.series) + "&x="+ safeGetValue(current_query_params.podaci.x.selected) +
            "&xParams=" + safeGetValue(current_query_params.podaci.x.params) + "&y=" + safeGetValue(current_query_params.podaci.y.selected) +
            "&yParams=" + safeGetValue(current_query_params.podaci.y.params) +
            "&years=" + safeGetValue(current_query_params.filteri.godine);

    if(current_query_params.filteri.opstine.length != 147){
        filterString = filterString + "&municipalities=" + safeGetValue(current_query_params.filteri.opstine);
    }
    else{
        filterString = filterString + "&municipalities=" + "Ukupno";
    }

    if(current_query_params.filteri.delatnosti.length != 43){
        filterString = filterString + "&activities=" + safeGetValue(current_query_params.filteri.delatnosti);
    }
    else{
        filterString = filterString + "&activities=" + "Ukupno";
    }

    if(current_query_params.filteri.preduzeca.length != 417){
        filterString = filterString + "&companies=" + safeGetValue(current_query_params.filteri.preduzeca);
    }
    else{
        filterString = filterString + "&companies=" + "Ukupno";
    }

    if(current_query_params.filteri.osnivaci.length != 254){
        filterString = filterString + "&owners=" + safeGetValue(current_query_params.filteri.osnivaci);
    }
    else{
        filterString = filterString + "&owners=" + "Ukupno";
    }

    var shareUrl = HOST_AND_DIR + "?" + filterString;

    console.log(shareUrl);

    return shareUrl;
}

function buildMapShareUrl(){
    var filterString = "tipGrafikona="+ safeGetValue(current_query_params.tipGrafikona) +
            "&series=" + safeGetValue(current_query_params.podaci.selected) + "&params=" + safeGetValue(current_query_params.podaci.params) +
            "&years=" + safeGetValue(current_query_params.filteri.godine);

    if(current_query_params.filteri.opstine.length != 147){
        filterString = filterString + "&municipalities=" + safeGetValue(current_query_params.filteri.opstine);
    }
    else{
        filterString = filterString + "&municipalities=" + "Ukupno";
    }

    if(current_query_params.filteri.delatnosti.length != 43){
        filterString = filterString + "&activities=" + safeGetValue(current_query_params.filteri.delatnosti);
    }
    else{
        filterString = filterString + "&activities=" + "Ukupno";
    }

    if(current_query_params.filteri.preduzeca.length != 417){
        filterString = filterString + "&companies=" + safeGetValue(current_query_params.filteri.preduzeca);
    }
    else{
        filterString = filterString + "&companies=" + "Ukupno";
    }

    if(current_query_params.filteri.osnivaci.length != 254){
        filterString = filterString + "&owners=" + safeGetValue(current_query_params.filteri.osnivaci);
    }
    else{
        filterString = filterString + "&owners=" + "Ukupno";
    }

    var shareUrl = HOST_AND_DIR + "?" + filterString;

    console.log(shareUrl);

    return shareUrl;
}

function buildFloatingBubblesShareUrl(){
    var filterString = "tipGrafikona="+ safeGetValue(current_query_params.tipGrafikona) +
            "&series=" + safeGetValue(current_query_params.podaci.selected) + "&disaggregateBy=" + safeGetValue(current_query_params.podaci.disaggregateBy) +
            "&grupisanoPo=" + safeGetValue(current_query_params.podaci.grupisanoPo) +
            "&years=" + safeGetValue(current_query_params.filteri.godine);

    if(current_query_params.filteri.opstine.length != 147){
        filterString = filterString + "&municipalities=" + safeGetValue(current_query_params.filteri.opstine);
    }
    else{
        filterString = filterString + "&municipalities=" + "Ukupno";
    }

    if(current_query_params.filteri.delatnosti.length != 43){
        filterString = filterString + "&activities=" + safeGetValue(current_query_params.filteri.delatnosti);
    }
    else{
        filterString = filterString + "&activities=" + "Ukupno";
    }

    if(current_query_params.filteri.preduzeca.length != 417){
        filterString = filterString + "&companies=" + safeGetValue(current_query_params.filteri.preduzeca);
    }
    else{
        filterString = filterString + "&companies=" + "Ukupno";
    }

    if(current_query_params.filteri.osnivaci.length != 254){
        filterString = filterString + "&owners=" + safeGetValue(current_query_params.filteri.osnivaci);
    }
    else{
        filterString = filterString + "&owners=" + "Ukupno";
    }

    var shareUrl = HOST_AND_DIR + "?" + filterString;

    console.log(shareUrl);

    return shareUrl;
}

/***
 * Returns blank value if variable is undefined.
 * @param variable
 * @returns {*}
 */
function safeGetValue(variable){
    if (variable){
        return variable;
    }else{
        return "";
    }
}

function renderShareButton(shortShareUrl) {
    $('#socialShare').empty();

     $('#socialShare').share({
        networks: ['twitter','facebook','googleplus', 'linkedin'],
        orientation: 'vertical',
        urlToShare: shortShareUrl,
        affix: 'right center'
    });
}

function renderBarchart(chartData){

    Highcharts.setOptions({
		lang: {
			decimalPoint: ',',
            thousandsSep: '.'
		}
	});

    var options = {
        chart: {
            type: 'column',
            inverted: true,
            events: {
                load: function(event) {
                    renderShortenedShareUrlButton(buildBarchartShareUrl, renderShareButton)
            }
        }

        },
        title: {
            text: 'Budzet.info vizualizacija'
        },

        xAxis: {
            categories: chartData['categories']
        },

        yAxis: {
            allowDecimals: true,
            min: 0,
            stackLabels: {
                style: {
                    color: 'black'
                },
                enabled: true,
                formatter: function () {
                    return this.stack;
                }
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' +  this.x + '</b><br/>' +
                    this.series.name + ': ' + Highcharts.numberFormat(this.y, 2) + '<br/>' +
                    'Total value: ' + Highcharts.numberFormat(this.point.stackTotal, 2);
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            },
            series:{
                pointWidth: 14,
                pointPadding: 0.4,
                groupPadding: 0.04
            }
        },

        series: chartData['series']
    };

    // Get rid of the welcome message jumbotron
    $('.jumbotron').hide();

    $('#chart-container').highcharts(options);
}


function renderMap(chartData){

    var minValue = 0;

    $(chartData).each(function(i, selected){
        if(i == 0){
            minValue = selected['value'];
        }
    });

    $.getJSON(MAP_GEOJSON_URL, function (geojson) {

        var options = {
            chart: {
                events: {
                    load: function (event) {
                        renderShortenedShareUrlButton(buildMapShareUrl, renderShareButton)
                    }
                }
            },
            title: {
                text: 'Budzet.info vizualizacija'
            },
            mapNavigation: {
                enabled: false
            },
            colorAxis: {
                min: minValue
                //type: 'logarithmic'
            },
            series: [{
                data: chartData,
                mapData: geojson,
                joinBy: ['NAME_2', 'name'],
                name: $("input[type='radio'][name='map_data_series']:checked").val(),
                states: {
                    hover: {
                        color: '#BADA55'
                    }
                },
                dataLabels: {
                    enabled: false,
                    format: ''
                },
                tooltip: {
                    pointFormat: '{point.NAME_2}: {point.value:,.2f}<br/>'
                }
            }]
        };

        // Get rid of the welcome message jumbotron
        $('.jumbotron').hide();

        // Initiate the chart
        $('#chart-container').highcharts('Map', options);
    });
}


function renderScatterplot(chartData, x_series, y_series){
    if (y_series == undefined){
        y_axis_titile = $('input[name=y_axis]:checked').val();
    }
    else{
        y_axis_titile = y_series;
    }

    if (x_series == undefined) {
        x_axis_title = $('input[name=x_axis]:checked').val();
    }
    else{
        x_axis_title = x_series;
    }

    var options = {
        chart: {
            type: 'scatter',
            zoomType: 'xy',
            events: {
                load: function(event) {
                    renderShortenedShareUrlButton(buildScatterplotShareUrl, renderShareButton)
                }
            }
        },
        title: {
            text: "Budzet.info vizualizacija"
        },
        subtitle: {
            text: 'Zoom in by selecting the area you want, for a better view'
        },
        xAxis: {
            title: {},
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
        },
        legend: {
           align: 'center',
           verticalAlign: 'bottom',
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')
           },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    valueDecimals: 2,
                    headerFormat: '<b></b>',
                    pointFormat: '<b>{point.label}</b><br/>'+ y_axis_titile + ': {point.y:,.2f}<br/>'+ x_axis_title + ': {point.x:,.2f}<br/>'
                }
            }
        },
        series: chartData
    };

    // set the Y and X axis title based on the selected option in the filter bar
    options['yAxis'] = {
        title: {
            text: y_axis_titile
        }
    };

    options['xAxis']['title'] = {
        enabled: true,
        text: x_axis_title
    };

    // Get rid of the welcome message jumbotron
    $('.jumbotron').hide();

    $('#chart-container').highcharts(options);

}

function initOnLoadScatterplotCategories(dataseriesX, dataseriesY, categories_x, categories_y){

    // For X-AXIS
        if(dataseriesX == "Ukupni rashodi"){
            $('.form-control-scatterplot-x-axis.ukupni-rashodi-categories').val(categories_x);
            displayCategoriesField(true, ".form-control-scatterplot-x-axis.ukupni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-x-axis.ukupni-rashodi-categories");
        }

        if(dataseriesX == "Poslovni rashodi"){
            $('.form-control-scatterplot-x-axis.poslovni-rashodi-categories').val(categories_x);
            displayCategoriesField(true, ".form-control-scatterplot-x-axis.poslovni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-x-axis.poslovni-rashodi-categories");
        }

        if(dataseriesX == "Ostali"){
            $('.form-control-scatterplot-x-axis.others-categories').val(categories_x);
            displayCategoriesField(true, ".form-control-scatterplot-x-axis.others-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-x-axis.others-categories");
        }

    // For Y-AXIS
        if(dataseriesY == "Ukupni rashodi"){
            $('.form-control-scatterplot-y-axis.ukupni-rashodi-categories').val(categories_y);
            displayCategoriesField(true, ".form-control-scatterplot-y-axis.ukupni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-y-axis.ukupni-rashodi-categories");
        }

        if(dataseriesY == "Poslovni rashodi"){
            $('.form-control-scatterplot-y-axis.poslovni-rashodi-categories').val(categories_y);
            displayCategoriesField(true, ".form-control-scatterplot-y-axis.poslovni-rashodi-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-y-axis.poslovni-rashodi-categories");
        }

        if(dataseriesY == "Ostali"){
            $('.form-control-scatterplot-y-axis.others-categories').val(categories_y);
            displayCategoriesField(true, ".form-control-scatterplot-y-axis.others-categories");
        }else{
            displayCategoriesField(false, ".form-control-scatterplot-y-axis.others-categories");
        }
}

function initOnLoadBarchartCategories(dataSeries, categories_uk_rash, posl_rash_categories, ostali){

    if (dataSeries[0] == true){
        if (categories_uk_rash){
            $(".form-control-barchart.ukupni-rashodi-categories").val(categories_uk_rash);
        }
        displayCategoriesField(true, ".form-control-barchart.ukupni-rashodi-categories");
    }else{
        displayCategoriesField(false, ".form-control-barchart.ukupni-rashodi-categories");
    }

    if(dataSeries[1] == true){
        if (posl_rash_categories){
            $(".form-control-barchart.poslovni-rashodi-categories").val(posl_rash_categories);
        }
        displayCategoriesField(true, ".form-control-barchart.poslovni-rashodi-categories");
    }else{
        displayCategoriesField(false, ".form-control-barchart.poslovni-rashodi-categories");
    }

    if(dataSeries[2] == true){
        if (ostali){
            $(".form-control-barchart.others-categories").val(ostali);
        }
        displayCategoriesField(true, ".form-control-barchart.others-categories");
    }else{
        displayCategoriesField(false, ".form-control-barchart.others-categories");
    }
}

function initOnLoadMapCategories(dataSeries, categories){

    if(dataSeries == "Ukupni rashodi"){
        $(".form-control-map.ukupni-rashodi-categories").val(categories);
        displayCategoriesField(true, ".form-control-map.ukupni-rashodi-categories");
    }else{
        displayCategoriesField(false, ".form-control-map.ukupni-rashodi-categories");
    }

    if(dataSeries == "Poslovni rashodi"){
        $(".form-control-map.poslovni-rashodi-categories").val(categories);
        displayCategoriesField(true, ".form-control-map.poslovni-rashodi-categories");
    }else{
        displayCategoriesField(false, ".form-control-map.poslovni-rashodi-categories");
    }

    if(dataSeries == "Ostali"){
        $(".form-control-map.others-categories").val(categories);
        displayCategoriesField(true, ".form-control-map.others-categories");
    }else{
        displayCategoriesField(false, ".form-control-map.others-categories");
    }
}

function generateLabelsFlotingBubbles(data_selected){
    var selected_elem_arr = data_selected;
    for(var i=0; i<selected_elem_arr.length; i++){
        if (selected_elem_arr.length == 4){
           $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'" style="width: 200px; margin-left: 50px; margin-right: 50px;word-wrap: break-word;" align="center">' + selected_elem_arr[i] + '</label>');
        }
        else if (selected_elem_arr.length == 3){
           $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'"  style="width: 170px; margin-left: 90px; margin-right: 20px;word-wrap: break-word;">' + selected_elem_arr[i] + '</label>');
        }
        else if (selected_elem_arr.length == 2){
           $('#floatingLabels').append('<label class="' + selected_elem_arr[i] + ' control-label toollabel" data-toggle="tooltip" title="'+selected_elem_arr[i] +'"  style="width: 170px; margin-left: 150px; margin-right: 20px;word-wrap: break-word;">' + selected_elem_arr[i] + '</label>');
        }
    }
}

