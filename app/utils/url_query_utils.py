from urlparse import parse_qsl
from app.mod_lokalna_javna_produzeca.forms.utils import FormUtils


class UrlQueryUtils():
    @staticmethod
    def build_ljp_query_params_json_from_url_query(queries_dict):
        '''

        :param url_query:
        :return:
        '''

        chart_type = queries_dict['tipGrafikona']

        if chart_type == "Barchart":
            return UrlQueryUtils.build_ljp_barchart_query_params_json_from_dict(queries_dict)

        elif chart_type == "Map":
            return UrlQueryUtils.build_ljp_map_query_params_json_from_dict(queries_dict)

        elif chart_type == "Scatterplot":
            return UrlQueryUtils.build_ljp_scatterplot_query_params_json_from_dict(queries_dict)

        elif chart_type == "Floating Bubbles":
            return UrlQueryUtils.build_ljp_floating_bubbles_query_params_json_from_dict(queries_dict)

        return None

    @staticmethod
    def build_ljp_barchart_query_params_json_from_dict(queries_dict):
        '''
        :param queries_dict:
        :return:
        '''

        ukupni_prihodi = False
        if 'ukupniPrihodi' in queries_dict:
            ukupni_prihodi = (queries_dict['ukupniPrihodi'].lower() == 'true')

        ukupni_rashodi = False
        ukupni_rashodi_params = []
        if 'ukupniRashodi' in queries_dict:
            ukupni_rashodi = (queries_dict['ukupniRashodi'].lower() == 'true')

            if ukupni_rashodi:
                ukupni_rashodi_params = queries_dict['ukupniRashodiParams'].split(',')

        poslovni_rashodi = False
        poslovni_rashodi_params = []
        if 'poslovniRashodi' in queries_dict:
            poslovni_rashodi = (queries_dict['poslovniRashodi'].lower() == 'true')

            if poslovni_rashodi:
                poslovni_rashodi_params = queries_dict['poslovniRashodiParams'].split(',')

        ostali = False
        ostali_params = []
        if 'ostali' in queries_dict:
            ostali = (queries_dict['ostali'].lower() == 'true')

            if ostali:
                ostali_params = queries_dict['ostaliParams'].split(',')

        grupisano_po = queries_dict['grupisanoPo']

        filter_params = UrlQueryUtils.build_ljp_filters_query_params_from_dict(queries_dict)

        return UrlQueryUtils.build_ljp_barchart_query_params_json(
            ukupni_prihodi,
            ukupni_rashodi,
            ukupni_rashodi_params,
            poslovni_rashodi,
            poslovni_rashodi_params,
            ostali,
            ostali_params,
            grupisano_po,
            filter_params['years'],
            filter_params['municipalities'],
            filter_params['activities'],
            filter_params['companies'],
            filter_params['owners'])

    @staticmethod
    def build_ljp_map_query_params_json_from_dict(queries_dict):
        '''

        :param queries_dict:
        :return:
        '''

        series = queries_dict['series']
        params = None

        if 'params' in queries_dict:
            params = queries_dict['params']

        filter_params = UrlQueryUtils.build_ljp_filters_query_params_from_dict(queries_dict)

        return UrlQueryUtils.build_ljp_map_query_params_json(
            series,
            params,
            filter_params['years'],
            filter_params['municipalities'],
            filter_params['activities'],
            filter_params['companies'],
            filter_params['owners']
        )


    @staticmethod
    def build_ljp_floating_bubbles_query_params_json_from_dict(queries_dict):
        group_by = None
        disaggregate_by = None
        series = None

        if 'series' in queries_dict:
            series = queries_dict['series']

        if 'grupisanoPo' in queries_dict:
            group_by = queries_dict['grupisanoPo']

        if 'disaggregateBy' in queries_dict:
            disaggregate_by = queries_dict['disaggregateBy']

        filter_params = UrlQueryUtils.build_ljp_filters_query_params_from_dict(queries_dict)

        return UrlQueryUtils.build_ljp_bubbles_query_params_json(series, disaggregate_by, group_by, filter_params)

    @staticmethod
    def build_ljp_scatterplot_query_params_json_from_dict(queries_dict):
        '''

        :param queries_dict:
        :return:
        '''

        series = queries_dict['series'].split(',')
        x_axis = queries_dict['x']
        x_params = []
        if 'xParams' in queries_dict:
            x_params = queries_dict['xParams'].split(',')

        y_axis = queries_dict['y']
        y_params = []
        if 'yParams' in queries_dict:
            y_params = queries_dict['yParams'].split(',')

        filter_params = UrlQueryUtils.build_ljp_filters_query_params_from_dict(queries_dict)

        return UrlQueryUtils.build_ljp_scatterplot_query_params_json(
            series,
            x_axis,
            x_params,
            y_axis,
            y_params,
            filter_params['years'],
            filter_params['municipalities'],
            filter_params['activities'],
            filter_params['companies'],
            filter_params['owners'])

    @staticmethod
    def build_ljp_filters_query_params_from_dict(queries_dict):
        '''

        :param queries_dict:
        :return:
        '''
        years = []
        if 'years' in queries_dict:
            years = [int(n) for n in queries_dict['years'].split(',')]

        municipalities = []
        if 'municipalities' in queries_dict:

            if queries_dict['municipalities'] == "Ukupno":
                municipalities = []
            else:
                municipalities = queries_dict['municipalities'].split(',')

        activities = []
        if 'activities' in queries_dict:

            if queries_dict['activities'] == "Ukupno":
                activities = []
            else:
                activities = [int(n) for n in queries_dict['activities'].split(',')]

        companies = []
        if 'companies' in queries_dict:

            if queries_dict['companies'] == "Ukupno":
                companies = []
            else:
                companies = [int(n) for n in queries_dict['companies'].split(',')]

        owners = []
        if 'owners' in queries_dict:
            if queries_dict['owners'] == "Ukupno":
                owners = FormUtils.get_owners()
            else:
                owners = queries_dict['owners'].split(',')

        if queries_dict['tipGrafikona'] == "Floating Bubbles":
            return {
                'godine': years,
                'opstine': municipalities,
                'delatnosti': activities,
                'preduzeca': companies,
                'osnivaci': owners
            }
        else:
            return {
                'years': years,
                'municipalities': municipalities,
                'activities': activities,
                'companies': companies,
                'owners': owners
            }


    @staticmethod
    def build_ljp_bubbles_query_params_json(series, disaggregate_by, group_by, filter_params):
        '''

        :param series:
        :param disaggregate_by:
        :param group_by:
        :param filter_params:
        :return:
        '''

        qp = {
            'tipGrafikona': 'Floating Bubbles',
            'podaci':{
                'selected': series,
                'disaggregateBy': disaggregate_by,
                'grupisanoPo': group_by
            },
            'filteri': filter_params

        }
        return qp

    @staticmethod
    def build_ljp_barchart_query_params_json(
            ukupni_prihodi,
            ukupni_rashodi,
            ukupni_rashodi_params,
            poslovni_rashodi,
            poslovni_rashodi_params,
            ostali,
            ostali_params,
            grupisano_po,
            years,
            municipalities,
            activities,
            companies,
            owners):
        '''

        :param ukupni_prihodi: A boolean.
        :param ukupni_rashodi: A boolean.
        :param ukupni_rashodi_params: A list.
        :param poslovni_rashodi: A boolean.
        :param poslovni_rashodi_params: A list.
        :param ostali: A boolean.
        :param ostali_params: A list.
        :param grupisanoPo: A string.
        :param years: A list.
        :param municipalities: A list.
        :param activities: A list.
        :param companies: A list.
        :param owners: A list.
        :return:
        '''
        qp = {
            "tipGrafikona": "Barchart",
            "podaci": {
                "ukupniPrihodi": ukupni_prihodi,
                "ukupniRashodi": {
                    "selected": ukupni_rashodi

                },
                "poslovniRashodi": {
                    "selected": poslovni_rashodi

                },
                "ostali": {
                    "selected": ostali
                },
                "grupisanoPo": grupisano_po
            },
            "filteri": UrlQueryUtils.build_ljp_query_params_filter_json(years, municipalities, activities, companies,
                                                                        owners)
        }

        if qp['podaci']['ukupniRashodi']['selected'] is True and ukupni_rashodi_params != []:
            qp['podaci']['ukupniRashodi']["params"] = ukupni_rashodi_params

        if qp['podaci']['poslovniRashodi']['selected'] is True and poslovni_rashodi_params != []:
            qp['podaci']['poslovniRashodi']["params"] = poslovni_rashodi_params

        if qp['podaci']['ostali']['selected'] is True and ostali_params != []:
            qp['podaci']['ostali']['params'] = ostali_params

        return qp

    @staticmethod
    def build_ljp_scatterplot_query_params_json(
            series,
            x_axis,
            x_params,
            y_axis,
            y_params,
            years,
            municipalities,
            activities,
            companies,
            owners):
        ''' Build a JSON doc for the Scatterplot query parameters.

        :param series: A list.
        :param x_axis: A string.
        :param x_params: A list.
        :param y_axis: A string.
        :param y_params: A list.
        :param years: A list.
        :param municipalities: A list.
        :param activities: A list.
        :param companies: A list.
        :param owners: A list.
        :return:
        '''

        qp = {
            "tipGrafikona": "Scatterplot",
            "podaci": {
                "series": series,
                "x": {
                    "selected": x_axis,
                    "params": x_params
                },
                "y": {
                    "selected": y_axis,
                    "params": y_params

                }
            },
            "filteri": UrlQueryUtils.build_ljp_query_params_filter_json(years, municipalities, activities, companies,
                                                                        owners)
        }

        return qp

    @staticmethod
    def build_ljp_map_query_params_json(series, params, years, municipalities, activities, companies, owners):
        '''

        :param series: A string.
        :param params: A list.
        :param years: A list.
        :param municipalities: A list.
        :param activities: A list.
        :param companies: A list.
        :param owners: A list.
        :return:
        '''
        qp = {
            "tipGrafikona": "Map",
            "podaci": {
                "selected": series,
                "params": params
            },
            "filteri": UrlQueryUtils.build_ljp_query_params_filter_json(years, municipalities, activities, companies,
                                                                        owners)
        }

        return qp


    @staticmethod
    def build_ljp_query_params_filter_json(years, municipalities, activities, companies, owners):
        return {
            "godine": years,
            "opstine": municipalities,
            "delatnosti": activities,
            "preduzeca": companies,
            "osnivaci": owners
        }

    @staticmethod
    def build_bd_query_params_from_url_query(url_query):
        ''' Extract values from the URL query and uses them to build the query params JSON doc.

        :param url_query:
        :return:
        '''
        queries = parse_qsl(url_query)
        queries_dict = {}

        # Transform the list of tuples into a dictionary.
        # We do this so we can elegantly handle optional parameters (descriptionRevenues and descriptionExpenditures).
        for query in queries:
            queries_dict[query[0]] = query[1]


        chart_type = queries_dict['chartType']

        fiscal_type = ''
        if 'fiscalType' in queries_dict:
            fiscal_type = queries_dict['fiscalType']

        group_by = ''
        if chart_type == 'Barchart':
            group_by = queries_dict['groupBy']

        floating_bubbles_group_by = ''
        disaggregated_by = ''
        bubble_data_series = ''
        if chart_type == 'Floating Bubbles':
            fiscal_type = queries_dict['bubbleFiscalType']
            floating_bubbles_group_by = queries_dict['floatingBubblesGroupBy']
            disaggregated_by = queries_dict['disaggregateBy']
            bubble_data_series = queries_dict['bubbleSeries']

        if chart_type == 'Scatterplot':
            data_series = queries_dict['series'].split(',')
        else:
            data_series = queries_dict['series']

        if 'years' in queries_dict:
            years = [int(n) for n in queries_dict['years'].split(',')]
        else:
            years = []

        if 'municipalities' in queries_dict:
            municipalities = queries_dict['municipalities'].split(',')
        else:
            municipalities = []

        if 'incomeTypes' in queries_dict:
            income_types = queries_dict['incomeTypes'].split(',')
        else:
            income_types = []

        desc_rev_ids = []
        if 'descriptionRevenues' in queries_dict:
            desc_rev_ids = [int(n) for n in queries_dict['descriptionRevenues'].split(',')]

        desc_exp_ids = []
        if 'descriptionExpenditures' in queries_dict:
            desc_exp_ids = [int(n) for n in queries_dict['descriptionExpenditures'].split(',')]

        return UrlQueryUtils.build_bd_query_params_json(
            chart_type, fiscal_type, data_series, years, municipalities,
            income_types, desc_rev_ids, desc_exp_ids, group_by,
            floating_bubbles_group_by, disaggregated_by, bubble_data_series
        )

    @staticmethod
    def build_bd_query_params_json(chart_type, fiscal_type, data_series, years, municipalities, income_types,
                                   desc_rev_ids, desc_exp_ids, group_by=None, floatingBubblesGroupBy=None, disaggregated_by=None, bubble_data_series=None):
        ''' This function builds query parameter JSON document expected in the POST request body for the API calls.

        :param chart_type: A string...
        :param fiscal_type: A string...
        :param data_series: A string... Except when chart type is scatterplot then it is an list.
        :param years: A list of integers.
        :param municipalities: A list of strings.
        :param income_types: A list of strings.
        :param desc_rev_ids: A list of integers.
        :param desc_exp_ids: A list of integers.
        :param group_by: A string (Only required if chart type is barchart).
        :return: JSON document containing query parameters.
        '''
        qp = {
            "chartType": chart_type,
            "data": {
                "fiscalType": {
                    "barchart": fiscal_type,
                    "map": fiscal_type,
                    "floatingBubbles": fiscal_type
                },
                "groupBy": group_by,
                "disaggregateBy": disaggregated_by,
                "floatingBubblesGroupBy": floatingBubblesGroupBy,
                "series": {
                    "floatingBubbles": bubble_data_series,
                    "barchart": data_series,
                    "map": data_series,
                    "scatterplot": data_series
                }
            },
            "filters": {
                "years": years,
                "municipalities": municipalities,
                "incomeTypes": income_types,
                "descriptions": {
                    "revenues": desc_rev_ids,
                    "expenditures": desc_exp_ids
                }
            }
        }

        return qp
