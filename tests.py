import argparse
import unittest

import os
from coverage import coverage
from importer.tests import *
from app.mod_lokalna_javna_produzeca.forms.tests import *
from app.mod_konsolidovani_budzet.forms.tests import *
from app.mod_api.tests.konsolidovani_budzet_tests import *
from app.mod_api.tests.lokalna_javna_produzeca_tests import *

cov = coverage(branch=True, omit=['venv/*', 'importer/*', '*/tests,py', 'konsolidovani_budzet_tests.py'])
cov.start()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--coverage', action='store_true', default=False, help='Execute test coverage analysis: [%(default)s].')

    args = parser.parse_args()

    try:
        suites = []

        # Importer test suites.
        suites.append(unittest.TestLoader().loadTestsFromTestCase(KonsolidovaniBudzetTestCase))
        suites.append(unittest.TestLoader().loadTestsFromTestCase(LokalnaJavnaPreduzecaTestCase))

        # API test suites KB.
        suites.append(unittest.TestLoader().loadTestsFromTestCase(KonsolidovaniBudzetBarchartTestCase))
        suites.append(unittest.TestLoader().loadTestsFromTestCase(KonsolidovaniBudzetMapTestCase))
        suites.append(unittest.TestLoader().loadTestsFromTestCase(KonsolidovaniBudzetScatterplotTestCase))

        #API test suites LJP
        suites.append(unittest.TestLoader().loadTestsFromTestCase(LokalnaJavnaProduzecaBarchartTestCase))
        suites.append(unittest.TestLoader().loadTestsFromTestCase(LokalnaJavnaProduzecaScatterPlotTestCase))
        suites.append(unittest.TestLoader().loadTestsFromTestCase(LokalnaJavnaProduzecaMapTestCase))


        # Lokalna Javan Preduzeca Form Utils
        suites.append(unittest.TestLoader().loadTestsFromTestCase(LokalnaJavanProducezaFormsUtilsTestCase))

        # Konsolidovani Budzet Form Utils
        suites.append(unittest.TestLoader().loadTestsFromTestCase(KonsolidovaniBudzetFormsUtilsTestCase))


        # Run all tests.
        all_tests = unittest.TestSuite(suites)
        unittest.TextTestRunner().run(all_tests)

    except:
        pass

    cov.stop()
    cov.save()

    if args.coverage:
        print("\n\nCoverage Report:\n")
        cov.report()

        par_dir = os.path.join(__file__, os.pardir)
        par_dir_abs_path = os.path.abspath(par_dir)
        app_dir = os.path.dirname(par_dir_abs_path)

        print("HTML version: " + os.path.join(app_dir, "coverage/index.html"))
        cov.html_report(directory='coverage')
        cov.erase()
