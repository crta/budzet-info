# -*- coding: utf-8 -*-
import os


def get_app_dir():
    ''' Get the path of application directory
    '''
    par_dir = os.path.join(__file__, os.pardir)
    par_dir_abs_path = os.path.abspath(par_dir)
    app_dir = os.path.dirname(par_dir_abs_path)

    return app_dir


def convert_to_float(num):
    if num == " " or num == "":
        num = 0
        return num

    elif num == ".":
        num = 0
        return num

    elif num != '.':
        return float(num)

    else:
        num = 0
        return num
