# -*- coding: utf-8 -*-
import csv
import os
from pymongo import MongoClient
from slugify import slugify
from importer import get_app_dir, convert_to_float


# Connect to defualt local instance of MongoClient
client = MongoClient()

# Get database and collection
db = client.fiskalnimonitor

mongo_document_keys = {
    'Matični broj': 'maticniBroj',
    'Naziv preduzeća': 'preduzece',
    'Sedište': 'sediste',
    'Adresa': 'adresa',
    'Opština': 'opstina',
    'Naziv opštine': 'opstina',
    'Delatnost (KD 2010)':'delatnost',
    'Naziv delatnosti': 'delatnost',
    'Veličina preduzeća': 'velicinaPreduzeca',
    'Naziv osnivača': 'osnivaci',
    'Napomena': 'napomena',
    'Procenat udela osnivača': 'procenatUdelaOsnivaca',
    'Ukupni prihodi (AOP 201+215+217)':('ukupniPrihodi', 'AOP 201+215+217'),
    'Ukupni rashodi (AOP 207+216+218)': ('ukupniRashodi', 'AOP 207+216+218'),
    'Svega (AOP 207)': ('poslovniRashodi', 'AOP 207'),
    'Nabavna vrednost prodate robe (AOP 208)': ('nabavnaVrednostProdateRobe', 'AOP 208'),
    'Troškovi materijala (AOP 209)': ('troskoviMaterijala', 'AOP 209'),
    'Troškovi zarada, naknada zarada i ostali lični rashodi (AOP 210)': ('troskoviZaradaNaknadaZaradaOstaliLicniRashodi', 'AOP 210'),
    'Troškovi amortizacije i rezervisanja (AOP 211)': ('troskoviAmortizacijeRezervisanja', 'AOP 211'),
    'Ostali poslovni rashodi (AOP 212)': ('ostaliPoslovniRashodi', 'AOP 212'),
    'Finansijski rashodi (AOP 216)': ('finansijskiRashodi', 'AOP 216'),
    'Ostali rashodi (AOP 218)': ('ostaliRashodi', 'AOP 218'),
    'Neto dobitak (AOP 229)': ('netoDobitak', 'AOP 229'),
    'Neto gubitak (AOP 230)': ('netoGubitak', 'AOP 230'),
    'Broj zaposlenih (AOP 605)': ('brojZaposlenih', 'AOP 605')
}


def import_lokalna_javna_preduzeca_data():

    db.lokalnajavnapreduzeca.remove({})

    # Iterate through every file(year) in the following directory
    data_dir = '%s/importer/data/lokalna_javna_preduzeca/' % get_app_dir()

    previous_year = ""
    for filename in os.listdir(data_dir):
        if(filename.endswith(".csv")):
            year = int(filename.replace('.csv', ''))

            print (" ")
            print ("\nImporting Lokalna Javna Produzeca data for year: %i ...") % year
            print (" ")

            with open((data_dir + '%s') % filename, 'rt') as csvfile:
                reader = csv.reader(csvfile, delimiter=',')

                company_details_handler = {}
                previous_reg_number = ''
                founders_list = []
                # Loop through each row in the CSV
                for row_index, row in enumerate(reader):
                    if row_index == 0:
                        # Keep the header handy, we will use the column names as the JSON document keys.
                        header = row

                    elif row_index >= 1:

                        if str(row[0]) not in company_details_handler:
                            if row[0] != previous_reg_number:
                                if  previous_reg_number != '':
                                    # Save in database
                                    db.lokalnajavnapreduzeca.insert(doc)
                                    founders_list = []
                                elif previous_reg_number == "" and previous_year != year and previous_year != "":
                                    # Save in database
                                    db.lokalnajavnapreduzeca.insert(doc)
                                    founders_list = []

                            # to avoid blank value fileds for companies details, such as company name, address etc
                            # , we will pass the data from this dictionary
                            company_details_handler[row[0]] = {
                                'naziv_preduzeca': row[1],
                                'sediste': row[2],
                                'adresa': row[3],
                                'opstine': row[4],
                                'naziv_opstine': row[5],
                                'delatnost': row[6],
                                'naziv_delatnost': row[7],
                                'velicina_preduzeca': row[8]
                            }
                            founders_list.append({"vrednost": row[9].strip(), 'slug': slugify(row[9].strip(), to_lower=True)})

                            # Import data from each colum for the current row.
                            # The document we will store in MongoDB.
                            doc = {}
                            for column_index in range(0, 25):

                                sub_doc = create_sub_document(year, company_details_handler, header, row, column_index)

                                if column_index > 11:
                                    if column_index == 14:
                                        sub_doc['polje'] = 'Poslovani Rashodi'
                                        sub_doc['id'] = mongo_document_keys[header[column_index]][1]
                                    else:
                                        sub_doc['id'] = mongo_document_keys[header[column_index]][1]
                                        filed_name = sub_doc['polje'][0: sub_doc['polje'].find('(')]
                                        sub_doc['polje'] = filed_name.strip()
                                    doc[mongo_document_keys[header[column_index]][0]] = sub_doc

                                elif column_index == 5:
                                    sub_doc['id'] = int(company_details_handler[row[0]]['opstine'])
                                    doc[mongo_document_keys[header[5]]] = sub_doc

                                elif column_index == 7:
                                    sub_doc['id'] = int(company_details_handler[row[0]]['delatnost'])
                                    doc[mongo_document_keys[header[7]]] = sub_doc

                                elif column_index == 9:
                                    sub_doc['imena'] = [{"vrednost": row[9].strip(), 'slug': slugify(row[9].strip(), to_lower=True)}]
                                    doc[mongo_document_keys[header[9]]] = sub_doc

                                else:
                                    if column_index not in [0, 4, 6, 9, 11]:
                                        doc[mongo_document_keys[header[column_index]]] = sub_doc
                            # Let's add year property to every document we create so we can filter docs by year
                            doc['godine'] = year
                            print ("\n Reg. Number: %s - Company name: %s - City: %s, year: %i" % (doc['preduzece']['maticniBroj'], doc['preduzece']['naziv'], doc['opstina']['grad']['vrednost'], year))

                        else:
                            doc['osnivaci']['imena'].append({"vrednost": row[9].strip(), 'slug': slugify(row[9].strip(), to_lower=True)})
                        previous_reg_number = row[0]
                        previous_year = year

    db.lokalnajavnapreduzeca.insert(doc)
    print ("\n All Lokalna Javna Produzeca data source imported!")
    print (" ")


def create_sub_document(year, data_handler, header, row, column_index):
    '''
        Create a sub document from the given column value for the given row.
        :param fs_type is used to define sub-document structure based on data type
    '''

    sub_doc = {
        'polje': header[column_index]
    }

    if column_index > 11:
        if row[column_index] != '':
            val = int(row[column_index])
        else:
            val = 0
        sub_doc['vrednost'] = val

    elif column_index == 1:
        sub_doc = {}

        sub_doc['maticniBroj'] = int(row[0])

        val = data_handler[row[0]]['naziv_preduzeca'].strip()
        sub_doc['naziv'] = val.replace("?", "").replace("\"", "").strip()
        sub_doc['slug'] = slugify(sub_doc['naziv'], to_lower=True)

    elif column_index == 2:
        val = data_handler[row[0]]['sediste'].strip()
        sub_doc['vrednost'] = val
        sub_doc['slug'] = slugify(sub_doc['vrednost'], to_lower=True)

    elif column_index == 3:
        val = data_handler[row[0]]['adresa'].strip()
        sub_doc['vrednost'] = val.replace(" /", "").replace(" //", "").strip()

    elif column_index == 5:
        sub_doc = {
            'polje': header[column_index],
            'grad': {
                'vrednost': '',
                'slug': ''
            }
        }
        city_array = data_handler[row[0]]['naziv_opstine'].strip().split('-')

        if len(city_array) > 1 and city_array[1] != "grad":
            sub_doc['naziv'] = {
                'vrednost': '',
                'slug': ''
            }
            if city_array[1] == 'Mediana':
                sub_doc['naziv']['vrednost'] = 'Medijana'

            elif city_array[1] == 'Crveni krst':
                sub_doc['naziv']['vrednost'] = 'Crveni Krst'
            else:
                sub_doc['naziv']['vrednost'] = city_array[1].strip()

            sub_doc['grad']['vrednost'] = city_array[0].strip()

        else:
            sub_doc['naziv'] = {
                'vrednost': '',
                'slug': ''
            }
            sub_doc['grad']['vrednost'] = city_array[0].strip()
            sub_doc['naziv']['vrednost'] = city_array[0].strip()

        sub_doc['grad']['slug'] = slugify(sub_doc['grad']['vrednost'], to_lower=True)
        sub_doc['naziv']['slug'] = slugify(sub_doc['naziv']['vrednost'], to_lower=True)

    elif column_index == 7:
        sub_doc = {}
        val = data_handler[row[0]]['naziv_delatnost'].strip()
        sub_doc['naziv'] = val
        sub_doc['slug'] = slugify(val, to_lower=True)

    elif column_index == 8:
        val = data_handler[row[0]]['velicina_preduzeca']
        sub_doc['vrednost'] = val

    else:
        if column_index not in [0, 4, 6, 9, 11]:
            val = row[column_index].strip()
            sub_doc['vrednost'] = val

    return sub_doc
