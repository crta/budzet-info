# -*- coding: utf-8 -*-
import csv
import os
import collections
from pymongo import MongoClient
from importer import get_app_dir, convert_to_float
from slugify import slugify

# Connect to defualt local instance of MongoClient
client = MongoClient()

# Get database and collection
db = client.fiskalnimonitor


print_description_dict = True

descriptions = {
    "rashodi": collections.OrderedDict(), # expenditures
    "prihodi": collections.OrderedDict()  # revenues
}

description_importer_tracker = {}

municipality_importer_tracker = {}


municipalities = {
    "КОВИН": "Kovin",
    "МАЛИ ИЂОШ": "Mali Iđoš",
    "КУЛА": "Kula",
    "ПЛАНДИШТЕ": "Plandište",
    "ЧАЧАК": "Čačak",
    "ПЕЋИНЦИ": "Pećinci",
    "ГОРЊИ МИЛАНОВАЦ": "Gornji Milanovac",
    "КОВАЧИЦА": "Kovačica",
    "ГАЏИН ХАН": "Gadžin Han",
    "КИКИНДА": "Kikinda",
    "ГОЛУБАЦ": "Golubac",
    "КАЊИЖА": "Kanjiža",
    "ДОЉЕВАЦ": "Doljevac",
    "БЛАЦЕ": "Blace",
    "БАБУШНИЦА": "Babušnica",
    "ВРБАС": "Vrbas",
    "ТИТЕЛ": "Titel",
    "ТЕМЕРИН": "Temerin",
    "АЛЕКСИНАЦ": "Aleksinac",
    "АРИЉЕ": "Arilje",
    "ВРШАЦ": "Vršac",
    "БОГАТИЋ": "Bogatić",
    "БОЈНИК": "Bojnik",
    "КОСОВСКА МИТРОВИЦА": "Kosovska Mitrovica ",
    "ЖИТИШТЕ": "Žitište",
    "СРЕМСКИ КАРЛОВЦИ": "Sremski Karlovci",
    "ЖАБАЉ": "Žabalj",
    "ЗРЕЊАНИН": "Zrenjanin",
    "ШИД": "Šid",
    "БОР": "Bor",
    "БОСИЛЕГРАД": "Bosilegrad",
    "СРБОБРАН": "Srbobran",
    "БРУС": "Brus",
    "СОМБОР": "Sombor",
    "БУЈАНОВАЦ": "Bujanovac",
    "СЕНТА": "Senta",
    "СЕЧАЊ": "Sečanj",
    "СРЕМСКА МИТРОВИЦА": "Sremska Mitrovica",
    "ОПОВО": "Opovo",
    "ОЏАЦИ": "Odžaci",
    "ЧАЈЕТИНА": "Čajetina",
    "НОВИ КНЕЖЕВАЦ": "Novi Kneževac",
    "ЦРНА ТРАВА": "Crna Trava",
    "СУБОТИЦА": "Subotica",
    "БОЉЕВАЦ": "Boljevac",
    "СТАРА ПАЗОВА": "Stara Pazova",
    "ПАНЧЕВО": "Pančevo",
    "ДЕСПОТОВАЦ": "Despotovac",
    "ЋИЋЕВАЦ": "Ćićevac",
    "РУМА": "Ruma",
    "ЋУПРИЈА": "Ćuprija",
    "ВРЊАЧКА БАЊА": "Vrnjačka Banja",
    "ДИМИТРОВГРАД": "Dimitrovgrad",
    "ЖАБАРИ": "Žabari",
    "ЖАГУБИЦА": "Žagubica",
    "ЖИТОРАЂА": "Žitorađa",
    "ИВАЊИЦА": "Ivanjica",
    "ИНЂИЈА": "Inđija",
    "ИРИГ": "Irig",
    "КЛАДОВО": "Kladovo",
    "КНИЋ": "Knić",
    "КЊАЖЕВАЦ": "Knjaževac",
    "КОСЈЕРИЋ": "Kosjerić",
    "КОЦЕЉЕВА": "Koceljeva",
    "КРУПАЊ": "Krupanj",
    "КУРШУМЛИЈА": "Kuršumlija",
    "КУЧЕВО": "Kučevo",
    "ЛАЈКОВАЦ": "Lajkovac",
    "ЛАПОВО": "Lapovo",
    "ЛЕБАНЕ": "Lebane",
    "ЛУЧАНИ": "Lučani",
    "ЉИГ": "Ljig",
    "ЉУБОВИЈА": "Ljubovija",
    "МАЛИ ЗВОРНИК": "Mali Zvornik",
    "МАЛО ЦРНИЋЕ": "Malo Crniće",
    "МЕДВЕЂА": "Medveđa",
    "МЕРОШИНА": "Merošina",
    "МИОНИЦА": "Mionica",
    "НЕГОТИН": "Negotin",
    "НОВА ВАРОШ": "Nova Varoš",
    "НОВА ЦРЊА": "Nova Crnja",
    "НОВИ БЕЧЕЈ": "Novi Bečej",
    "ОСЕЧИНА": "Osečina",
    "ПАРАЋИН": "Paraćin",
    "ПЕТРОВАЦ НА МЛАВИ": "Petrovac na Mlavi",
    "ПИРОТ": "Pirot",
    "ПОЖЕГА": "Požega",
    "ПРЕШЕВО": "Preševo",
    "ПРИБОЈ": "Priboj",
    "ПРИЈЕПОЉЕ": "Prijepolje",
    "ПРОКУПЉЕ": "Prokuplje",
    "РАЖАЊ": "Ražanj",
    "РАЧА": "Rača",
    "РАШКА": "Raška",
    "РЕКОВАЦ": "Rekovac",
    "СВИЛАЈНАЦ": "Svilajnac",
    "СЈЕНИЦА": "Sjenica",
    "СМЕДЕРЕВСКА ПАЛАНКА": "Smederevska Palanka",
    "СОКОБАЊА": "Sokobanja",
    "СУРДУЛИЦА": "Surdulica",
    "ТОПОЛА": "Topola",
    "ТРГОВИШТЕ": "Trgovište",
    "ТРСТЕНИК": "Trstenik",
    "ТУТИН": "Tutin",
    "УБ": "Ub",
    "ЧОКА": "Čoka",
    "ЗВЕЧАН": "Zvečan",
    "ЛЕПОСАВИЋ": "Leposavić",
    "НОВИ САД": "Novi Sad",
    "КРАГУЈЕВАЦ": "Kragujevac",
    "БАТОЧИНА": "Batočina",
    "БАЈИНА БАШТА": "Bajina Bašta",
    "АП ВОЈВОДИНА": "Vojvodina",
    "БЕОГРАД": "Belgrade",
    "ВАЉЕВО": "Valjevo",
    "ВРАЊЕ": "Vranje",
    "ЗАЈЕЧАР": "Zaječar",
    "ЈАГОДИНА": "Jagodina",
    "КРАЉЕВО": "Kraljevo",
    "КРУШЕВАЦ":"Kruševac",
    "ЛЕСКОВАЦ": "Leskovac",
    "ЛОЗНИЦА": "Loznica",
    "НИШ": "Niš",
    "НОВИ ПАЗАР": "Novi Pazar",
    "СМЕДЕРЕВО": "Smederevo",
    "УЖИЦЕ": "Užice",
    "ШАБАЦ": "Šabac",
    "АДА": "Ada",
    "АЛЕКСАНДРОВАЦ": "Aleksandrovac",
    "АЛИБУНАР": "Alibunar",
    "АПАТИН": "Apatin",
    "АРАНЂЕЛОВАЦ": "Aranđelovac",
    "БАЧ": "Bač",
    "БАЧКА ПАЛАНКА": "Bačka Palanka",
    "БАЧКА ТОПОЛА": "Bačka Topola",
    "БАЧКИ ПЕТРОВАЦ": "Bački Petrovac",
    "БЕЛА ПАЛАНКА": "Bela Palanka",
    "БЕЛА ЦРКВА": "Bela Crkva",
    "БЕОЧИН": "Beočin",
    "БЕЧЕЈ": "Bečej",
    "ВАРВАРИН": "Varvarin",
    "ВЕЛИКА ПЛАНА": "Velika Plana",
    "ВЕЛИКО ГРАДИШТЕ": "Veliko Gradište",
    "ВЛАДИМИРЦИ": "Vladimirci",
    "ВЛАДИЧИН ХАН": "Vladičin Han",
    "ВЛАСОТИНЦЕ": "Vlasotince",
    "МАЈДАНПЕК": "Majdanpek",
    "СВРЉИГ": "Svrljig",
    "БРАЊЕ": "Vranje",
    "ПЕТРОВАЦ": "Petrovac",
    "ПОЖАРЕВАЦ": "Požarevac"


}

misspelled_municipalities = {
    "ДЕЦСПОТОВАЦ": ( "ДЕСПОТОВАЦ", "Despotovac"),
    "МАЈДАМПЕК": ("МАЈДАНПЕК", "Majdanpek"),
    "БАЈНА БАШТА": ("БАЈИНА БАШТА", "Bajina Bašta"),
    "СВИРЉИГ": ("СВРЉИГ", "Svrljig"),
    "ВЛАДИМИРОВЦИ": ("ВЛАДИМИРЦИ", "Vladimirci"),
    "КОСАВСКА МИТРОВИЦА": ("КОСОВСКА МИТРОВИЦА", "Kosovska Mitrovica"),
    "AЛЕКСАНДРОВАЦ": ("АЛЕКСАНДРОВАЦ", "Aleksandrovac"),
    "ЋУПРИЈА samo ukupno": ("ЋУПРИЈА", "Ćuprija")
}

expenditures_descriptions_types = [
    ('411', 'Плате, додаци и накнаде запослених (зараде)'),
    ('412', 'СОЦИЈАЛНИ ДОПРИНОСИ НА ТЕРЕТ ПОСЛОДАВЦА (од 5176 до 5178)'),
    ('413', 'НАКНАДЕ У НАТУРИ (5180)'),
    ('414', 'СОЦИЈАЛНА ДАВАЊА ЗАПОСЛЕНИМА (од 5182 до 5185)'),
    ('415', 'НАКНАДА ТРОШКОВА ЗА ЗАПОСЛЕНЕ (5187)'),
    ('416', 'НАГРАДЕ ЗАПОСЛЕНИМА И ОСТАЛИ ПОСЕБНИ РАСХОДИ (5189)'),
    ('417', 'ПОСЛАНИЧКИ ДОДАТАК (5191)'),
    ('418', 'СУДИЈСКИ ДОДАТАК (5193)'),
    ('421', 'СТАЛНИ ТРОШКОВИ (од 5196 до 5202)'),
    ('422', 'ТРОШКОВИ ПУТОВАЊА (од 5204 до 5208)'),
    ('423', 'УСЛУГЕ ПО УГОВОРУ (од 5210 до 5217)'),
    ('424', 'СПЕЦИЈАЛИЗОВАНЕ УСЛУГЕ (од 5219 до 5225)'),
    ('425', 'ТЕКУЋЕ ПОПРАВКЕ И ОДРЖАВАЊЕ (5227 + 5228)'),
    ('426', 'МАТЕРИЈАЛ (од 5230 до 5238)'),
    ('431', 'АМОРТИЗАЦИЈА НЕКРЕТНИНА И ОПРЕМЕ (од 5241 до 5243)'),
    ('432', 'АМОРТИЗАЦИЈА КУЛТИВИСАНЕ ИМОВИНЕ (5245)'),
    ('433', 'УПОТРЕБА ДРАГОЦЕНОСТИ (5247)'),
    ('434', 'УПОТРЕБА ПРИРОДНЕ ИМОВИНЕ (од 5249 до 5251)'),
    ('435', 'АМОРТИЗАЦИЈА НЕМАТЕРИЈАЛНЕ ИМОВИНЕ (5253)'),
    ('441', 'ОТПЛАТЕ ДОМАЋИХ КАМАТА (од 5256 до 5264)'),
    ('442', 'ОТПЛАТА СТРАНИХ КАМАТА (од 5266 до 5271)'),
    ('443', 'ОТПЛАТА КАМАТА ПО ГАРАНЦИЈАМА (5273)'),
    ('444', 'ПРАТЕЋИ ТРОШКОВИ ЗАДУЖИВАЊА (од 5275 до 5277)'),
    ('451', 'СУБВЕНЦИЈЕ ЈАВНИМ НЕФИНАНСИЈСКИМ ПРЕДУЗЕЋИМА И ОРГАНИЗАЦИЈАМА (5280 + 5281)'),
    ('452', 'СУБВЕНЦИЈЕ ПРИВАТНИМ ФИНАНСИЈСКИМ ИНСТИТУЦИЈАМА (5283 + 5284)'),
    ('453', 'СУБВЕНЦИЈЕ ЈАВНИМ ФИНАНСИЈСКИМ ИНСТИТУЦИЈАМА (5286 + 5287)'),
    ('454', 'СУБВЕНЦИЈЕ ПРИВАТНИМ ПРЕДУЗЕЋИМА (5289 + 5290)'),
    ('461', 'ДОНАЦИЈЕ СТРАНИМ ВЛАДАМА (5293 + 5294)'),
    ('462', 'ДОТАЦИЈЕ МЕЂУНАРОДНИМ ОРГАНИЗАЦИЈАМА (5296 + 5297)'),
    ('463', 'ТРАНСФЕРИ ОСТАЛИМ НИВОИМА ВЛАСТИ (5299 + 5300)'),
    ('464', 'ДОТАЦИЈЕ ОРГАНИЗАЦИЈАМА ОБАВЕЗНОГ СОЦИЈАЛНОГ ОСИГУРАЊА (5302 + 5303)'),
    ('465', 'ОСТАЛЕ ДОТАЦИЈЕ И ТРАНСФЕРИ (5305 + 5306)'),
    ('471', 'ПРАВА ИЗ СОЦИЈАЛНОГ ОСИГУРАЊА (ОРГАНИЗАЦИЈЕ ОБАВЕЗНОГ СОЦИЈАЛНОГ ОСИГУРАЊА) (од 5309 до 5311)'),
    ('472', 'НАКНАДЕ ЗА СОЦИЈАЛНУ ЗАШТИТУ ИЗ БУЏЕТА (од 5313 до 5321)'),
    ('481', 'ДОТАЦИЈЕ НЕВЛАДИНИМ ОРГАНИЗАЦИЈАМА (5324 + 5325)'),
    ('482', 'ПОРЕЗИ, ОБАВЕЗНЕ ТАКСЕ И КАЗНЕ (од 5327 до 5329)'),
    ('483', 'НОВЧАНЕ КАЗНЕ И ПЕНАЛИ ПО РЕШЕЊУ СУДОВА (5331)'),
    ('484', 'НАКНАДА ШТЕТЕ ЗА ПОВРЕДЕ ИЛИ ШТЕТУ НАСТАЛУ УСЛЕД ЕЛЕМЕНТАРНИХ НЕПОГОДА ИЛИ ДРУГИХ ПРИРОДНИХ УЗРОКА (5333 + 5334)'),
    ('485', 'НАКНАДА ШТЕТЕ ЗА ПОВРЕДЕ ИЛИ ШТЕТУ НАНЕТУ ОД СТРАНЕ ДРЖАВНИХ ОРГАНА (5336)'),
    ('489', 'РАСХОДИ КОЈИ СЕ ФИНАНСИРАЈУ ИЗ СРЕДСТАВА ЗА РЕАЛИЗАЦИЈУ НАЦИОНАЛНОГ ИНВЕСТИЦИОНОГ ПЛАНА (5338)'),
    ('499', 'Средства резерве'),
    ('511', 'ЗГРАДЕ И ГРАЂЕВИНСКИ ОБЈЕКТИ (од 5342 до 5345)'),
    ('512', 'МАШИНЕ И ОПРЕМА (од 5347 до 5355)'),
    ('513', 'ОСТАЛЕ НЕКРЕТНИНЕ И ОПРЕМА (5357)'),
    ('514', 'КУЛТИВИСАНА ИМОВИНА (5359)'),
    ('515', 'НЕМАТЕРИЈАЛНА ИМОВИНА (5361)'),
    ('521', 'РОБНЕ РЕЗЕРВЕ (5364)'),
    ('522', 'ЗАЛИХЕ ПРОИЗВОДЊЕ (од 5366 до 5368)'),
    ('523', 'ЗАЛИХЕ РОБЕ ЗА ДАЉУ ПРОДАЈУ (5370)'),
    ('531', 'ДРАГОЦЕНОСТИ (5373)'),
    ('541', 'ЗЕМЉИШТЕ (5376)'),
    ('542', 'РУДНА БОГАТСТВА (5378)'),
    ('543', 'ШУМЕ И ВОДЕ (5380 + 5381)'),
    ('551', 'НЕФИНАНСИЈСКА ИМОВИНА КОЈА СЕ ФИНАНСИРА ИЗ СРЕДСТАВА ЗА РЕАЛИЗАЦИЈУ НАЦИОНАЛНОГ ИНВЕСТИЦИОНОГ ПЛАНА (5384)'),
    ('611', 'ОТПЛАТА ГЛАВНИЦЕ ДОМАЋИМ КРЕДИТОРИМА (од 5388 до 5396)'),
    ('612', 'ОТПЛАТА ГЛАВНИЦЕ СТРАНИМ КРЕДИТОРИМА (од 5398 до 5404)'),
    ('613', 'ОТПЛАТА ГЛАВНИЦЕ ПО ГАРАНЦИЈАМА (5406)'),
    ('614', 'ОТПЛАТА ГЛАВНИЦЕ ЗА ФИНАНСИЈСКИ ЛИЗИНГ (5408)'),
    ('621', 'НАБАВКА ДОМАЋЕ ФИНАНСИЈСКЕ ИМОВИНЕ (од 5411 до 5419)'),
    ('622', 'НАБАВКА СТРАНЕ ФИНАНСИЈСКЕ ИМОВИНЕ (од 5421 до 5428)'),
    ('623', 'НАБАВКА ФИНАНСИЈСКЕ ИМОВИНЕ КОЈА СЕ ФИНАНСИРА ИЗ СРЕДСТАВА ЗА РЕАЛИЗАЦИЈУ НАЦИОНАЛНОГ ИНВЕСТИЦИОНОГ ПЛАНА (5430)'),

]

revenues_description_types = [
    ('711', 'ПОРЕЗ НА ДОХОДАК, ДОБИТ И КАПИТАЛНЕ ДОБИТКЕ'),
    ('712', 'ПОРЕЗ НА ФОНД ЗАРАДА (5009)'),
    ('713', 'ПОРЕЗ НА ИМОВИНУ (од 5011 до 5016)'),
    ('714', 'ПОРЕЗ НА ДОБРА И УСЛУГЕ (од 5018 до 5022)'),
    ('715', 'ПОРЕЗ НА МЕЂУНАРОДНУ ТРГОВИНУ И ТРАНСАКЦИЈЕ (од 5024 до 5029)'),
    ('716', 'ДРУГИ ПОРЕЗИ (5031 + 5032)'),
    ('717', 'АКЦИЗЕ (од 5034 до 5039)'),
    ('719', 'ЈЕДНОКРАТНИ ПОРЕЗ НА ЕКСТРА ПРОФИТ И ЕКСТРА ИМОВИНУ СТЕЧЕНУ КОРИШЋЕЊЕМ ПОСЕБНИХ ПОГОДНОСТИ (од 5041до 5046)'),
    ('721', 'ДОПРИНОСИ ЗА СОЦИЈАЛНО ОСИГУРАЊЕ (од 5049 до 5052)'),
    ('722', 'ОСТАЛИ СОЦИЈАЛНИ ДОПРИНОСИ (од 5054 до 5056)'),
    ('731', 'ДОНАЦИЈЕ ОД ИНОСТРАНИХ ДРЖАВА (5059 + 5060)'),
    ('732', 'ДОНАЦИЈЕ ОД МЕЂУНАРОДНИХ ОРГАНИЗАЦИЈА (5062 + 5063)'),
    ('733', 'ТРАНСФЕРИ ОД ДРУГИХ НИВОА ВЛАСТИ (5065 + 5066)'),
    ('741', 'ПРИХОДИ ОД ИМОВИНЕ (од 5069 до 5074)'),
    ('742', 'ПРИХОДИ ОД ПРОДАЈЕ ДОБАРА И УСЛУГА (од 5076 до 5079)'),
    ('743', 'НОВЧАНЕ КАЗНЕ И ОДУЗЕТА ИМОВИНСКА КОРИСТ (од 5081 до 5086)'),
    ('744', 'ДОБРОВОЉНИ ТРАНСФЕРИ ОД ФИЗИЧКИХ И ПРАВНИХ ЛИЦА (5088 + 5089)'),
    ('745', 'МЕШОВИТИ И НЕОДРЕЂЕНИ ПРИХОДИ (5091)'),
    ('771', 'МЕМОРАНДУМСКЕ СТАВКЕ ЗА РЕФУНДАЦИЈУ РАСХОДА (5094)'),
    ('772', 'МЕМОРАНДУМСКЕ СТАВКЕ ЗА РЕФУНДАЦИЈУ РАСХОДА ИЗ ПРЕТХОДНЕ ГОДИНЕ (5096)'),
    ('781', 'ТРАНСФЕРИ ИЗМЕЂУ БУЏЕТСКИХ КОРИСНИКА НА ИСТОМ НИВОУ (5099 + 5100)'),
    ('791', 'ПРИХОДИ ИЗ БУЏЕТА (5103)'),
    ('811', 'ПРИМАЊА ОД ПРОДАЈЕ НЕПОКРЕТНОСТИ (5107)'),
    ('812', 'ПРИМАЊА ОД ПРОДАЈЕ ПОКРЕТНЕ ИМОВИНЕ (5109)'),
    ('813', 'ПРИМАЊА ОД ПРОДАЈЕ ОСТАЛИХ ОСНОВНИХ СРЕДСТАВА (5111)'),
    ('821', 'ПРИМАЊА ОД ПРОДАЈЕ РОБНИХ РЕЗЕРВИ (5114)'),
    ('822', 'ПРИМАЊА ОД ПРОДАЈЕ ЗАЛИХА  ПРОИЗВОДЊЕ (5116)'),
    ('823', 'ПРИМАЊА ОД ПРОДАЈЕ РОБЕ ЗА ДАЉУ ПРОДАЈУ (5118)'),
    ('831', 'ПРИМАЊА ОД ПРОДАЈЕ ДРАГОЦЕНОСТИ (5121)'),
    ('841', 'ПРИМАЊА ОД ПРОДАЈЕ ЗЕМЉИШТА (5124)'),
    ('842', 'ПРИМАЊА ОД ПРОДАЈЕ ПОДЗЕМНИХ БЛАГА (5126)'),
    ('843', 'ПРИМАЊА ОД ПРОДАЈЕ ШУМА И ВОДА (5128)'),
    ('911', 'ПРИМАЊА ОД ДОМАЋИХ ЗАДУЖИВАЊА (од 5132 до 5140)'),
    ('912', 'ПРИМАЊА ОД ИНОСТРАНОГ ЗАДУЖИВАЊА (од 5142 до 5148)'),
    ('913', 'ПРИМАЊА ПО ОСНОВУ ГАРАНЦИЈА'),
    ('921', 'ПРИМАЊА ОД ПРОДАЈЕ ДОМАЋЕ ФИНАНСИЈСКЕ ИМОВИНЕ (од 5151 до 5159)'),
    ('922', 'ПРИМАЊА ОД ПРОДАЈЕ СТРАНЕ ФИНАНСИЈСКЕ ИМОВИНЕ (од 5161 до 5168)'),
]


def import_konsolidovani_budzet_data():

    print ("\nImporting data for municipality data started...")
    print (" ")
    db.konsolidovanibudzet.remove({})

    # Iterate through every folder(year) in the following directory
    # Start with the latest year, so that we use the latest description texts
    data_dir = '%s/importer/data/konsolidovani_budzet/' % get_app_dir()
    years_dir = sorted(os.listdir(data_dir), reverse=True)

    for foldername in years_dir:
        year = int(foldername)
        print "\nImporting SESSION of municipality data for %i" % year

        # Iterate through every file in current folder
        for filename in os.listdir((data_dir + '%s/') % foldername):
            if(filename.endswith(".csv")):
                fiscal_type = filename.replace('.csv', '')
                print " Importing %s data" % fiscal_type
                description_importer_tracker = init_description_importer_track_dictionary(fiscal_type)
                municipality_importer_tracker = init_municipality_importer_track_dictionary()

                # for every csv file in current directory, open and read the file
                with open((data_dir + '%s/%s') % (foldername, filename), 'rt') as csvfile:
                    reader = csv.reader(csvfile, delimiter=',')

                    # we'll use temporary municipality name variable to pass it to every document of that municipality
                    temp_municipality_name = ""
                    previous_municipality = ''
                    for index, row in enumerate(reader):

                        # skip the header of the table
                        if index > 1:
                            municipality = get_municipality_name(row[1:])
                            if municipality != "":
                                temp_municipality_name = municipality

                            # To resolve conflicts on later on data usage and for querying purposes, we are going to insert
                            # all description types with zero values, that description_importer_tracker function
                            # tracked as missing for a certain  municipality
                            if municipality != "" and previous_municipality != '' and municipality != previous_municipality:

                                for desc_id in description_importer_tracker:
                                    if description_importer_tracker[desc_id] is False:
                                        if fiscal_type == 'rashodi':
                                            for expenditures in expenditures_descriptions_types:
                                                if expenditures[0] == desc_id:
                                                    desc = expenditures[1]
                                        else:
                                            for expenditures in revenues_description_types:
                                                if expenditures[0] == desc_id:
                                                    desc = expenditures[1]
                                        dummy_data_doc = build_konsolidovani_budzet_json(temp_municipality_name, year, fiscal_type, desc_id, desc,  0, 0, 0, 0, 0, 0, 0)

                                        db.konsolidovanibudzet.insert(dummy_data_doc)

                                description_importer_tracker = init_description_importer_track_dictionary(fiscal_type)
                            previous_municipality = temp_municipality_name

                            json_obj = None
                            if row[1] == "" and row[2] != "" and row[2] != "укупно" and row[2] != 'конто':

                                json_obj = get_konsolidovani_budzet_json(row, temp_municipality_name, year, fiscal_type)

                            elif row[1] != "" and row[2] != "" and row[2] != 'конто':

                                json_obj = get_konsolidovani_budzet_json(row, temp_municipality_name, year, fiscal_type)

                            if json_obj is not None:
                                db.konsolidovanibudzet.insert(json_obj)
                for mun in municipality_importer_tracker:
                    if municipality_importer_tracker[mun] is False:
                        if fiscal_type == "rashodi":
                            for exp in expenditures_descriptions_types:
                                dummy_municipalities_doc = build_konsolidovani_budzet_json(mun, year, fiscal_type, exp[0], exp[1],  0, 0, 0, 0, 0, 0, 0)
                                db.konsolidovanibudzet.insert(dummy_municipalities_doc)
                        else:
                            for revenue in revenues_description_types:
                                dummy_municipalities_doc = build_konsolidovani_budzet_json(mun, year, fiscal_type, revenue[0], revenue[1],  0, 0, 0, 0, 0, 0, 0)
                                db.konsolidovanibudzet.insert(dummy_municipalities_doc)
    if print_description_dict:
        print_description_dic()

    print ("\nImporting data for municipality data finished...")
    print (" ")


# Get Municipality name form the first row of municipality table so we can pass to every other doc of that table
def get_municipality_name(row):
     if row[1:3] != "":
        municipality = row[0]
        return municipality
     else:
        return None


def get_konsolidovani_budzet_json(row, temp_municipality_name, year, fiscal_type):

        if row[2] in descriptions[fiscal_type]:
            descriptions[fiscal_type][row[2]].append(row[3].strip())
        else:
            descriptions[fiscal_type][row[2]] = [row[3].strip()]

        # convert values to float than pass them as function parameters, so we can have all in one JSON
        republika = convert_to_float(row[4].replace(',',''))
        autonomna_pokrajina = convert_to_float(row[5].replace(',',''))
        opstina_grada = convert_to_float(row[6].replace(',',''))
        ooco = convert_to_float(row[7].replace(',',''))
        donacije = convert_to_float(row[8].replace(',',''))

        ostali_izvori = 0
        ukupno = 0
        if len(row[9]) > 40:
            ukupno = republika + autonomna_pokrajina + opstina_grada + ooco + donacije

        else:
            ostali_izvori = convert_to_float(row[9].replace(',',''))
            ukupno = republika + autonomna_pokrajina + opstina_grada + ooco + donacije + ostali_izvori

        # pass all required values to this function, add you'll have everything structured in a json document (~_~)
        json_doc = build_konsolidovani_budzet_json(temp_municipality_name, year, fiscal_type, row[2], None, republika, autonomna_pokrajina, opstina_grada, ooco, donacije, ostali_izvori, ukupno)


        # we'll keep track which description type is not missing in a certain municipality by setting the value True
        desc_id = json_doc['konta']['id']
        description_importer_tracker[desc_id] = True

        municipality = json_doc['opstina']['cirilica']
        municipality_importer_tracker[municipality] = True

        return json_doc

# Here we build the JSON document structure, which will be used every time we insert data into konsolidovanibudzet collection
def build_konsolidovani_budzet_json(municipality, year, fiscal_type, desc_id, description, republika, autonomna_pokrajina, opstina_grada, ooco, donacije, ostali_izvori, ukupno):

    if description is None:
        description_value = descriptions[fiscal_type][desc_id][0]
    else:
        description_value = description

    doc = {

        "godine": year,
        "prihodRashod": fiscal_type,
        "konta":  {
            "id": int(desc_id),
            "opis": {
                "cirilica": description_value,
                "latinica": ""
            }
        },
        "izvoriPrihodaRashoda": {
            "republika": {
                "polje": {
                    "cirilica": "Република",
                    "latinica": "Republika"
                },
                "vrednost": republika
            },
            "autonomnaPokrajina":{
                "polje": {
                    'cirilica': "Аутономна покрајина",
                    'latinica': 'Autonomna pokrajina'
                },
                "vrednost": autonomna_pokrajina
            },
            "opstinaGrada":{
                "polje": {
                    'cirilica': "Општина града",
                    'latinica': 'Opstina grada'
                },
                "vrednost": opstina_grada
            },
            "ooco":{
                "polje": {
                    'cirilica': "ООСО",
                    'latinica': 'OOCO'
                },
                "vrednost": ooco
            },
            "donacije": {
                "polje": {
                    'cirilica': "Донације",
                    'latinica': 'Donacije'
                },
                "vrednost": donacije
            },
            'ostaliIzvori': {
                "polje": {
                    'cirilica': "Остали извори",
                    'latinica': 'Ostali izvori'
                },
                "vrednost": ostali_izvori
            },
            "ukupno": {
                "polje": {
                    'cirilica': "Укупно",
                    'latinica': 'Ukupno'
                },
                "vrednost": ukupno
            }
        }
    }

    if municipality in misspelled_municipalities:
        doc['opstina'] = {
            "latinica": misspelled_municipalities[municipality.strip()][1],
            "slug": slugify(misspelled_municipalities[municipality.strip()][1], to_lower=True),
            "cirilica": misspelled_municipalities[municipality.strip()][0]
        }
    else:
        doc['opstina'] = {
            "latinica": municipalities[municipality.strip()],
            "slug": slugify(municipalities[municipality.strip()], to_lower=True),
            "cirilica": municipality.strip()
        }


    return doc


def print_description_dic():
    # print dictionary of description, for re-use in application code
    print ""
    print "revenue_descriptions = OrderedDict(["
    for key, value in descriptions["prihodi"].iteritems():
        print "    ('%s', '%s')," % (key, value[0])
    print "])"

    print "expenditure_descriptions = OrderedDict(["
    for key, value in descriptions["rashodi"].iteritems():
        print "    ('%s', '%s')," % (key, value[0])
    print "])"


# This function is used to build description-importer-tracker dictionary
def init_description_importer_track_dictionary(fiscal_type):

    json_obj = {}
    if fiscal_type == "rashodi":
        for item in expenditures_descriptions_types:
            json_obj[item[0]] = False
    else:
        for item in revenues_description_types:
            json_obj[item[0]] = False

    return json_obj

def init_municipality_importer_track_dictionary():
    json_obj = {}

    for item in municipalities.keys():
        json_obj[item] = False

    return json_obj
"""
for mun in municipality_importer_tracker:
    if municipality_importer_tracker[mun] is False:
        municipality = mun
    dummy_municipalities_doc = build_konsolidovani_budzet_json(municipality, year, fiscal_type, )
"""