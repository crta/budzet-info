# coding=utf-8
from flask.ext.testing import TestCase
from app import create_app, mongo


class ImporterTestCase(TestCase):
    ''' A test case for the Importer
    '''
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def create_app(self):
        app = create_app()
        return app

class KonsolidovaniBudzetTestCase(ImporterTestCase):
    pass

class LokalnaJavnaPreduzecaTestCase(ImporterTestCase):

    # http://tinyurl.com/nknz2yq
    expected_cities_ljp = {
        'Beograd': True,
        'Čačak': True,
        'Jagodina': True,
        'Kraljevo': True,
        'Kragujevac': True,
        'Kruševac': True,
        'Leskovac': True,
        'Loznica': True,
        'Novi Pazar': True,
        'Novi Sad': True,
        'Niš': True,
        'Pančevo': True,
        'Požarevac': True,
        'Smederevo': True,
        'Sombor': True,
        'Sremska Mitrovica': True,
        'Subotica': True,
        'Šabac': True,
        'Užice': True,
        'Valjevo': True,
        'Vranje': True,
        'Zaječar': True,
        'Zrenjanin': True
    }


    expected_cities_with_city_municipalities_ljp = {
         "Beograd": True,
         "Novi Sad": False,
         "Niš": True,
         "Kragujevac": False,
         "Požarevac": False,
         "Vranje": False
    }

    # Complete list of municipalities retrieved from Wikipedia: http://tinyurl.com/p84safn
    exptected_municipalities_without_parent_cities_ljp = {
        'Čačak': True,
        'Jagodina': True,
        'Kraljevo': True,
        'Kruševac': True,
        'Leskovac': True,
        'Loznica': True,
        'Novi Pazar': True,
        'Pančevo': True,
        'Smederevo': True,
        'Sombor': True,
        'Sremska Mitrovica': True,
        'Subotica': True,
        'Šabac': True,
        'Užice': True,
        'Valjevo': True,
        'Zaječar': True,
        'Zrenjanin': True,
        'Ada': True,
        'Aleksandrovac': True,
        'Aleksinac': True,
        'Alibunar': True,
        'Apatin': True,
        'Aranđelovac': True,
        'Arilje': True,
        'Babušnica': True,
        'Bajina Bašta': True,
        'Batočina': True,
        'Bač': True,
        'Bačka Palanka': True,
        'Bačka Topola': True,
        'Bački Petrovac': True,
        'Bela Crkva': True,
        'Bela Palanka': True,
        'Beočin': True,
        'Bečej': True,
        'Blace': True,
        'Bogatić': True,
        'Bojnik': True,
        'Boljevac': True,
        'Bor': True,
        'Bosilegrad': True,
        'Brus': True,
        'Bujanovac': True,
        'Crna Trava': True,
        'Despotovac': True,
        'Dečani': False,
        'Dimitrovgrad': True,
        'Doljevac': True,
        'Gadžin Han': True,
        'Glogovac': False,
        'Gnjilane': False,
        'Golubac': True,
        'Gora1': False,
        'Gornji Milanovac': True,
        'Inđija': True,
        'Irig': False,
        'Istok': False,
        'Ivanjica': True,
        'Kanjiža': True,
        'Kačanik': False,
        'Kikinda': True,
        'Kladovo': True,
        'Klina': False,
        'Knić': True,
        'Knjaževac': True,
        'Koceljeva': True,
        'Kosjerić': True,
        'Kosovo Polje': False,
        'Kosovska Kamenica': False,
        'Kosovska Mitrovica': False,
        'Kovačica': True,
        'Kovin': True,
        'Krupanj': False,
        'Kula': True,
        'Kuršumlija': True,
        'Kučevo': True,
        'Lajkovac': True,
        'Lapovo': True,
        'Lebane': True,
        'Leposavić': False,
        'Lipljan': False,
        'Ljig': True,
        'Ljubovija': False,
        'Lučani': False,
        'Majdanpek': True,
        'Mali Iđoš': False,
        'Mali Zvornik': True,
        'Malo Crniće': False,
        'Medveđa': True,
        'Merošina': True,
        'Mionica': True,
        'Negotin': True,
        'Nova Crnja': True,
        'Nova Varoš': True,
        'Novi Bečej': True,
        'Novi Kneževac': True,
        'Novo Brdo': False,
        'Obilić': False,
        'Odžaci': True,
        'Opovo': True,
        'Orahovac': False,
        'Osečina': True,
        'Paraćin': True,
        'Petrovac na Mlavi': True,
        'Peć': False,
        'Pećinci': True,
        'Pirot': True,
        'Plandište': False,
        'Podujevo': False,
        'Požega': True,
        'Preševo': True,
        'Priboj': True,
        'Prijepolje': True,
        'Prizren': False,
        'Prokuplje': True,
        'Rača': True,
        'Raška': True,
        'Ražanj': True,
        'Rekovac': True,
        'Ruma': True,
        'Senta': True,
        'Sečanj': True,
        'Sjenica': True,
        'Smederevska Palanka': True,
        'Sokobanja': True,
        'Srbica': False,
        'Srbobran': True,
        'Sremski Karlovci': False,
        'Stara Pazova': True,
        'Surdulica': True,
        'Suva Reka': False,
        'Svilajnac': True,
        'Svrljig': True,
        'Temerin': True,
        'Titel': True,
        'Topola': True,
        'Trgovište': False,
        'Trstenik': True,
        'Tutin': True,
        'Ub': True,
        'Uroševac': False,
        'Varvarin': True,
        'Velika Plana': True,
        'Veliko Gradište': False,
        'Vitina': False,
        'Vladimirci': True,
        'Vladičin Han': True,
        'Vlasotince': True,
        'Vrbas': True,
        'Vrnjačka Banja': True,
        'Vršac': False,
        'Vučitrn': False,
        'Zubin Potok': False,
        'Zvečan': False,
        'Ćićevac': True,
        'Ćuprija': True,
        'Čajetina': True,
        'Čoka': True,
        'Đakovica': False,
        'Šid': True,
        'Štimlje': False,
        'Štrpce': False,
        'Žabalj': True,
        'Žabari': False,
        'Žagubica': True,
        'Žitište': True,
        'Žitorađa': True
    }

    def test_cities_with_city_municipalities(self):
        '''

        :return:
        '''
        for city, expected in self.expected_cities_with_city_municipalities_ljp.items():
            if expected:
                # These cities we expect to have imported a bunch of city municipalities
                results = mongo.db.lokalnajavnapreduzeca\
                    .find({'opstina.grad.vrednost': city})\
                    .distinct('opstina.naziv.vrednost')

                self.assertTrue(len(results) > 0, "Expected to have imported several municipalities for the city of %s." % city)

            else:
                # For these these, even though they do have city municipalities,
                # We don't expect the dataset to actually have any.
                # So we just define one municipality: the city itself.
                results = mongo.db.lokalnajavnapreduzeca\
                    .find({'opstina.grad.vrednost': city})\
                    .distinct('opstina.grad.vrednost')

                self.assertTrue(len(results) == 1, "Did not expect to have imported several municipalities for the city of %s." % city)


    def test_cities(self):
        ''' Check that every city is not actually a municipality.
            Do this by checking that the city in question is not in the expected_municipalities_ljp dict
        :return:
        '''

        results_of_cities = mongo.db.lokalnajavnapreduzeca\
            .find({"opstina.grad.vrednost": {'$in': list(self.expected_cities_ljp.keys())}})\
            .distinct("opstina.grad.vrednost")

        self.municipality_asserts(results_of_cities, None, self.expected_cities_ljp, -1)


    def test_municipalities(self):

        results_of_cities_with_muns = mongo.db.lokalnajavnapreduzeca\
            .find({"opstina.grad.vrednost": {'$nin': list(self.expected_cities_with_city_municipalities_ljp.keys())}})\
            .distinct("opstina.naziv.vrednost")

        self.municipality_asserts(results_of_cities_with_muns, None, self.exptected_municipalities_without_parent_cities_ljp, -1)


    def test_belgrade_municipalities(self):
        '''
        :return:
        '''

        # dictionary of Belgrade municipalities: http://tinyurl.com/pgrbrrc
        # Key: name of the municipality (String)
        # Value: Boolean indicating we expect to have imported data for that municipality
        belgrade_municipalities = {
            'Barajevo': True,
            'Čukarica': True,
            'Grocka': True,
            'Lazarevac': True,
            'Mladenovac': True,
            'Novi Beograd': True,
            'Obrenovac': True,
            'Palilula': True,
            'Rakovica': True,
            'Savski Venac': False,
            'Sopot': True,
            'Stari Grad': True,
            'Surčin': False,
            'Voždovac': True,
            'Vračar': True,
            'Zemun': False,
            'Zvezdara': True
        }

        results = mongo.db.lokalnajavnapreduzeca\
            .find({'opstina.grad.slug': 'beograd'})\
            .distinct("opstina.naziv.vrednost")

        expected_number_of_beograd_municipalities = 14
        self.municipality_asserts(results, 'Beograd', belgrade_municipalities, expected_number_of_beograd_municipalities)


    def test_nis_municipalities(self):
        # Same deal with Nis municipalities: http://tinyurl.com/ncgy6h6
        nis_municipalities = {
            'Medijana': True,
            'Palilula': True,
            'Pantelej': False,
            'Crveni Krst': True,
            'Niška Banja':  False
        }

        results = mongo.db.lokalnajavnapreduzeca\
            .find({'opstina.grad.slug': 'nis'})\
            .distinct("opstina.naziv.vrednost")

        expected_number_of_nis_municipalities = 3
        self.municipality_asserts(results, "Nis", nis_municipalities, expected_number_of_nis_municipalities)


    def municipality_asserts(self, results, parent_city, expected_municipalities, expected_number_of_imported_municipalities):
        ''' This function runs all of the asserts to test of cities and their municipalities (subdivisions) have
        been imported correctly.
        :param results: The MongoDB query results: list of imported city subdivisions for either Belgrade or Nis.
        :param city_name: The city we want to test subdivision for (beograd or nis)
        :param expected_municipalities: A dictionary indicating which municipalities do we expect to have imported.
        :param expected_number_of_imported_municipalities: The expected number of imported municipalities
        :return:
        '''

        if expected_number_of_imported_municipalities > 0:
            # Check that we imported the expected amount of municipalities.
            self.assertEqual(len(results), expected_number_of_imported_municipalities, "Unexpected number of municipalities imported for %s." % parent_city)

        for municipality in results:
            # Check that every municipality is a valid name.
            self.assertTrue(municipality.encode("utf-8") in expected_municipalities, "Unexpected municipality name '%s' for %s." % (municipality, parent_city))

            # Check that we did indeed expect to import that municipality:
            self.assertTrue(expected_municipalities[municipality.encode("utf-8")])

        # Check if we imported all the expected municipalities
        for municipality, expected in expected_municipalities.items():
            if expected is True:
                self.assertTrue(municipality.decode("utf-8") in results, "Expected to import '%s' municipality for %s." % (municipality, parent_city))


    def test_first_row_of_each_year(self):

        years = [2006, 2007, 2008, 2009, 2010]
        result_counts = mongo.db.lokalnajavnapreduzeca\
            .find({'godine': {'$in': years}, 'preduzece.maticniBroj': 7100124}).count()

        self.assertEqual(result_counts, 5, 'Unexpected number of first row imported')


    def test_last_row_of_each_year(self):

        years = [2006, 2007, 2008, 2009, 2010]
        result_counts = mongo.db.lokalnajavnapreduzeca\
            .find({'godine': {'$in': years}, 'preduzece.maticniBroj': 8755418}).count()

        self.assertEqual(result_counts, 5, 'Unexpected number of last row imported')

    def test_multiple_founders(self):

        # RZAV - 2010 -
        self.asserts_for_multiple_founders_counts(2010, 7350538, 5, 'RZAV')
        # check if the sum of the fields value in the database equal to the expected value
        self.asserts_for_multiple_founders_values(7350538, 2010, 225234, 'RZAV')

        # Test that all these companies have the same founders throughout the years
        founders_names = [
            'skupstina-opstine-arilje',
            'skupstina-opstine-lucani',
            'skupshtina-opshtine-gornji-milanovats',
            'skupstina-opstine-pozega',
            'skupstina-grada-cacka'
        ]
        # Test that all these companies have the same founders throughout the years
        number_of_founders = self.mongo_query_for_oweners_assert(founders_names)
        self.assertEqual(number_of_founders, 25, 'Unexpected number of founders for RZAV company')

        # JKP BEOGRAD-PUT BEOGRAD - 2010
        self.asserts_for_multiple_founders_counts(2010, 7023332, 2, 'JKP BEOGRAD-PUT BEOGRAD')
        # check if the sum of the fields value in the database equal to the expected value
        self.asserts_for_multiple_founders_values(7023332, 2010, 7120254, 'JKP BEOGRAD-PUT BEOGRAD')

        # JKP TRŽNICA NOVI SAD <--- At least test one that has a single founder that precedes a multiple founder one
        self.asserts_for_multiple_founders_counts(2010, 8073074, 1, 'JKP TRŽNICA NOVI SAD')

        # JP URBANIZAM  NOVI SAD - 2010
        self.asserts_for_multiple_founders_counts(2010, 8113700, 2, 'JP URBANIZAM  NOVI SAD')

        # STAN - 2010 <--- At least test one that has a single founder that is between two multiple founders one.
        self.asserts_for_multiple_founders_counts(2010, 7382685, 1, 'STAN')

        # JP SPORTSKI I POSLOVNI CENTAR VOJVODINA  NOVI SAD - 2010
        self.asserts_for_multiple_founders_counts(2010, 8157359, 2, 'JP SPORTSKI I POSLOVNI CENTAR VOJVODINA')

        # PUT - 2010
        self.asserts_for_multiple_founders_counts(2010, 8171963, 2, 'PUT')

        # JP APOLO NOVI SAD - 2010 <--- At least test that has a single founder following multiple founder one.
        self.asserts_for_multiple_founders_counts(2010, 8691193, 1, 'JP APOLO NOVI SAD')

        # JKP STANDARD VRBAS - 2010
        self.asserts_for_multiple_founders_counts(2010, 8057982, 2, 'JKP STANDARD VRBAS')


    def asserts_for_multiple_founders_counts(self, year, reg_number, expected_counts, company_name):
        '''

        :param year:
        :param reg_number:
        :param expected_counts:
        :param company_name:
        :return:
        '''
        result = mongo.db.lokalnajavnapreduzeca.aggregate([
            {
                '$match': {
                    "preduzece.maticniBroj": reg_number,
                    "godine": year
                }
            },
            {
                 "$unwind": "$osnivaci.imena"
            },
            {
                 "$group": {
                         '_id': {
                                 "_id": {'osnivac': "$osnivaci.imena.slug"}
                         },
                         'count': {
                                 '$sum': 1
                         }
                 }
            },
            {
                 "$group": {
                         '_id': {
                                 "_id": {}
                         },
                         'sum': {
                                 '$sum': '$count'
                         }
                 }
            }])

        result_counts = int(result['result'][0]['sum'])

        self.assertEqual(result_counts, expected_counts, 'Unexpected number of %s company docs imported' % company_name)


    def asserts_for_multiple_founders_values(self, reg_number, year, expected_value, company_name):
        '''

        :param reg_number: company's registration number
        :param year: fiscal year
        :param expected_value: the sum that the mongo aggregation query have to return
        :param company_name: comapny's name
        :return:
        '''
        result = mongo.db.lokalnajavnapreduzeca.aggregate([
            {
                '$match': {
                    "preduzece.maticniBroj": reg_number,
                    "godine": year
                }
            },
            {
                '$group':{
                    "_id": {
                        "year":"$godine", "opstinaNumber":"$preduzece.maticniBroj"
                    },
                    "nabavna_vrednost_prodate_robe_aop_208":{'$sum':"nabavnaVrednostProdateRobe.vrednost"},
                    'troskovi_materijala_aop_209':{'$sum': "$troskoviMaterijala.vrednost"},
                    'troskoviZaradaNaknadaZaradaOstaliLicniRashodi': {'$sum': '$troskoviZaradaNaknadaZaradaOstaliLicniRashodi.vrednost'},
                    'troskovi_amortizacije_i_rezervisanja_aop_211': {'$sum': '$troskoviAmortizacijeRezervisanja.vrednost'},
                    'ostali_poslovni_rashodi_aop_212': {'$sum': '$ostaliPoslovniRashodi.vrednost'}
                }
            }
        ])

        total_sum = 0
        for key, value in result['result'][0].items():
            if key != '_id':
                total_sum += value

        # check if the sum of the fields value in the database equal to the expected value
        self.assertEqual(total_sum, expected_value, 'Unexpected sum for %s company' % company_name)

    def mongo_query_for_oweners_assert(self, founders_names):

        result = mongo.db.lokalnajavnapreduzeca.aggregate([
            {
                 "$unwind": "$osnivaci.imena"
            },
            {
                '$match':{
                    "preduzece.maticniBroj": 7350538,
                    'osnivaci.imena.slug': {'$in': founders_names}
                }

            },
            {
                 "$group": {
                         '_id': {
                                 "_id": {'osnivac': "$osnivaci.imena.slug"}
                         },
                         'count': {
                                 '$sum': 1
                         }
                 }
            },
            {
                 "$group": {
                         '_id': {
                                 "_id": {}
                         },
                         'sum': {
                                 '$sum': '$count'
                         }
                 }
            }])

        return int(result['result'][0]['sum'])
