Budget Info Visualizer
======================

Budget Info Visualizer

This project requires version 3.0.7 of MongoDB.

To install this version run this command on terminal:

sudo apt-get install -y mongodb-org=3.0.7