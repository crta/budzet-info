import argparse
from importer.konsolidovani_budzet_importer import import_konsolidovani_budzet_data
from importer.lokalna_javna_preduzeca_importer import import_lokalna_javna_preduzeca_data

def main_importer(data_source):

    ds_list = data_source.split(',')

    for ds in ds_list:
        # Run importer(s)
        if ds == "kb":
            import_konsolidovani_budzet_data()

        elif ds == "ljp":
            import_lokalna_javna_preduzeca_data()


# Run the app
if __name__ == '__main__':

    # Define the arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument("--data", help="The data source we want to import")

    # Parse arguemnts and run the app.
    args = parser.parse_args()

    data_source = args.data
    main_importer(data_source)
